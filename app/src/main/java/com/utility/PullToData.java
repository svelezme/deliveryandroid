package com.utility;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import com.bean.AddressListBean;
import com.bean.BillingBean;
import com.bean.ListaFormaPagosBean;
import com.com.rest.API;
import com.com.rest.GlobalValues;
import com.com.rest.MethodListener;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by administrator on 27/3/17.
 */

public class PullToData extends IntentService{

    String TAG = getClass().getName();

    public PullToData(){
        super(PullToData.class.getName());
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        Bundle bundle = intent.getExtras();
        final int syncId = bundle.getInt("syncId");

        if(SessionManager.getUserId(getApplicationContext())!=0){
            /**
             * Call Delivery Address & Billing Address
             */
            getDeliveryAddress();
            getBillingAddress();

        }

        if(syncId==1){
            int pIdRestaurante = bundle.getInt("pIdRestaurante");
            if(Config.alListPagos.size()==0){
                getListaFormaPagos(pIdRestaurante);
            }else{
                if(Config.alListPagos.get(0).getIdRestaurante()!=pIdRestaurante){
                    getListaFormaPagos(pIdRestaurante);
                }
            }
        }
    }

    private void getListaFormaPagos(int pIdRestaurante){
        String url = API.ListaFormaPagos+"pIdRestaurante="+pIdRestaurante+"&callback=";
        GlobalValues.getMethodManagerObj(getApplicationContext()).makeStringReq(url, "apiListaFormaPagos", new MethodListener() {
            @Override
            public void onError() {

            }

            @Override
            public void onError(String response) {
                //Log.v("error",""+response);
            }

            @Override
            public void onSuccess(String response) {
                //Log.v("onSuccess",""+response);
                try {
                    response = response.substring(1,response.length()-1);
                    JSONObject mObj = new JSONObject(response);
                    JSONArray mArray = mObj.optJSONArray("data");
                    if(mArray!=null){

                        Config.alListPagos.clear();

                        for(int i=0;i<mArray.length();i++){
                            JSONObject object = mArray.optJSONObject(i);
                            ListaFormaPagosBean lb = new ListaFormaPagosBean();
                            lb.setIdRelaRestaFormaPago(object.optInt("IdRelaRestaFormaPago"));
                            lb.setIdRestaurante(object.optInt("IdRestaurante"));
                            lb.setIdFormaPago(object.optInt("IdFormaPago"));
                            lb.setNombreRestaurante(object.optString("NombreRestaurante"));
                            lb.setNombre(object.optString("Nombre"));
                            lb.setValor(object.optString("Valor"));

                            Config.alListPagos.add(lb);
                        }
                    }
                }catch (JSONException e){
                    Log.v(TAG,""+e.toString());
                }
            }
        });
    }

    private void getBillingAddress(){
        String url = API.new_billing_address_url+"id_cliente="+ SessionManager.getUserId(getApplicationContext())+"&callback=";
       // String url = API.billingAddressList+"pIdCliente="+SessionManager.getUserId(getApplicationContext())+"&callback=";
        GlobalValues.getMethodManagerObj(getApplicationContext()).makeStringReq(url, "apiBillingList", new MethodListener() {
            @Override
            public void onError() {

            }

            @Override
            public void onError(String response) {

            }

            @Override
            public void onSuccess(String response) {
                try{
                    //response = response.substring(1,response.length()-1);
                    JSONObject object = new JSONObject(response);
                    JSONArray mArray = object.optJSONArray("data");
                    if(mArray!=null){

                        Config.alBilling.clear();

                        for(int i=0;i<mArray.length();i++){
                            JSONObject mObj = mArray.optJSONObject(i);
                            BillingBean billingBean = new BillingBean();
                            billingBean.setIdDireccionFactura(mObj.optInt("IdDireccionFactura"));
                            billingBean.setIdCliente(mObj.optInt("IdCliente"));
                            billingBean.setRazonSocial(mObj.optString("RazonSocial"));
                            billingBean.setIdentificacion(mObj.optString("Identificacion"));
                            billingBean.setDescripcion(mObj.optString("Descripcion"));
                            billingBean.setDireccion(mObj.optString("Direccion"));
                            billingBean.setTelefono(mObj.optString("Telefono"));
                            billingBean.setNombrePerfil(mObj.optString("NombrePerfil"));

                            Config.alBilling.add(billingBean);
                        }


                        BillingBean billingBean = new BillingBean();
                        billingBean.setIdDireccionFactura(0);
                        billingBean.setIdCliente(0);
                        billingBean.setRazonSocial("0");
                        billingBean.setIdentificacion("0");
                        billingBean.setDescripcion("CONSUMIDOR FINAL");
                        billingBean.setDireccion("0");
                        billingBean.setTelefono("0");
                        billingBean.setNombrePerfil("0");
                        Config.alBilling.add(billingBean);
                    }
                }catch (JSONException e){
                    Log.e(TAG,""+e.toString());
                }catch (Exception e){
                    Log.e(TAG,""+e.toString());
                }
            }
        });
    }

    private void getDeliveryAddress(){
        String url = API.new_delivery_address_url+"id_cliente="+ SessionManager.getUserId(getApplicationContext())+"&callback=";
       // String url = API.deliveryAddress+"pIdCliente="+SessionManager.getUserId(getApplicationContext())+"&callback=";
        GlobalValues.getMethodManagerObj(getApplicationContext()).makeStringReq(url, "apiRequest", new MethodListener() {
            @Override
            public void onError() {
            }

            @Override
            public void onError(String response) {
            }

            @Override
            public void onSuccess(String response) {
                try {
                    //response = response.substring(1,response.length()-1);
                    JSONObject mobj = new JSONObject(response);
                    JSONArray mData = mobj.optJSONArray("data");
                    if(mData!=null) {
                        Config.alDeliveryAddress.clear();

                        for (int i = 0; i < mData.length(); i++) {
                            JSONObject object = mData.optJSONObject(i);
                            AddressListBean addressListBean = new AddressListBean();
                            addressListBean.setIdDirecciones(object.optInt("IdDirecciones"));
                            addressListBean.setIdCiudad(object.optInt("IdCiudad"));
                            addressListBean.setIdCliente(object.optInt("IdCliente"));
                            addressListBean.setIdSector(object.optInt("IdSector"));
                            addressListBean.setDescripcion(object.optString("Descripcion"));
                            addressListBean.setTipo(object.optString("Tipo"));
                            addressListBean.setDireccion(object.optString("Direccion"));
                            addressListBean.setDireccion2(object.optString("Direccion2"));
                            addressListBean.setTelefono(object.optString("Telefono"));
                            addressListBean.setCelular(object.optString("Celular"));
                            addressListBean.setReferencia(object.optString("Referencia"));
                            addressListBean.setNombreCiudad(object.optString("NombreCiudad"));
                            addressListBean.setNombreSector(object.optString("NombreSector"));
                            addressListBean.setNombrePerfil(object.optString("NombrePerfil"));

                            Config.alDeliveryAddress.add(addressListBean);
                        }

                        AddressListBean addressListBean = new AddressListBean();
                        addressListBean.setIdDirecciones(0);
                        addressListBean.setIdCiudad(0);
                        addressListBean.setIdCliente(0);
                        addressListBean.setIdSector(0);
                        addressListBean.setDescripcion("RECOGE EN LOCAL");
                        addressListBean.setTipo("0");
                        addressListBean.setDireccion("0");
                        addressListBean.setDireccion2("0");
                        addressListBean.setTelefono("0");
                        addressListBean.setCelular("0");
                        addressListBean.setReferencia("0");
                        addressListBean.setNombreCiudad("0");
                        addressListBean.setNombreSector("0");
                        addressListBean.setNombrePerfil("0");

                        Config.alDeliveryAddress.add(addressListBean);


                    }
                }catch (JSONException e){
                    Log.e(TAG,""+e.toString());
                }catch (Exception e){
                    Log.e(TAG,""+e.toString());
                }
            }
        });
    }
}
