package com.utility;

import android.util.Log;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by administrator on 26/9/17.
 */

public class DateFormatters {

    String TAG = getClass().getName();

    public static String dateFormatters(String date){

        String newDate = null;

        try{
            //Convert String to Date
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
            Date oldDate = dateFormat.parse(date);

            //using locale
            DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
            newDate = formatter.format(oldDate);
            Log.e("newDate",""+newDate);
        }catch (Exception e){
            newDate = null;
            Log.e("DateFormatters",""+e.toString());
        }
        return newDate;
    }
}
