package com.utility;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.app.grupodeliveryec.deliveryEC.R;

/**
 * Created by administrator on 15/3/17.
 */

public class ClearCart extends Dialog{

    cartOnClickListener cartOnClickListener;

    // This is my interface //
    public interface cartOnClickListener {
        void yesOnButtonClick();
        void noOnButtonClick();
    }

    public ClearCart(Context context, int theamResId,cartOnClickListener cartOnClickListener){
        super(context, theamResId);
        this.cartOnClickListener = cartOnClickListener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.clear_cart_xml);

        TextView clear_cart_no = (TextView) findViewById(R.id.clear_cart_no);
        TextView clear_cart_yes = (TextView)findViewById(R.id.clear_cart_yes);

        clear_cart_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cartOnClickListener.noOnButtonClick();
            }
        });

        clear_cart_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cartOnClickListener.yesOnButtonClick();
            }
        });
    }
}
