package com.utility;


import android.content.Context;
import android.view.Menu;


import com.app.grupodeliveryec.deliveryEC.R;
import com.bean.MenuBean;

import java.util.ArrayList;

/**
 * Created by administrator on 12/9/16.
 */
public class MenuList {

    public MenuBean dashBoardListItem;

    private ArrayList<MenuBean> dashBoardListItems;

    public ArrayList<MenuBean> getDashBoardList(Context mContext)
    {
        dashBoardListItems = new ArrayList<MenuBean>();

        dashBoardListItem = new MenuBean();
        dashBoardListItem.setMenu_name(mContext.getResources().getString(R.string.RESTAURANTES));
        dashBoardListItem.setResId(R.drawable.restaurents);
        dashBoardListItems.add(dashBoardListItem);

        dashBoardListItem = new MenuBean();
        dashBoardListItem.setMenu_name(mContext.getResources().getString(R.string.PROMOCIONES));
        dashBoardListItem.setResId(R.drawable.promociones);
        dashBoardListItems.add(dashBoardListItem);

        dashBoardListItem = new MenuBean();
        dashBoardListItem.setMenu_name(mContext.getResources().getString(R.string.PERFIL));
        dashBoardListItem.setResId(R.drawable.perfil);
        dashBoardListItems.add(dashBoardListItem);

        dashBoardListItem = new MenuBean();
        dashBoardListItem.setMenu_name(mContext.getResources().getString(R.string.NOSOTROS));
        dashBoardListItem.setResId(R.drawable.nosotros);
        dashBoardListItems.add(dashBoardListItem);

        return dashBoardListItems;
    }

}
