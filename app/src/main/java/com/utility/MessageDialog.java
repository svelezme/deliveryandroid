package com.utility;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.app.grupodeliveryec.deliveryEC.R;

/**
 * Created by administrator on 21/1/17.
 */

public class MessageDialog extends Dialog {
    public okOnClickListener okListener;
    public String message;

    // This is my interface //
    public interface okOnClickListener {
        void onButtonClick();
    }


    public MessageDialog(Context context, int theamResId, String message,okOnClickListener okListener) {
        super(context, theamResId);
        this.okListener = okListener;
        this.message = message;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.message_dialog_xml);

        TextView message_dialog_message = (TextView) findViewById(R.id.message_dialog_message);
        TextView message_dialog_accept = (TextView) findViewById(R.id.message_dialog_accept);

        message_dialog_message.setText(message);

        message_dialog_accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                okListener.onButtonClick();
            }
        });

    }
}
