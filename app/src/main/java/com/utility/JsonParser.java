package com.utility;

import android.content.Context;
import android.util.Log;

import com.com.rest.API;
import com.com.rest.GlobalValues;
import com.com.rest.MethodListener;
import com.fragment.ProductOffersFragment;
import com.model.ItemAdditionalProduct;
import com.model.ItemProductPrice;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;
import java.util.TreeMap;

/**
 * Created by administrator on 29/3/17.
 */

public class JsonParser {

    String TAG = getClass().getName();
    String apiProductDetails = "apiProductDetails";

    public void getProductDetails(Context mContext, String idRestaProducto, final ProductOffersFragment.ProductInterface productInterface) {
        String url = API.ProductDetails_New + idRestaProducto;
        //Log.e("product_details", "" + url);
        GlobalValues.getMethodManagerObj(mContext).makeStringReq(url, apiProductDetails, new MethodListener() {
            @Override
            public void onError() {
            }

            @Override
            public void onError(String response) {
            }

            @Override
            public void onSuccess(String response) {
                try {
                    JSONObject mObj = new JSONObject(response);
                    JSONArray mPrecious = mObj.optJSONArray("ProductoPrecios");
                    JSONArray mAdicionales = mObj.optJSONArray("ProductoAdicionales");


                    //TreeMap<Integer,ArrayList<ItemProductPrice>> alItemProdPrecio = null;
                    //TreeMap<Integer,ArrayList<ItemAdditionalProduct>> alItemAdicionales =null;
                    ArrayList<ItemProductPrice> alPrecio = null;
                    if (mPrecious != null) {
                        alPrecio = displayProductPrecious(mPrecious);
                    }
                    ArrayList<ItemAdditionalProduct> alProduct = null;
                    if (mAdicionales != null) {
                        alProduct = getProductoAdicionales(mAdicionales);
                    }
                    productInterface.setProduct(alPrecio,alProduct);
                } catch (JSONException e) {
                    Log.e(TAG, "" + e.toString());
                }
            }
        });
    }


    private ArrayList<ItemProductPrice> displayProductPrecious(JSONArray mPrecious) {
        ArrayList<ItemProductPrice> alPrice = null;
        for (int i = 0; i < mPrecious.length(); i++) {
            JSONArray mArray = mPrecious.optJSONArray(i);
            if (mArray != null && mArray.length() > 0) {

                int key = mArray.optJSONObject(0).optInt("IdRestaProducto");
                // Log.e("key", i + " " + key);

                alPrice = new ArrayList<>();
                for (int j = 0; j < mArray.length(); j++) {
                    JSONObject priceObject = mArray.optJSONObject(j);

                    ItemProductPrice itemProductPrice = new ItemProductPrice();
                    itemProductPrice.setIdRestaProdPrecio(priceObject.optInt("IdRestaProdPrecio"));
                    itemProductPrice.setIdRestaProducto(priceObject.optInt("IdRestaProducto"));
                    itemProductPrice.setGrupo(priceObject.optString("Grupo"));
                    itemProductPrice.setMedida(priceObject.optString("Medida").toUpperCase(Locale.ENGLISH));

                    if (!priceObject.isNull("Precio")) {
                        itemProductPrice.setPrecio(Float.parseFloat(priceObject.optString("Precio").replace(",",".")));
                    } else {
                        itemProductPrice.setPrecio(0.0f);
                    }
                    itemProductPrice.setPosicion(priceObject.optInt("Posicion"));
                    itemProductPrice.setCantidad(priceObject.optInt("Cantidad"));
                    itemProductPrice.setEstado(priceObject.optString("Estado"));

                    alPrice.add(itemProductPrice);
                }
            }
        }
        return alPrice;
    }


    private ArrayList<ItemAdditionalProduct> getProductoAdicionales(JSONArray mAdicionales) {
       //TreeMap<Integer,ArrayList<ItemAdditionalProduct>> alItemAdicionales = new TreeMap<>();
        ArrayList<ItemAdditionalProduct> alProduct = null;
        for (int i = 0; i < mAdicionales.length(); i++) {
            JSONArray mArray = mAdicionales.optJSONArray(i);

            if (mArray != null && mArray.length() > 0) {
                int key = mArray.optJSONObject(0).optInt("IdRestaProducto");

                alProduct = new ArrayList<>();
                for (int j = 0; j < mArray.length(); j++) {
                    JSONObject object = mArray.optJSONObject(j);
                    ItemAdditionalProduct product = new ItemAdditionalProduct();

                    product.setIdRelaRestaProduAdiciona(object.optInt("IdRelaRestaProduAdiciona"));
                    product.setIdRestaProducto(object.optInt("IdRestaProducto"));
                    product.setIdRestaExtradicional(object.optInt("IdRestaExtradicional"));
                    product.setNombre(object.optString("Nombre"));
                    product.setGrupo(object.optString("Grupo"));
                    product.setMedida(object.optString("Medida"));
                    product.setPrecio(object.optString("Precio"));
                    product.setPosicion(object.optInt("Posicion"));
                    product.setCantidad(object.optInt("Cantidad"));
                    product.setEstado(object.optString("Estado"));


                    alProduct.add(product);
                }
            }

        }
        return alProduct;
    }
}
