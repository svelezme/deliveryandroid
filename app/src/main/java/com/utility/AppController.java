package com.utility;

/*import info.androidhive.volleyexamples.utils.LruBitmapCache;*/
/**
 * Support Url
 * http://www.androidhive.info/2014/05/android-working-with-volley-library-1/
 * https://developer.android.com/training/volley/request.html
 * http://stackoverflow.com/questions/31897189/android-setup-volley-to-use-from-cache
 */

import android.app.Application;
import android.text.TextUtils;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.splunk.mint.Mint;

public class AppController extends Application {

	public static final String TAG = AppController.class
			.getSimpleName();

	public static int notification_Status = 0;

	private RequestQueue mRequestQueue;
	private ImageLoader mImageLoader;

	private static AppController mInstance;

	@Override
	public void onCreate() {
		super.onCreate();
		mInstance = this;
		Mint.initAndStartSession(this, "ef0b9bf0");

	}

	public static synchronized AppController getInstance() {
		return mInstance;
	}

	public RequestQueue getRequestQueue() {
		if (mRequestQueue == null) {
			mRequestQueue = Volley.newRequestQueue(getApplicationContext());
		}

		return mRequestQueue;
	}

	/*public ImageLoader getImageLoader() {
		getRequestQueue();
		if (mImageLoader == null) {
			mImageLoader = new ImageLoader(this.mRequestQueue,
					new LruBitmapCache());
		}
		return this.mImageLoader;
	}*/

	public <T> void addToRequestQueue(Request<T> req, String tag) {
		// set the default tag if tag is empty
		req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
		getRequestQueue().add(req);
	}

	public <T> void addToRequestQueue(Request<T> req) {
		req.setTag(TAG);
		getRequestQueue().add(req);
	}

	public void cancelPendingRequests(Object tag) {
		if (mRequestQueue != null) {
			mRequestQueue.cancelAll(tag);
		}
	}

	public void setErrorMessage(){
		Mint.setApplicationEnvironment(Mint.appEnvironmentUserAcceptanceTesting);
		Mint.initAndStartSession(getApplicationContext(), "ef0b9bf0");
	}
}
