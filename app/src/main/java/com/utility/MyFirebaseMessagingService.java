package com.utility;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;

import com.activity.DashBoardActivity;
import com.activity.SplashActivity;
import com.app.grupodeliveryec.deliveryEC.R;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

/**
 * www.androidhive.info
 */
public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = MyFirebaseMessagingService.class.getSimpleName();

    //private NotificationUtils notificationUtils;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
       // Log.e(TAG, "From: " + remoteMessage.getData());

        if (remoteMessage == null)
            return;

        if (remoteMessage.getData().size() > 0) {

            AppController.notification_Status = 0;

            Map<String, String> map = remoteMessage.getData();

            if (NotificationUtils.isAppIsInBackground(getApplicationContext())) {

                if (!map.get("image_url").equals("")){
                    addNotificationWithBIgImage(map.get("msg"), map.get("title"), map.get("image_url"));
                }else {
                    addNotification(map.get("msg"),map.get("title"));
                }
            } else {

                //Intent intentPush = new Intent(getApplicationContext(), DashBoardActivity.class);
               // intentPush.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
               // intentPush.putExtra("title", map.get("title"));
               // intentPush.putExtra("Message", map.get("msg"));
               // intentPush.putExtra("image_url", map.get("image_url"));
                //DashBoardActivity.showdialog(intentPush);

                Intent intentPush = new Intent("custom-event-name");
                intentPush.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                intentPush.putExtra("title", map.get("title"));
                intentPush.putExtra("Message", map.get("msg"));
                intentPush.putExtra("image_url", map.get("image_url"));
                LocalBroadcastManager.getInstance(this).sendBroadcast(intentPush);
            }
        }
    }

    private void addNotificationWithBIgImage(String msg, String title, String url) {
        convertUrlToBitmap convertUrlToBitmap = new convertUrlToBitmap(msg,title, url);
        convertUrlToBitmap.execute();
    }

    public  Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
             return null;
        }
    }

    public class convertUrlToBitmap extends AsyncTask<Void, Void, Bitmap> {
        String titile;
        String url;
        String msg;

        convertUrlToBitmap(String msg,final String titile, final String url) {
            this.msg = msg;
            this.titile = titile;
            this.url = url;
        }

        @Override
        protected Bitmap doInBackground(Void... params) {
            Bitmap bitmap = getBitmapFromURL(url);
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);

            NotificationCompat.Builder nc = new NotificationCompat.Builder(MyFirebaseMessagingService.this);
            NotificationCompat.BigPictureStyle bigPictureNotification = new NotificationCompat.BigPictureStyle();
            NotificationManager nm = (NotificationManager) MyFirebaseMessagingService.this.getSystemService(Context.NOTIFICATION_SERVICE);

            nc.setSmallIcon(getNotificationIcon());
            nc.setAutoCancel(true);
            nc.setContentTitle("" + titile);
            //nc.setContentText(""+msg);
            bigPictureNotification.bigPicture(bitmap);
            bigPictureNotification.setSummaryText(""+msg);
            //bigPictureNotification.bigPicture(BitmapFactory.decodeResource(this.getResources(), R.drawable.bg_img));
            //bigPictureNotification.setBigContentTitle("" + titile);
            nc.setStyle(bigPictureNotification);

            // nm.notify(3, nc.build());

            Intent notificationIntent = new Intent(MyFirebaseMessagingService.this, DashBoardActivity.class);

            notificationIntent.putExtra("Message",msg);
            notificationIntent.putExtra("image_url",url);

            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            PendingIntent contentIntent = PendingIntent.getActivity(MyFirebaseMessagingService.this, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            nc.setContentIntent(contentIntent);

            // Add as notification
            // NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            int id = (int)System.currentTimeMillis();
            nm.notify(id, nc.build());
        }
    }

    private void addNotification(String s, String title) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                .setSmallIcon(getNotificationIcon())
                .setContentTitle("" + title)
                .setAutoCancel(true)
                .setContentText("" + s);

        Intent notificationIntent = new Intent(this, DashBoardActivity.class);

        notificationIntent.putExtra("Message",s);
        notificationIntent.putExtra("image_url","");

        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(contentIntent);

        // Add as notification
        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        int id = (int)System.currentTimeMillis();
        manager.notify(id, builder.build());
    }


    private int getNotificationIcon() {
        boolean useWhiteIcon = (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP);
        return useWhiteIcon ? R.drawable.white_notification_icon : R.drawable.notification_icon;
        //return useWhiteIcon ? R.drawable.notification_icon : R.drawable.white_notification_icon;
    }
}