package com.utility;



/**
 * Created by administrator on 20/1/17.
 */

public class FragmentIDs {

    public static final int ResturentSearchFragment_Id = 0;
    public static final int ResturentSearchResultFragment_Id = 1;
    public static final int ResturentInfoFragment_Id = 2;
    public static final int ResturentMenuFragment_Id = 3;
    public static final int ResturentMenuItemFragment_Id = 4;
    public static final int AboutUsFragment_Id = 5;
    public static final int PerfilFragment_Id = 6;
    public static final int PerfilListFragment_Id =7;
    public static final int RegistraClienteFragment_Id = 8;
    public static final int DeliveryAddressFragment_Id = 9;
    public static final int BillingAddressFragment_Id = 10;
    public static final int UserProfileFragment_Id =11;
    public static final int DeliveryAddressListFragment_Id = 12;
    public static final int BillingAddressListFragment_Id = 13;
    public static final int ChangePasswordFragment_Id = 14;
    public static final int DispositivosListFragment_Id = 15;
    public static final int MyOrdersFragment_Id = 16;
    public static final int MyOrderDetailsFragment_Id = 17;
    public static final int OrderFragment_Id = 18;
    public static final int CongratulationsFragment_Id = 19;
    public static final int ProductOffersFragment_Id = 20;
    public static final int PromocionesFragment_Id = 21;
    public static final int NewOrderFragment_Id = 24;
    public static final int AddToCartFragment_Id = 23;

    public static final String ResturentSearchFragment_Tag = "ResturentSearchFragment";
    public static final String ResturentSearchResultFragment_Tag = "ResturentSearchResultFragment";
    public static final String ResturentInfoFragment_Tag = "ResturentInfoFragment";
    public static final String ResturentMenuFragment_Tag = "ResturentMenuFragment";
    public static final String ResturentMenuItemFragment_Tag = "ResturentMenuItemFragment";
    public static final String AboutUsFragment_Tag = "AboutUsFragment";
    public static final String PerfilFragment_Tag = "PerfilFragment";
    public static final String PerfilListFragment_Tag = "PerfilListFragment";
    public static final String RegistraClienteFragment_Tag = "RegistraClienteFragment";
    public static final String DeliveryAddressFragment_Tag = "DeliveryAddressFragment";
    public static final String BillingAddressFragment_Tag = "BillingAddressFragment";
    public static final String UserProfileFragment_Tag = "UserProfileFragment";
    public static final String DeliveryAddressListFragment_Tag = "DeliveryAddressListFragment";
    public static final String BillingAddressListFragment_Tag = "BillingAddressListFragment";
    public static final String ChangePasswordFragment_Tag = "ChangePasswordFragment";
    public static final String DispositivosListFragment_Tag = "DispositivosListFragment";
    public static final String MyOrdersFragment_Tag = "MyOrdersFragment";
    public static final String MyOrderDetailsFragment_Tag = "MyOrderDetailsFragment";
    public static final String OrderFragment_Tag = "OrderFragment";
    public static final String CongratulationsFragment_Tag = "CongratulationsFragment";
    public static final String ProductOffersFragment_Tag = "ProductOffersFragment";
    public static final String PromocionesFragment_Tag = "PromocionesFragment";
    public static final String NewOrderFragment_Tag = "NewOrderFragment";
    public static final String AddToCartFragment_Tag = "AddtoCartFragment";
}
