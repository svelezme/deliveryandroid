package com.utility;

import android.os.Bundle;

import com.bean.AddressListBean;
import com.bean.BillingBean;
import com.bean.ListaFormaPagosBean;
import com.model.ItemAdditionalProduct;
import com.model.ItemProductPrice;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

/**
 * Created by administrator on 8/2/17.
 */

public class Config {

    // global topic to receive app wide push notifications
   // public static final String TOPIC_GLOBAL = "global";

    // broadcast receiver intent filters
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
   // public static final String PUSH_NOTIFICATION = "pushNotification";

    // id to handle the notification in the notification tray
    public static final int NOTIFICATION_ID = 100;
    public static final int NOTIFICATION_ID_BIG_IMAGE = 101;

   // public static final String SHARED_PREF = "ah_firebase";






    public static final String paso = "paso2";
    public static final String deviceType = "AD";
    public static final int addListAddressType= 2; // Add a address when user click on address list
    public static final int addRegistrationAddressType = 1; //When new user is created then add delivery Address
    public static final int updateListAddressType = 3;


    public static final int addListBillingAddressType = 2;
    public static final int addRegistrationBiilingAddressType = 1;
    public static final int updateListBillingType = 3;

    public static final String[] selectGender = new String[]{
            "Seleccione Tipo",
            "Oficina",
            "Residencia",
            "Otros"
    };

    public static ArrayList<ItemAdditionalProduct> alhmAditional = new ArrayList<>();
    public static Map<String, ItemProductPrice> alhmPrecios = new HashMap<>();
    public static float totalAditional= 0.0f;
    public static float totalPrecios = 0.0f;

    public static int orderScreenStatus = 0;

    public static ArrayList<ListaFormaPagosBean> alListPagos = new ArrayList<>();
    public static Vector<BillingBean> alBilling = new Vector<>();
    public static Vector<AddressListBean> alDeliveryAddress = new Vector<>();


    public static Map<String, ItemProductPrice> alhmProductOfferPrecios = new HashMap<>();
    public static ArrayList<ItemAdditionalProduct> alhmProductOfferAditional = new ArrayList<>();
    public static float totalProductOfferAditional= 0.0f;
    public static float totalProductOfferPrecios = 0.0f;

    public static int idRestaCategoria = 0;
    public static String Nombre;

}
