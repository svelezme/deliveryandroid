package com.utility;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by administrator on 8/2/17.
 */

public class SessionManager {

    private static String deliverECPref = "deliverECPref";

    public static void saveToken(Context mContext, String token) {
        SharedPreferences pref = mContext.getSharedPreferences(deliverECPref, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("token", token);
        editor.commit();
    }

    public static String getSaveToken(Context mContext) {
        SharedPreferences pref = mContext.getSharedPreferences(deliverECPref, Context.MODE_PRIVATE);
        String token = pref.getString("token", null);
        return token;
    }

    public static void saveUserLogin(Context mContext, int Id, String email, String fullName, String Identificacion) {
        SharedPreferences pref = mContext.getSharedPreferences(deliverECPref, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putInt("Id", Id);
        editor.putString("email", email);
        editor.putString("fullName", fullName);
        editor.putString("Identificacion", Identificacion);
        editor.commit();
    }

    public static int getUserId(Context mContext) {
        SharedPreferences pref = mContext.getSharedPreferences(deliverECPref, Context.MODE_PRIVATE);
        int Id = pref.getInt("Id", 0);
        return Id;
    }

    public static void clearLoginUser(Context mContext) {
        SharedPreferences pref = mContext.getSharedPreferences(deliverECPref, Context.MODE_PRIVATE);
        pref.edit().remove("Id").commit();
        pref.edit().remove("email").commit();
        pref.edit().remove("fullName").commit();
        pref.edit().remove("Identificacion").commit();
    }

    public static void save_username(Context mContext, String username) {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences("username", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("username", username);
        editor.commit();
    }

    public static String get_username(Context mContext) {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences("username", Context.MODE_PRIVATE);
        String username = sharedPreferences.getString("username", null);
        return username;
    }


    public static void save_fb_image(Context mContext, String username) {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences("fb", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("fb", username);
        editor.commit();
    }

    public static String get_fb_image(Context mContext) {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences("fb", Context.MODE_PRIVATE);
        String username = sharedPreferences.getString("fb", null);
        return username;
    }


    public static void save_rastaurant_id(Context mContext, String username) {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences("rid", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("rid", username);
        editor.commit();
    }

    public static String get_rastaurant_id(Context mContext) {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences("rid", Context.MODE_PRIVATE);
        String username = sharedPreferences.getString("rid", null);
        return username;
    }


    public static void save_cityid_Ciudad(Context mContext, String username) {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences("cid", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("cid", username);
        editor.commit();
    }

    public static String get_city_id_Ciudad(Context mContext) {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences("cid", Context.MODE_PRIVATE);
        String username = sharedPreferences.getString("cid", null);
        return username;
    }

    public static void save_areaid_sector(Context mContext, String username) {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences("areaid", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("areaid", username);
        editor.commit();
    }

    public static String get_city_idSector(Context mContext) {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences("areaid", Context.MODE_PRIVATE);
        String username = sharedPreferences.getString("areaid", null);
        return username;
    }

}
