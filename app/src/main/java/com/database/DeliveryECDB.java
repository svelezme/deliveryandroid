package com.database;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by administrator on 15/3/17.
 */

public class DeliveryECDB extends SQLiteOpenHelper{

    public static final String DBName = "DeliveryECDB";
    public static final int DBVersion = 1;
    public static  DeliveryECDB mInstance = null;

    public static final String tbl_AddToCart = "tbl_AddToCart";
    public static final String tbl_ProductoPrecios = "ProductoPrecios";

    public static final String tbl_ProductoPreciosE2x = "ProductoPreciosE2x";

    public static final String tbl_ProductoAdicionales = "ProductoAdicionales";

    public static final String tbl_ProductoAdicionalesE2x = "tbl_ProductoAdicionalesE2x";

    public static final String tbl_products = "products";

    public static final String prod_E2xoffers = "prod_E2xoffers";

    public static final String cart_Id = "cart_Id";
    public static final String product_qty = "product_qty";

    public static final String precios_Id = "precios_Id";
    public static final String precios_IdRestaProdPrecio = "IdRestaProdPrecio";
    public static final String precios_IdRestaProducto = "IdRestaProducto";
    public static final String precios_Grupo = "Grupo";
    public static final String precios_Medida = "Medida";
    public static final String precios_price = "Precio";
    public static final String precios_Posicion = "Posicion";
    public static final String precios_Cantidad = "Cantidad";
    public static final String precios_Estado = "Estado";
    public static final String precios_qty = "precios_qty";
    public static final String TipoModelo2Por1 = "TipoModelo2Por1";
    public static final String TipoModelAdicional2Por1 = "TipoModelAdicional2Por1";
    public static final String ESDia2x1 = "ESDia2x1";


    public static final String adiciona_id = "adiciona_id";
    public static final String adiciona_IdRelaRestaProduAdiciona = "IdRelaRestaProduAdiciona";
    public static final String adiciona_IdRestaProducto = "IdRestaProducto";
    public static final String adiciona_IdRestaExtradicional = "IdRestaExtradicional";
    public static final String adiciona_Nombre = "Nombre";
    public static final String adiciona_Grupo = "Grupo";
    public static final String adiciona_Medida = "Medida";
    public static final String adiciona_Precio = "Precio";
    public static final String adiciona_Posicion = "Posicion";
    public static final String adiciona_Cantidad = "Cantidad";
    public static final String adiciona_Estado = "Estado";
    public static final String adiciona_qty = "adiciona_qty";

    public static final String product_productId = "product_productId";

    public static final String product_E2xoffers_productId = "product_E2xoffers_productId";

    public static final String product_idRestaProducto = "idRestaProducto";
   // public static final String product_IdRestaCategoria = "IdRestaCategoria";
   // public static final String product_IdRestaurante = "IdRestaurante";
  //  public static final String product_IdRestaLocal = "IdRestaLocal";
    public static final String product_Nombre = "Nombre";
    public static final String product_Descripcion = "Descripcion";
   // public static final String product_qty = "qty";
    public static final String product_price = "price";


    public DeliveryECDB(Context context) {
        super(context, DBName, null, DBVersion);
    }

    public static DeliveryECDB getmInstance(Context mContext){
        if(mInstance==null){
            mInstance = new DeliveryECDB(mContext);
        }
        return mInstance;
    }



    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE if not exists "+tbl_AddToCart+"("+cart_Id+" INTEGER PRIMARY KEY AUTOINCREMENT,"+product_qty+" INTEGER)");

        db.execSQL("CREATE TABLE if not exists "+tbl_ProductoPrecios+
                "("+precios_Id +" INTEGER PRIMARY KEY AUTOINCREMENT,"+precios_IdRestaProdPrecio+" TEXT, "+
                precios_IdRestaProducto+" TEXT, "+precios_Grupo+" TEXT, "+
                precios_Medida+" TEXT, "+precios_price+" REAL, "+
                precios_Posicion+" INTEGER, "+precios_Cantidad+" INTEGER, "+
                precios_Estado+" TEXT, "+
                precios_qty+" INTEGER, "+
                product_productId+" INTEGER"+")");

        db.execSQL("CREATE TABLE if not exists "+tbl_ProductoAdicionales+
            "("+adiciona_id+ " INTEGER PRIMARY KEY AUTOINCREMENT, "+
                adiciona_IdRelaRestaProduAdiciona+" TEXT, "+
                adiciona_IdRestaProducto+" TEXT, "+
                adiciona_IdRestaExtradicional+" TEXT, "+
                adiciona_Nombre+" TEXT, "+
                adiciona_Grupo+" TEXT, "+
                adiciona_Medida+" TEXT, "+
                adiciona_Precio+" REAL, "+
                adiciona_Posicion+" INTEGER, "+
                adiciona_Cantidad+" INTEGER, "+
                adiciona_Estado+" TEXT, "+
                adiciona_qty+" INTEGER, "+
                product_productId+" INTEGER"+")");

        db.execSQL("CREATE TABLE if not exists "+tbl_products+
            "("+product_productId+" INTEGER PRIMARY KEY AUTOINCREMENT, "+
                product_idRestaProducto+" TEXT, "+
                product_Nombre+" TEXT, "+
                product_Descripcion+" TEXT, "+
                product_qty+" TEXT, "+
                product_price+" REAL,"+
                TipoModelo2Por1+" TEXT,"+
                TipoModelAdicional2Por1+" TEXT,"+
                ESDia2x1+" TEXT"+")");

        /***
         * E2x Offers.
         */
        db.execSQL("CREATE TABLE if not exists "+prod_E2xoffers+
                "("+product_E2xoffers_productId+" INTEGER PRIMARY KEY AUTOINCREMENT, "+
                product_idRestaProducto+" TEXT, "+
                product_Nombre+" TEXT, "+
                product_Descripcion+" TEXT, "+
                product_qty+" TEXT, "+
                product_price+" REAL,"+
                product_productId+" INTEGER,"+
                TipoModelo2Por1+" TEXT,"+
                TipoModelAdicional2Por1+" TEXT"+")");


        db.execSQL("CREATE TABLE if not exists "+tbl_ProductoAdicionalesE2x+
                "("+adiciona_id+ " INTEGER PRIMARY KEY AUTOINCREMENT, "+
                adiciona_IdRelaRestaProduAdiciona+" TEXT, "+
                adiciona_IdRestaProducto+" TEXT, "+
                adiciona_IdRestaExtradicional+" TEXT, "+
                adiciona_Nombre+" TEXT, "+
                adiciona_Grupo+" TEXT, "+
                adiciona_Medida+" TEXT, "+
                adiciona_Precio+" REAL, "+
                adiciona_Posicion+" INTEGER, "+
                adiciona_Cantidad+" INTEGER, "+
                adiciona_Estado+" TEXT, "+
                adiciona_qty+" INTEGER, "+
                product_E2xoffers_productId+" INTEGER"+")");


        db.execSQL("CREATE TABLE if not exists "+tbl_ProductoPreciosE2x+
                "("+precios_Id +" INTEGER PRIMARY KEY AUTOINCREMENT,"+precios_IdRestaProdPrecio+" TEXT, "+
                precios_IdRestaProducto+" TEXT, "+precios_Grupo+" TEXT, "+
                precios_Medida+" TEXT, "+precios_price+" REAL, "+
                precios_Posicion+" INTEGER, "+precios_Cantidad+" INTEGER, "+
                precios_Estado+" TEXT, "+
                precios_qty+" INTEGER, "+
                product_E2xoffers_productId+" INTEGER"+")");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("DROP TABLE IF EXISTS " + tbl_AddToCart);
        db.execSQL("DROP TABLE IF EXISTS " + tbl_ProductoPrecios);
        db.execSQL("DROP TABLE IF EXISTS " + tbl_products);
        onCreate(db);
    }
}
