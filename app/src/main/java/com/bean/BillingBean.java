package com.bean;

import java.io.Serializable;

/**
 * Created by administrator on 15/2/17.
 */

public class BillingBean implements Serializable{

    int IdDireccionFactura;
    int IdCliente;
    String RazonSocial;
    String Identificacion;
    String Descripcion;
    String Direccion;
    String Telefono;
    String NombrePerfil;

    public int getIdDireccionFactura() {
        return IdDireccionFactura;
    }

    public void setIdDireccionFactura(int idDireccionFactura) {
        IdDireccionFactura = idDireccionFactura;
    }

    public int getIdCliente() {
        return IdCliente;
    }

    public void setIdCliente(int idCliente) {
        IdCliente = idCliente;
    }

    public String getRazonSocial() {
        return RazonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        RazonSocial = razonSocial;
    }

    public String getIdentificacion() {
        return Identificacion;
    }

    public void setIdentificacion(String identificacion) {
        Identificacion = identificacion;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String descripcion) {
        Descripcion = descripcion;
    }

    public String getDireccion() {
        return Direccion;
    }

    public void setDireccion(String direccion) {
        Direccion = direccion;
    }

    public String getTelefono() {
        return Telefono;
    }

    public void setTelefono(String telefono) {
        Telefono = telefono;
    }

    public String getNombrePerfil() {
        return NombrePerfil;
    }

    public void setNombrePerfil(String nombrePerfil) {
        NombrePerfil = nombrePerfil;
    }
}
