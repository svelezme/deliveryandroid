package com.bean;

/**
 * Created by administrator on 15/4/17.
 */

public class PromocionesBean {

    long IdRestaPromocion;
    long IdRestaurante;
    String NombreRestaurante;
    long IdCiudad;
    long IdSector;
    String Nombre;
    String Publicidad;
    String Imagen;

    public long getIdRestaPromocion() {
        return IdRestaPromocion;
    }

    public void setIdRestaPromocion(long idRestaPromocion) {
        IdRestaPromocion = idRestaPromocion;
    }

    public long getIdRestaurante() {
        return IdRestaurante;
    }

    public void setIdRestaurante(long idRestaurante) {
        IdRestaurante = idRestaurante;
    }

    public String getNombreRestaurante() {
        return NombreRestaurante;
    }

    public void setNombreRestaurante(String nombreRestaurante) {
        NombreRestaurante = nombreRestaurante;
    }

    public long getIdCiudad() {
        return IdCiudad;
    }

    public void setIdCiudad(long idCiudad) {
        IdCiudad = idCiudad;
    }

    public long getIdSector() {
        return IdSector;
    }

    public void setIdSector(long idSector) {
        IdSector = idSector;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getPublicidad() {
        return Publicidad;
    }

    public void setPublicidad(String publicidad) {
        Publicidad = publicidad;
    }

    public String getImagen() {
        return Imagen;
    }

    public void setImagen(String imagen) {
        Imagen = imagen;
    }
}
