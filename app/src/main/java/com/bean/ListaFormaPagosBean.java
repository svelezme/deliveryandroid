package com.bean;

/**
 * Created by administrator on 27/3/17.
 */

public class ListaFormaPagosBean {

    int IdRelaRestaFormaPago;
    int IdRestaurante;
    int IdFormaPago;
    String NombreRestaurante;
    String Nombre;
    String Valor;

    public int getIdRelaRestaFormaPago() {
        return IdRelaRestaFormaPago;
    }

    public void setIdRelaRestaFormaPago(int idRelaRestaFormaPago) {
        IdRelaRestaFormaPago = idRelaRestaFormaPago;
    }

    public int getIdRestaurante() {
        return IdRestaurante;
    }

    public void setIdRestaurante(int idRestaurante) {
        IdRestaurante = idRestaurante;
    }

    public int getIdFormaPago() {
        return IdFormaPago;
    }

    public void setIdFormaPago(int idFormaPago) {
        IdFormaPago = idFormaPago;
    }

    public String getNombreRestaurante() {
        return NombreRestaurante;
    }

    public void setNombreRestaurante(String nombreRestaurante) {
        NombreRestaurante = nombreRestaurante;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getValor() {
        return Valor;
    }

    public void setValor(String valor) {
        Valor = valor;
    }
}
