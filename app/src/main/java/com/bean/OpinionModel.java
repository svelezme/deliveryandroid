package com.bean;

/**
 * Created by administrator on 23/5/17.
 */

public class OpinionModel {
    String id_review;
    int id_restalocal;
    String revision;
    String tx_apellidos;
    String fecha_de_revision;
    String resta_average_rate;
    String tx_nombres;

    public String getTx_nombres() {
        return tx_nombres;
    }

    public void setTx_nombres(String tx_nombres) {
        this.tx_nombres = tx_nombres;
    }

    public String getId_review() {
        return id_review;
    }

    public void setId_review(String id_review) {
        this.id_review = id_review;
    }

    public int getId_restalocal() {
        return id_restalocal;
    }

    public void setId_restalocal(int id_restalocal) {
        this.id_restalocal = id_restalocal;
    }

    public String getRevision() {
        return revision;
    }

    public void setRevision(String revision) {
        this.revision = revision;
    }

    public String getTx_apellidos() {
        return tx_apellidos;
    }

    public void setTx_apellidos(String tx_apellidos) {
        this.tx_apellidos = tx_apellidos;
    }

    public String getFecha_de_revision() {
        return fecha_de_revision;
    }

    public void setFecha_de_revision(String fecha_de_revision) {
        this.fecha_de_revision = fecha_de_revision;
    }

    public String getResta_average_rate() {
        return resta_average_rate;
    }

    public void setResta_average_rate(String resta_average_rate) {
        this.resta_average_rate = resta_average_rate;
    }
}
