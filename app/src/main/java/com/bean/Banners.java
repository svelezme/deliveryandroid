package com.bean;

/**
 * Created by administrator on 19/8/17.
 */

public class Banners {

    String Bid;
    String imageurl;

    public Banners(String bid, String imageurl) {
        Bid = bid;
        this.imageurl = imageurl;
    }

    public String getBid() {
        return Bid;
    }

    public void setBid(String bid) {
        Bid = bid;
    }

    public String getImageurl() {
        return imageurl;
    }

    public void setImageurl(String imageurl) {
        this.imageurl = imageurl;
    }
}
