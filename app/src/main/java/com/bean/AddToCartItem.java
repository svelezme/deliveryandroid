package com.bean;

import com.model.ItemAdditionalProduct;
import com.model.ItemProductPrice;

import java.util.ArrayList;

/**
 * Created by administrator on 21/3/17.
 */

public class AddToCartItem {

    int id;
    ArrayList<ItemProductPrice> alPriceo;
    ArrayList<ItemAdditionalProduct> aladiciona;
    String Nombre;
    String Descripcion;
    String qty;
    float price;
    String idRestaProducto;
    ArrayList<AddToCartItem>  item_2x;

    /**
     * 2x1 Value
     */
    int idE2x1;
    String NombreE2x1;
    String DescripcionE2x1;
    String qtyE2x1;
    float priceE2x1;
    String idRestaProductoE2x1;
    ArrayList<ItemProductPrice> alPriceoE2x1;
    ArrayList<ItemAdditionalProduct> aladicionaE2x1;

    String TipoModelAdicional2Por1;
    String TipoModelo2Por1;
    int isIdStatus;
    String EsDia2x1;

    public String getEsDia2x1() {
        return EsDia2x1;
    }

    public void setEsDia2x1(String esDia2x1) {
        EsDia2x1 = esDia2x1;
    }

    public String getTipoModelAdicional2Por1() {
        return TipoModelAdicional2Por1;
    }

    public void setTipoModelAdicional2Por1(String tipoModelAdicional2Por1) {
        TipoModelAdicional2Por1 = tipoModelAdicional2Por1;
    }

    public int getIsIdStatus() {
        return isIdStatus;
    }

    public void setIsIdStatus(int isIdStatus) {
        this.isIdStatus = isIdStatus;
    }

    public String getTipoModelo2Por1() {
        return TipoModelo2Por1;
    }

    public void setTipoModelo2Por1(String tipoModelo2Por1) {
        TipoModelo2Por1 = tipoModelo2Por1;
    }

    public ArrayList<ItemAdditionalProduct> getAladicionaE2x1() {
        return aladicionaE2x1;
    }

    public void setAladicionaE2x1(ArrayList<ItemAdditionalProduct> aladicionaE2x1) {
        this.aladicionaE2x1 = aladicionaE2x1;
    }

    public ArrayList<ItemProductPrice> getAlPriceoE2x1() {
        return alPriceoE2x1;
    }

    public void setAlPriceoE2x1(ArrayList<ItemProductPrice> alPriceoE2x1) {
        this.alPriceoE2x1 = alPriceoE2x1;
    }

    public int getIdE2x1() {
        return idE2x1;
    }

    public void setIdE2x1(int idE2x1) {
        this.idE2x1 = idE2x1;
    }

    public float getPriceE2x1() {
        return priceE2x1;
    }

    public void setDescripcionE2x1(String descripcionE2x1) {
        DescripcionE2x1 = descripcionE2x1;
    }

    public String getDescripcionE2x1() {
        return DescripcionE2x1;
    }

    public void setIdRestaProductoE2x1(String idRestaProductoE2x1) {
        this.idRestaProductoE2x1 = idRestaProductoE2x1;
    }

    public String getIdRestaProductoE2x1() {
        return idRestaProductoE2x1;
    }

    public void setNombreE2x1(String nombreE2x1) {
        NombreE2x1 = nombreE2x1;
    }

    public String getNombreE2x1() {
        return NombreE2x1;
    }

    public void setPriceE2x1(float priceE2x1) {
        this.priceE2x1 = priceE2x1;
    }

    public String getQtyE2x1() {
        return qtyE2x1;
    }

    public void setQtyE2x1(String qtyE2x1) {
        this.qtyE2x1 = qtyE2x1;
    }

    public ArrayList<AddToCartItem> getItem_2x() {
        return item_2x;
    }

    public void setItem_2x(ArrayList<AddToCartItem> item_2x) {
        this.item_2x = item_2x;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIdRestaProducto() {
        return idRestaProducto;
    }

    public void setIdRestaProducto(String idRestaProducto) {
        this.idRestaProducto = idRestaProducto;
    }

    public ArrayList<ItemProductPrice> getAlPriceo() {
        return alPriceo;
    }

    public void setAlPriceo(ArrayList<ItemProductPrice> alPriceo) {
        this.alPriceo = alPriceo;
    }

    public ArrayList<ItemAdditionalProduct> getAladiciona() {
        return aladiciona;
    }

    public void setAladiciona(ArrayList<ItemAdditionalProduct> aladiciona) {
        this.aladiciona = aladiciona;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String descripcion) {
        Descripcion = descripcion;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }
}
