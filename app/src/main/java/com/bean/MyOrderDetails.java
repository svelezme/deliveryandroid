package com.bean;

/**
 * Created by administrator on 18/2/17.
 */

public class MyOrderDetails {

    int IdPedidoDetalle;
    int IdPedido;
    int IdRestaProducto;
    String NombreProducto;
    String Observacion;
    double Precio;
    String Valor;
    int Cantidad;

    public String getNombreProducto() {
        return NombreProducto;
    }

    public void setNombreProducto(String nombreProducto) {
        NombreProducto = nombreProducto;
    }

    public int getIdPedidoDetalle() {
        return IdPedidoDetalle;
    }

    public void setIdPedidoDetalle(int idPedidoDetalle) {
        IdPedidoDetalle = idPedidoDetalle;
    }

    public int getIdPedido() {
        return IdPedido;
    }

    public void setIdPedido(int idPedido) {
        IdPedido = idPedido;
    }

    public int getIdRestaProducto() {
        return IdRestaProducto;
    }

    public void setIdRestaProducto(int idRestaProducto) {
        IdRestaProducto = idRestaProducto;
    }

    public String getObservacion() {
        return Observacion;
    }

    public void setObservacion(String observacion) {
        Observacion = observacion;
    }

    public double getPrecio() {
        return Precio;
    }

    public void setPrecio(double precio) {
        Precio = precio;
    }

    public String getValor() {
        return Valor;
    }

    public void setValor(String valor) {
        Valor = valor;
    }

    public int getCantidad() {
        return Cantidad;
    }

    public void setCantidad(int cantidad) {
        Cantidad = cantidad;
    }
}
