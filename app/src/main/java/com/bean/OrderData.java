package com.bean;

/**
 * Created by administrator on 2/11/17.
 */

public class OrderData {

    int product_productId;
    int product_E2xoffers_productId;
    String idRestaProducto;
    float productPrice;
    float product2x1Price;
    int isIdStatus;
    String TipoModelo2Por1_P;
    String TipoModelAdicional2Por1;
    String EsDia2x1;

    public String getEsDia2x1() {
        return EsDia2x1;
    }

    public void setEsDia2x1(String esDia2x1) {
        EsDia2x1 = esDia2x1;
    }

    public String getTipoModelAdicional2Por1() {
        return TipoModelAdicional2Por1;
    }

    public void setTipoModelAdicional2Por1(String tipoModelAdicional2Por1) {
        TipoModelAdicional2Por1 = tipoModelAdicional2Por1;
    }

    public String getTipoModelo2Por1_P() {
        return TipoModelo2Por1_P;
    }

    public void setTipoModelo2Por1_P(String tipoModelo2Por1_P) {
        TipoModelo2Por1_P = tipoModelo2Por1_P;
    }

    public int getProduct_productId() {
        return product_productId;
    }

    public void setProduct_productId(int product_productId) {
        this.product_productId = product_productId;
    }

    public int getProduct_E2xoffers_productId() {
        return product_E2xoffers_productId;
    }

    public void setProduct_E2xoffers_productId(int product_E2xoffers_productId) {
        this.product_E2xoffers_productId = product_E2xoffers_productId;
    }

    public String getIdRestaProducto() {
        return idRestaProducto;
    }

    public void setIdRestaProducto(String idRestaProducto) {
        this.idRestaProducto = idRestaProducto;
    }

    public float getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(float productPrice) {
        this.productPrice = productPrice;
    }

    public float getProduct2x1Price() {
        return product2x1Price;
    }

    public void setProduct2x1Price(float product2x1Price) {
        this.product2x1Price = product2x1Price;
    }

    public int getIsIdStatus() {
        return isIdStatus;
    }

    public void setIsIdStatus(int isIdStatus) {
        this.isIdStatus = isIdStatus;
    }
}
