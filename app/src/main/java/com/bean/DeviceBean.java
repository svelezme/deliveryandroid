package com.bean;

/**
 * Created by administrator on 16/2/17.
 */

public class DeviceBean {

    int IdClienteDispositivo;
    int IdCliente;
    String NombreDispositivo;
    String TipoCelular;
    String IdUUID;
    String FechaRegistro;


    public int getIdClienteDispositivo() {
        return IdClienteDispositivo;
    }

    public void setIdClienteDispositivo(int idClienteDispositivo) {
        IdClienteDispositivo = idClienteDispositivo;
    }

    public int getIdCliente() {
        return IdCliente;
    }

    public void setIdCliente(int idCliente) {
        IdCliente = idCliente;
    }

    public String getNombreDispositivo() {
        return NombreDispositivo;
    }

    public void setNombreDispositivo(String nombreDispositivo) {
        NombreDispositivo = nombreDispositivo;
    }

    public String getTipoCelular() {
        return TipoCelular;
    }

    public void setTipoCelular(String tipoCelular) {
        TipoCelular = tipoCelular;
    }

    public String getIdUUID() {
        return IdUUID;
    }

    public void setIdUUID(String idUUID) {
        IdUUID = idUUID;
    }

    public String getFechaRegistro() {
        return FechaRegistro;
    }

    public void setFechaRegistro(String fechaRegistro) {
        FechaRegistro = fechaRegistro;
    }
}
