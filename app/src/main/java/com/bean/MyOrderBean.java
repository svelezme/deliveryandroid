package com.bean;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by administrator on 17/2/17.
 */

public class MyOrderBean implements Serializable{

    int IdPedido;
    int IdRestaLocal;
    int IdCliente;
    String Observacion;
    Double Subtotal;
    Double IVA;
    Double Servicio;
    Double Total;
    String Recibe;
    String FormaPago;
    String FechaRegistro;
    String Estado;

    String Nombres;
    String Apellidos;
    String Identificacion;
    String NombreCompletoCliente;
    String NombreRestaurante;
    String NombreLocal;
    String DescripcionLocal;
    String TipoComida;
    String ImagenLocal;
    String DirecEntrega;
    String DirecEntregaTipo;
    String DirecEntregaDireccion;
    String DirecEntregaDireccion2;
    String DirecEntregaCiudad;
    String DirecEntregaSector;
    String DirecEntregaTelefono;
    String DirecEntregaReferencia;
    String DirecFactura;
    String DirecFacturaRazonSocial;
    String DirecFacturaIdentificacion;
    String DirecFacturaDireccion;
    String DirecFacturaTelefono;
    String revision_status;

    public String getRevision_status() {
        return revision_status;
    }

    public void setRevision_status(String revision_status) {
        this.revision_status = revision_status;
    }

    ArrayList<MyOrderDetails> alOrderDetails;

    public ArrayList<MyOrderDetails> getAlOrderDetails() {
        return alOrderDetails;
    }

    public void setAlOrderDetails(ArrayList<MyOrderDetails> alOrderDetails) {
        this.alOrderDetails = alOrderDetails;
    }

    public int getIdPedido() {
        return IdPedido;
    }

    public void setIdPedido(int idPedido) {
        IdPedido = idPedido;
    }

    public int getIdRestaLocal() {
        return IdRestaLocal;
    }

    public void setIdRestaLocal(int idRestaLocal) {
        IdRestaLocal = idRestaLocal;
    }

    public int getIdCliente() {
        return IdCliente;
    }

    public void setIdCliente(int idCliente) {
        IdCliente = idCliente;
    }

    public String getObservacion() {
        return Observacion;
    }

    public void setObservacion(String observacion) {
        Observacion = observacion;
    }

    public Double getSubtotal() {
        return Subtotal;
    }

    public void setSubtotal(Double subtotal) {
        Subtotal = subtotal;
    }

    public Double getIVA() {
        return IVA;
    }

    public void setIVA(Double IVA) {
        this.IVA = IVA;
    }

    public Double getServicio() {
        return Servicio;
    }

    public void setServicio(Double servicio) {
        Servicio = servicio;
    }

    public Double getTotal() {
        return Total;
    }

    public void setTotal(Double total) {
        Total = total;
    }

    public String getRecibe() {
        return Recibe;
    }

    public void setRecibe(String recibe) {
        Recibe = recibe;
    }

    public String getFormaPago() {
        return FormaPago;
    }

    public void setFormaPago(String formaPago) {
        FormaPago = formaPago;
    }

    public String getFechaRegistro() {
        return FechaRegistro;
    }

    public void setFechaRegistro(String fechaRegistro) {
        FechaRegistro = fechaRegistro;
    }

    public String getEstado() {
        return Estado;
    }

    public void setEstado(String estado) {
        Estado = estado;
    }

    public String getNombres() {
        return Nombres;
    }

    public void setNombres(String nombres) {
        Nombres = nombres;
    }

    public String getApellidos() {
        return Apellidos;
    }

    public void setApellidos(String apellidos) {
        Apellidos = apellidos;
    }

    public String getIdentificacion() {
        return Identificacion;
    }

    public void setIdentificacion(String identificacion) {
        Identificacion = identificacion;
    }

    public String getNombreCompletoCliente() {
        return NombreCompletoCliente;
    }

    public void setNombreCompletoCliente(String nombreCompletoCliente) {
        NombreCompletoCliente = nombreCompletoCliente;
    }

    public String getNombreRestaurante() {
        return NombreRestaurante;
    }

    public void setNombreRestaurante(String nombreRestaurante) {
        NombreRestaurante = nombreRestaurante;
    }

    public String getNombreLocal() {
        return NombreLocal;
    }

    public void setNombreLocal(String nombreLocal) {
        NombreLocal = nombreLocal;
    }

    public String getDescripcionLocal() {
        return DescripcionLocal;
    }

    public void setDescripcionLocal(String descripcionLocal) {
        DescripcionLocal = descripcionLocal;
    }

    public String getTipoComida() {
        return TipoComida;
    }

    public void setTipoComida(String tipoComida) {
        TipoComida = tipoComida;
    }

    public String getImagenLocal() {
        return ImagenLocal;
    }

    public void setImagenLocal(String imagenLocal) {
        ImagenLocal = imagenLocal;
    }

    public String getDirecEntrega() {
        return DirecEntrega;
    }

    public void setDirecEntrega(String direcEntrega) {
        DirecEntrega = direcEntrega;
    }

    public String getDirecEntregaTipo() {
        return DirecEntregaTipo;
    }

    public void setDirecEntregaTipo(String direcEntregaTipo) {
        DirecEntregaTipo = direcEntregaTipo;
    }

    public String getDirecEntregaDireccion() {
        return DirecEntregaDireccion;
    }

    public void setDirecEntregaDireccion(String direcEntregaDireccion) {
        DirecEntregaDireccion = direcEntregaDireccion;
    }

    public String getDirecEntregaDireccion2() {
        return DirecEntregaDireccion2;
    }

    public void setDirecEntregaDireccion2(String direcEntregaDireccion2) {
        DirecEntregaDireccion2 = direcEntregaDireccion2;
    }

    public String getDirecEntregaCiudad() {
        return DirecEntregaCiudad;
    }

    public void setDirecEntregaCiudad(String direcEntregaCiudad) {
        DirecEntregaCiudad = direcEntregaCiudad;
    }

    public String getDirecEntregaSector() {
        return DirecEntregaSector;
    }

    public void setDirecEntregaSector(String direcEntregaSector) {
        DirecEntregaSector = direcEntregaSector;
    }

    public String getDirecEntregaTelefono() {
        return DirecEntregaTelefono;
    }

    public void setDirecEntregaTelefono(String direcEntregaTelefono) {
        DirecEntregaTelefono = direcEntregaTelefono;
    }

    public String getDirecEntregaReferencia() {
        return DirecEntregaReferencia;
    }

    public void setDirecEntregaReferencia(String direcEntregaReferencia) {
        DirecEntregaReferencia = direcEntregaReferencia;
    }

    public String getDirecFactura() {
        return DirecFactura;
    }

    public void setDirecFactura(String direcFactura) {
        DirecFactura = direcFactura;
    }

    public String getDirecFacturaRazonSocial() {
        return DirecFacturaRazonSocial;
    }

    public void setDirecFacturaRazonSocial(String direcFacturaRazonSocial) {
        DirecFacturaRazonSocial = direcFacturaRazonSocial;
    }

    public String getDirecFacturaIdentificacion() {
        return DirecFacturaIdentificacion;
    }

    public void setDirecFacturaIdentificacion(String direcFacturaIdentificacion) {
        DirecFacturaIdentificacion = direcFacturaIdentificacion;
    }

    public String getDirecFacturaDireccion() {
        return DirecFacturaDireccion;
    }

    public void setDirecFacturaDireccion(String direcFacturaDireccion) {
        DirecFacturaDireccion = direcFacturaDireccion;
    }

    public String getDirecFacturaTelefono() {
        return DirecFacturaTelefono;
    }

    public void setDirecFacturaTelefono(String direcFacturaTelefono) {
        DirecFacturaTelefono = direcFacturaTelefono;
    }
}
