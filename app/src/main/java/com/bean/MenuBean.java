package com.bean;

/**
 * Created by administrator on 20/1/17.
 */

public class MenuBean {
    String Menu_name;
    int ResId;

    public int getResId() {
        return ResId;
    }

    public void setResId(int resId) {
        ResId = resId;
    }

    public String getMenu_name() {
        return Menu_name;
    }

    public void setMenu_name(String menu_name) {
        Menu_name = menu_name;
    }
}
