package com.bean;

import java.io.Serializable;

/**
 * Created by administrator on 14/2/17.
 */

public class AddressListBean implements Serializable{

    int IdDirecciones;
    int IdCiudad;
    int IdCliente;
    int IdSector;
    String Descripcion;
    String Tipo;
    String Direccion;
    String Direccion2;
    String Telefono;
    String Celular;
    String Referencia;
    String NombreCiudad;
    String NombreSector;
    String NombrePerfil;

    public int getIdDirecciones() {
        return IdDirecciones;
    }

    public void setIdDirecciones(int idDirecciones) {
        IdDirecciones = idDirecciones;
    }

    public int getIdCiudad() {
        return IdCiudad;
    }

    public void setIdCiudad(int idCiudad) {
        IdCiudad = idCiudad;
    }

    public int getIdCliente() {
        return IdCliente;
    }

    public void setIdCliente(int idCliente) {
        IdCliente = idCliente;
    }

    public int getIdSector() {
        return IdSector;
    }

    public void setIdSector(int idSector) {
        IdSector = idSector;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String descripcion) {
        Descripcion = descripcion;
    }

    public String getTipo() {
        return Tipo;
    }

    public void setTipo(String tipo) {
        Tipo = tipo;
    }

    public String getDireccion() {
        return Direccion;
    }

    public void setDireccion(String direccion) {
        Direccion = direccion;
    }

    public String getDireccion2() {
        return Direccion2;
    }

    public void setDireccion2(String direccion2) {
        Direccion2 = direccion2;
    }

    public String getTelefono() {
        return Telefono;
    }

    public void setTelefono(String telefono) {
        Telefono = telefono;
    }

    public String getCelular() {
        return Celular;
    }

    public void setCelular(String celular) {
        Celular = celular;
    }

    public String getReferencia() {
        return Referencia;
    }

    public void setReferencia(String referencia) {
        Referencia = referencia;
    }

    public String getNombreCiudad() {
        return NombreCiudad;
    }

    public void setNombreCiudad(String nombreCiudad) {
        NombreCiudad = nombreCiudad;
    }

    public String getNombreSector() {
        return NombreSector;
    }

    public void setNombreSector(String nombreSector) {
        NombreSector = nombreSector;
    }

    public String getNombrePerfil() {
        return NombrePerfil;
    }

    public void setNombrePerfil(String nombrePerfil) {
        NombrePerfil = nombrePerfil;
    }
}
