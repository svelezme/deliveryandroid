package com.view;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by administrator on 19/4/17.
 */

public class ExoBoldItalicTextView extends TextView{

    public ExoBoldItalicTextView(Context context) {
        super(context);
        init();
    }

    public ExoBoldItalicTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ExoBoldItalicTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }
    private void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/Exo-BoldItalic.otf");
        setTypeface(tf);
    }
}
