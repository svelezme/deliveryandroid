package com.view;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by administrator on 6/4/17.
 */

public class GothamBoldTextView extends TextView{

    public GothamBoldTextView(Context context) {
        super(context);
        init();
    }

    public GothamBoldTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public GothamBoldTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }
    private void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/Gotham-Bold.otf");
        setTypeface(tf);
    }
}
