package com.view;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by administrator on 6/4/17.
 */

public class SemiBoldItalicTextView extends TextView{

    public SemiBoldItalicTextView(Context context) {
        super(context);
        init();
    }

    public SemiBoldItalicTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public SemiBoldItalicTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }
    private void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/Exo-SemiBoldItalic.otf");
        setTypeface(tf);
    }
}
