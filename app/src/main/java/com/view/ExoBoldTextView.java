package com.view;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by administrator on 6/4/17.
 */

public class ExoBoldTextView extends TextView{

    public ExoBoldTextView(Context context) {
        super(context);
        init();
    }

    public ExoBoldTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ExoBoldTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }
    private void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/Exo-Bold.otf");
        setTypeface(tf);
    }
}
