package com.model;

import java.io.Serializable;

/**
 * Created by administrator on 30/1/17.
 */

public class RestaurantMenuList implements Serializable{
    int IdRestaCategoria;
    int IdRestaurante;
    String Nombre;
    String Posicion;
    String Siglas;
    String ImagenRestaCategoria;
    String Estado;
    String NombreRestaurante;
    String rating;

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public int getIdRestaCategoria() {
        return IdRestaCategoria;
    }

    public void setIdRestaCategoria(int idRestaCategoria) {
        IdRestaCategoria = idRestaCategoria;
    }

    public int getIdRestaurante() {
        return IdRestaurante;
    }

    public void setIdRestaurante(int idRestaurante) {
        IdRestaurante = idRestaurante;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getPosicion() {
        return Posicion;
    }

    public void setPosicion(String posicion) {
        Posicion = posicion;
    }

    public String getSiglas() {
        return Siglas;
    }

    public void setSiglas(String siglas) {
        Siglas = siglas;
    }

    public String getImagenRestaCategoria() {
        return ImagenRestaCategoria;
    }

    public void setImagenRestaCategoria(String imagenRestaCategoria) {
        ImagenRestaCategoria = imagenRestaCategoria;
    }

    public String getEstado() {
        return Estado;
    }

    public void setEstado(String estado) {
        Estado = estado;
    }

    public String getNombreRestaurante() {
        return NombreRestaurante;
    }

    public void setNombreRestaurante(String nombreRestaurante) {
        NombreRestaurante = nombreRestaurante;
    }
}
