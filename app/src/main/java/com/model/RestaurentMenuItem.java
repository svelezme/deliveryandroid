package com.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by administrator on 1/2/17.
 */

public class RestaurentMenuItem implements Parcelable{

    @Override
    public void writeToParcel(Parcel parcel, int i) {

    }

    @Override
    public int describeContents() {
        return 0;
    }

    int idRestaProducto;
    int IdRestaCategoria;
    int IdRestaurante;
    int IdRestaLocal;
    String Nombre;
    String Descripcion;
    String DiasSemana;
    String Dias2Por1;
    String Valida2Por1;
    String Siglas;
    String ImagenRestaProducto;
    String ImagenRestaProductoGrande;
    String Estado;
    ArrayList<ItemProductPrice> alProductPrice;
    ArrayList<ItemAdditionalProduct> alItemAdditionalProduct;
    Float Precio;
    int ValidaMultiPrecios;
    String EsDia2x1;
    int ValidaAdicionales;
    String NombreCategoria;
    String NombreLocal;
    String NombreRestaurante;
    int es2Status;
    boolean isImageValid;

    public boolean isImageValid() {
        return isImageValid;
    }

    public void setImageValid(boolean imageValid) {
        isImageValid = imageValid;
    }

    public int getEs2Status() {
        return es2Status;
    }

    public void setEs2Status(int es2Status) {
        this.es2Status = es2Status;
    }

    public Float getPrecio() {
        return Precio;
    }

    public void setPrecio(Float precio) {
        Precio = precio;
    }

    public int getValidaMultiPrecios() {
        return ValidaMultiPrecios;
    }

    public void setValidaMultiPrecios(int validaMultiPrecios) {
        ValidaMultiPrecios = validaMultiPrecios;
    }

    public String getEsDia2x1() {
        return EsDia2x1;
    }

    public void setEsDia2x1(String esDia2x1) {
        EsDia2x1 = esDia2x1;
    }

    public int getValidaAdicionales() {
        return ValidaAdicionales;
    }

    public void setValidaAdicionales(int validaAdicionales) {
        ValidaAdicionales = validaAdicionales;
    }

    public String getNombreCategoria() {
        return NombreCategoria;
    }

    public void setNombreCategoria(String nombreCategoria) {
        NombreCategoria = nombreCategoria;
    }

    public String getNombreLocal() {
        return NombreLocal;
    }

    public void setNombreLocal(String nombreLocal) {
        NombreLocal = nombreLocal;
    }

    public String getNombreRestaurante() {
        return NombreRestaurante;
    }

    public void setNombreRestaurante(String nombreRestaurante) {
        NombreRestaurante = nombreRestaurante;
    }

    public int getIdRestaProducto() {
        return idRestaProducto;
    }

    public void setIdRestaProducto(int idRestaProducto) {
        this.idRestaProducto = idRestaProducto;
    }

    public int getIdRestaCategoria() {
        return IdRestaCategoria;
    }

    public void setIdRestaCategoria(int idRestaCategoria) {
        IdRestaCategoria = idRestaCategoria;
    }

    public int getIdRestaurante() {
        return IdRestaurante;
    }

    public void setIdRestaurante(int idRestaurante) {
        IdRestaurante = idRestaurante;
    }

    public int getIdRestaLocal() {
        return IdRestaLocal;
    }

    public void setIdRestaLocal(int idRestaLocal) {
        IdRestaLocal = idRestaLocal;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String descripcion) {
        Descripcion = descripcion;
    }

    public String getDiasSemana() {
        return DiasSemana;
    }

    public void setDiasSemana(String diasSemana) {
        DiasSemana = diasSemana;
    }

    public String getDias2Por1() {
        return Dias2Por1;
    }

    public void setDias2Por1(String dias2Por1) {
        Dias2Por1 = dias2Por1;
    }

    public String getValida2Por1() {
        return Valida2Por1;
    }

    public void setValida2Por1(String valida2Por1) {
        Valida2Por1 = valida2Por1;
    }

    public String getSiglas() {
        return Siglas;
    }

    public void setSiglas(String siglas) {
        Siglas = siglas;
    }

    public String getImagenRestaProducto() {
        return ImagenRestaProducto;
    }

    public void setImagenRestaProducto(String imagenRestaProducto) {
        ImagenRestaProducto = imagenRestaProducto;
    }

    public String getImagenRestaProductoGrande() {
        return ImagenRestaProductoGrande;
    }

    public void setImagenRestaProductoGrande(String imagenRestaProductoGrande) {
        ImagenRestaProductoGrande = imagenRestaProductoGrande;
    }

    public String getEstado() {
        return Estado;
    }

    public void setEstado(String estado) {
        Estado = estado;
    }

    public ArrayList<ItemProductPrice> getAlProductPrice() {
        return alProductPrice;
    }

    public void setAlProductPrice(ArrayList<ItemProductPrice> alProductPrice) {
        this.alProductPrice = alProductPrice;
    }

    public ArrayList<ItemAdditionalProduct> getAlItemAdditionalProduct() {
        return alItemAdditionalProduct;
    }

    public void setAlItemAdditionalProduct(ArrayList<ItemAdditionalProduct> alItemAdditionalProduct) {
        this.alItemAdditionalProduct = alItemAdditionalProduct;
    }
}
