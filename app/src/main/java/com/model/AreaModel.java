package com.model;

/**
 * Created by administrator on 21/1/17.
 */

public class AreaModel {

    int IdCiudad;
    int IdSector;
    String Nombre;
    String Siglas;
    String Estado;

    public String getSiglas() {
        return Siglas;
    }

    public void setSiglas(String siglas) {
        Siglas = siglas;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public int getIdCiudad() {
        return IdCiudad;
    }

    public void setEstado(String estado) {
        Estado = estado;
    }

    public int getIdSector() {
        return IdSector;
    }

    public void setIdCiudad(int idCiudad) {
        IdCiudad = idCiudad;
    }

    public String getEstado() {
        return Estado;
    }

    public void setIdSector(int idSector) {
        IdSector = idSector;
    }
}

