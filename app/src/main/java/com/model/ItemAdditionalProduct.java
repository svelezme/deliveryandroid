package com.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by administrator on 1/2/17.
 */

public class ItemAdditionalProduct implements Parcelable{

    int IdRelaRestaProduAdiciona;
    int IdRestaProducto;
    int IdRestaExtradicional;
    String Nombre;
    String Grupo;
    String Medida;
    String Precio;
    int Posicion;
    int Cantidad;
    String Estado;
    int adiciona_qty;
    float price2x1;

    public float getPrice2x1() {
        return price2x1;
    }

    public void setPrice2x1(float price2x1) {
        this.price2x1 = price2x1;
    }

    public int getAdiciona_qty() {
        return adiciona_qty;
    }

    public void setAdiciona_qty(int adiciona_qty) {
        this.adiciona_qty = adiciona_qty;
    }

    public int getIdRelaRestaProduAdiciona() {
        return IdRelaRestaProduAdiciona;
    }

    public void setIdRelaRestaProduAdiciona(int idRelaRestaProduAdiciona) {
        IdRelaRestaProduAdiciona = idRelaRestaProduAdiciona;
    }

    public int getIdRestaProducto() {
        return IdRestaProducto;
    }

    public void setIdRestaProducto(int idRestaProducto) {
        IdRestaProducto = idRestaProducto;
    }

    public int getIdRestaExtradicional() {
        return IdRestaExtradicional;
    }

    public void setIdRestaExtradicional(int idRestaExtradicional) {
        IdRestaExtradicional = idRestaExtradicional;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getGrupo() {
        return Grupo;
    }

    public void setGrupo(String grupo) {
        Grupo = grupo;
    }

    public String getMedida() {
        return Medida;
    }

    public void setMedida(String medida) {
        Medida = medida;
    }

    public String getPrecio() {
        return Precio;
    }

    public void setPrecio(String precio) {
        Precio = precio;
    }

    public int getPosicion() {
        return Posicion;
    }

    public void setPosicion(int posicion) {
        Posicion = posicion;
    }

    public int getCantidad() {
        return Cantidad;
    }

    public void setCantidad(int cantidad) {
        Cantidad = cantidad;
    }

    public String getEstado() {
        return Estado;
    }

    public void setEstado(String estado) {
        Estado = estado;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {

    }

    @Override
    public int describeContents() {
        return 0;
    }
}
