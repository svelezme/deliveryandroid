package com.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by administrator on 23/1/17.
 */

public class RestaurantListModel implements Serializable{

    String BaseUrl= "http://www.deliveryec.com/images/app/";

    int IdRestaurante;
    int IdRestaLocal;
    int IdSector;
    int rating;
    String NombreRestaurante;
    String NombreLocal;
    String Telefono;
    String Direccion;
    String ImagenLocal;
    String Minimo;
    String TiempoEntrega;
    String TipoComida;
    String InfoGeneral;
    String DireccionWeb;
    String Facebook;
    String Twitter;
    String Instagram;
    String HoraApertura;
    String HoraCierre;
    String TotalEstado;
    String Activo;
    String Retira;
    String TipoModelo2Por1;
    String TipoModeloAdicional2Por1;
    String CostoEnvio;


    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    ArrayList<RestaurantMenuList> arrayList;

    public ArrayList<RestaurantMenuList> getArrayList() {
        return arrayList;
    }

    public void setArrayList(ArrayList<RestaurantMenuList> arrayList) {
        this.arrayList = arrayList;
    }

    public int getIdSector() {
        return IdSector;
    }

    public void setIdSector(int idSector) {
        IdSector = idSector;
    }

    public int getIdRestaLocal() {
        return IdRestaLocal;
    }

    public void setActivo(String activo) {
        Activo = activo;
    }

    public int getIdRestaurante() {
        return IdRestaurante;
    }

    public void setBaseUrl(String baseUrl) {
        BaseUrl = baseUrl;
    }

    public String getActivo() {
        return Activo;
    }

    public void setCostoEnvio(String costoEnvio) {
        CostoEnvio = costoEnvio;
    }

    public String getBaseUrl() {
        return BaseUrl;
    }

    public void setDireccion(String direccion) {
        Direccion = direccion;
    }

    public String getCostoEnvio() {
        return CostoEnvio;
    }

    public void setDireccionWeb(String direccionWeb) {
        DireccionWeb = direccionWeb;
    }

    public String getDireccion() {
        return Direccion;
    }

    public void setFacebook(String facebook) {
        Facebook = facebook;
    }

    public String getDireccionWeb() {
        return DireccionWeb;
    }

    public void setHoraApertura(String horaApertura) {
        HoraApertura = horaApertura;
    }

    public String getFacebook() {
        return Facebook;
    }

    public void setHoraCierre(String horaCierre) {
        HoraCierre = horaCierre;
    }

    public String getHoraApertura() {
        return HoraApertura;
    }

    public void setIdRestaLocal(int idRestaLocal) {
        IdRestaLocal = idRestaLocal;
    }

    public String getHoraCierre() {
        return HoraCierre;
    }

    public void setIdRestaurante(int idRestaurante) {
        IdRestaurante = idRestaurante;
    }

    public String getImagenLocal() {
        return ImagenLocal;
    }

    public void setImagenLocal(String imagenLocal) {
        ImagenLocal = imagenLocal;
    }

    public String getInfoGeneral() {
        return InfoGeneral;
    }

    public void setInfoGeneral(String infoGeneral) {
        InfoGeneral = infoGeneral;
    }

    public String getInstagram() {
        return Instagram;
    }

    public void setInstagram(String instagram) {
        Instagram = instagram;
    }

    public String getMinimo() {
        return Minimo;
    }

    public void setMinimo(String minimo) {
        Minimo = minimo;
    }

    public String getNombreLocal() {
        return NombreLocal;
    }

    public void setNombreLocal(String nombreLocal) {
        NombreLocal = nombreLocal;
    }

    public String getNombreRestaurante() {
        return NombreRestaurante;
    }

    public void setNombreRestaurante(String nombreRestaurante) {
        NombreRestaurante = nombreRestaurante;
    }

    public String getRetira() {
        return Retira;
    }

    public void setRetira(String retira) {
        Retira = retira;
    }

    public String getTelefono() {
        return Telefono;
    }

    public void setTelefono(String telefono) {
        Telefono = telefono;
    }

    public String getTiempoEntrega() {
        return TiempoEntrega;
    }

    public void setTiempoEntrega(String tiempoEntrega) {
        TiempoEntrega = tiempoEntrega;
    }

    public String getTipoComida() {
        return TipoComida;
    }

    public void setTipoComida(String tipoComida) {
        TipoComida = tipoComida;
    }

    public String getTipoModelo2Por1() {
        return TipoModelo2Por1;
    }

    public void setTipoModelo2Por1(String tipoModelo2Por1) {
        TipoModelo2Por1 = tipoModelo2Por1;
    }

    public String getTipoModeloAdicional2Por1() {
        return TipoModeloAdicional2Por1;
    }

    public void setTipoModeloAdicional2Por1(String tipoModeloAdicional2Por1) {
        TipoModeloAdicional2Por1 = tipoModeloAdicional2Por1;
    }

    public String getTotalEstado() {
        return TotalEstado;
    }

    public void setTotalEstado(String totalEstado) {
        TotalEstado = totalEstado;
    }

    public String getTwitter() {
        return Twitter;
    }

    public void setTwitter(String twitter) {
        Twitter = twitter;
    }
}
