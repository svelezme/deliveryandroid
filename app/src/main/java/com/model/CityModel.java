package com.model;

import java.util.List;

/**
 * Created by administrator on 21/1/17.
 */

public class CityModel {

    int IdCiudad;
    String Nombre;
    String Siglas;
    String Estado;

    public int getIdCiudad() {
        return IdCiudad;
    }

    public void setEstado(String estado) {
        Estado = estado;
    }

    public String getEstado() {
        return Estado;
    }

    public void setIdCiudad(int idCiudad) {
        IdCiudad = idCiudad;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getSiglas() {
        return Siglas;
    }

    public void setSiglas(String siglas) {
        Siglas = siglas;
    }
}

