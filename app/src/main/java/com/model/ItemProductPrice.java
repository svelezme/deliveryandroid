package com.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by administrator on 1/2/17.
 */

public class ItemProductPrice implements Parcelable{

    @Override
    public void writeToParcel(Parcel parcel, int i) {

    }

    @Override
    public int describeContents() {
        return 0;
    }

    int IdRestaProdPrecio;
    int IdRestaProducto;
    String Grupo;
    String Medida;
    Float Precio;
    int Posicion;
    int Cantidad;
    String Estado;


    int product_E2xoffers_productId;

    public int getProduct_E2xoffers_productId() {
        return product_E2xoffers_productId;
    }

    public void setProduct_E2xoffers_productId(int product_E2xoffers_productId) {
        this.product_E2xoffers_productId = product_E2xoffers_productId;
    }

    public int getIdRestaProdPrecio() {
        return IdRestaProdPrecio;
    }

    public void setIdRestaProdPrecio(int idRestaProdPrecio) {
        IdRestaProdPrecio = idRestaProdPrecio;
    }

    public int getIdRestaProducto() {
        return IdRestaProducto;
    }

    public void setIdRestaProducto(int idRestaProducto) {
        IdRestaProducto = idRestaProducto;
    }

    public String getGrupo() {
        return Grupo;
    }

    public void setGrupo(String grupo) {
        Grupo = grupo;
    }

    public String getMedida() {
        return Medida;
    }

    public void setMedida(String medida) {
        Medida = medida;
    }

    public Float getPrecio() {
        return Precio;
    }

    public void setPrecio(Float precio) {
        Precio = precio;
    }

    public int getPosicion() {
        return Posicion;
    }

    public void setPosicion(int posicion) {
        Posicion = posicion;
    }

    public int getCantidad() {
        return Cantidad;
    }

    public void setCantidad(int cantidad) {
        Cantidad = cantidad;
    }

    public String getEstado() {
        return Estado;
    }

    public void setEstado(String estado) {
        Estado = estado;
    }


}
