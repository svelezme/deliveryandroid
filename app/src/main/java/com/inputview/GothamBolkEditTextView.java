package com.inputview;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

/**
 * Created by administrator on 6/4/17.
 */

public class GothamBolkEditTextView extends EditText{

    public GothamBolkEditTextView(Context context) {
        super(context);
        init();
    }

    public GothamBolkEditTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public GothamBolkEditTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }
    private void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/Gotham-Bolk.otf");
        setTypeface(tf);
    }
}
