package com.inputview;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

/**
 * Created by administrator on 7/4/17.
 */

public class RegularEditTextView extends EditText{

    public RegularEditTextView(Context context) {
        super(context);
        init();
    }

    public RegularEditTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public RegularEditTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/Exo-Regular.otf");
        setTypeface(tf);
    }
}
