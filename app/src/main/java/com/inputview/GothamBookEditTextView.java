package com.inputview;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Created by administrator on 6/4/17.
 */

public class GothamBookEditTextView extends EditText{

    public GothamBookEditTextView(Context context) {
        super(context);
        init();
    }

    public GothamBookEditTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public GothamBookEditTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }
    private void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/Gotham-Book.otf");
        setTypeface(tf);
    }
}
