package com.buttonView;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

/**
 * Created by administrator on 6/4/17.
 */

public class ExoBold extends Button{

    public ExoBold(Context context) {
        super(context);
        init();
    }

    public ExoBold(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ExoBold(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }
    private void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/Exo-Bold.otf");
        setTypeface(tf);
    }
}
