package com.buttonView;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.EditText;

/**
 * Created by administrator on 7/4/17.
 */

public class RegularButtonView extends Button{

    public RegularButtonView(Context context) {
        super(context);
        init();
    }

    public RegularButtonView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public RegularButtonView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/Exo-Regular.otf");
        setTypeface(tf);
    }
}
