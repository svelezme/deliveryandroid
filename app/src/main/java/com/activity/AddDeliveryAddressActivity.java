package com.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.TextView;

import com.app.grupodeliveryec.deliveryEC.R;
import com.bean.AddressListBean;
import com.bruce.pickerview.LoopView;
import com.com.rest.API;
import com.com.rest.GlobalValues;
import com.com.rest.MethodListener;
import com.model.AreaModel;
import com.model.CityModel;
import com.utility.AppController;
import com.utility.Config;
import com.utility.KeyboadSetting;
import com.utility.MessageDialog;
import com.utility.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.ArrayList;

/**
 * Created by administrator on 27/3/17.
 */

public class AddDeliveryAddressActivity extends AppCompatActivity {

    String TAG = getClass().getName();
    String apiCityRequest = "CityRequest";
    String apiClientDireccion = "apiClientDireccion";

    ImageView resturent_progressBar;
    EditText delivery_descripcion;
    EditText delivery_ciudad;
    EditText delivery_sector;
    EditText delivery_Tipo;
    EditText delivery_address1;
    EditText delivery_address2;
    EditText delivery_convencional;
    EditText delivery_celuar;
    EditText delivery_office;
    LinearLayout delivery_address;
    LinearLayout delivery_address_cancel;
    TextView delivery_submitTitle;

    ArrayList<CityModel> alCityList;
    ArrayList<AreaModel> alAreaList;
    int IdCiudad, IdSector;

    MessageDialog messageDialog;
    Animation animation;
    Context context;

    //TextView tv_title;
    ImageView tv_back; //It is ref of deliveryaddress_toolbar xml


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppController.getInstance().setErrorMessage();
        setContentView(R.layout.deliveryaddress_xml_for_order_activity);

        context = this;

        resturent_progressBar = (ImageView) findViewById(R.id.resturent_progressBar);
        delivery_descripcion = (EditText) findViewById(R.id.delivery_descripcion);
        delivery_ciudad = (EditText) findViewById(R.id.delivery_ciudad);
        delivery_sector = (EditText) findViewById(R.id.delivery_sector);
        delivery_Tipo = (EditText) findViewById(R.id.delivery_Tipo);
        delivery_address1 = (EditText) findViewById(R.id.delivery_address1);
        delivery_address2 = (EditText) findViewById(R.id.delivery_address2);
        delivery_convencional = (EditText) findViewById(R.id.delivery_convencional);
        delivery_celuar = (EditText) findViewById(R.id.delivery_celuar);
        delivery_office = (EditText) findViewById(R.id.delivery_office);
        delivery_address = (LinearLayout) findViewById(R.id.delivery_address);
        delivery_address_cancel = (LinearLayout) findViewById(R.id.delivery_address_cancel);
        delivery_submitTitle = (TextView) findViewById(R.id.delivery_submitTitle);

        tv_back = (ImageView) findViewById(R.id.tv_back);

        delivery_submitTitle.setText(getResources().getString(R.string.guardar_y_salir));
        delivery_address_cancel.setVisibility(View.VISIBLE);

        delivery_address_cancel.setOnClickListener(addToCancel);
        delivery_address.setOnClickListener(addToSubmit);
        //delivery_submitTitle.setOnClickListener(addToSubmit);

        delivery_Tipo.setOnClickListener(addTipo);
        delivery_ciudad.setOnClickListener(addCiudad);
        delivery_sector.setOnClickListener(addSector);
        tv_back.setOnClickListener(addBackListener);

        getCityList();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.animation_enter, R.anim.animation_leave);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    View.OnClickListener addBackListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            View v = ((Activity) context).getCurrentFocus();
            KeyboadSetting.hideKeyboadUsingView(v, context);

            finish();
            overridePendingTransition(R.anim.animation_enter, R.anim.animation_leave);
        }
    };

    View.OnClickListener addToCancel = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            View v = ((Activity) context).getCurrentFocus();
            KeyboadSetting.hideKeyboadUsingView(v, context);

            finish();
            overridePendingTransition(R.anim.animation_enter, R.anim.animation_leave);
        }
    };

    View.OnClickListener addToSubmit = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            View v = ((Activity) context).getCurrentFocus();
            KeyboadSetting.hideKeyboadUsingView(v, context);

            doSubmitDeliveryAddress();
        }
    };

    View.OnClickListener addTipo = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            openGenderDialog();
        }
    };

    View.OnClickListener addCiudad = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            View v = ((Activity) context).getCurrentFocus();
            //View v = getActivity().getCurrentFocus();
            KeyboadSetting.hideKeyboadUsingView(v, context);
            //KeyboadSetting.hideKeyboard(context,register_surname);
            ciudadHandler();
        }
    };

    View.OnClickListener addSector = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            openAreaDialog();
        }
    };

    private void openGenderDialog() {
        final Dialog genderDialog = new Dialog(new ContextThemeWrapper(context, R.style.DialogSlideAnim));
        genderDialog.getWindow().setGravity(Gravity.BOTTOM);
        genderDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        genderDialog.setContentView(R.layout.citydialog_xml_ciudad);
        final LoopView loopView = (LoopView) genderDialog.findViewById(R.id.loop_view);
        final TextView cityDialog_cancel = (TextView) genderDialog.findViewById(R.id.cityDialog_cancel);
        final TextView cityDialog_accept = (TextView) genderDialog.findViewById(R.id.cityDialog_accept);

        loopView.setInitPosition(0);
        loopView.setCanLoop(false);
        final ArrayList<String> area_list = new ArrayList<>();
        for (int i = 0; i < Config.selectGender.length; i++) {
            area_list.add(i, Config.selectGender[i]);
        }

        loopView.setTextSize(22);//must be called before setDateList
        loopView.setDataList(area_list);

        cityDialog_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                genderDialog.dismiss();
            }
        });

        cityDialog_accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                delivery_Tipo.setText("" + area_list.get(loopView.getSelectedItem()));
                genderDialog.dismiss();
            }
        });

        genderDialog.show();
    }

    private void showCityDialog() {
        final Dialog cityDialog = new Dialog(new ContextThemeWrapper(context, R.style.DialogSlideAnim));
        cityDialog.getWindow().setGravity(Gravity.BOTTOM);
        cityDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        cityDialog.setContentView(R.layout.citydialog_xml_ciudad);
        final LoopView loopView = (LoopView) cityDialog.findViewById(R.id.loop_view);
        final TextView cityDialog_cancel = (TextView) cityDialog.findViewById(R.id.cityDialog_cancel);
        final TextView cityDialog_accept = (TextView) cityDialog.findViewById(R.id.cityDialog_accept);

        loopView.setInitPosition(0);
        loopView.setCanLoop(false);

        ArrayList<String> area_list = new ArrayList<>();
        // String[] displayValues = new String[alFilterAreaList.size()];
        for (int i = 0; i < alCityList.size(); i++) {
            area_list.add(i, alCityList.get(i).getNombre());
            // displayValues[i] = alFilterAreaList.get(i).getNombre();
        }

        loopView.setTextSize(22);//must be called before setDateList
        loopView.setDataList(area_list);

        cityDialog_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (cityDialog != null) {
                    cityDialog.dismiss();
                }
            }
        });
        cityDialog_accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Log.e("value",""+cityDialog_cityPicker.getValue());
                // Log.e("displayvalue",""+cityDialog_cityPicker.getDisplayedValues()[cityDialog_cityPicker.getValue()]);
                if (cityDialog != null) {
                    cityDialog.dismiss();

                    IdCiudad = alCityList.get(loopView.getSelectedItem()).getIdCiudad();
                    // IdSector = loopView.getSelectedItem().getIdSector();
                    delivery_ciudad.setText("" + alCityList.get(loopView.getSelectedItem()).getNombre());
                }
            }
        });
        cityDialog.show();
    }

    private void openAreaDialog() {

        if (alAreaList != null) {
            if (alAreaList.size() > 0) {
                final Dialog areaDialog = new Dialog(new ContextThemeWrapper(context, R.style.DialogSlideAnim));
                areaDialog.getWindow().setGravity(Gravity.BOTTOM);
                areaDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                areaDialog.setContentView(R.layout.citydialog_xml_ciudad);

                final TextView cityDialog_cancel = (TextView) areaDialog.findViewById(R.id.cityDialog_cancel);
                final TextView cityDialog_accept = (TextView) areaDialog.findViewById(R.id.cityDialog_accept);
                final LoopView loopView = (LoopView) areaDialog.findViewById(R.id.loop_view);

                loopView.setInitPosition(0);
                loopView.setCanLoop(false);

                ArrayList<String> area_list = new ArrayList<>();
                for (int i = 0; i < alAreaList.size(); i++) {
                    area_list.add(i, alAreaList.get(i).getNombre());
                }

                loopView.setTextSize(22);//must be called before setDateList
                loopView.setDataList(area_list);

                cityDialog_cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (areaDialog != null) {
                            areaDialog.dismiss();
                        }
                    }
                });

                cityDialog_accept.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (areaDialog != null) {
                            IdSector = alAreaList.get(loopView.getSelectedItem()).getIdSector();
                            // IdSector = loopView.getSelectedItem().getIdSector();
                            delivery_sector.setText("" + alAreaList.get(loopView.getSelectedItem()).getNombre());

                           // delivery_sector.setText(areaDialog_cityPicker.getDisplayedValues()[areaDialog_cityPicker.getValue()]);
                            areaDialog.dismiss();
                        }
                    }
                });

                areaDialog.show();
            }
        }
    }

    private void setMessageErrorDialog(String message) {
        messageDialog = new MessageDialog(context,
                android.R.style.Theme_Holo_Light_Dialog,
                message,
                new MessageDialog.okOnClickListener() {
                    @Override
                    public void onButtonClick() {
                        messageDialog.dismiss();
                    }
                });

        messageDialog.setCancelable(false);
        messageDialog.setCanceledOnTouchOutside(false);
        messageDialog.show();
    }


    private void getCityList() {
        jsonParserCity();
    }


    private void jsonParserCity() {
        delivery_ciudad.setClickable(false);
        delivery_ciudad.setEnabled(false);

        String url = API.localList;
        showProgressBar();
        GlobalValues.getMethodManagerObj(context).makeStringReq(url, apiCityRequest, new MethodListener() {
            @Override
            public void onError() {
                hideProgressbar();
                delivery_ciudad.setClickable(true);
                delivery_ciudad.setEnabled(true);
            }

            @Override
            public void onError(String response) {
                hideProgressbar();
                delivery_ciudad.setClickable(true);
                delivery_ciudad.setEnabled(true);
            }

            @Override
            public void onSuccess(String response) {
                delivery_ciudad.setClickable(true);
                delivery_ciudad.setEnabled(true);

                hideProgressbar();
                try {
                    JSONObject mObj = new JSONObject(response);
                    JSONArray mCity = mObj.optJSONArray("city");
                    if (mCity != null) {
                        getParseLocalList(mCity);
                    }
                } catch (JSONException e) {
                    Log.e(TAG, "" + e.toString());
                }
            }
        });

    }

    private void getParseLocalList(JSONArray mCity) {
        alCityList = new ArrayList<>();
        alAreaList = new ArrayList<>();

        for (int i = 0; i < mCity.length(); i++) {
            JSONObject object = mCity.optJSONObject(i);

            CityModel cityModel = new CityModel();
            cityModel.setIdCiudad(object.optInt("IdCiudad"));
            cityModel.setNombre(object.optString("Nombre"));
            cityModel.setSiglas(object.optString("Siglas"));
            cityModel.setEstado(object.optString("Estado"));

            JSONArray msectors = object.optJSONArray("sectors");
            getSectors(msectors);

            alCityList.add(cityModel);
        }

    }

    private void getSectors(JSONArray msectors) {
        for (int i = 0; i < msectors.length(); i++) {
            JSONObject object = msectors.optJSONObject(i);
            AreaModel areaModel = new AreaModel();
            areaModel.setIdCiudad(object.optInt("IdCiudad"));
            areaModel.setIdSector(object.optInt("IdSector"));
            areaModel.setNombre(object.optString("Nombre"));
            areaModel.setSiglas(object.optString("Siglas"));
            areaModel.setEstado(object.optString("Estado"));
            alAreaList.add(areaModel);
        }
    }

    private void showProgressBar() {
        resturent_progressBar.setVisibility(View.VISIBLE);
        animation = AnimationUtils.loadAnimation(context, R.anim.progress_anim);
        animation.setDuration(1000);
        resturent_progressBar.startAnimation(animation);
    }

    private void hideProgressbar() {
        if (animation != null) {
            resturent_progressBar.clearAnimation();
            resturent_progressBar.setVisibility(View.GONE);
        }
    }

    private Handler ciudadHandler;

    private void ciudadHandler() {
        ciudadHandler = new Handler(Looper.getMainLooper());
        ciudadHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (alCityList != null) {
                    if (alCityList.size() > 0) {
                        showCityDialog();
                    }
                } else {
                    getCityList();
                }
            }
        }, 500);
    }

    private void doSubmitDeliveryAddress() {
        if (delivery_descripcion.getText().toString().equals("") &&
                delivery_ciudad.getText().toString().equals("") &&
                delivery_sector.getText().toString().equals("") &&
                delivery_sector.getText().toString().equals("") &&
                delivery_Tipo.getText().toString().equals("") &&
                delivery_address1.getText().toString().equals("") &&
                delivery_address2.getText().toString().equals("") &&
                delivery_convencional.getText().toString().equals("") &&
                delivery_celuar.getText().toString().equals("") &&
                delivery_office.getText().toString().equals("")) {

            String message = getResources().getString(R.string.empty_Debe_ingresar_descripcion)
                    + "\n" + getResources().getString(R.string.empty_Debe_escoger_ciudad)
                    + "\n" + getResources().getString(R.string.empty_Debe_escoger_sector)
                    + "\n" + getResources().getString(R.string.empty_Debe_seleccionar_tipo)
                    + "\n" + getResources().getString(R.string.empty_Debe_ingresar_direccion)
                    + "\n" + getResources().getString(R.string.empty_Debe_ingresar_segunda_linea_de_direccion)
                    + "\n" + getResources().getString(R.string.empty_Debe_ingresar_segunda_telefono)
                    + "\n" + getResources().getString(R.string.empty_Debe_ingresar_celular)
                    + "\n" + getResources().getString(R.string.empty_Debe_ingresar_referencia);

            setMessageErrorDialog(message);

        } else if (delivery_descripcion.getText().toString().equals("")) {
            String message = getResources().getString(R.string.empty_Debe_ingresar_descripcion);
            setMessageErrorDialog(message);
        } else if (delivery_ciudad.getText().toString().equals("")) {
            String message = getResources().getString(R.string.empty_Debe_escoger_ciudad);
            setMessageErrorDialog(message);
        } else if (delivery_sector.getText().toString().equals("")) {
            String message = getResources().getString(R.string.empty_Debe_escoger_sector);
            setMessageErrorDialog(message);
        } else if (delivery_Tipo.getText().toString().equals("")) {
            String message = getResources().getString(R.string.empty_Debe_seleccionar_tipo);
            setMessageErrorDialog(message);
        } else if (delivery_address1.getText().toString().equals("")) {
            String message = getResources().getString(R.string.empty_Debe_ingresar_direccion);
            setMessageErrorDialog(message);
        } else if (delivery_address2.getText().toString().equals("")) {
            String message = getResources().getString(R.string.empty_Debe_ingresar_segunda_linea_de_direccion);
            setMessageErrorDialog(message);
        } else if (delivery_convencional.getText().toString().equals("")) {
            String message = getResources().getString(R.string.empty_Debe_ingresar_segunda_telefono);
            setMessageErrorDialog(message);
        } else if (delivery_celuar.getText().toString().equals("")) {
            String message = getResources().getString(R.string.empty_Debe_ingresar_celular);
            setMessageErrorDialog(message);
        } else if (delivery_office.getText().toString().equals("")) {
            String message = getResources().getString(R.string.empty_Debe_ingresar_referencia);
            setMessageErrorDialog(message);
        } else {
            String descripcion = delivery_descripcion.getText().toString().replace(" ", "%20");
            //String ciudad = delivery_ciudad.getText().toString();
            //String sector = delivery_sector.getText().toString();
            String tipo = delivery_Tipo.getText().toString().replace(" ", "%20");
            String address1 = delivery_address1.getText().toString().replace(" ", "%20");
            String address2 = delivery_address2.getText().toString().replace(" ", "%20");
            String convencional = delivery_convencional.getText().toString().replace(" ", "%20");
            String celuar = delivery_celuar.getText().toString().replace(" ", "%20");
            String referencia = delivery_office.getText().toString().replace(" ", "%20");

            String url = API.RegistraClientDireccion
                    + "pIdCliente=" + SessionManager.getUserId(context)
                    + "&pIdCiudad=" + IdCiudad
                    + "&pIdSector=" + IdSector
                    + "&pTipo=" + tipo
                    + "&pDescripcion=" + descripcion
                    + "&pDireccion2=" + address2
                    + "&pDireccion=" + address1
                    + "&pTelefono=" + convencional
                    + "&pCelular=" + celuar
                    + "&pReferencia=" + referencia
                    + "&callback=" + "";


            showProgressBar();
            GlobalValues.getMethodManagerObj(context).makeStringReq(url, apiClientDireccion, new MethodListener() {
                @Override
                public void onError() {
                    hideProgressbar();
                }

                @Override
                public void onError(String response) {
                    hideProgressbar();
                }

                @Override
                public void onSuccess(String response) {
                    response = response.substring(1, response.length() - 1);
                    try {
                        JSONObject mObj = new JSONObject(response);
                        JSONArray dataArray = mObj.optJSONArray("data");
                        if (dataArray != null) {
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject object = dataArray.optJSONObject(i);
                                if (object.has("respuesta")) {
                                    if (object.optString("respuesta").equals("OK")) {
                                        updateDeliveryList();
                                    } else {
                                        setMessageErrorDialog(object.optString("Error"));
                                    }
                                }
                            }
                        }
                    } catch (JSONException e) {
                        Log.e(TAG, "" + e.toString());
                        hideProgressbar();
                    }
                }
            });
        }
    }

    private void updateDeliveryList() {
        String url = API.deliveryAddress + "pIdCliente=" + SessionManager.getUserId(getApplicationContext()) + "&callback=";
        GlobalValues.getMethodManagerObj(context).makeStringReqWithoutCache(url, "apiRequest", new MethodListener() {
            @Override
            public void onError() {
            }

            @Override
            public void onError(String response) {

            }

            @Override
            public void onSuccess(String response) {
                hideProgressbar();
                try {
                    response = response.substring(1, response.length() - 1);
                    JSONObject mobj = new JSONObject(response);
                    JSONArray mData = mobj.optJSONArray("data");
                    if (mData != null) {
                        Config.alDeliveryAddress.clear();

                        for (int i = 0; i < mData.length(); i++) {
                            JSONObject object = mData.optJSONObject(i);
                            AddressListBean addressListBean = new AddressListBean();
                            addressListBean.setIdDirecciones(object.optInt("IdDirecciones"));
                            addressListBean.setIdCiudad(object.optInt("IdCiudad"));
                            addressListBean.setIdCliente(object.optInt("IdCliente"));
                            addressListBean.setIdSector(object.optInt("IdSector"));
                            addressListBean.setDescripcion(object.optString("Descripcion"));
                            addressListBean.setTipo(object.optString("Tipo"));
                            addressListBean.setDireccion(object.optString("Direccion"));
                            addressListBean.setDireccion2(object.optString("Direccion2"));
                            addressListBean.setTelefono(object.optString("Telefono"));
                            addressListBean.setCelular(object.optString("Celular"));
                            addressListBean.setReferencia(object.optString("Referencia"));
                            addressListBean.setNombreCiudad(object.optString("NombreCiudad"));
                            addressListBean.setNombreSector(object.optString("NombreSector"));
                            addressListBean.setNombrePerfil(object.optString("NombrePerfil"));

                            Config.alDeliveryAddress.add(addressListBean);
                        }

                        AddressListBean addressListBean = new AddressListBean();
                        addressListBean.setIdDirecciones(0);
                        addressListBean.setIdCiudad(0);
                        addressListBean.setIdCliente(0);
                        addressListBean.setIdSector(0);
                        addressListBean.setDescripcion("RECOGE EN LOCAL");
                        addressListBean.setTipo("0");
                        addressListBean.setDireccion("0");
                        addressListBean.setDireccion2("0");
                        addressListBean.setTelefono("0");
                        addressListBean.setCelular("0");
                        addressListBean.setReferencia("0");
                        addressListBean.setNombreCiudad("0");
                        addressListBean.setNombreSector("0");
                        addressListBean.setNombrePerfil("0");

                        Config.alDeliveryAddress.add(addressListBean);


                    }

                    /**
                     * Finish the task
                     */
                    Intent i = new Intent();
                    setResult(RESULT_OK, i);
                    finish();

                } catch (JSONException e) {
                    Log.e(TAG, "" + e.toString());
                } catch (Exception e) {
                    Log.e(TAG, "" + e.toString());
                }
            }
        });
    }
}
