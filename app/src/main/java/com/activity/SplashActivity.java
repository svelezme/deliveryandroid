package com.activity;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import com.app.grupodeliveryec.deliveryEC.R;
import com.manager.AddToCartManager;
import com.utility.AppController;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import android.Manifest;

public class SplashActivity extends AppCompatActivity {

    String TAG = getClass().getName();
    Intent I;
    Context mContext;
    Handler splashHandler;
    private int requestCode = 100;
    private long dealyMillis = 1000;
    AddToCartManager addToCartManager;
    boolean isPermissionGranted = false;
    int PERMISSION_ALL = 101;
    String[] PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE,};



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        splashHandler = new Handler();
        checkPermissions();

        AppController.getInstance().setErrorMessage();
        setContentView(R.layout.splash_xml);
        mContext = this;
        //printHashKey(mContext);

        //SessionManager.clearLoginUser(mContext);

        addToCartManager = new AddToCartManager(mContext);
        addToCartManager.dropCart();
       // printHashKey();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    private void loadSplashScreen() {
        try {
            I = new Intent(mContext, DashBoardActivity.class);
            startActivityForResult(I, requestCode);
            finish();
        } catch (ActivityNotFoundException e) {
            Log.e(TAG, "" + e.toString());
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (splashHandler != null) {
            splashHandler.removeCallbacks(runnable);
            finish();
        }
    }
    public void printHashKey() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.app.grupodeliveryec.deliveryEC",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");//QgC/Uarxm0tDGhcSVHmGf5uVZZg=
                md.update(signature.toByteArray());
                String key = Base64.encodeToString(md.digest(), Base64.DEFAULT);
                Log.e("key" , ""+key);
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {
        }
    }

    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            loadSplashScreen();
        }
    };

    public  void printHashKey(Context pContext) {
        // Add code to print out the key hash
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.app.grupodeliveryec.deliveryEC",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String keyHash = Base64.encodeToString(md.digest(), Base64.DEFAULT);
                Log.d("KeyHash:", ""+keyHash);
            }
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(TAG,""+e.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.e(TAG,""+e.toString());
        }
    }



    private  void checkPermissions() {
        if (Build.VERSION.SDK_INT >= 23) {
            int result;
            List<String> listPermissionsNeeded = new ArrayList<>();
            for (String p : PERMISSIONS) {
                result = ContextCompat.checkSelfPermission(this, p);
                if (result != PackageManager.PERMISSION_GRANTED) {
                    listPermissionsNeeded.add(p);
                }
            }
            if (!listPermissionsNeeded.isEmpty()) {
                ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), PERMISSION_ALL);

            }else{
                Log.i("permission-","Permission is empty");
                splashHandler.postDelayed(runnable, dealyMillis);
            }
        }else{
            /**
             * SDK is less then 23 load the
             */
            // checkUserValidation();
            splashHandler.postDelayed(runnable, dealyMillis);
        }

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        isPermissionGranted = true;
        switch(requestCode) {
            case 101:
                if (grantResults != null && grantResults.length == PERMISSIONS.length) {
                    for (int i = 0; i < grantResults.length; i++) {
                        if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                            isPermissionGranted = false;
                        }else{
                            Log.i("PERMISSION_GRANTED",PERMISSIONS[i]);
                        }
                    }
                }
                break;
        }
        if(isPermissionGranted){
            /**
             * Permission is granted
             */
            // checkUserValidation();
            splashHandler.postDelayed(runnable, dealyMillis);

        }else{
            /**
             * Other wise go the permission
             */
            //checkUserValidation();
            splashHandler.postDelayed(runnable, dealyMillis);
        }

    }
}