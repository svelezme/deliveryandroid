package com.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.app.grupodeliveryec.deliveryEC.R;
import com.bean.BillingBean;
import com.com.rest.API;
import com.com.rest.GlobalValues;
import com.com.rest.MethodListener;
import com.utility.AppController;
import com.utility.Config;
import com.utility.FragmentIDs;
import com.utility.KeyboadSetting;
import com.utility.MessageDialog;
import com.utility.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by administrator on 27/3/17.
 */

public class AddBillingAddressActivity extends AppCompatActivity{

    String TAG = getClass().getName();

    ImageView resturent_progressBar;
    EditText billing_descripcion;
    EditText billing_socialAddress;
    EditText billing_cedula;
    EditText billing_direccion;
    EditText billing_telefono;
    LinearLayout billing_termsconditionsLayout;
    LinearLayout billing_update;
    LinearLayout billing_cancel;

    ImageView tv_back;

    Animation animation;
    Context context;
    MessageDialog messageDialog;

    String apiRequest = "BillingAddress";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppController.getInstance().setErrorMessage();
        setContentView(R.layout.billing_xml_activity);

        context = this;


        resturent_progressBar = (ImageView)findViewById(R.id.resturent_progressBar);
        billing_descripcion = (EditText)findViewById(R.id.billing_descripcion);
        billing_socialAddress = (EditText)findViewById(R.id.billing_socialAddress);
        billing_cedula = (EditText)findViewById(R.id.billing_cedula);
        billing_direccion = (EditText)findViewById(R.id.billing_direccion);
        billing_telefono = (EditText)findViewById(R.id.billing_telefono);
        billing_termsconditionsLayout = (LinearLayout)findViewById(R.id.billing_termsconditionsLayout);
        billing_update = (LinearLayout)findViewById(R.id.billing_update);
        billing_cancel = (LinearLayout)findViewById(R.id.billing_cancel);
        tv_back = (ImageView)findViewById(R.id.tv_back);


        billing_termsconditionsLayout.setVisibility(View.GONE);
        billing_update.setVisibility(View.VISIBLE);
        billing_cancel.setVisibility(View.VISIBLE);

        billing_update.setOnClickListener(addToSubmit);
        billing_cancel.setOnClickListener(addToCancel);
        tv_back.setOnClickListener(addBackListener);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                KeyboadSetting.showKeyboard(context,billing_descripcion);
            }
        },200);

    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.animation_enter, R.anim.animation_leave);
    }

    View.OnClickListener addBackListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            View v = ((Activity) context).getCurrentFocus();
            KeyboadSetting.hideKeyboadUsingView(v, context);

            finish();
            overridePendingTransition(R.anim.animation_enter, R.anim.animation_leave);
        }
    };

    View.OnClickListener addToSubmit = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            View v = ((Activity) context).getCurrentFocus();
            //View v = getActivity().getCurrentFocus();
            KeyboadSetting.hideKeyboadUsingView(v,context);
            //KeyboadSetting.hideKeyboard(context,register_surname);

            updateList();

        }
    };

    View.OnClickListener addToCancel = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            View v = ((Activity) context).getCurrentFocus();
            KeyboadSetting.hideKeyboadUsingView(v, context);

            finish();
            overridePendingTransition(R.anim.animation_enter, R.anim.animation_leave);
        }
    };

    private void showProgressBar(){
        resturent_progressBar.setVisibility(View.VISIBLE);
        animation = AnimationUtils.loadAnimation(context, R.anim.progress_anim);
        animation.setDuration(1000);
        resturent_progressBar.startAnimation(animation);
    }
    private void hideProgressbar(){
        if(animation!=null){
            resturent_progressBar.clearAnimation();
            resturent_progressBar.setVisibility(View.GONE);
        }
    }

    private void setMessageErrorDialog(String message){
        messageDialog = new MessageDialog(context,
                android.R.style.Theme_Holo_Light_Dialog,
                message,
                new MessageDialog.okOnClickListener() {
                    @Override
                    public void onButtonClick() {
                        messageDialog.dismiss();
                    }
                });

        messageDialog.setCancelable(false);
        messageDialog.setCanceledOnTouchOutside(false);
        messageDialog.show();
    }

    private void updateList(){
        if(billing_descripcion.getText().toString().equals("")
                && billing_socialAddress.getText().toString().equals("")
                && billing_cedula.getText().toString().equals("")
                && billing_direccion.getText().toString().equals("")
                && billing_telefono.getText().toString().equals("")){

            String message = getResources().getString(R.string.empty_Debe_ingresar_descripcion)
                    +"\n"+getResources().getString(R.string.empty_Debe_ingresar_razon_social)
                    +"\n"+getResources().getString(R.string.enter_Debe_ingresar_identificacion)
                    +"\n"+getResources().getString(R.string.empty_Debe_ingresar_direccion)
                    +"\n"+getResources().getString(R.string.empty_Debe_ingresar_telefono);
            setMessageErrorDialog(message);

        }else if(billing_descripcion.getText().toString().equals("")){
            String message = getResources().getString(R.string.empty_Debe_ingresar_descripcion);
            setMessageErrorDialog(message);
        }else if(billing_socialAddress.getText().toString().equals("")){
            String message = getResources().getString(R.string.empty_Debe_ingresar_razon_social);
            setMessageErrorDialog(message);
        }else if(billing_cedula.getText().toString().equals("")){
            String message = getResources().getString(R.string.enter_Debe_ingresar_identificacion);
            setMessageErrorDialog(message);
        }else if(billing_direccion.getText().toString().equals("")){
            String message = getResources().getString(R.string.empty_Debe_ingresar_direccion);
            setMessageErrorDialog(message);
        }else if(billing_telefono.getText().toString().equals("")){
            String message = getResources().getString(R.string.empty_Debe_ingresar_telefono);
            setMessageErrorDialog(message);
        }else{
            String pDescripcion = billing_descripcion.getText().toString().replace(" ","%20");
            String pRazonSocial = billing_socialAddress.getText().toString().replace(" ","%20");
            String pIdentificacion = billing_cedula.getText().toString().replace(" ","%20");
            String pDireccion = billing_direccion.getText().toString().replace(" ","%20");
            String pTelefono = billing_telefono.getText().toString().replace(" ","%20");

            String url  = API.billingAddress+"pIdCliente="+ SessionManager.getUserId(context)
                    +"&pRazonSocial="+pRazonSocial
                    +"&pIdentificacion="+pIdentificacion
                    +"&pDescripcion="+pDescripcion
                    +"&pDireccion="+pDireccion
                    +"&pTelefono="+pTelefono
                    +"&callback=";

            showProgressBar();

            GlobalValues.getMethodManagerObj(context).makeStringReq(url, apiRequest, new MethodListener() {
                @Override
                public void onError() {
                    hideProgressbar();
                }

                @Override
                public void onError(String response) {
                    hideProgressbar();
                }

                @Override
                public void onSuccess(String response) {
                    try{
                        response = response.substring(1,response.length()-1);
                        JSONObject mObj = new JSONObject(response);
                        JSONArray mArray = mObj.optJSONArray("data");
                        if(mArray!=null){
                            for(int i=0;i<mArray.length();i++){
                                JSONObject object = mArray.optJSONObject(i);
                                if(object.has("respuesta")){
                                    if(object.optString("respuesta").equals("OK")){
                                        updateBillingList();
                                    }else{
                                        setMessageErrorDialog(object.optString("Error"));
                                    }
                                }
                            }
                        }
                    }catch (JSONException e){
                        hideProgressbar();
                        Log.e(TAG,""+e.toString());
                    }
                }
            });
        }
    }

    private void updateBillingList(){
        String url = API.billingAddressList+"pIdCliente="+SessionManager.getUserId(context)+"&callback=";

        GlobalValues.getMethodManagerObj(context).makeStringReqWithoutCache(url, "apiBillingList", new MethodListener() {
            @Override
            public void onError() {
            }

            @Override
            public void onError(String response) {
            }

            @Override
            public void onSuccess(String response) {
                try{
                    response = response.substring(1,response.length()-1);
                    JSONObject object = new JSONObject(response);
                    JSONArray mArray = object.optJSONArray("data");
                    if(mArray!=null){

                        Config.alBilling.clear();

                        for(int i=0;i<mArray.length();i++){
                            JSONObject mObj = mArray.optJSONObject(i);
                            BillingBean billingBean = new BillingBean();
                            billingBean.setIdDireccionFactura(mObj.optInt("IdDireccionFactura"));
                            billingBean.setIdCliente(mObj.optInt("IdCliente"));
                            billingBean.setRazonSocial(mObj.optString("RazonSocial"));
                            billingBean.setIdentificacion(mObj.optString("Identificacion"));
                            billingBean.setDescripcion(mObj.optString("Descripcion"));
                            billingBean.setDireccion(mObj.optString("Direccion"));
                            billingBean.setTelefono(mObj.optString("Telefono"));
                            billingBean.setNombrePerfil(mObj.optString("NombrePerfil"));

                            Config.alBilling.add(billingBean);
                        }


                        BillingBean billingBean = new BillingBean();
                        billingBean.setIdDireccionFactura(0);
                        billingBean.setIdCliente(0);
                        billingBean.setRazonSocial("0");
                        billingBean.setIdentificacion("0");
                        billingBean.setDescripcion("RECOGE EN LOCAL");
                        billingBean.setDireccion("0");
                        billingBean.setTelefono("0");
                        billingBean.setNombrePerfil("0");
                        Config.alBilling.add(billingBean);
                    }

                    Intent i = new Intent();
                    setResult(RESULT_OK, i);
                    finish();
                }catch (JSONException e){
                    Log.e(TAG,""+e.toString());
                }catch (Exception e){
                    Log.e(TAG,""+e.toString());
                }
            }
        });
    }
}
