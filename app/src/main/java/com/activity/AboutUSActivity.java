package com.activity;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;
import com.app.grupodeliveryec.deliveryEC.R;
import com.utility.AppController;

/**
 * Created by administrator on 6/2/17.
 */

public class AboutUSActivity extends AppCompatActivity{

    WebView aboutus_webview;
    ImageView aboutus_toolbar_backButton,resturent_progressBar;
    TextView aboutus_toolbar_heading;
    Context mContext;

    String url;
    Animation animation;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.aboutus_list);
        AppController.getInstance().setErrorMessage();
        mContext = this;
        aboutus_webview = (WebView)findViewById(R.id.aboutus_webview);
        resturent_progressBar = (ImageView)findViewById(R.id.resturent_progressBar);
        aboutus_toolbar_backButton = (ImageView)findViewById(R.id.aboutus_toolbar_backButton);
        aboutus_toolbar_heading = (TextView)findViewById(R.id.aboutus_toolbar_heading);

      /*  aboutus_toolbar_heading.setText(getIntent().
                getStringExtra("headingTitle"));*/
        url = getIntent().getStringExtra("url");

        aboutus_webview.setWebViewClient(new MyClient());
        aboutus_webview.getSettings().setJavaScriptEnabled(true);
        aboutus_webview.loadUrl(url);

        aboutus_toolbar_backButton.setOnClickListener(addToBackButton);
    }

    private void showProgressBar() {
        resturent_progressBar.setVisibility(View.VISIBLE);
        animation = AnimationUtils.loadAnimation(mContext, R.anim.progress_anim);
        animation.setDuration(1000);
        resturent_progressBar.startAnimation(animation);
    }

    private void hideProgressbar() {
        if (animation != null) {
            resturent_progressBar.clearAnimation();
            resturent_progressBar.setVisibility(View.GONE);
        }
    }

    public class MyClient extends WebViewClient{
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            showProgressBar();
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            super.shouldOverrideUrlLoading(view, url);
            hideProgressbar();
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            hideProgressbar();
        }
    }

/*    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK) && aboutus_webview.canGoBack()) {
            aboutus_webview.goBack();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }*/

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    View.OnClickListener addToBackButton = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            finish();
        }
    };
}
