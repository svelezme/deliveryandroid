package com.activity;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.adapter.MenuAdapter;
import com.app.grupodeliveryec.deliveryEC.R;
import com.facebook.CallbackManager;
import com.fragment.AboutUsFragment;
import com.fragment.BillingAddressFragment;
import com.fragment.BillingAddressListFragment;
import com.fragment.ChangePasswordFragment;
import com.fragment.CongratulationsFragment;
import com.fragment.DeliveryAddressFragment;
import com.fragment.DeliveryAddressListFragment;
import com.fragment.DispositivosListFragment;
import com.fragment.MyOrderDetailsFragment;
import com.fragment.MyOrdersFragment;
import com.fragment.NewOrderFragment;
import com.fragment.OrderFragment;
import com.fragment.PerfilFragment;
import com.fragment.PerfilListFragment;
import com.fragment.ProductOffersFragment;
import com.fragment.PromocionesFragment;
import com.fragment.RegistraClienteFragment;
import com.fragment.ResturentInfoFragment;
import com.fragment.ResturentMenuFragment;
import com.fragment.ResturentMenuItemFragment;
import com.fragment.ResturentSearchFragment;
import com.fragment.ResturentSearchResultFragment;
import com.fragment.UserProfileFragment;
import com.manager.AddToCartManager;
import com.squareup.picasso.Picasso;
import com.utility.AppController;
import com.utility.ClearCart;
import com.utility.Config;
import com.utility.FragmentIDs;
import com.utility.SessionManager;
import com.view.BebaseNeueTextView;

import static com.utility.FragmentIDs.OrderFragment_Tag;

/**
 * Created by administrator on 30/9/16.
 */
public class DashBoardActivity extends AppCompatActivity {

    public static TextView nav_cerrar, nav_info;
    public static ImageView tv_back, nav_search, nav_logout, nav_addDeliveryAddress;
    public static ImageView nav_userPerfilIcon;
    public static BebaseNeueTextView tv_title;
    public static LinearLayout nav_resturents, nav_promociones;
    public static LinearLayout nav_perfil, nav_nosotros;
    public static LinearLayout nav_HorizentalBar;
    public static ClearCart clearCart;
    /**
     * Show the bottom Add to card & Menu button
     */
    public static LinearLayout bottom_linearLayout, menuButtonLayout, cartButtonLayout;
    public static TextView menu_totalqty;
    public static Context mContext;
    public static DashBoardActivity dashBoardActivity;
    static FragmentTransaction ft;
    public static FragmentManager fm;
    public DrawerLayout drawer_layout;
    public ImageView iv_menu;
    ListView dash_board_list_layout;
    MenuAdapter menuAdapter;
    NavigationView nav_view;
    public static CallbackManager callbackManager;
    public static int order_back_status = 0;
    public static int login_back_status = 0; //If 0 for reset and 1 for set

    public static void setQuntity(int quntity) {
        if (menu_totalqty != null) {
            menu_totalqty.setText("" + quntity);
        }
    }

    public static void hideQuntity() {
        if (menu_totalqty != null) {
            menu_totalqty.setVisibility(View.GONE);
        }
    }

    public static void showQuntity() {
        menu_totalqty.setVisibility(View.VISIBLE);
    }

    public static void hideMenuPanel() {
        if (bottom_linearLayout != null) {
            bottom_linearLayout.setVisibility(View.GONE);
        }
    }

    public static void showMenuPanel() {
        if (bottom_linearLayout != null) {
            bottom_linearLayout.setVisibility(View.VISIBLE);
        }
    }

    public static void updateTitle(String title) {
        if (tv_title != null) {
            tv_title.setText(title);
        }
    }

    public static void hideSearchButton() {
        if (nav_search != null) {
            nav_search.setVisibility(View.GONE);
        }
    }

    public static void showUserPerfilIocn() {
        if (nav_userPerfilIcon != null) {
            nav_userPerfilIcon.setVisibility(View.VISIBLE);
        }
    }

    public static void hideUserPerfilIcon() {
        if (nav_userPerfilIcon != null) {
            nav_userPerfilIcon.setVisibility(View.GONE);
        }
    }

    public static void setBackgroundColor(int restaurantes, int promociones, int perfil, int nosotros) {
        nav_resturents.setBackgroundColor(restaurantes);
        nav_promociones.setBackgroundColor(promociones);
        nav_perfil.setBackgroundColor(perfil);
        nav_nosotros.setBackgroundColor(nosotros);
    }

  /*  public static void showSearchButton() {
        if (nav_search != null) {
            nav_search.setVisibility(View.VISIBLE);
        }
    }*/

    public static void hideBackButton() {
        if (tv_back != null) {
            tv_back.setVisibility(View.GONE);
        }
    }

    public static void showBackButton() {
        if (tv_back != null) {
            tv_back.setVisibility(View.VISIBLE);
        }
    }


    public static void showCerrar() {
        if (nav_cerrar != null) {
            nav_cerrar.setVisibility(View.GONE);
            //nav_cerrar.setVisibility(View.VISIBLE);
        }
    }

    public static void hideCerrar() {
        if (nav_cerrar != null) {
            nav_cerrar.setVisibility(View.GONE);
        }
    }

/*    public static void showInfo() {
        if (nav_info != null) {
            nav_info.setVisibility(View.VISIBLE);
        }
    }*/

    public static void hideInfo() {
        if (nav_info != null) {
            nav_info.setVisibility(View.GONE);
        }
    }

    public static void showHorizentalBar() {
        if (nav_HorizentalBar != null) {
            nav_HorizentalBar.setVisibility(View.GONE);
            //nav_HorizentalBar.setVisibility(View.VISIBLE);
        }
    }

    public static void hideHorizentalBar() {
        if (nav_HorizentalBar != null) {
            nav_HorizentalBar.setVisibility(View.GONE);
        }
    }

    public static void showLogout() {
        if (nav_logout != null) {
            nav_logout.setVisibility(View.VISIBLE);
        }
    }

    public static void hideLogout() {
        if (nav_logout != null) {
            nav_logout.setVisibility(View.GONE);
        }
    }

    public static void showAddDeliveryAddress() {
        if (nav_addDeliveryAddress != null) {
            nav_addDeliveryAddress.setVisibility(View.VISIBLE);
        }
    }

    public static void hideAddDeliveryAddress() {
        if (nav_addDeliveryAddress != null) {
            nav_addDeliveryAddress.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (callbackManager != null) {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    public static void popBackStack() {
        if (fm != null) {
            //fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);


            ResturentMenuFragment resturentMenuFragment = (ResturentMenuFragment)
                    fm.findFragmentByTag(FragmentIDs.ResturentMenuFragment_Tag);
            if (resturentMenuFragment != null && resturentMenuFragment.isVisible()) {

                final AddToCartManager addToCartManager = new AddToCartManager(mContext);

                int qty = addToCartManager.getTotalQuntity();
                if (qty > 0) {
                    clearCart = new ClearCart(mContext, android.R.style.Theme_Holo_Light_Dialog, new ClearCart.cartOnClickListener() {
                        @Override
                        public void yesOnButtonClick() {
                            clearCart.dismiss();
                            addToCartManager.dropCart();
                            fm.popBackStack();
                        }

                        @Override
                        public void noOnButtonClick() {
                            clearCart.dismiss();
                        }
                    });

                    clearCart.setCanceledOnTouchOutside(false);
                    clearCart.setCancelable(false);
                    clearCart.show();
                } else {
                    fm.popBackStack();
                }

            } else {
                fm.popBackStack();
            }
        } else {
            fm.popBackStack();
        }
    }

    public static void onBackViaKey() {
        if (fm != null) {
            fm.popBackStack();
        }
    }

    public static void removeOrderFragment(String tag) {
        if (fm != null) {
            OrderFragment orderFragment = (OrderFragment) fm.findFragmentByTag(tag);
            ft = fm.beginTransaction();
            ft.remove(orderFragment);
            ft.commit();
            fm.popBackStack();
        }
    }

    public static void getOrderFrg(PerfilFragment.RefOfOrderFrg refOfOrderFrg) {
        if (fm != null) {
            OrderFragment orderFragment = (OrderFragment) fm.findFragmentByTag(OrderFragment_Tag);
            refOfOrderFrg.getOrderFrg(orderFragment);
        }
    }

    public static void removeAllFragment() {
        if (fm != null) {
            for (Fragment fragment : fm.getFragments()) {
                if (fragment instanceof ResturentSearchResultFragment) { //if (fragment instanceof ResturentSearchFragment)
                    continue;
                } else {
                    if (fm != null) {
                        if (fragment.getTag() != null) {
                            fm.popBackStack(fragment.getTag(), FragmentManager.POP_BACK_STACK_INCLUSIVE);
                        }
                    }
                }
            }
        }
    }

    public static void removePopupBackstack(String tag) {
        if (fm != null) {
            fm.popBackStack(tag, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }

    public static void displayFragment(int fragmentId, Bundle bundle) {
        Fragment fragment = null;
        switch (fragmentId) {
            case 0:
                fragment = ResturentSearchFragment.getInstance(mContext, fm);
                ft = fm.beginTransaction();
                ft.replace(R.id.content_frame, fragment, FragmentIDs.ResturentSearchFragment_Tag);
                //ft.addToBackStack(FragmentIDs.ResturentSearchFragment_Tag);
                ft.commit();
                break;
            case 1:
                fragment = ResturentSearchResultFragment.getInstance(mContext, fm, bundle);
                ft = fm.beginTransaction();
                ft.replace(R.id.content_frame, fragment, FragmentIDs.ResturentSearchResultFragment_Tag);
                ft.addToBackStack(FragmentIDs.ResturentSearchResultFragment_Tag);
                ft.commit();
                break;
            case 2:
                fragment = ResturentInfoFragment.getInstance(mContext, fm, bundle);
                ft = fm.beginTransaction();
                ft.replace(R.id.content_frame, fragment, FragmentIDs.ResturentInfoFragment_Tag);
                ft.addToBackStack(FragmentIDs.ResturentInfoFragment_Tag);
                ft.commit();
                break;
            case 3:
                fragment = ResturentMenuFragment.getInstance(mContext, fm, bundle);
                ft = fm.beginTransaction();
                ft.replace(R.id.content_frame, fragment, FragmentIDs.ResturentMenuFragment_Tag);
                ft.addToBackStack(FragmentIDs.ResturentMenuFragment_Tag);
                ft.commit();
                break;
            case 4:
                fragment = ResturentMenuItemFragment.getInstance(mContext, fm, bundle);
                ft = fm.beginTransaction();
                ft.replace(R.id.content_frame, fragment, FragmentIDs.ResturentMenuItemFragment_Tag);
                ft.addToBackStack(FragmentIDs.ResturentMenuItemFragment_Tag);
                ft.commit();
                break;
            case 5:
                fragment = AboutUsFragment.getInstance(mContext, fm, bundle);
                ft = fm.beginTransaction();
                ft.replace(R.id.content_frame, fragment, FragmentIDs.AboutUsFragment_Tag);
                ft.addToBackStack(FragmentIDs.AboutUsFragment_Tag);
                ft.commit();
                break;
            case 6:
                fragment = PerfilFragment.getInstance(mContext, fm, bundle);
                ft = fm.beginTransaction();
                ft.replace(R.id.content_frame, fragment, FragmentIDs.PerfilFragment_Tag);
                ft.addToBackStack(FragmentIDs.PerfilFragment_Tag);
                ft.commit();
                break;
            case 7:

                /**
                 * Remove the login fragment
                 */
                removePopupBackstack(FragmentIDs.PerfilFragment_Tag);

                fragment = PerfilListFragment.getInstance(mContext, fm, bundle);
                ft = fm.beginTransaction();
                ft.replace(R.id.content_frame, fragment, FragmentIDs.PerfilListFragment_Tag);
                ft.addToBackStack(FragmentIDs.PerfilListFragment_Tag);
                ft.commit();
                break;
            case 8:
                fragment = RegistraClienteFragment.getInstance(mContext, fm, bundle);
                ft = fm.beginTransaction();
                ft.replace(R.id.content_frame, fragment, FragmentIDs.RegistraClienteFragment_Tag);
                ft.addToBackStack(FragmentIDs.RegistraClienteFragment_Tag);
                ft.commit();
                break;
            case 9:
                fragment = DeliveryAddressFragment.getInstance(mContext, fm, bundle);
                ft = fm.beginTransaction();
                ft.replace(R.id.content_frame, fragment, FragmentIDs.DeliveryAddressFragment_Tag);
                ft.addToBackStack(FragmentIDs.DeliveryAddressFragment_Tag);
                ft.commit();
                break;
            case 10:
                fragment = BillingAddressFragment.getInstance(mContext, fm, bundle);
                ft = fm.beginTransaction();
                ft.replace(R.id.content_frame, fragment, FragmentIDs.BillingAddressFragment_Tag);
                ft.addToBackStack(FragmentIDs.BillingAddressFragment_Tag);
                ft.commit();
                break;
            case 11:
                fragment = UserProfileFragment.getInstance(mContext, fm, bundle);
                ft = fm.beginTransaction();
                ft.replace(R.id.content_frame, fragment, FragmentIDs.UserProfileFragment_Tag);
                ft.addToBackStack(FragmentIDs.UserProfileFragment_Tag);
                ft.commit();
                break;
            case 12:
                fragment = DeliveryAddressListFragment.getInstance(mContext, fm, bundle);
                ft = fm.beginTransaction();
                ft.replace(R.id.content_frame, fragment, FragmentIDs.DeliveryAddressListFragment_Tag);
                ft.addToBackStack(FragmentIDs.DeliveryAddressListFragment_Tag);
                ft.commit();
                break;
            case 13:
                fragment = BillingAddressListFragment.getInstance(mContext, fm, bundle);
                ft = fm.beginTransaction();
                ft.replace(R.id.content_frame, fragment, FragmentIDs.BillingAddressListFragment_Tag);
                ft.addToBackStack(FragmentIDs.BillingAddressListFragment_Tag);
                ft.commit();
                break;
            case 14:
                fragment = ChangePasswordFragment.getInstance(mContext, fm, bundle);
                ft = fm.beginTransaction();
                ft.replace(R.id.content_frame, fragment, FragmentIDs.ChangePasswordFragment_Tag);
                ft.addToBackStack(FragmentIDs.ChangePasswordFragment_Tag);
                ft.commit();
                break;
            case 15:
                fragment = DispositivosListFragment.getInstance(mContext, fm, bundle);
                ft = fm.beginTransaction();
                ft.replace(R.id.content_frame, fragment, FragmentIDs.DispositivosListFragment_Tag);
                ft.addToBackStack(FragmentIDs.DispositivosListFragment_Tag);
                ft.commit();
                break;
            case 16:
                fragment = MyOrdersFragment.getInstance(mContext, fm, bundle);
                ft = fm.beginTransaction();
                ft.replace(R.id.content_frame, fragment, FragmentIDs.MyOrdersFragment_Tag);
                ft.addToBackStack(FragmentIDs.MyOrdersFragment_Tag);
                ft.commit();
                break;
            case 17:
                fragment = MyOrderDetailsFragment.getInstance(mContext, fm, bundle);
                ft = fm.beginTransaction();
                ft.replace(R.id.content_frame, fragment, FragmentIDs.MyOrderDetailsFragment_Tag);
                ft.addToBackStack(FragmentIDs.MyOrderDetailsFragment_Tag);
                ft.commit();
                break;
            case 18:
                fragment = OrderFragment.getInstance(mContext, fm, bundle);
                ft = fm.beginTransaction();
                ft.replace(R.id.content_frame, fragment, OrderFragment_Tag);
                ft.addToBackStack(OrderFragment_Tag);
                ft.commit();
                break;
            case 19:
                fragment = CongratulationsFragment.getInstance(mContext, fm, bundle);
                ft = fm.beginTransaction();
                ft.replace(R.id.content_frame, fragment, FragmentIDs.CongratulationsFragment_Tag);
                ft.addToBackStack(FragmentIDs.CongratulationsFragment_Tag);
                ft.commit();
                break;
            case 20:
                fragment = ProductOffersFragment.getInstance(mContext, fm, bundle);
                ft = fm.beginTransaction();
                ft.replace(R.id.content_frame, fragment, FragmentIDs.ProductOffersFragment_Tag);
                ft.addToBackStack(FragmentIDs.ProductOffersFragment_Tag);
                ft.commit();
                break;
            case 21:
                fragment = PromocionesFragment.getInstance(mContext, fm, bundle);
                ft = fm.beginTransaction();
                ft.replace(R.id.content_frame, fragment, FragmentIDs.PromocionesFragment_Tag);
                ft.addToBackStack(FragmentIDs.PromocionesFragment_Tag);
                ft.commit();
                break;
            case 22:
                fragment = PromocionesFragment.getInstance(mContext, fm, bundle);
                ft = fm.beginTransaction();
                ft.replace(R.id.content_frame, fragment, FragmentIDs.PromocionesFragment_Tag);
                ft.addToBackStack(FragmentIDs.PromocionesFragment_Tag);
                ft.commit();
                break;

            case 24:
                fragment = NewOrderFragment.getInstance(mContext, fm, bundle);
                ft = fm.beginTransaction();
                ft.replace(R.id.content_frame, fragment, FragmentIDs.NewOrderFragment_Tag);
                ft.addToBackStack(FragmentIDs.NewOrderFragment_Tag);
                ft.commit();
                break;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppController.getInstance().setErrorMessage();
        setContentView(R.layout.activity_dashboard);
        mContext = this;
        dashBoardActivity = this;
        fm = getSupportFragmentManager();
        initLayout();

        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
                new IntentFilter("custom-event-name"));

        displayFragment(FragmentIDs.ResturentSearchFragment_Id, new Bundle());
    }

    @Override
    public void onBackPressed() {

        PerfilFragment perfilFragment = (PerfilFragment) fm.findFragmentByTag(FragmentIDs.PerfilFragment_Tag);
        OrderFragment orderFragment = (OrderFragment) fm.findFragmentByTag(OrderFragment_Tag);
        NewOrderFragment newOrderFragment = (NewOrderFragment) fm.findFragmentByTag(FragmentIDs.NewOrderFragment_Tag);
        CongratulationsFragment congratulationsFragment = (CongratulationsFragment) fm.findFragmentByTag(FragmentIDs.CongratulationsFragment_Tag);
        if (congratulationsFragment != null && congratulationsFragment.isVisible()) {
            DashBoardActivity.removeAllFragment();
        }
        if (orderFragment != null && orderFragment.isVisible() && DashBoardActivity.order_back_status == 2) {
            orderFragment.showprevious_fragment();
        } else if (orderFragment == null && DashBoardActivity.order_back_status == 2) {
            Bundle bundle = (Bundle) DashBoardActivity.cartButtonLayout.getTag(R.id.orderData);
        } else if (orderFragment != null && OrderFragment.isConfirm) {
            OrderFragment.isConfirm = false;
            orderFragment.updateUi();
        } else if (DashBoardActivity.login_back_status == 1 && perfilFragment != null && perfilFragment.isVisible()) {
            NewOrderFragment.isFromNewOrder = false;
            DashBoardActivity.login_back_status = 0;
            fm.popBackStack();

            //DashBoardActivity.displayFragment(FragmentIDs.OrderFragment_Id,OrderFragment.b);
            DashBoardActivity.displayFragment(FragmentIDs.NewOrderFragment_Id, NewOrderFragment.b);

        } else if (DashBoardActivity.login_back_status == 0 && newOrderFragment != null && newOrderFragment.isVisible()) {
            fm.popBackStack();
            DashBoardActivity.displayFragment(FragmentIDs.OrderFragment_Id, OrderFragment.b);
        } else {
            super.onBackPressed();
        }
    }

/*    public static void showdialog(final Intent in) {
        try {
            if (in != null) {
                if (mContext != null) {
                    dashBoardActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            dashBoardActivity.onNewIntent(in);
                        }
                    });
                }
            }
        } catch (Exception e) {
            Log.e("exception@dashh", e.toString());
        }
    }*/

    @Override
    protected void onResume() {
        super.onResume();
        Intent intent = getIntent();
        if (intent.getExtras() != null && AppController.notification_Status==0) {
            notification_dialog(intent);
        }
    }

/*    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (intent.getExtras() != null && AppController.notification_Status==0) {
            notification_dialog(intent);
        }
    }*/

    @Override
    protected void onDestroy() {
        // Unregister since the activity is about to be closed.
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
        super.onDestroy();
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            if(intent!=null) {
                notification_dialog(intent);
            }
        }
    };


    public void notification_dialog(Intent intent) {

        AppController.notification_Status = 1;

        final Dialog dialog = new Dialog(mContext);
        // Include dialog.xml file
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog);
        // Set dialog title
        dialog.setCancelable(false);

        Bundle bb = intent.getExtras();


        //dialog.setTitle(""+bb.get("title"));
        // set values for custom dialog components - text, image and button
      /*  TextView text = (TextView) dialog.findViewById(R.id.textDialog);
        text.setText(""+bb.get("title"));*/

        TextView MESSAGE = (TextView) dialog.findViewById(R.id.MESSAGE);
        ImageView notification_image = (ImageView) dialog.findViewById(R.id.notification_image);

        if (bb.containsKey("Message")) {
            MESSAGE.setText("" + bb.get("Message"));
        }

        if (bb.containsKey("image_url")) {
            String image_url = bb.getString("image_url");
            if (!image_url.equals("") && image_url != null) {
                Picasso.with(mContext).
                        load(image_url).
                        into(notification_image);
            } else {
                notification_image.setVisibility(View.GONE);
            }
        }
        dialog.show();

        Button declineButton = (Button) dialog.findViewById(R.id.declineButton);
        // if decline button is clicked, close the custom dialog
        declineButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                dialog.dismiss();
            }
        });
    }

    public void initLayout() {
        dash_board_list_layout = (ListView) findViewById(R.id.dash_board_list_layout);
        nav_view = (NavigationView) findViewById(R.id.nav_view);
        drawer_layout = (DrawerLayout) findViewById(R.id.drawer_layout);
        nav_search = (ImageView) findViewById(R.id.nav_search);
        nav_logout = (ImageView) findViewById(R.id.nav_logout);
        nav_resturents = (LinearLayout) findViewById(R.id.nav_resturents);
        nav_promociones = (LinearLayout) findViewById(R.id.nav_promociones);
        nav_perfil = (LinearLayout) findViewById(R.id.nav_perfil);
        nav_nosotros = (LinearLayout) findViewById(R.id.nav_nosotros);
        tv_back = (ImageView) findViewById(R.id.tv_back);
        nav_cerrar = (TextView) findViewById(R.id.nav_cerrar);
        nav_info = (TextView) findViewById(R.id.nav_info);
        nav_HorizentalBar = (LinearLayout) findViewById(R.id.nav_HorizentalBar);
        nav_addDeliveryAddress = (ImageView) findViewById(R.id.nav_addDeliveryAddress);
        nav_userPerfilIcon = (ImageView) findViewById(R.id.nav_userPerfilIcon);

        bottom_linearLayout = (LinearLayout) findViewById(R.id.bottom_linearLayout);
        menuButtonLayout = (LinearLayout) findViewById(R.id.menuButtonLayout);
        cartButtonLayout = (LinearLayout) findViewById(R.id.cartButtonLayout);
        menu_totalqty = (TextView) findViewById(R.id.menu_totalqty);

        nav_resturents.setOnClickListener(addRestaurants);
        nav_promociones.setOnClickListener(addPromociones);
        nav_perfil.setOnClickListener(addPerfil);
        nav_nosotros.setOnClickListener(addNostros);
        tv_back.setOnClickListener(addToback);
        nav_cerrar.setOnClickListener(addCerrar);
        nav_info.setOnClickListener(addInfo);
        nav_HorizentalBar.setOnClickListener(addHorizentalBar);
        nav_logout.setOnClickListener(addToLogout);
        nav_addDeliveryAddress.setOnClickListener(addDeliveryAddress);
        nav_userPerfilIcon.setOnClickListener(add_userPerfilIcon);

        bottom_linearLayout.setOnClickListener(addBottomMenuLayout);
        menuButtonLayout.setOnClickListener(addmenuButtonLayout);
        cartButtonLayout.setOnClickListener(addCartLayout);

        iv_menu = (ImageView) findViewById(R.id.iv_menu);
        tv_title = (BebaseNeueTextView) findViewById(R.id.tv_title);

        menuAdapter = new MenuAdapter(mContext);
        dash_board_list_layout.setAdapter(menuAdapter);

        drawer_layout.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
            }

            @Override
            public void onDrawerOpened(View drawerView) {

            }

            @Override
            public void onDrawerClosed(View drawerView) {
            }

            @Override
            public void onDrawerStateChanged(int newState) {
            }
        });

        iv_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // drawer_layout.openDrawer(Gravity.LEFT);
            }
        });
        nav_search.setOnClickListener(addToResturentSearch);

        dash_board_list_layout.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                //drawer_layout.closeDrawer(Gravity.LEFT);
            }
        });

        drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
    }

    View.OnClickListener addToResturentSearch = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            ResturentSearchResultFragment resturentSearchResultFragment = (ResturentSearchResultFragment) fm.findFragmentByTag(FragmentIDs.ResturentSearchResultFragment_Tag);
            if (resturentSearchResultFragment != null) {
                if (resturentSearchResultFragment.isVisible()) {
                    resturentSearchResultFragment.showSearchView();
                }
            }
        }
    };
    View.OnClickListener addRestaurants = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            DashBoardActivity.setBackgroundColor(getResources().getColor(R.color.hint_color_1),
                    Color.TRANSPARENT,
                    Color.TRANSPARENT,
                    Color.TRANSPARENT);

            Bundle bundle = new Bundle();
            DashBoardActivity.displayFragment(FragmentIDs.ResturentSearchFragment_Id, bundle);
        }
    };
    View.OnClickListener addPromociones = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            DashBoardActivity.setBackgroundColor(
                    Color.TRANSPARENT,
                    getResources().getColor(R.color.hint_color_1),
                    Color.TRANSPARENT,
                    Color.TRANSPARENT);
        }
    };
    View.OnClickListener addPerfil = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            DashBoardActivity.setBackgroundColor(
                    Color.TRANSPARENT,
                    Color.TRANSPARENT,
                    getResources().getColor(R.color.hint_color_1),
                    Color.TRANSPARENT);

            Bundle bundle = new Bundle();
            if (SessionManager.getUserId(mContext) != 0) {
                DashBoardActivity.displayFragment(FragmentIDs.PerfilListFragment_Id, bundle);
            } else {
                DashBoardActivity.displayFragment(FragmentIDs.PerfilFragment_Id, bundle);
            }
        }
    };
    View.OnClickListener addNostros = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            DashBoardActivity.setBackgroundColor(
                    Color.TRANSPARENT,
                    Color.TRANSPARENT,
                    Color.TRANSPARENT,
                    getResources().getColor(R.color.hint_color_1));

            /**
             * About us functionality
             */
            Bundle bundle = new Bundle();
            DashBoardActivity.displayFragment(FragmentIDs.AboutUsFragment_Id, bundle);

        }
    };
    View.OnClickListener addHorizentalBar = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

        }
    };
    View.OnClickListener addToback = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            PerfilFragment perfilFragment = (PerfilFragment) fm.findFragmentByTag(FragmentIDs.PerfilFragment_Tag);
            OrderFragment orderFragment = (OrderFragment) fm.findFragmentByTag(OrderFragment_Tag);
            NewOrderFragment newOrderFragment = (NewOrderFragment) fm.findFragmentByTag(FragmentIDs.NewOrderFragment_Tag);

            if (orderFragment != null && orderFragment.isVisible() && DashBoardActivity.order_back_status == 2) {
                orderFragment.showprevious_fragment();
            } else if (orderFragment != null && OrderFragment.isConfirm) {
                OrderFragment.isConfirm = false;
                orderFragment.updateUi();
            } else if (DashBoardActivity.login_back_status == 1 && perfilFragment != null && perfilFragment.isVisible()) {
                NewOrderFragment.isFromNewOrder = false;
                DashBoardActivity.login_back_status = 0;
                fm.popBackStack();
                //DashBoardActivity.displayFragment(FragmentIDs.OrderFragment_Id,OrderFragment.b);
                DashBoardActivity.displayFragment(FragmentIDs.NewOrderFragment_Id, NewOrderFragment.b);
            } else if (DashBoardActivity.login_back_status == 0 && newOrderFragment != null && newOrderFragment.isVisible()) {
                fm.popBackStack();
                DashBoardActivity.displayFragment(FragmentIDs.OrderFragment_Id, OrderFragment.b);
            } else {
                popBackStack();
            }
        }
    };
    View.OnClickListener addCerrar = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            ProductOffersFragment productOffersFragment =
                    (ProductOffersFragment) fm.findFragmentByTag(FragmentIDs.ProductOffersFragment_Tag);
            if (productOffersFragment != null && productOffersFragment.isVisible()) {
                productOffersFragment.updateProduct();
            }
            popBackStack();
        }
    };
    View.OnClickListener addInfo = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            ResturentMenuFragment resturentMenuFragment = (ResturentMenuFragment)
                    fm.findFragmentByTag(FragmentIDs.ResturentMenuFragment_Tag);
            if (resturentMenuFragment != null) {
                resturentMenuFragment.showRestaurants();
            }
        }
    };
    View.OnClickListener addToLogout = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            PerfilListFragment perfilListFragment = (PerfilListFragment)
                    fm.findFragmentByTag(FragmentIDs.PerfilListFragment_Tag);

            if (perfilListFragment != null) {
                if (perfilListFragment.isVisible()) {
                    perfilListFragment.showLogoutDialog(fm);
                }
            }
        }
    };
    View.OnClickListener addDeliveryAddress = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            Fragment deliveryAddressList = fm.findFragmentByTag(FragmentIDs.DeliveryAddressListFragment_Tag);
            if (deliveryAddressList instanceof DeliveryAddressListFragment) {
                Bundle b = new Bundle();
                b.putInt("addressType", Config.addListAddressType);
                DashBoardActivity.displayFragment(FragmentIDs.DeliveryAddressFragment_Id, b);
            }

            Fragment billingAddressList = fm.findFragmentByTag(FragmentIDs.BillingAddressListFragment_Tag);
            if (billingAddressList instanceof BillingAddressListFragment) {
                Bundle b = new Bundle();
                b.putInt("billingType", Config.addListBillingAddressType);
                DashBoardActivity.displayFragment(FragmentIDs.BillingAddressFragment_Id, b);
            }
        }
    };
    View.OnClickListener addBottomMenuLayout = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

        }
    };
    View.OnClickListener addmenuButtonLayout = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

        }
    };
    View.OnClickListener addCartLayout = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

        }
    };

    View.OnClickListener add_userPerfilIcon = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Bundle bundle = new Bundle();
            if (SessionManager.getUserId(mContext) != 0) {
                DashBoardActivity.displayFragment(FragmentIDs.PerfilListFragment_Id, bundle);
            } else {
                DashBoardActivity.displayFragment(FragmentIDs.PerfilFragment_Id, bundle);
            }
        }
    };


}
