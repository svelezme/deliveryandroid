package com.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.view.ContextThemeWrapper;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import com.activity.DashBoardActivity;
import com.app.grupodeliveryec.deliveryEC.R;
import com.bean.AddressListBean;
import com.bruce.pickerview.LoopView;
import com.com.rest.API;
import com.com.rest.GlobalValues;
import com.com.rest.MethodListener;
import com.model.AreaModel;
import com.model.CityModel;
import com.utility.AppController;
import com.utility.Config;
import com.utility.FragmentIDs;
import com.utility.KeyboadSetting;
import com.utility.MessageDialog;
import com.utility.SessionManager;
import com.utility.UtilityMessage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by administrator on 9/2/17.
 */

public class DeliveryAddressFragment extends Fragment {

    String apiCityRequest = "CityRequest";
    String apiClientDireccion = "apiClientDireccion";
    String TAG = getClass().getName();
    protected static Context context;
    protected static Fragment fragment;
    protected static FragmentManager fragmentManager;
    int IdDirecciones;
    View view;
    NumberPicker areaDialog_cityPicker;
    EditText delivery_descripcion, delivery_ciudad;
    EditText delivery_sector, delivery_Tipo;
    EditText delivery_address1, delivery_address2;
    EditText delivery_convencional, delivery_celuar;
    EditText delivery_office;
    LinearLayout delivery_address, delivery_address_cancel;
    Animation animation;
    ImageView resturent_progressBar;
    MessageDialog.okOnClickListener okOnClickListener;
    MessageDialog messageDialog;

    ArrayList<CityModel> alCityList;
    ArrayList<AreaModel> alAreaList;
    int IdCiudad, IdSector;


    private static Bundle b;

    public static Fragment getInstance(Context mContext, FragmentManager fragmentManager, Bundle bundle) {
        fragment = new DeliveryAddressFragment();
        context = mContext;
        b = bundle;
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.deliveryaddress_xml, container, false);

        DashBoardActivity.hideAddDeliveryAddress();
        DashBoardActivity.showBackButton();
        DashBoardActivity.hideInfo();
        DashBoardActivity.hideLogout();
        DashBoardActivity.hideSearchButton();
        DashBoardActivity.hideCerrar();
        DashBoardActivity.hideHorizentalBar();
        DashBoardActivity.updateTitle(getResources().getString(R.string.direccion_de_entrega));
        DashBoardActivity.hideMenuPanel();

        delivery_descripcion = (EditText) view.findViewById(R.id.delivery_descripcion);
        delivery_ciudad = (EditText) view.findViewById(R.id.delivery_ciudad);
        delivery_sector = (EditText) view.findViewById(R.id.delivery_sector);
        delivery_Tipo = (EditText) view.findViewById(R.id.delivery_Tipo);
        delivery_address1 = (EditText) view.findViewById(R.id.delivery_address1);
        delivery_address2 = (EditText) view.findViewById(R.id.delivery_address2);
        delivery_convencional = (EditText) view.findViewById(R.id.delivery_convencional);
        delivery_celuar = (EditText) view.findViewById(R.id.delivery_celuar);
        delivery_office = (EditText) view.findViewById(R.id.delivery_office);
        delivery_address = (LinearLayout) view.findViewById(R.id.delivery_address);
        resturent_progressBar = (ImageView) view.findViewById(R.id.resturent_progressBar);
        delivery_address_cancel = (LinearLayout) view.findViewById(R.id.delivery_address_cancel);

        delivery_address.setOnClickListener(addDeliveryAddress);
        delivery_address_cancel.setOnClickListener(addTocancel);

        delivery_ciudad.setOnClickListener(addCiudad);
        delivery_sector.setOnClickListener(addSector);
        delivery_Tipo.setOnClickListener(addTipo);

        if (b.getInt("addressType") == 3) {
            DashBoardActivity.showHorizentalBar();
            delivery_address_cancel.setVisibility(View.VISIBLE);

            AddressListBean addressListBean = (AddressListBean)
                    b.getSerializable("addressListBean");

            displayUI(addressListBean);
        }

        if(b.getInt("addressType")==2){
            DashBoardActivity.showHorizentalBar();
            delivery_address_cancel.setVisibility(View.VISIBLE);
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                delivery_descripcion.setCursorVisible(true);
                delivery_descripcion.requestFocus();
                KeyboadSetting.showKeyboard(context,delivery_descripcion);
            }
        },200);


        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        try {
            final InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
        }catch (Exception e){
            Log.e(TAG,""+e.toString());
        }
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        AppController.getInstance().cancelPendingRequests(apiClientDireccion);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        AppController.getInstance().cancelPendingRequests(apiCityRequest);
    }

    private void setGenderPIckerDividerColor(NumberPicker number_picker) {
        // final int count = number_picker.getChildCount();
        for (int i = 0; i < Config.selectGender.length; i++) {
            View child = number_picker.getChildAt(i);

            try {
                Field dividerField = number_picker.getClass().getDeclaredField("mSelectionDivider");
                dividerField.setAccessible(true);
                ColorDrawable colorDrawable = new ColorDrawable(this.getResources().getColor(R.color
                        .hint_color_1));
                dividerField.set(number_picker, colorDrawable);

                if (child instanceof EditText) {

                    Field selectorWheelPaintField = number_picker.getClass()
                            .getDeclaredField("mSelectorWheelPaint");
                    selectorWheelPaintField.setAccessible(true);
                    ((Paint) selectorWheelPaintField.get(number_picker)).setColor(getResources().getColor(R.color.navigation_bg));
                    ((EditText) child).setTextSize(18.0f);
                    ((EditText) child).setTextColor(getResources().getColor(R.color.navigation_bg));
                    number_picker.invalidate();
                }
            } catch (NoSuchFieldException e) {
                Log.w("PickerTextColor", e);
            } catch (IllegalAccessException e) {
                Log.w("PickerTextColor", e);
            } catch (IllegalArgumentException e) {
                Log.w("PickerTextColor", e);
            }
        }
    }

    private void openGenderDialog() {
        final Dialog genderDialog = new Dialog(new ContextThemeWrapper(context, R.style.DialogSlideAnim));
        genderDialog.getWindow().setGravity(Gravity.BOTTOM);
        genderDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        genderDialog.setContentView(R.layout.citydialog_xml_ciudad);
        final LoopView loopView = (LoopView) genderDialog.findViewById(R.id.loop_view);
        final TextView cityDialog_cancel = (TextView) genderDialog.findViewById(R.id.cityDialog_cancel);
        final TextView cityDialog_accept = (TextView) genderDialog.findViewById(R.id.cityDialog_accept);

        loopView.setInitPosition(0);
        loopView.setCanLoop(false);
        final ArrayList<String> area_list = new ArrayList<>();
        for (int i = 0; i < Config.selectGender.length; i++) {
            area_list.add(i, Config.selectGender[i]);
        }

        loopView.setTextSize(22);//must be called before setDateList
        loopView.setDataList(area_list);

        cityDialog_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                genderDialog.dismiss();
            }
        });

        cityDialog_accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                delivery_Tipo.setText("" + area_list.get(loopView.getSelectedItem()));
                genderDialog.dismiss();
            }
        });

        genderDialog.show();
    }

    private void showCityDialog() {
        final Dialog cityDialog = new Dialog(new ContextThemeWrapper(context, R.style.DialogSlideAnim));
        cityDialog.getWindow().setGravity(Gravity.BOTTOM);
        cityDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        cityDialog.setContentView(R.layout.citydialog_xml_ciudad);
        final LoopView loopView = (LoopView) cityDialog.findViewById(R.id.loop_view);
        final TextView cityDialog_cancel = (TextView) cityDialog.findViewById(R.id.cityDialog_cancel);
        final TextView cityDialog_accept = (TextView) cityDialog.findViewById(R.id.cityDialog_accept);

        /*cityDialog_cityPicker.setMinValue(0);
        cityDialog_cityPicker.setMaxValue(alCityList.size() - 1);*/

        loopView.setInitPosition(0);
        loopView.setCanLoop(false);

        ArrayList<String> area_list = new ArrayList<>();
        // String[] displayValues = new String[alFilterAreaList.size()];
        for (int i = 0; i < alCityList.size(); i++) {
            area_list.add(i, alCityList.get(i).getNombre());
            // displayValues[i] = alFilterAreaList.get(i).getNombre();
        }

        loopView.setTextSize(22);//must be called before setDateList
        loopView.setDataList(area_list);

        cityDialog_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (cityDialog != null) {
                    cityDialog.dismiss();
                }
            }
        });
        cityDialog_accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (cityDialog != null) {
                    cityDialog.dismiss();

                    IdCiudad = alCityList.get(loopView.getSelectedItem()).getIdCiudad();
                    delivery_ciudad.setText("" + alCityList.get(loopView.getSelectedItem()).getNombre());

                }
            }
        });
        cityDialog.show();
    }

    private void openAreaDialog() {

        if (alAreaList != null) {
            if (alAreaList.size() > 0) {
                final Dialog areaDialog = new Dialog(new ContextThemeWrapper(context, R.style.DialogSlideAnim));
                areaDialog.getWindow().setGravity(Gravity.BOTTOM);
                areaDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                areaDialog.setContentView(R.layout.citydialog_xml_ciudad);

                final TextView cityDialog_cancel = (TextView) areaDialog.findViewById(R.id.cityDialog_cancel);
                final TextView cityDialog_accept = (TextView) areaDialog.findViewById(R.id.cityDialog_accept);
               /* TextView areaDialog_cancel = (TextView) areaDialog.findViewById(R.id.areaDialog_cancel);
                TextView areaDialog_accept = (TextView) areaDialog.findViewById(R.id.areaDialog_accept);*/
                final LoopView loopView = (LoopView) areaDialog.findViewById(R.id.loop_view);

                loopView.setInitPosition(0);
                loopView.setCanLoop(false);

                ArrayList<String> area_list = new ArrayList<>();
                for (int i = 0; i < alAreaList.size(); i++) {
                    area_list.add(i, alAreaList.get(i).getNombre());
                }

                loopView.setTextSize(22);//must be called before setDateList
                loopView.setDataList(area_list);

              /*  areaDialog_cityPicker.setMinValue(0);
                areaDialog_cityPicker.setMaxValue(alAreaList.size() - 1);

                String[] displayValues = new String[alAreaList.size()];
                for (int i = 0; i < alAreaList.size(); i++) {
                    displayValues[i] = alAreaList.get(i).getNombre();
                }
                areaDialog_cityPicker.setDisplayedValues(displayValues);
                setPIckerDividerColor(areaDialog_cityPicker, alAreaList.size());*/

                cityDialog_cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (areaDialog != null) {
                            areaDialog.dismiss();
                        }
                    }
                });

                cityDialog_accept.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (areaDialog != null) {
                            IdSector = alAreaList.get(loopView.getSelectedItem()).getIdSector();
                            // IdSector = loopView.getSelectedItem().getIdSector();
                            delivery_sector.setText("" + alAreaList.get(loopView.getSelectedItem()).getNombre());

                            // delivery_sector.setText(areaDialog_cityPicker.getDisplayedValues()[areaDialog_cityPicker.getValue()]);
                            areaDialog.dismiss();
                        }
                    }
                });

                areaDialog.show();
            }
        }
    }
    private void setMessageErrorDialog(String message) {
        messageDialog = new MessageDialog(context,
                android.R.style.Theme_Holo_Light_Dialog,
                message,
                new MessageDialog.okOnClickListener() {
                    @Override
                    public void onButtonClick() {
                        messageDialog.dismiss();
                    }
                });

        messageDialog.setCancelable(false);
        messageDialog.setCanceledOnTouchOutside(false);
        messageDialog.show();
    }

    private void displayUI(AddressListBean addressListBean) {
       /* Log.e("1",""+addressListBean.getIdDirecciones());
        Log.e("2",""+addressListBean.getIdCiudad());
        Log.e("3",""+addressListBean.getIdSector());
        Log.e("4",""+addressListBean.getIdDirecciones());*/

        getCityList();

        IdDirecciones = addressListBean.getIdDirecciones();

        delivery_descripcion.setText(addressListBean.getDescripcion());
        delivery_ciudad.setText(addressListBean.getNombreCiudad());
        IdCiudad = addressListBean.getIdCiudad();

        delivery_sector.setText(addressListBean.getNombreSector());
        IdSector = addressListBean.getIdSector();

        delivery_Tipo.setText(addressListBean.getTipo());

        delivery_address1.setText(addressListBean.getDireccion());

        delivery_address2.setText(addressListBean.getDireccion2());

        delivery_convencional.setText(addressListBean.getTelefono());

        delivery_celuar.setText(addressListBean.getCelular());

        delivery_office.setText(addressListBean.getReferencia());

    }

    private void doAddDeliveryAddress() {
        if (delivery_descripcion.getText().toString().equals("") &&
                delivery_ciudad.getText().toString().equals("") &&
                delivery_sector.getText().toString().equals("") &&
                delivery_sector.getText().toString().equals("") &&
                delivery_Tipo.getText().toString().equals("") &&
                delivery_address1.getText().toString().equals("") &&
                delivery_address2.getText().toString().equals("") &&
                delivery_convencional.getText().toString().equals("") &&
                delivery_celuar.getText().toString().equals("") &&
                delivery_office.getText().toString().equals("")) {

            String message = getResources().getString(R.string.empty_Debe_ingresar_descripcion)
                    + "\n" + getResources().getString(R.string.empty_Debe_escoger_ciudad)
                    + "\n" + getResources().getString(R.string.empty_Debe_escoger_sector)
                    + "\n" + getResources().getString(R.string.empty_Debe_seleccionar_tipo)
                    + "\n" + getResources().getString(R.string.empty_Debe_ingresar_direccion)
                    + "\n" + getResources().getString(R.string.empty_Debe_ingresar_segunda_linea_de_direccion)
                    + "\n" + getResources().getString(R.string.empty_Debe_ingresar_segunda_telefono)
                    + "\n" + getResources().getString(R.string.empty_Debe_ingresar_celular)
                    + "\n" + getResources().getString(R.string.empty_Debe_ingresar_referencia);

            setMessageErrorDialog(message);

        } else if (delivery_descripcion.getText().toString().equals("")) {
            String message = getResources().getString(R.string.empty_Debe_ingresar_descripcion);
            setMessageErrorDialog(message);
        } else if (delivery_ciudad.getText().toString().equals("")) {
            String message = getResources().getString(R.string.empty_Debe_escoger_ciudad);
            setMessageErrorDialog(message);
        } else if (delivery_sector.getText().toString().equals("")) {
            String message = getResources().getString(R.string.empty_Debe_escoger_sector);
            setMessageErrorDialog(message);
        } else if (delivery_Tipo.getText().toString().equals("")) {
            String message = getResources().getString(R.string.empty_Debe_seleccionar_tipo);
            setMessageErrorDialog(message);
        } else if (delivery_address1.getText().toString().equals("")) {
            String message = getResources().getString(R.string.empty_Debe_ingresar_direccion);
            setMessageErrorDialog(message);
        } else if (delivery_address2.getText().toString().equals("")) {
            String message = getResources().getString(R.string.empty_Debe_ingresar_segunda_linea_de_direccion);
            setMessageErrorDialog(message);
        } else if (delivery_convencional.getText().toString().equals("")) {
            String message = getResources().getString(R.string.empty_Debe_ingresar_segunda_telefono);
            setMessageErrorDialog(message);
        } else if (delivery_celuar.getText().toString().equals("")) {
            String message = getResources().getString(R.string.empty_Debe_ingresar_celular);
            setMessageErrorDialog(message);
        } else if (delivery_office.getText().toString().equals("")) {
            String message = getResources().getString(R.string.empty_Debe_ingresar_referencia);
            setMessageErrorDialog(message);
        } else {

            String descripcion = delivery_descripcion.getText().toString().replace(" ", "%20");
           // String ciudad = delivery_ciudad.getText().toString();
           // String sector = delivery_sector.getText().toString();
            String tipo = delivery_Tipo.getText().toString().replace(" ", "%20");
            String address1 = delivery_address1.getText().toString().replace(" ", "%20");
            String address2 = delivery_address2.getText().toString().replace(" ", "%20");
            String convencional = delivery_convencional.getText().toString().replace(" ", "%20");
            String celuar = delivery_celuar.getText().toString().replace(" ", "%20");
            String referencia = delivery_office.getText().toString().replace(" ", "%20");

            String url = null;

            if (b.getInt("addressType") == 2 || b.getInt("addressType") == 1) {
                url = API.RegistraClientDireccion
                        + "pIdCliente=" + SessionManager.getUserId(context)
                        + "&pIdCiudad=" + IdCiudad
                        + "&pIdSector=" + IdSector
                        + "&pTipo=" + tipo
                        + "&pDescripcion=" + descripcion
                        + "&pDireccion2=" + address2
                        + "&pDireccion=" + address1
                        + "&pTelefono=" + convencional
                        + "&pCelular=" + celuar
                        + "&pReferencia=" + referencia
                        + "&callback=" + "";
            }

            if (b.getInt("addressType") == 3) {
                url = API.updateDeliveryAddress
                        + "&pIdDirecciones=" + IdDirecciones
                        + "&pIdCliente=" + SessionManager.getUserId(context)
                        + "&pIdCiudad=" + IdCiudad
                        + "&pIdSector=" + IdSector
                        + "&pTipo=" + tipo
                        + "&pDescripcion=" + descripcion
                        + "&pDireccion=" + address1
                        + "&pDireccion2=" + address2
                        + "&pTelefono=" + convencional
                        + "&pCelular=" + celuar
                        + "&pReferencia=" + referencia
                        + "&callback=" + "";
            }

            if (url == null)
                return;

            showProgressBar();
            GlobalValues.getMethodManagerObj(context).makeStringReqWithoutCache(url, apiClientDireccion, new MethodListener() {
                @Override
                public void onError() {
                    hideProgressbar();
                }

                @Override
                public void onError(String response) {
                    hideProgressbar();
                }

                @Override
                public void onSuccess(String response) {
                    hideProgressbar();
                    response = response.substring(1, response.length() - 1);
                    //Log.e("response",""+response);
                    try {
                        JSONObject mObj = new JSONObject(response);
                        JSONArray dataArray = mObj.optJSONArray("data");
                        if (dataArray != null) {
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject object = dataArray.optJSONObject(i);
                                if (object.has("respuesta")) {
                                    if (object.optString("respuesta").equals("OK")) {
                                        /**
                                         * Billing Address
                                         */

                                        if (b.getInt("addressType") == 1) {
                                            Bundle bundle = new Bundle();
                                            bundle.putInt("billingType",Config.addRegistrationBiilingAddressType);
                                            DashBoardActivity.displayFragment(FragmentIDs.BillingAddressFragment_Id, bundle);
                                        }
                                        if (b.getInt("addressType") == 3 || b.getInt("addressType")==2) {
                                            DashBoardActivity.popBackStack();
                                        }


                                    } else {
                                        setMessageErrorDialog(object.optString("Error"));
                                    }
                                }
                            }
                        }
                    } catch (JSONException e) {
                        Log.e(TAG, "" + e.toString());
                    }
                }
            });
        }
    }

    private void getCityList() {
        jsonParserCity();
    }


    private void jsonParserCity() {
        delivery_ciudad.setClickable(false);
        delivery_ciudad.setEnabled(false);

        String url = API.localList;
        showProgressBar();
        GlobalValues.getMethodManagerObj(context).makeStringReq(url, apiCityRequest, new MethodListener() {
            @Override
            public void onError() {
                hideProgressbar();
                delivery_ciudad.setClickable(true);
                delivery_ciudad.setEnabled(true);
            }

            @Override
            public void onError(String response) {
                hideProgressbar();
                delivery_ciudad.setClickable(true);
                delivery_ciudad.setEnabled(true);
            }

            @Override
            public void onSuccess(String response) {
                delivery_ciudad.setClickable(true);
                delivery_ciudad.setEnabled(true);

                hideProgressbar();
                try{
                    JSONObject mObj = new JSONObject(response);
                    JSONArray mCity = mObj.optJSONArray("city");
                    if(mCity!=null){
                        getParseLocalList(mCity);
                    }
                }catch (JSONException e){
                    Log.e(TAG,""+e.toString());
                }
            }
        });

    }

    private void getParseLocalList(JSONArray mCity){
        alCityList =  new ArrayList<>();
        alAreaList = new ArrayList<>();

        for(int i=0;i<mCity.length();i++){
            JSONObject object = mCity.optJSONObject(i);

            CityModel cityModel = new CityModel();
            cityModel.setIdCiudad(object.optInt("IdCiudad"));
            cityModel.setNombre(object.optString("Nombre"));
            cityModel.setSiglas(object.optString("Siglas"));
            cityModel.setEstado(object.optString("Estado"));

            JSONArray msectors = object.optJSONArray("sectors");
            getSectors(msectors);

            alCityList.add(cityModel);
        }

    }

    private void getSectors(JSONArray msectors) {
        for (int i = 0; i < msectors.length(); i++) {
            JSONObject object = msectors.optJSONObject(i);
            AreaModel areaModel = new AreaModel();
            areaModel.setIdCiudad(object.optInt("IdCiudad"));
            areaModel.setIdSector(object.optInt("IdSector"));
            areaModel.setNombre(object.optString("Nombre"));
            areaModel.setSiglas(object.optString("Siglas"));
            areaModel.setEstado(object.optString("Estado"));
            alAreaList.add(areaModel);
        }
    }

    private void showProgressBar() {
        resturent_progressBar.setVisibility(View.VISIBLE);
        animation = AnimationUtils.loadAnimation(context, R.anim.progress_anim);
        animation.setDuration(1000);
        resturent_progressBar.startAnimation(animation);
    }

    private void hideProgressbar() {
        if (animation != null) {
            resturent_progressBar.clearAnimation();
            resturent_progressBar.setVisibility(View.GONE);
        }
    }

    private Handler ciudadHandler;
    private void ciudadHandler(){
        ciudadHandler = new Handler(Looper.getMainLooper());
        ciudadHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (alCityList != null) {
                    if (alCityList.size() > 0) {
                        showCityDialog();
                    }
                } else {
                    getCityList();
                }
            }
        },500);
    }



    View.OnClickListener addDeliveryAddress = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            doAddDeliveryAddress();
        }
    };

    View.OnClickListener addCiudad = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            View v = ((Activity) context).getCurrentFocus();
            //View v = getActivity().getCurrentFocus();
            KeyboadSetting.hideKeyboadUsingView(v,context);
            //KeyboadSetting.hideKeyboard(context,register_surname);
            ciudadHandler();
        }
    };

    View.OnClickListener addTocancel = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            View v = ((Activity) context).getCurrentFocus();
            //View v = getActivity().getCurrentFocus();
            KeyboadSetting.hideKeyboadUsingView(v,context);
            //KeyboadSetting.hideKeyboard(context,register_surname);
            DashBoardActivity.popBackStack();
        }
    };

    View.OnClickListener addSector = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            openAreaDialog();
        }
    };

    View.OnClickListener addTipo = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            openGenderDialog();
        }
    };
}
