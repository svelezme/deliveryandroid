package com.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.InputType;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.activity.DashBoardActivity;
import com.app.grupodeliveryec.deliveryEC.R;
import com.buttonView.BbaseNeueButtonView;
import com.com.rest.API;
import com.com.rest.GlobalValues;
import com.com.rest.MethodListener;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.inputview.ExoSemiEditTextView;
import com.model.RestaurantListModel;
import com.receiver.ThirtyMinutesReceiver;
import com.utility.AppController;
import com.utility.Config;
import com.utility.FragmentIDs;
import com.utility.KeyboadSetting;
import com.utility.MessageDialog;
import com.utility.SessionManager;
import com.utility.UtilityMessage;
import com.view.ExoSemiBoldTextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Arrays;
import java.util.Calendar;

/**
 * Created by administrator on 6/2/17.
 */

public class PerfilFragment extends Fragment {

    protected static Context context;
    protected static Fragment fragment;
    protected static FragmentManager fragmentManager;
    private static Bundle b;
    String TAG = getClass().getName();
    String apiLogin = "apiLogin";
    String apiForgotPW = "apiForgotPW";
    View view;
    int orderScreen = 0;
    CheckBox perfile_termsConditions;
    ExoSemiEditTextView perfil_email, perfil_password;
    BbaseNeueButtonView perfil_submit;
    ExoSemiBoldTextView perfil_forgotPW;
    RelativeLayout perfil_relativeLayout;
    MessageDialog messageDialog;
    Animation animation;
    CallbackManager callbackManager;
    // LoginButton login_button;
    ImageButton login;
    ImageView resturent_progressBar;


    View.OnClickListener addToRegistration = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Bundle bundle = new Bundle();
            DashBoardActivity.displayFragment(FragmentIDs.RegistraClienteFragment_Id, bundle);
        }
    };
    View.OnClickListener addLogin = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            callToLogin();

        }
    };

    View.OnClickListener addForgot = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            showForgotPW();
        }
    };


    private PendingIntent pendingIntent;
    private AlarmManager manager;


    public static Fragment getInstance(Context mContext, FragmentManager fragmentManager, Bundle bundle) {
        fragment = new PerfilFragment();
        context = mContext;
        b = bundle;
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FacebookSdk.sdkInitialize(getActivity().getApplicationContext());

        DashBoardActivity.callbackManager = CallbackManager.Factory.create();


    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        view = inflater.inflate(R.layout.perfile_xml, container, false);
        orderScreen = b.getInt("orderScreen");

        perfil_relativeLayout = (RelativeLayout)view.findViewById(R.id.perfil_relativeLayout);



        DashBoardActivity.showBackButton();
        DashBoardActivity.hideInfo();
        DashBoardActivity.hideCerrar();
        DashBoardActivity.hideSearchButton();
        DashBoardActivity.hideLogout();
        DashBoardActivity.hideHorizentalBar();
        DashBoardActivity.hideAddDeliveryAddress();
        DashBoardActivity.updateTitle(getResources().getString(R.string.ingresar).toUpperCase());
        DashBoardActivity.hideMenuPanel();
        DashBoardActivity.hideUserPerfilIcon();

        //login_button = (LoginButton) view.findViewById(R.id.login_button);
        login = (ImageButton) view.findViewById(R.id.login_button);


        perfil_email = (ExoSemiEditTextView) view.findViewById(R.id.perfil_email);
        perfil_password = (ExoSemiEditTextView) view.findViewById(R.id.perfil_password);
        perfil_submit = (BbaseNeueButtonView) view.findViewById(R.id.perfil_submit);
        perfil_forgotPW = (ExoSemiBoldTextView) view.findViewById(R.id.perfil_forgotPW);
        resturent_progressBar = (ImageView) view.findViewById(R.id.resturent_progressBar);
        perfile_termsConditions = (CheckBox) view.findViewById(R.id.perfile_termsConditions);
        perfil_email.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
       // perfil_password.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);

        perfil_submit.setOnClickListener(addLogin);
        perfil_forgotPW.setOnClickListener(addForgot);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginManager.getInstance().logInWithReadPermissions(getActivity(), Arrays.asList("public_profile", "user_friends", "email"));
                LoginManager.getInstance().registerCallback(DashBoardActivity.callbackManager,
                        new FacebookCallback<LoginResult>() {
                            @Override
                            public void onSuccess(LoginResult loginResult) {
                                RequestData();
                            }

                            @Override
                            public void onCancel() {
                            }

                            @Override
                            public void onError(FacebookException exception) {
                            }
                        });
            }
        });

     /*   perfile_termsConditions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (perfile_termsConditions.isChecked()) {
                    isChecked = true;
                } else {
                    isChecked = false;
                }
            }
        });*/

       /* perfil_email.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
                return false;
            }
        });*/
        // getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        /*accessTokenTracker.stopTracking();
        profileTracker.stopTracking();*/
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        AppController.getInstance().cancelPendingRequests(apiLogin);
        AppController.getInstance().cancelPendingRequests(apiForgotPW);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void displayMessage(Profile profile) {
        if (profile != null) {
            //Log.e("Name", "" + profile.getName());
            // Log.e("id", "" + profile.getId());
            // Log.e("firstName", "" + profile.getFirstName());
            // Log.e("lastName", "" + profile.getLastName());


            GraphRequest request = new GraphRequest(
                    AccessToken.getCurrentAccessToken(),
                    "/" + profile.getId() + "",
                    null,
                    HttpMethod.GET,
                    new GraphRequest.Callback() {
                        public void onCompleted(GraphResponse response) {
                            /* handle the result */

                            try {

                                int responseCode = response.getConnection().getResponseCode();
                                //int errorCode = response.getError().getErrorCode();
                                //String errorMessage = response.getError().getErrorMessage();
                                if (responseCode == 200) {

                                    Log.e("response", "" + response);
                                    String rawResponse = response.getRawResponse();
                                    Log.e("rawResponse", "" + rawResponse);

                                    JSONObject mObj = new JSONObject(rawResponse);

                                    String id = mObj.optString("id");
                                    String name = mObj.optString("name");
                                    String first_name = mObj.optString("first_name");
                                    String last_name = mObj.optString("last_name");
                                    String email = mObj.optString("email");
                                    String gender = mObj.optString("gender");
                                    String birthday = mObj.optString("birthday");

                                    Bundle bundle = new Bundle();
                                    bundle.putString("id", id);
                                    bundle.putString("name", name);
                                    bundle.putString("first_name", first_name);
                                    bundle.putString("last_name", last_name);
                                    bundle.putString("email", email);
                                    bundle.putString("gender", gender);

                                    DashBoardActivity.displayFragment(FragmentIDs.RegistraClienteFragment_Id, bundle);
                                }

                            } catch (JSONException e) {
                                Log.e(TAG, "" + e.toString());
                            } catch (IOException e) {
                                Log.e(TAG, "" + e.toString());
                            }

                        }
                    }
            );

            Bundle parameters = new Bundle();
            parameters.putString("fields", "id,name,first_name,last_name,email,gender,birthday,cover");
            request.setParameters(parameters);
            request.executeAsync();

            disconnectFromFacebook();
        }
    }


    public void RequestData() {
        GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject mObj, GraphResponse response) {

                try {
                    if (mObj != null) {

                        String id = mObj.optString("id");
                        String name = mObj.optString("name");
                        String first_name = mObj.optString("first_name");
                        String last_name = mObj.optString("last_name");
                        String email = mObj.optString("email");
                        String gender = mObj.optString("gender");

                        JSONObject projson = mObj.optJSONObject("picture");
                        JSONObject projson2 = projson.optJSONObject("data");
                        String profilepic = projson2.optString("url");
                        String birthday = mObj.optString("birthday");

                        Bundle bundle = new Bundle();
                        bundle.putString("id", id);
                        bundle.putString("name", name);
                        bundle.putString("first_name", first_name);
                        bundle.putString("last_name", last_name);
                        bundle.putString("email", email);
                        bundle.putString("gender", gender);
                        bundle.putString("profilepic", profilepic);

                        DashBoardActivity.displayFragment(FragmentIDs.RegistraClienteFragment_Id, bundle);

                        LoginManager.getInstance().logOut();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,first_name,last_name,link,email,picture.type(large)");
        request.setParameters(parameters);
        request.executeAsync();
    }

    public void disconnectFromFacebook() {

        if (AccessToken.getCurrentAccessToken() == null) {
            return; // already logged out
        }

        LoginManager.getInstance().logOut();
    }

    private void setMessageErrorDialog(String message) {
        messageDialog = new MessageDialog(context,
                android.R.style.Theme_Holo_Light_Dialog,
                message,
                new MessageDialog.okOnClickListener() {
                    @Override
                    public void onButtonClick() {
                        messageDialog.dismiss();
                    }
                });

        messageDialog.setCancelable(false);
        messageDialog.setCanceledOnTouchOutside(false);
        messageDialog.show();
    }

    private void showProgressBar() {
        resturent_progressBar.setVisibility(View.VISIBLE);
        animation = AnimationUtils.loadAnimation(context, R.anim.progress_anim);
        animation.setDuration(1000);
        resturent_progressBar.startAnimation(animation);
    }

    private void hideProgressbar() {
        if (animation != null) {
            resturent_progressBar.clearAnimation();
            resturent_progressBar.setVisibility(View.GONE);
        }
    }

    private void showForgotPW() {
        final Dialog forgotDialog = new Dialog(context);
        forgotDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        forgotDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        forgotDialog.setCancelable(false);
        forgotDialog.setCanceledOnTouchOutside(false);
        forgotDialog.setContentView(R.layout.forgot_xml);
        final EditText forgot_email = (EditText) forgotDialog.findViewById(R.id.forgot_email);
        TextView forgot_accept = (TextView) forgotDialog.findViewById(R.id.forgot_accept);
        TextView forgot_cancel = (TextView) forgotDialog.findViewById(R.id.forgot_cancel);

        forgot_accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                forgotDialog.dismiss();
                if (!forgot_email.getText().toString().equals("")) {

                    //Call to forgot pw

                    KeyboadSetting.hideKeyboard(context, forgot_email);

                    String url = API.forgotPW + "pEmail=" + forgot_email.getText().toString()
                            + "&callback=";
                    showProgressBar();
                    GlobalValues.getMethodManagerObj(context).makeStringReq(url, apiForgotPW, new MethodListener() {
                        @Override
                        public void onError() {
                            hideProgressbar();
                        }


                        @Override
                        public void onError(String response) {
                            hideProgressbar();
                        }

                        @Override
                        public void onSuccess(String response) {
                            hideProgressbar();
                            try {
                                response = response.substring(1, response.length() - 1);
                                JSONObject mOBj = new JSONObject(response);
                                JSONArray mData = mOBj.optJSONArray("data");
                                if (mData != null) {
                                    for (int i = 0; i < mData.length(); i++) {
                                        JSONObject object = mData.optJSONObject(i);
                                        if (object.has("respuesta")) {
                                            if (object.optString("respuesta").equals("OK")) {
                                                setMessageErrorDialog(object.optString("mensaje"));
                                            } else {
                                                setMessageErrorDialog(object.optString("Error"));
                                            }
                                        }
                                    }
                                }

                            } catch (JSONException e) {
                                Log.e(TAG, "" + e.toString());
                            } catch (Exception e) {
                                Log.e(TAG, "" + e.toString());
                            }
                        }
                    });

                } else {
                    String message = getResources().getString(R.string.empty_forgotpw);
                    setMessageErrorDialog(message);
                }
            }
        });

        forgot_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                KeyboadSetting.hideKeyboard(context, forgot_email);
                forgotDialog.dismiss();
            }
        });

        forgotDialog.show();
    }

    private void callToLogin() {
        if (perfil_email.getText().toString().equals("")
                && perfil_password.getText().toString().equals("")) {
            String message1 = getResources().getString(R.string.login_email_validation);
            String message2 = getResources().getString(R.string.login_password_validation);

            String message = message1 + "\n" + message2;
            setMessageErrorDialog(message);

        } else if (perfil_email.getText().toString().equals("")) {
            String message = getResources().getString(R.string.login_email_validation);
            setMessageErrorDialog(message);
        } else if (perfil_password.getText().toString().equals("")) {
            String message = getResources().getString(R.string.login_password_validation);
            setMessageErrorDialog(message);
        } else {

            KeyboadSetting.hideKeyboard(context, perfil_password);

            //Call to login ws

            String email = perfil_email.getText().toString();
            String password = perfil_password.getText().toString();
            String paso = Config.paso;
            final String Token = SessionManager.getSaveToken(context);
            String pOS = Config.deviceType;
            String pNombreDispo = UtilityMessage.deviceInfo().replace(" ", "%20");
            String pUUID = UtilityMessage.deviceUUID(context);

            String url = API.new_login_url + "user=" + email + "&password=" + password + "&paso=" + paso + "&pToken=" + Token + "&pOS=" + pOS + "&pNombreDispo=" + pNombreDispo + "&pUUID=" + pUUID + "&callback=";

            showProgressBar();
            GlobalValues.getMethodManagerObj(context).makeStringReqWithoutCache(url, apiLogin, new MethodListener() {
                @Override
                public void onError() {
                    hideProgressbar();
                }

                @Override
                public void onError(String response) {
                    hideProgressbar();
                }

                @Override
                public void onSuccess(String response) {
                    hideProgressbar();
                   // response = response.substring(1, response.length());
                    try {
                        JSONObject object = new JSONObject(response);
                        JSONArray mArray = object.optJSONArray("data");
                        if (mArray != null) {
                            for (int i = 0; i < mArray.length(); i++) {
                                JSONObject mObj = mArray.optJSONObject(i);
                                if (mObj.has("respuesta")) {
                                    if (mObj.optString("respuesta").equals("OK")) {
                                        if (mObj.has("Estado")) {
                                            if (mObj.optString("Estado").equals("A")) {
                                                int Id = mObj.optInt("Id");
                                                String email = mObj.optString("Correo");
                                                String fullName = mObj.optString("NombreCompleto");
                                                String Identificacion = mObj.optString("Identificacion");
                                                String FB_Image = mObj.optString("FB_Image");
                                                if (FB_Image.contains("_@_")){
                                                    SessionManager.save_fb_image(context , FB_Image.replace("_@_" ,"&"));
                                                }else {
                                                    SessionManager.save_fb_image(context , FB_Image);
                                                }
                                                SessionManager.save_username(context , fullName);

                                                SessionManager.saveUserLogin(context, Id, email, fullName, Identificacion);

                                                perfil_email.setText("");
                                                perfil_password.setText("");

                                                if (orderScreen == 1) {

                                                    Bundle bundle = new Bundle();
                                                    bundle.putInt("screenStatus", 2);
                                                    RestaurantListModel restaurantListModel = (RestaurantListModel) b.getSerializable("restaurantModel");
                                                    bundle.putSerializable("restaurantModel", restaurantListModel);
                                                    //Toast.makeText(context,"d"+b.getString("commentOnOrder"),Toast.LENGTH_LONG).show();
                                                    //String commentOnOrder = b.getString("commentOnOrder");
                                                    //OrderFragment.commentOnOrder = commentOnOrder;
                                                    //bundle.putString("commentOnOrder", commentOnOrder);
                                                    bundle.putInt("idRestaLocal", b.getInt("idRestaLocal"));
                                                    bundle.putString("NombreRestaurante", b.getString("NombreRestaurante"));
                                                    bundle.putString("NombreLocal", b.getString("NombreLocal"));
                                                    bundle.putString("TipoComida", b.getString("TipoComida"));
                                                    bundle.putString("ImagenLocal", b.getString("ImagenLocal"));
                                                    bundle.putInt("idRestaCategoria", b.getInt("idRestaCategoria"));
                                                    bundle.putString("Nombre", b.getString("Nombre"));

                                                    DashBoardActivity.getOrderFrg(new RefOfOrderFrg() {
                                                        @Override
                                                        public void getOrderFrg(OrderFragment fragment) {
                                                            fragment.getDetails();
                                                        }
                                                    });
                                                    DashBoardActivity.order_back_status =2;
                                                    Config.orderScreenStatus = 2;
                                                    DashBoardActivity.popBackStack();
                                                    Intent alarmIntent = new Intent(getActivity(), ThirtyMinutesReceiver.class);
                                                    pendingIntent = PendingIntent.getBroadcast(getActivity(), 0, alarmIntent, 0);
                                                    manager = (AlarmManager)getActivity().getSystemService(Context.ALARM_SERVICE);
                                                    int interval = 30 * 1000 * 60;
                                                    manager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(),interval, pendingIntent);

                                                    //DashBoardActivity.displayFragment(FragmentIDs.OrderFragment_Id,bundle);
                                                } else {
                                                    Intent alarmIntent = new Intent(getActivity(), ThirtyMinutesReceiver.class);
                                                    pendingIntent = PendingIntent.getBroadcast(getActivity(), 0, alarmIntent, 0);
                                                    manager = (AlarmManager)getActivity().getSystemService(Context.ALARM_SERVICE);
                                                    int interval = 30 * 1000 * 60;
                                                    manager.setRepeating(AlarmManager.RTC_WAKEUP, Calendar.getInstance().getTimeInMillis(),interval, pendingIntent);

                                                    if (NewOrderFragment.isFromNewOrder) {
                                                        DashBoardActivity.removePopupBackstack(FragmentIDs.PerfilFragment_Tag);
                                                        Bundle bundle = new Bundle();
                                                        DashBoardActivity.displayFragment(FragmentIDs.OrderFragment_Id, bundle);
                                                    }else {
                                                        Bundle bundle = new Bundle();
                                                        DashBoardActivity.displayFragment(FragmentIDs.PerfilListFragment_Id, bundle);
                                                    }

                                                }
                                            }
                                        }
                                    } else {
                                        if (mObj.has("Error")) {
                                            setMessageErrorDialog(mObj.optString("Error"));
                                            //UtilityMessage.showMessage(context,mObj.optString("Error"));
                                        }
                                    }
                                }
                            }
                        }
                    } catch (JSONException e) {
                        Log.e(TAG, "" + e.toString());
                    }
                }
            });
        }
    }

    public interface RefOfOrderFrg {
        public void getOrderFrg(OrderFragment fragment);
    }
}
