package com.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.activity.AboutUSActivity;
import com.activity.DashBoardActivity;
import com.app.grupodeliveryec.deliveryEC.R;

/**
 * Created by administrator on 6/2/17.
 */

public class AboutUsFragment extends Fragment{

    String TAG = getClass().getName();
    protected static Context context;
    protected static Fragment fragment;
    protected static FragmentManager fragmentManager;
    View view;

    String[] aboutUsContain = new String[] {"Quienes somos","Prensa","Contactos",
            "Terminos y condiciones",
    "Politica de privacidad","Preguntas frecuentes","Aplicaciones"};

    ListView about_listView;

    public static Fragment getInstance(Context mContext, FragmentManager fragmentManager, Bundle bundle){
        fragment = new AboutUsFragment();
        context = mContext;
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        view = inflater.inflate(R.layout.about_us,container,false);
        DashBoardActivity.hideCerrar();
        DashBoardActivity.hideSearchButton();
        DashBoardActivity.hideInfo();
        DashBoardActivity.hideBackButton();
        DashBoardActivity.updateTitle(getResources().getString(R.string.NOSOTROS));
        DashBoardActivity.showHorizentalBar();
        DashBoardActivity.hideLogout();
        DashBoardActivity.hideAddDeliveryAddress();
        DashBoardActivity.hideMenuPanel();

        about_listView = (ListView) view.findViewById(R.id.about_listView);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context,
                android.R.layout.simple_list_item_1, android.R.id.text1, aboutUsContain);

        about_listView.setAdapter(adapter);

        about_listView.setOnItemClickListener(mOnItemClickListener);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void switchAboutUS(int position){
        String url,headingTitle;
        switch (position){
            case 0:
                url = "http://deliveryec.azurewebsites.net/Nosotros/Quienes.html";
                headingTitle = aboutUsContain[position];
                openNavigationActivity(url,headingTitle);
                break;
            case 1:
                url = "http://deliveryec.azurewebsites.net/Nosotros/Prensa.html";
                headingTitle = aboutUsContain[position];
                openNavigationActivity(url,headingTitle);
                break;
            case 2:
                url = "http://deliveryec.azurewebsites.net/Nosotros/Contactos.html";
                headingTitle = aboutUsContain[position];
                openNavigationActivity(url,headingTitle);
                break;
            case 3:
                url = "http://deliveryec.azurewebsites.net/Nosotros/Terminos.html";
                headingTitle = aboutUsContain[position];
                openNavigationActivity(url,headingTitle);
                break;
            case 4:
                url = "http://deliveryec.azurewebsites.net/Nosotros/Politica.html";
                headingTitle = aboutUsContain[position];
                openNavigationActivity(url,headingTitle);
                break;
            case 5:
                url = "http://deliveryec.azurewebsites.net/Nosotros/Preguntas.html";
                headingTitle = aboutUsContain[position];
                openNavigationActivity(url,headingTitle);
                break;
            case 6:
                url = "http://deliveryec.azurewebsites.net/Nosotros/Aplicaciones.html";
                headingTitle = aboutUsContain[position];
                openNavigationActivity(url,headingTitle);
                break;
        }
    }

    private void openNavigationActivity(String url,String headingTitle){
        try {
            Intent I = new Intent(context, AboutUSActivity.class);
            I.putExtra("url",url);
            I.putExtra("headingTitle",headingTitle);
            startActivity(I);
        }catch (Exception e){
            Log.e(TAG,""+e.toString());
        }

    }

    AdapterView.OnItemClickListener mOnItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
            switchAboutUS(position);
        }
    };
}
