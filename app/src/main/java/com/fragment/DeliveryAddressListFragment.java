package com.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import com.activity.DashBoardActivity;
import com.adapter.AddressListAdapter;
import com.app.grupodeliveryec.deliveryEC.R;
import com.bean.AddressListBean;
import com.com.rest.API;
import com.com.rest.GlobalValues;
import com.com.rest.MethodListener;
import com.utility.AppController;
import com.utility.Config;
import com.utility.DividerItemDecoration;
import com.utility.FragmentIDs;
import com.utility.RecyclerTouchListener;
import com.utility.SessionManager;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.Vector;

/**
 * Created by administrator on 14/2/17.
 */

public class DeliveryAddressListFragment extends Fragment{

    String apiRequest = "apiRequest";
    String TAG = getClass().getName();

    private static Context context;
    private static FragmentManager fm;
    private static Bundle bundle;
    private static Fragment fragment;
    View view;

    Animation animation;
    ImageView resturent_progressBar;
    private RecyclerView address_recycleView;
    AddressListAdapter addressListAdapter;
    Vector<AddressListBean> alList;

    public static Fragment getInstance(Context mContext, FragmentManager fragmentManager, Bundle bundle){
        fragment = new DeliveryAddressListFragment();
        context = mContext;
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        DashBoardActivity.updateTitle(getResources().getString(R.string.direccion_de_entrega));
        DashBoardActivity.showAddDeliveryAddress();
        DashBoardActivity.hideLogout();
        DashBoardActivity.showBackButton();
        DashBoardActivity.hideInfo();
        DashBoardActivity.hideSearchButton();
        DashBoardActivity.showHorizentalBar();
        DashBoardActivity.hideCerrar();
        DashBoardActivity.hideMenuPanel();

        view = inflater.inflate(R.layout.delivery_address_list,container,false);

        address_recycleView = (RecyclerView)view.findViewById(R.id.address_recycleView);
        resturent_progressBar = (ImageView)view.findViewById(R.id.resturent_progressBar);

        address_recycleView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
        address_recycleView.setLayoutManager(mLayoutManager);
        address_recycleView.addItemDecoration(new DividerItemDecoration(context, LinearLayoutManager.VERTICAL));
        address_recycleView.setItemAnimator(new DefaultItemAnimator());

        getBasicDetails();

        /*address_recycleView.addOnItemTouchListener(new RecyclerTouchListener(context, address_recycleView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                AddressListBean addressListBean = alList.get(position);
                Bundle b = new Bundle();
                b.putInt("addressType", Config.updateListAddressType);
                b.putSerializable("addressListBean",addressListBean);
                DashBoardActivity.displayFragment(FragmentIDs.DeliveryAddressFragment_Id,b);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));*/

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        AppController.getInstance().cancelPendingRequests(apiRequest);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void showProgressBar(){
        resturent_progressBar.setVisibility(View.VISIBLE);
        animation = AnimationUtils.loadAnimation(context, R.anim.progress_anim);
        animation.setDuration(1000);
        resturent_progressBar.startAnimation(animation);
    }
    private void hideProgressbar(){
        if(animation!=null){
            resturent_progressBar.clearAnimation();
            resturent_progressBar.setVisibility(View.GONE);
        }
    }

    private void getBasicDetails(){
       // String url = API.deliveryAddress+"pIdCliente="+ SessionManager.getUserId(context)+"&callback=";
        String url = API.new_delivery_address_url+"id_cliente="+ SessionManager.getUserId(context)+"&callback=";

        //https://deliveryec.azurewebsites.net/ServiciosWeb/WebRestauranteLogin.asmx/ListarClienteDireccion?pIdCliente=10004&callback=

        showProgressBar();

        GlobalValues.getMethodManagerObj(context).makeStringReqWithoutCache(url, apiRequest, new MethodListener() {
            @Override
            public void onError() {
                hideProgressbar();
            }

            @Override
            public void onError(String response) {
                hideProgressbar();
            }

            @Override
            public void onSuccess(String response) {
                hideProgressbar();
                try {
                    //response = response.substring(1,response.length()-1);
                    JSONObject mobj = new JSONObject(response);
                    JSONArray mData = mobj.optJSONArray("data");
                    if(mData!=null) {
                        alList = new Vector<AddressListBean>();
                        for (int i = 0; i < mData.length(); i++) {
                            JSONObject object = mData.optJSONObject(i);
                            AddressListBean addressListBean = new AddressListBean();
                            addressListBean.setIdDirecciones(object.optInt("IdDirecciones"));
                            addressListBean.setIdCiudad(object.optInt("IdCiudad"));
                            addressListBean.setIdCliente(object.optInt("IdCliente"));
                            addressListBean.setIdSector(object.optInt("IdSector"));
                            addressListBean.setDescripcion(object.optString("Descripcion"));
                            addressListBean.setTipo(object.optString("Tipo"));
                            addressListBean.setDireccion(object.optString("Direccion"));
                            addressListBean.setDireccion2(object.optString("Direccion2"));
                            addressListBean.setTelefono(object.optString("Telefono"));
                            addressListBean.setCelular(object.optString("Celular"));
                            addressListBean.setReferencia(object.optString("Referencia"));
                            addressListBean.setNombreCiudad(object.optString("NombreCiudad"));
                            addressListBean.setNombreSector(object.optString("NombreSector"));
                            addressListBean.setNombrePerfil(object.optString("NombrePerfil"));

                            alList.add(addressListBean);
                        }

                        addressListAdapter = new AddressListAdapter(context,alList);
                        address_recycleView.setAdapter(addressListAdapter);
                    }
                }catch (JSONException e){
                    Log.e(TAG,""+e.toString());
                }catch (Exception e){
                    Log.e(TAG,""+e.toString());
                }
            }
        });
    }
}
