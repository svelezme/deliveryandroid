package com.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.activity.DashBoardActivity;
import com.app.grupodeliveryec.deliveryEC.R;
import com.com.rest.API;
import com.com.rest.GlobalValues;
import com.com.rest.MethodListener;
import com.utility.AppController;
import com.utility.MessageDialog;
import com.utility.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by administrator on 15/2/17.
 */

public class ChangePasswordFragment extends Fragment{

    String TAG = getClass().getName();
    String apiRequest = "apiChangePW";
    private static Context context;
    private static FragmentManager fm;
    private Bundle b;
    private static Fragment fragment;
    View view;
    EditText contrasena_oldPassword;
    EditText changepw_newPassword;
    EditText changepw_confirmationPw;
    LinearLayout changepw_saveExits;
    LinearLayout changepw_cancel;
    ImageView resturent_progressBar;
    MessageDialog.okOnClickListener okOnClickListener;
    MessageDialog messageDialog;
    Animation animation;

    public static Fragment getInstance(Context mContext, FragmentManager fragmentManager, Bundle bundle){
        fragment = new ChangePasswordFragment();
        context = mContext;
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        DashBoardActivity.updateTitle(getResources().getString(R.string.change_PW));
        DashBoardActivity.showBackButton();
        DashBoardActivity.hideCerrar();
        DashBoardActivity.hideSearchButton();
        DashBoardActivity.hideInfo();
        DashBoardActivity.hideLogout();
        DashBoardActivity.hideAddDeliveryAddress();
        DashBoardActivity.showHorizentalBar();
        DashBoardActivity.hideMenuPanel();

        view = inflater.inflate(R.layout.changepassword_xml,container,false);
        contrasena_oldPassword = (EditText)view.findViewById(R.id.contrasena_oldPassword);
        changepw_newPassword = (EditText)view.findViewById(R.id.changepw_newPassword);
        changepw_confirmationPw = (EditText)view.findViewById(R.id.changepw_confirmationPw);

        changepw_saveExits = (LinearLayout)view.findViewById(R.id.changepw_saveExits);
        changepw_cancel = (LinearLayout)view.findViewById(R.id.changepw_cancel);

        resturent_progressBar = (ImageView) view.findViewById(R.id.resturent_progressBar);

        changepw_saveExits.setOnClickListener(addSaveToExit);
        changepw_cancel.setOnClickListener(addToCancel);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        AppController.getInstance().cancelPendingRequests(apiRequest);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void showProgressBar(){
        resturent_progressBar.setVisibility(View.VISIBLE);
        animation = AnimationUtils.loadAnimation(context, R.anim.progress_anim);
        animation.setDuration(1000);
        resturent_progressBar.startAnimation(animation);
    }
    private void hideProgressbar(){
        if(animation!=null){
            resturent_progressBar.clearAnimation();
            resturent_progressBar.setVisibility(View.GONE);
        }
    }

    private void setMessageErrorDialog(String message) {
        messageDialog = new MessageDialog(context,
                android.R.style.Theme_Holo_Light_Dialog,
                message,
                new MessageDialog.okOnClickListener() {
                    @Override
                    public void onButtonClick() {
                        messageDialog.dismiss();
                    }
                });

        messageDialog.setCancelable(false);
        messageDialog.setCanceledOnTouchOutside(false);
        messageDialog.show();
    }

    private void doChangePw(){
        if(contrasena_oldPassword.getText().toString().equals("")
                || changepw_newPassword.getText().toString().equals("")
                || changepw_confirmationPw.getText().toString().equals("")){

            setMessageErrorDialog(getResources().getString(R.string.previous_password_incorrect));

        }else{
            String oldPw = contrasena_oldPassword.getText().toString().replace(" ","%20");
            String newPW = changepw_newPassword.getText().toString().replace(" ","%20");
            String confirmationPW = changepw_confirmationPw.getText().toString().replace(" ","%20");

            //https://deliveryec.azurewebsites.net/ServiciosWeb/WebRestauranteLogin.asmx/ModificarContraClientepPorID?pIdCliente=82168&pContra=123&callback=
            if(!newPW.equals(confirmationPW)){
                setMessageErrorDialog(getResources().getString(R.string.not_match));
            }else{
                String url = API.changePW+"pIdCliente="+ SessionManager.getUserId(context)
                            +"&pContra="+newPW
                            +"&callback=";

                showProgressBar();

                GlobalValues.getMethodManagerObj(context).makeStringReq(url, apiRequest, new MethodListener() {
                    @Override
                    public void onError() {
                        hideProgressbar();
                    }

                    @Override
                    public void onError(String response) {
                        hideProgressbar();
                        //Log.e(TAG,""+response);
                    }

                    @Override
                    public void onSuccess(String response) {
                        hideProgressbar();
                        //Log.e(TAG,""+response);
                        try{
                            //({"data":[{"respuesta":"OK","idcliente":"10004"}]})
                            response = response.substring(1,response.length()-1);
                            JSONObject object = new JSONObject(response);

                            JSONArray mData = object.optJSONArray("data");
                            if(mData!=null){
                                for(int i=0;i<mData.length();i++){
                                    JSONObject obj = mData.optJSONObject(i);
                                    if(obj.has("respuesta")){
                                        if(obj.optString("respuesta").equals("OK")){
                                            getActivity().runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    DashBoardActivity.popBackStack();
                                                }
                                            });
                                        }else{
                                            setMessageErrorDialog(obj.optString("error"));
                                        }
                                    }
                                }
                            }

                        }catch (JSONException e){
                            Log.e(TAG,""+e.toString());
                        }catch (Exception e){
                            Log.e(TAG,""+e.toString());
                        }
                    }
                });
            }
        }
    }

    View.OnClickListener addSaveToExit = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            doChangePw();
        }
    };

    View.OnClickListener addToCancel = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    DashBoardActivity.popBackStack();
                }
            });
        }
    };
}
