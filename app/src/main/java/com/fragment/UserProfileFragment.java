package com.fragment;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.view.ContextThemeWrapper;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.TextView;

import com.activity.DashBoardActivity;
import com.app.grupodeliveryec.deliveryEC.R;
import com.com.rest.API;
import com.com.rest.GlobalValues;
import com.com.rest.MethodListener;
import com.utility.AppController;
import com.utility.KeyboadSetting;
import com.utility.MessageDialog;
import com.utility.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;

/**
 * Created by administrator on 14/2/17.
 */

public class UserProfileFragment extends Fragment{

    String apiRequest = "apiRequest";
    String apiUpdateProfile = "apiUpdateProfile";
    View view;
    String TAG = getClass().getName();
    private static Context context;
    private  FragmentManager fm;
    private static Bundle b;
    protected static Fragment fragment;

    ImageView resturent_progressBar;
    EditText datos_firstname,datos_lastname;
    EditText datos_gendar,datos_cedula_o_RUC;
    EditText datos_telefono_convencional;
    EditText datos_celular,datos_email;
    LinearLayout datos_guardar_y_salir,datos_cancel;

    Animation animation;

    MessageDialog.okOnClickListener okOnClickListener;
    MessageDialog messageDialog;

    String[] genderArray = new String[]{"Seleccione Genero","Masculino","Femenino"};
    String[] genderId = new String[]{"0","M","F"};
    String genderids = "0";


    public static Fragment getInstance(Context mContext, FragmentManager fragmentManager, Bundle bundle){
        fragment = new UserProfileFragment();
        context = mContext;
        b = bundle;
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        DashBoardActivity.updateTitle(getResources().getString(R.string.datos_basicos));
        DashBoardActivity.hideLogout();
        DashBoardActivity.showBackButton();
        DashBoardActivity.hideInfo();
        DashBoardActivity.hideSearchButton();
        DashBoardActivity.showHorizentalBar();
        DashBoardActivity.hideCerrar();
        DashBoardActivity.hideAddDeliveryAddress();
        DashBoardActivity.hideMenuPanel();

        view = inflater.inflate(R.layout.datos_basicos,container,false);
        resturent_progressBar = (ImageView)view.findViewById(R.id.resturent_progressBar);
        datos_firstname = (EditText)view.findViewById(R.id.datos_firstname);
        datos_lastname = (EditText)view.findViewById(R.id.datos_lastname);
        datos_gendar = (EditText)view.findViewById(R.id.datos_gendar);
        datos_cedula_o_RUC = (EditText)view.findViewById(R.id.datos_cedula_o_RUC);
        datos_telefono_convencional = (EditText)view.findViewById(R.id.datos_telefono_convencional);
        datos_celular = (EditText)view.findViewById(R.id.datos_celular);
        datos_email = (EditText)view.findViewById(R.id.datos_email);
        datos_guardar_y_salir = (LinearLayout)view.findViewById(R.id.datos_guardar_y_salir);
        datos_cancel = (LinearLayout)view.findViewById(R.id.datos_cancel);


        datos_guardar_y_salir.setOnClickListener(mUpdate);
        datos_cancel.setOnClickListener(mCancel);
        datos_gendar.setOnClickListener(addGender);

        showUserProfile();

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        AppController.getInstance().cancelPendingRequests(apiRequest);
        AppController.getInstance().cancelPendingRequests(apiUpdateProfile);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void showProgressBar(){
        resturent_progressBar.setVisibility(View.VISIBLE);
        animation = AnimationUtils.loadAnimation(context, R.anim.progress_anim);
        animation.setDuration(1000);
        resturent_progressBar.startAnimation(animation);
    }
    private void hideProgressbar(){
        if(animation!=null){
            resturent_progressBar.clearAnimation();
            resturent_progressBar.setVisibility(View.GONE);
        }
    }

    private void setMessageErrorDialog(String message) {
        messageDialog = new MessageDialog(context,
                android.R.style.Theme_Holo_Light_Dialog,
                message,
                new MessageDialog.okOnClickListener() {
                    @Override
                    public void onButtonClick() {
                        messageDialog.dismiss();
                    }
                });

        messageDialog.setCancelable(false);
        messageDialog.setCanceledOnTouchOutside(false);
        messageDialog.show();
    }

    private void setCityPickerDividerColour(NumberPicker number_picker) {

        // final int count = number_picker.getChildCount();
        for (int i = 0; i < genderArray.length; i++) {
            View child = number_picker.getChildAt(i);

            try {
                Field dividerField = number_picker.getClass().getDeclaredField("mSelectionDivider");
                dividerField.setAccessible(true);
                ColorDrawable colorDrawable = new ColorDrawable(this.getResources().getColor(R.color
                        .hint_color_1));
                dividerField.set(number_picker,colorDrawable);

                if (child instanceof EditText) {

                    Field selectorWheelPaintField = number_picker.getClass()
                            .getDeclaredField("mSelectorWheelPaint");
                    selectorWheelPaintField.setAccessible(true);
                    ((Paint) selectorWheelPaintField.get(number_picker)).setColor(getResources().getColor(R.color.navigation_bg));
                    ((EditText) child).setTextSize(18.0f);
                    ((EditText) child).setTextColor(getResources().getColor(R.color.navigation_bg));
                    number_picker.invalidate();
                }
            } catch (NoSuchFieldException e) {
                Log.w("PickerTextColor", e);
            } catch (IllegalAccessException e) {
                Log.w("PickerTextColor", e);
            } catch (IllegalArgumentException e) {
                Log.w("PickerTextColor", e);
            }
        }

    }

    private void getGender(){
        final Dialog cityDialog = new Dialog(new ContextThemeWrapper(context, R.style.DialogSlideAnim));
        cityDialog.getWindow().setGravity(Gravity.BOTTOM);
        cityDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        cityDialog.setContentView(R.layout.citydialog_xml);
        final NumberPicker cityDialog_cityPicker = (NumberPicker) cityDialog.findViewById(R.id.cityDialog_cityPicker);
        final TextView cityDialog_cancel = (TextView) cityDialog.findViewById(R.id.cityDialog_cancel);
        final TextView cityDialog_accept = (TextView) cityDialog.findViewById(R.id.cityDialog_accept);

        cityDialog_cityPicker.setMinValue(0);
        cityDialog_cityPicker.setMaxValue(genderArray.length - 1);
        // String[] displayValues = new String[genderArray.length];
        cityDialog_cityPicker.setDisplayedValues(genderArray);
        setCityPickerDividerColour(cityDialog_cityPicker);

        cityDialog_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cityDialog.dismiss();
            }
        });

        cityDialog_accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cityDialog.dismiss();
                datos_gendar.setText(cityDialog_cityPicker.getDisplayedValues()
                        [cityDialog_cityPicker.getValue()]);
                genderids = genderId[cityDialog_cityPicker.getValue()];
            }
        });

        cityDialog.show();
    }

    private void showUserProfile(){
        showProgressBar();

        String url = API.DatosCliente+"pIdCliente="+ SessionManager.getUserId(context)
                    +"&callback=";

        showProgressBar();

        GlobalValues.getMethodManagerObj(context).makeStringReqWithoutCache(url, apiRequest, new MethodListener() {
            @Override
            public void onError() {
                hideProgressbar();
            }

            @Override
            public void onError(String response) {
                hideProgressbar();
            }

            @Override
            public void onSuccess(String response) {
                hideProgressbar();
                try{
                    response = response.substring(1,response.length()-1);
                    JSONObject mObj = new JSONObject(response);
                    JSONArray mData = mObj.optJSONArray("data");
                    if(mData!=null){
                        for(int i=0;i<mData.length();i++){
                            JSONObject object = mData.optJSONObject(i);
                            if(object.has("respuesta")){
                                if(object.optString("respuesta").equals("OK")){
                                    datos_firstname.setText(object.optString("Nombres"));
                                    datos_lastname.setText(object.optString("Apellidos"));
                                    if(object.optString("Genero").equals("M")){
                                        datos_gendar.setText("Masculino");
                                        genderids = "M";
                                    }else{
                                        datos_gendar.setText("Femenino");
                                        genderids = "F";
                                    }

                                    datos_cedula_o_RUC.setText(object.optString("Identificacion"));
                                    datos_telefono_convencional.setText(object.optString("TelefonoFijo"));
                                    datos_celular.setText(object.optString("Celular"));
                                    datos_email.setText(object.optString("CorreoElectronico"));
                                }
                            }
                        }
                    }
                }catch (JSONException e){
                    Log.e(TAG,""+e.toString());
                }catch (Exception e){
                    Log.e(TAG,""+e.toString());
                }
            }
        });
    }

    private void updateClientInfo(){
        //&pTelefono=9981939390&pCelular=9981939390&callback=
        if(datos_firstname.getText().toString().equals("")
                ||datos_lastname.getText().toString().equals("")
                || datos_gendar.getText().toString().equals("Seleccione Genero")
                || datos_cedula_o_RUC.getText().toString().equals("")
                ||datos_telefono_convencional.getText().toString().equals("")
                || datos_celular.getText().toString().equals("")){

            String message = getResources().getString(R.string.enter_Debe_ingresar_nombres)
                    +"\n"+getResources().getString(R.string.enter_Debe_ingresar_apellidos)
                    +"\n"+getResources().getString(R.string.enter_Debe_seleccionar_genero)
                    +"\n"+getResources().getString(R.string.enter_Debe_ingresar_identificacion)
                    +"\n"+getResources().getString(R.string.enter_Debe_ingresar_telefono)
                    +"\n"+getResources().getString(R.string.enter_Debe_ingresar_celular);

            setMessageErrorDialog(message);


        }else if(datos_firstname.getText().toString().equals("")){
            String message = getResources().getString(R.string.enter_Debe_ingresar_nombres);
            setMessageErrorDialog(message);
        }else if(datos_lastname.getText().toString().equals("")){
            String message = getResources().getString(R.string.enter_Debe_ingresar_apellidos);
            setMessageErrorDialog(message);
        }else if(datos_gendar.getText().toString().equals("Seleccione Genero")||
        genderId==null || genderids.equals("") || genderId.equals("0")){
            String message = getResources().getString(R.string.enter_Debe_seleccionar_genero);
            setMessageErrorDialog(message);
        }else if(datos_cedula_o_RUC.getText().toString().equals("")){
            String message = getResources().getString(R.string.enter_Debe_ingresar_identificacion);
            setMessageErrorDialog(message);
        }else if(datos_telefono_convencional.getText().toString().equals("")){
            String message = getResources().getString(R.string.enter_Debe_ingresar_telefono);
            setMessageErrorDialog(message);
        }else if(datos_celular.getText().toString().equals("")){
            String message = getResources().getString(R.string.enter_Debe_ingresar_celular);
            setMessageErrorDialog(message);
        } else{
            String url = API.updateProfile+"pIdCliente="+SessionManager.getUserId(context)
                    +"&pNombres="+datos_firstname.getText().toString().replace(" ","%20")
                    +"&pApellidos="+datos_lastname.getText().toString().replace(" ","%20")
                    +"&pGenero="+genderids
                    +"&pIdentificacion="+datos_cedula_o_RUC.getText().toString().replace(" ","%20")
                    +"&pEmail="+datos_email.getText().toString().replace(" ","%20")
                    +"&pTelefono="+datos_telefono_convencional.getText().toString().replace(" ","%20")
                    +"&pCelular="+datos_celular.getText().toString().replace(" ","%20")
                    +"&callback=";

            //Log.e("url",""+url);
            showProgressBar();
            GlobalValues.getMethodManagerObj(context).makeStringReq(url, apiUpdateProfile, new MethodListener() {
                @Override
                public void onError() {
                    hideProgressbar();
                }

                @Override
                public void onError(String response) {
                    hideProgressbar();
                }

                @Override
                public void onSuccess(String response) {
                    hideProgressbar();
                    try{
                        response = response.substring(1,response.length()-1);
                        JSONObject mObj = new JSONObject(response);
                        JSONArray mArray = mObj.optJSONArray("data");
                        if(mArray!=null){
                            for(int i=0;i<mArray.length();i++){
                                JSONObject object = mArray.optJSONObject(i);
                                if(object.has("respuesta")){
                                    if(object.optString("respuesta").equals("OK")){
                                        DashBoardActivity.popBackStack();
                                    }else{
                                        setMessageErrorDialog(object.optString("Error"));
                                    }
                                }
                            }
                        }
                    }catch (JSONException e){
                        Log.e(TAG,""+e.toString());
                    }catch (Exception e){
                        Log.e(TAG,""+e.toString());
                    }
                }
            });
        }
    }

    View.OnClickListener mUpdate = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            updateClientInfo();
        }
    };

    View.OnClickListener mCancel = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            DashBoardActivity.popBackStack();
        }
    };

    View.OnClickListener addGender = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            KeyboadSetting.hideKeyboard(context,datos_lastname);
            getGender();
        }
    };
}
