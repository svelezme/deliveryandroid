package com.fragment;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;

import com.activity.DashBoardActivity;
import com.adapter.RestaurantResultAdapter;
import com.app.grupodeliveryec.deliveryEC.R;
import com.bean.Banners;
import com.com.rest.API;
import com.com.rest.GlobalValues;
import com.com.rest.MethodListener;
import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.inputview.BebaseNeueEditTextView;
import com.model.RestaurantListModel;
import com.model.RestaurantMenuList;
import com.utility.AppController;
import com.utility.FragmentIDs;
import com.utility.SessionManager;
import com.utility.UtilityMessage;
import com.view.BebaseNeueTextView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Locale;


/**
 * Created by administrator on 23/1/17.
 */

public class ResturentSearchResultFragment extends Fragment implements BaseSliderView.OnSliderClickListener, ViewPagerEx.OnPageChangeListener {

    public static Context context;
    protected static Fragment fragment;
    protected static FragmentManager fragmentManager;
    public static Bundle b;
    String TAG = getClass().getName();
    String ApiRequest = "ListaLocales";
    String ApiRequestBanner = "Banner";
    Animation animation;
    ListView resturent_lv;
    ArrayList<RestaurantListModel> alrestaurantListModels;
    RestaurantResultAdapter restaurantResultAdapter;
    View view;
    public static String send_id;
    private SliderLayout mDemoSlider;


    TextWatcher mTextWarcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
            if (restaurantResultAdapter != null) {
                String text = resturent_searchBox.getText().toString().toLowerCase(Locale.getDefault());
                restaurantResultAdapter.filter(text);
            }
        }
    };
    AdapterView.OnItemClickListener addOmItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            RestaurantListModel restaurantListModel = alrestaurantListModels.get(i);

            Bundle b = new Bundle();
            b.putSerializable("restaurantModel", restaurantListModel);
            DashBoardActivity.displayFragment(FragmentIDs.ResturentMenuFragment_Id, b);
        }
    };

    private ImageView resturent_progressBar;
    private BebaseNeueEditTextView resturent_searchBox;
    BebaseNeueTextView resturent_promociones;

    View.OnClickListener addToPromociones = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            /**
             * Display the Promociones
             */
            Bundle b11 = new Bundle();
            int idSector = b.getInt("IdSector");
            b11.putInt("idSector", idSector);
            DashBoardActivity.displayFragment(FragmentIDs.PromocionesFragment_Id, b11);
        }
    };

    public static Fragment getInstance(Context mContext, FragmentManager fragmentManager, Bundle bundle) {
        fragment = new ResturentSearchResultFragment();
        context = mContext;
        b = bundle;
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        DashBoardActivity.updateTitle(getResources().getString(R.string.RESTAURANTES).toUpperCase());
        DashBoardActivity.hideSearchButton();
        DashBoardActivity.showBackButton();
        DashBoardActivity.hideCerrar();
        DashBoardActivity.hideInfo();
        DashBoardActivity.showHorizentalBar();
        DashBoardActivity.hideLogout();
        DashBoardActivity.hideAddDeliveryAddress();
        DashBoardActivity.hideMenuPanel();
        DashBoardActivity.showUserPerfilIocn();

        DashBoardActivity.setBackgroundColor(getResources().getColor(R.color.hint_color_1),
                Color.TRANSPARENT,
                Color.TRANSPARENT,
                Color.TRANSPARENT);

        if (view == null) {
            view = inflater.inflate(R.layout.resturent_result_xml, container, false);
            resturent_progressBar = (ImageView) view.findViewById(R.id.resturent_progressBar);
            resturent_searchBox = (BebaseNeueEditTextView) view.findViewById(R.id.resturent_searchBox);
            resturent_promociones = (BebaseNeueTextView) view.findViewById(R.id.resturent_promociones);
            resturent_lv = (ListView) view.findViewById(R.id.resturent_lv);
            mDemoSlider = (SliderLayout) view.findViewById(R.id.slider);
            getRestaurantData();

            resturent_searchBox.addTextChangedListener(mTextWarcher);
            resturent_promociones.setOnClickListener(addToPromociones);
            resturent_lv.setOnItemClickListener(addOmItemClickListener);
            getbanners();
            return view;
        } else {
            return view;
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onPause() {
        super.onPause();
        send_id = b.getString("IdSector");
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        AppController.getInstance().cancelPendingRequests(ApiRequest);
        AppController.getInstance().cancelPendingRequests(ApiRequestBanner);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public void showSearchView() {

       /* if (isSearchChecked == true) {
            DashBoardActivity.nav_search.setImageResource(R.drawable.search_orange);
            resturent_searchBox.setVisibility(View.VISIBLE);
            isSearchChecked = false;
        } else {
            DashBoardActivity.nav_search.setImageResource(R.drawable.search_blue);
            resturent_searchBox.setVisibility(View.GONE);
            isSearchChecked = true;
            if (restaurantSearchResultAdapter != null) {
                resturent_searchBox.setText("");
                restaurantSearchResultAdapter.filter("");
            }
            KeyboadSetting.hideKeyboard(context, resturent_searchBox);
        }*/
    }

    private void showProgressBar() {
        resturent_progressBar.setVisibility(View.VISIBLE);
        animation = AnimationUtils.loadAnimation(context, R.anim.progress_anim);
        animation.setDuration(1000);
        resturent_progressBar.startAnimation(animation);
    }

   /* public class RestaurantDataListener{
        public void onSuccess(String responseString){
            hideProgressbar();
        }
        public void onError(String responseString){
            hideProgressbar();
            UtilityMessage.showMessage(context,responseString);
        }
    }*/

    private void hideProgressbar() {
        if (animation != null) {
            resturent_progressBar.clearAnimation();
            resturent_progressBar.setVisibility(View.GONE);
        }
    }

    private void getRestaurantData() {
        String idse = SessionManager.get_rastaurant_id(context);
        String url = API.restaurants_list + idse;
        //Log.e("Search Url", "" + url);
        showProgressBar();


        GlobalValues.getMethodManagerObj(context).makeStringReqWithoutCache(url, ApiRequest, new MethodListener() {
            @Override
            public void onError() {
                hideProgressbar();
            }

            @Override
            public void onError(String response) {
                hideProgressbar();
                UtilityMessage.showMessage(context, response);
            }

            @Override
            public void onSuccess(String response) {
                hideProgressbar();
                //response = response.substring(1, response.length() - 1);
                jsonParserLocal(response);

            }
        });
    }


    private void getbanners() {

        showProgressBar();

        String url = API.listDeBanner;
        GlobalValues.getMethodManagerObj(context).makeStringReq(url, ApiRequest, new MethodListener() {
            @Override
            public void onError() {
                hideProgressbar();
            }

            @Override
            public void onError(String response) {
                hideProgressbar();
                UtilityMessage.showMessage(context, response);
            }

            @Override
            public void onSuccess(String response) {
                hideProgressbar();
                //response = response.substring(1, response.length() - 1);

                ArrayList<Banners> banners_list = new ArrayList<Banners>();
                try {
                    JSONObject jsonObject = new JSONObject(response);

                    JSONArray banner_ary = jsonObject.getJSONArray("banners");
                    for (int p = 0 ; p<banner_ary.length() ; p++){
                        JSONObject jsonObject1 = banner_ary.optJSONObject(p);
                        banners_list.add(new Banners(jsonObject1.optString("Bid") , jsonObject1.optString("Image_url"))) ;
                    }
                    init(banners_list);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
    }

    private void jsonParserLocal(String responseString) {
        alrestaurantListModels = new ArrayList<>();
        try {
            JSONObject object = new JSONObject(responseString);
            JSONArray mArray = object.optJSONArray("restaurants");
            if (mArray != null) {

                for (int i = 0; i < mArray.length(); i++) {
                    JSONObject mObj = mArray.optJSONObject(i);
                    RestaurantListModel restaurantListModel = new RestaurantListModel();
                    restaurantListModel.setIdRestaurante(mObj.optInt("IdRestaurante"));
                    restaurantListModel.setIdRestaLocal(mObj.optInt("IdRestaLocal"));
                    restaurantListModel.setIdSector(mObj.getInt("IdSector"));
                    restaurantListModel.setNombreRestaurante(mObj.optString("NombreRestaurante"));
                    restaurantListModel.setNombreLocal(mObj.optString("NombreLocal"));
                    restaurantListModel.setTelefono(mObj.optString("Telefono"));
                    restaurantListModel.setDireccion(mObj.optString("Direccion"));
                    restaurantListModel.setImagenLocal(mObj.optString("ImagenLocal"));
                    restaurantListModel.setMinimo(mObj.optString("Minimo"));
                    restaurantListModel.setTiempoEntrega(mObj.optString("TiempoEntrega"));
                    restaurantListModel.setTipoComida(mObj.optString("TipoComida"));
                    restaurantListModel.setInfoGeneral(mObj.optString("InfoGeneral"));
                    restaurantListModel.setDireccionWeb(mObj.optString("DireccionWeb"));
                    restaurantListModel.setFacebook(mObj.optString("Facebook"));
                    restaurantListModel.setTwitter(mObj.optString("Twitter"));
                    restaurantListModel.setInstagram(mObj.optString("Instagram"));
                    restaurantListModel.setHoraApertura(mObj.optString("HoraApertura"));
                    restaurantListModel.setHoraCierre(mObj.optString("HoraCierre"));
                    restaurantListModel.setTotalEstado(mObj.optString("TotalEstado"));
                    restaurantListModel.setActivo(mObj.optString("Activo"));
                    restaurantListModel.setRetira(mObj.optString("Retira"));

                    if(mObj.isNull("TipoModelo2Por1")){
                        restaurantListModel.setTipoModelo2Por1("3");
                    }else{
                        restaurantListModel.setTipoModelo2Por1(mObj.optString("TipoModelo2Por1"));
                    }

                    if(mObj.isNull("TipoModeloAdicional2Por1")){
                        restaurantListModel.setTipoModeloAdicional2Por1("1");
                    }else{
                        restaurantListModel.setTipoModeloAdicional2Por1(mObj.optString("TipoModeloAdicional2Por1"));
                    }

                    restaurantListModel.setCostoEnvio(mObj.optString("CostoEnvio"));
                    restaurantListModel.setRating(mObj.optInt("Rating"));

                    JSONArray mCategories = mObj.optJSONArray("Categories");
                    if (mCategories != null) {

                        ArrayList<RestaurantMenuList> arrayList = new ArrayList<RestaurantMenuList>();

                        for (int j = 0; j < mCategories.length(); j++) {

                            JSONObject mCatObj = mCategories.optJSONObject(j);

                            // if (mCatObj.optString("Estado").equals("A")) {
                            RestaurantMenuList restaurantMenuList = new RestaurantMenuList();
                            restaurantMenuList.setIdRestaCategoria(mCatObj.optInt("IdRestaCategoria"));
                            restaurantMenuList.setIdRestaurante(mCatObj.optInt("IdRestaurante"));
                            restaurantMenuList.setNombre(mCatObj.optString("Nombre"));
                            restaurantMenuList.setPosicion(mCatObj.optString("Posicion"));
                            restaurantMenuList.setSiglas(mCatObj.optString("Siglas"));
                            restaurantMenuList.setImagenRestaCategoria(mCatObj.optString("ImagenRestaCategoria"));
                            restaurantMenuList.setEstado(mCatObj.optString("Estado"));
                            restaurantMenuList.setNombreRestaurante(mCatObj.optString("NombreRestaurante"));

                            arrayList.add(restaurantMenuList);

                        }
                        restaurantListModel.setArrayList(arrayList);
                    }

                    alrestaurantListModels.add(restaurantListModel);
                }

                Collections.sort(alrestaurantListModels, new Comparator<RestaurantListModel>() {
                    @Override
                    public int compare(RestaurantListModel o1, RestaurantListModel o2) {
                        return o1.getActivo().compareTo(o2.getActivo());
                    }
                });

                restaurantResultAdapter = new RestaurantResultAdapter(context, alrestaurantListModels);
                resturent_lv.setAdapter(restaurantResultAdapter);
            }

        } catch (JSONException e) {
            Log.e(TAG, "" + e.toString());
            UtilityMessage.showMessage(context, responseString);
        }
    }

    public void init( ArrayList<Banners> banners_list ) {

        mDemoSlider.removeAllSliders();
        for (int i = 0; i < banners_list.size(); i++) {
            TextSliderView textSliderView = new TextSliderView(getActivity());
            textSliderView
                    .image(banners_list.get(i).getImageurl())
                    .setScaleType(BaseSliderView.ScaleType.CenterCrop)
                    .setOnSliderClickListener(this);

            mDemoSlider.addSlider(textSliderView);
        }
        mDemoSlider.setPresetTransformer(SliderLayout.Transformer.Stack);
        mDemoSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        mDemoSlider.setCustomAnimation(new DescriptionAnimation());
        mDemoSlider.setDuration(5000);
        mDemoSlider.addOnPageChangeListener(this);

    }

    @Override
    public void onSliderClick(BaseSliderView slider) {

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
