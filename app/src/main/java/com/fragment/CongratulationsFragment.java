package com.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.activity.DashBoardActivity;
import com.app.grupodeliveryec.deliveryEC.R;
import com.manager.AddToCartManager;
import com.utility.AppController;

/**
 * Created by administrator on 28/3/17.
 */

public class CongratulationsFragment extends Fragment{

    LinearLayout cong_back;
    View view;

    String TAG = getClass().getName();

    private static Context mContext;
    private static FragmentManager fm;
    private static Bundle b;
    private static Fragment fragment;
    AddToCartManager addToCartManager;

    public static Fragment getInstance(Context context, FragmentManager fragmentManager, Bundle bundle){
        fragment = new CongratulationsFragment();
        mContext = context;
        fm = fragmentManager;
        b = bundle;
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        AppController.getInstance().setErrorMessage();

        view = inflater.inflate(R.layout.congratulations_xml,container,false);
        addToCartManager = new AddToCartManager(mContext);
        DashBoardActivity.hideMenuPanel();

        cong_back = (LinearLayout)view.findViewById(R.id.cong_back);

        cong_back.setOnClickListener(onBackListener);
        addToCartManager.dropCart();
        DashBoardActivity.hideBackButton();
        return view;
    }



    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    View.OnClickListener onBackListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            /**
             * Remove all the fragment from Backstack
             */
            DashBoardActivity.popBackStack();
            DashBoardActivity.removeAllFragment();
        }
    };
}
