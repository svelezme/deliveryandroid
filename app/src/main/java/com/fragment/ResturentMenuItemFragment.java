package com.fragment;


import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.view.ContextThemeWrapper;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.activity.DashBoardActivity;
import com.adapter.AdditionalProductAdapter;
import com.adapter.ItemOfRestaurantAdapter;
import com.adapter.PreciosAdapter;
import com.app.grupodeliveryec.deliveryEC.R;
import com.com.rest.API;
import com.com.rest.GlobalValues;
import com.com.rest.MethodListener;
import com.github.siyamed.shapeimageview.RoundedImageView;
import com.inputview.GothamBookEditTextView;
import com.listener.UpdateMessage;
import com.manager.AddToCartManager;
import com.model.ItemAdditionalProduct;
import com.model.ItemProductPrice;
import com.model.RestaurantListModel;
import com.model.RestaurentMenuItem;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.utility.AppController;
import com.utility.Config;
import com.utility.FragmentIDs;
import com.utility.MessageDialog;
import com.utility.UtilityMessage;
import com.view.BebaseNeueTextView;
import com.view.ExoBoldItalicTextView;
import com.view.ExtraBoldItalicTextView;
import com.view.GothamBoldTextView;
import com.view.GothamBookTextView;
import com.view.MediumTextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;
import java.util.TreeSet;

/**
 * Created by administrator on 31/1/17.
 */

public class ResturentMenuItemFragment extends Fragment implements UpdateMessage.UpdateMessageListener {

    final int sdk = android.os.Build.VERSION.SDK_INT;
    private static Context mContext;
    private static FragmentManager fm;
    private static Bundle b;
    private static Fragment fragment;
    String apiProductDetails = "apiProductDetails";
    RoundedImageView info_restImageview;
    ExtraBoldItalicTextView info_nameOfRestaurant;
    ExoBoldItalicTextView rest_list_openStatus;
    BebaseNeueTextView rest_list_minimo, rest_list_costodeenvio, rest_list_tempoentrega;
    LinearLayout menu_LinearMenu, menu_LinearOpiniones, menu_Linearinformacion;
    View menu_MenuLine, menu_OpinionesLine, menu_InformacionLine;
    TextView menuitem_backToWork;
    MediumTextView info_typeOfFood;
    ImageView menu_progressBar;
    ListView menuitem_lv;
    LinearLayout menuitem_noItemHasCart;
    Animation animation;
    View view;
    int idRestaLocal;
    String NombreRestaurante;
    String NombreLocal;
    String TipoComida;
    String ImagenLocal;
    int idRestaCategoria;
    RestaurantListModel restaurantListModel;
    String Nombre = null;
    ItemOfRestaurantAdapter itemOfRestaurantAdapter;
    int product_qty = 1;
    float totalPrice;
    float basePrice = 0.0f;
    boolean isNORMAL = false;
    float isBasePrice = 0.0f;
    boolean isPreciousValidation = false;

    //ItemProductPrice isNormal_ItemProductPrice;
    MessageDialog messageDialog;
    //Add to cart quantity
    GothamBookEditTextView addToCart_qty;
    GothamBookTextView addToCart_totalPrice;
    ListView addToCart_addAdditionallv;
    ListView addToCart_Precioslv;
    ScrollView addTocart_scrollView;
    String idRestaProducto;
    ArrayList<ItemProductPrice> alPrice;
    ArrayList<ItemAdditionalProduct> alProduct;
    ArrayList<RestaurentMenuItem> alItem;
    //Set a final TreeMap for Precios Data
    TreeMap<String, ArrayList<ItemProductPrice>> alSortPrecios;
    TreeMap<String, ArrayList<ItemAdditionalProduct>> additionalProduct;
    PreciosAdapter preciosAdapter;
    AdditionalProductAdapter additionalProductAdapter;
    AddToCartManager addToCartManager;
    DecimalFormat decimalFormat;
    RatingBar rest_ratingBar;

    UpdateMessage updateMessage;

    View.OnClickListener addToMenu = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Bundle bundle = new Bundle();
            bundle.putSerializable("restaurantModel", b.getSerializable("restaurantListModel"));
            DashBoardActivity.displayFragment(FragmentIDs.ResturentMenuFragment_Id, bundle);
        }
    };

    int triggerEveent = 0;
    private String TAG = getClass().getName();

    View.OnClickListener addMenuLayout = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            menu_MenuLine.setVisibility(View.VISIBLE);
            menu_OpinionesLine.setVisibility(View.GONE);
            menu_InformacionLine.setVisibility(View.GONE);
        }
    };

    View.OnClickListener addInformacionLayout = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            menu_MenuLine.setVisibility(View.GONE);
            menu_OpinionesLine.setVisibility(View.GONE);
            menu_InformacionLine.setVisibility(View.VISIBLE);
        }
    };

    View.OnClickListener addOpinionesLayout = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            menu_MenuLine.setVisibility(View.GONE);
            menu_OpinionesLine.setVisibility(View.VISIBLE);
            menu_InformacionLine.setVisibility(View.GONE);
        }
    };

    AdapterView.OnItemClickListener mOnItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, final int i, long l) {

            new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                @Override
                public void run() {
                    clearPrice();
                    if (alItem != null && alItem.size() > 0) {

                        final int key = alItem.get(i).getIdRestaProducto();

                        Log.d("ayy", ""+key);
                        final String Nombre = alItem.get(i).getNombre();
                        final String Descripcion = alItem.get(i).getDescripcion();
                        final String ImagenRestaProducto = alItem.get(i).getImagenRestaProducto();
                        final String EsDia2x1 = alItem.get(i).getEsDia2x1();
                        final String Dias2Por1 = alItem.get(i).getDias2Por1();
                        final String TipoModelo2Por1 = restaurantListModel.getTipoModelo2Por1();
                        final String TipoModelAdicional2Por1 = restaurantListModel.getTipoModeloAdicional2Por1();


                        //Log.e("key", "" + key);

                        alSortPrecios = new TreeMap<>();
                        additionalProduct = new TreeMap<>();
                        basePrice = 0.0f;

                        /**
                         * Key for Product Precio,aditional
                         */

                        final ArrayList<ItemProductPrice> alPrecios = new ArrayList<ItemProductPrice>();

                        if (alPrice != null && alPrice.size() > 0) {
                            for (int i = 0; i < alPrice.size(); i++) {
                                if (alPrice.get(i).getIdRestaProducto() == key) {
                                    alPrecios.add(alPrice.get(i));
                                }
                            }
                        }

                        if (alPrecios != null && alPrecios.size() > 0 && !alPrecios.equals("")) {
                            makeItPrecios(alPrecios);
                        } else {
                            if (isNORMAL == false) {
                                basePrice = alItem.get(i).getPrecio();
                            }
                            alSortPrecios.clear();
                        }

                        final ArrayList<ItemAdditionalProduct> alItemAdditional = new ArrayList<ItemAdditionalProduct>();

                        if (alProduct != null && alProduct.size() > 0) {
                            for (int i = 0; i < alProduct.size(); i++) {
                                if (alProduct.get(i).getIdRestaProducto() == key) {
                                    alItemAdditional.add(alProduct.get(i));
                                }
                            }
                        }

                        if (alItemAdditional != null && alItemAdditional.size() > 0 && !alItemAdditional.equals("")) {
                            makeItAditional(alItemAdditional);
                        } else {
                            additionalProduct.clear();
                        }

                        if (alSortPrecios != null && alSortPrecios.size() == 0 && additionalProduct != null && additionalProduct.size() == 0) {

                            new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                                @Override
                                public void run() {

                                    if (TipoModelo2Por1.equals("3")) {
                                        addToModelo3_WithOutAdicional(Nombre, Descripcion, ImagenRestaProducto,
                                                key, EsDia2x1, i, TipoModelo2Por1,
                                                TipoModelAdicional2Por1,
                                                alPrecios, alItemAdditional);
                                    } else {
                                        addTCardService_withoutadd(Nombre, Descripcion, ImagenRestaProducto,
                                                key, EsDia2x1, i, TipoModelo2Por1,
                                                TipoModelAdicional2Por1,
                                                alPrecios, alItemAdditional);
                                    }

                                    if (EsDia2x1.equals("S")) {
                                        isPreciousValidation = true;
                                        setMessageErrorDialog(mContext.getResources().getString(R.string.es2msg));
                                        Log.d("ayy", "1");

                                    }
                                }
                            }, 250);

                        } else if (alSortPrecios != null && alSortPrecios.size() > 0 && additionalProduct != null && additionalProduct.size() == 0) {

                            new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                                @Override
                                public void run() {

                                    if (TipoModelo2Por1.equals("3")) {
                                        addToModelo3_Adicional(Nombre, Descripcion, ImagenRestaProducto,
                                                key, EsDia2x1, i,
                                                TipoModelo2Por1,
                                                TipoModelAdicional2Por1,
                                                alPrecios, alItemAdditional);
                                    } else {
                                        addTCardService(Nombre, Descripcion, ImagenRestaProducto,
                                                key, EsDia2x1, i,
                                                TipoModelo2Por1,
                                                TipoModelAdicional2Por1,
                                                alPrecios, alItemAdditional);
                                    }
                                    if (EsDia2x1.equals("S")) {
                                        isPreciousValidation = true;
                                        setMessageErrorDialog(mContext.getResources().getString(R.string.es2msg));
                                        Log.d("ayy", "2");

                                    }
                                }
                            }, 250);
                        }

                        if (additionalProduct != null && additionalProduct.size() > 0) {
                            new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                                @Override
                                public void run() {

                                    if (TipoModelo2Por1.equals("3")) {
                                        addToModelo3_Adicional(Nombre, Descripcion, ImagenRestaProducto,
                                                key, EsDia2x1, i,
                                                TipoModelo2Por1,
                                                TipoModelAdicional2Por1,
                                                alPrecios, alItemAdditional);
                                    } else {
                                        addTCardService(Nombre, Descripcion,
                                                ImagenRestaProducto,
                                                key, EsDia2x1, i, TipoModelo2Por1,
                                                TipoModelAdicional2Por1,
                                                alPrecios, alItemAdditional);
                                    }
                                    if (EsDia2x1.equals("S")) {
                                        isPreciousValidation = true;
                                        setMessageErrorDialog(mContext.getResources().getString(R.string.es2msg));
                                        Log.d("ayy", "3");

                                    }
                                }
                            }, 250);
                        }
                    }
                }
            }, 500);
        }
    };

    private String apiRequestMenuItem = "apiRequestMenuItem";

    /**
     * @param context
     * @param fragmentManager
     * @param bundle
     * @return
     */

    public static Fragment getInstance(Context context, FragmentManager fragmentManager, Bundle bundle) {
        fragment = new ResturentMenuItemFragment();
        mContext = context;
        fm = fragmentManager;
        b = bundle;
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        updateMessage = new UpdateMessage(this);

        decimalFormat = doNumberFormatter();
        addToCartManager = new AddToCartManager(mContext);

        idRestaLocal = b.containsKey("idRestaLocal") ? b.getInt("idRestaLocal") : 0;

        NombreRestaurante = b.getString("NombreRestaurante");
        NombreLocal = b.getString("NombreLocal");
        TipoComida = b.getString("TipoComida");
        ImagenLocal = b.getString("ImagenLocal");
        idRestaCategoria = b.getInt("idRestaCategoria");
        Nombre = b.getString("Nombre");
        restaurantListModel = (RestaurantListModel) b.getSerializable("restaurantListModel");

        String output = Nombre.substring(0, 1).toUpperCase() + Nombre.substring(1).toLowerCase();

        DashBoardActivity.updateTitle(output);
        DashBoardActivity.showBackButton();
        DashBoardActivity.hideInfo();
        DashBoardActivity.hideSearchButton();
        DashBoardActivity.hideCerrar();
        DashBoardActivity.hideHorizentalBar();
        DashBoardActivity.hideLogout();
        DashBoardActivity.hideAddDeliveryAddress();
        DashBoardActivity.showMenuPanel();

        view = inflater.inflate(R.layout.resturent_menu_item, container, false);
        info_restImageview = (RoundedImageView) view.findViewById(R.id.info_restImageview);
        info_nameOfRestaurant = (ExtraBoldItalicTextView) view.findViewById(R.id.info_nameOfRestaurant);
        info_typeOfFood = (MediumTextView) view.findViewById(R.id.info_typeOfFood);
        rest_list_openStatus = (ExoBoldItalicTextView) view.findViewById(R.id.rest_list_openStatus);
        menuitem_lv = (ListView) view.findViewById(R.id.menuitem_lv);

        rest_list_minimo = (BebaseNeueTextView) view.findViewById(R.id.rest_list_minimo);
        rest_list_costodeenvio = (BebaseNeueTextView) view.findViewById(R.id.rest_list_costodeenvio);
        rest_list_tempoentrega = (BebaseNeueTextView) view.findViewById(R.id.rest_list_tempoentrega);

        menu_LinearMenu = (LinearLayout) view.findViewById(R.id.menu_LinearMenu);
        menu_LinearOpiniones = (LinearLayout) view.findViewById(R.id.menu_LinearOpiniones);
        menu_Linearinformacion = (LinearLayout) view.findViewById(R.id.menu_Linearinformacion);

        menu_MenuLine = (View) view.findViewById(R.id.menu_MenuLine);
        menu_OpinionesLine = (View) view.findViewById(R.id.menu_OpinionesLine);
        menu_InformacionLine = (View) view.findViewById(R.id.menu_InformacionLine);

        menu_MenuLine.setVisibility(View.VISIBLE);

        menu_progressBar = (ImageView) view.findViewById(R.id.menu_progressBar);
        menuitem_backToWork = (TextView) view.findViewById(R.id.menuitem_backToWork);
        menuitem_noItemHasCart = (LinearLayout) view.findViewById(R.id.menuitem_noItemHasCart);
        rest_ratingBar = (RatingBar) view.findViewById(R.id.rest_ratingBar);
        menu_LinearMenu.setOnClickListener(addMenuLayout);
        menu_LinearOpiniones.setOnClickListener(addOpinionesLayout);
        menu_Linearinformacion.setOnClickListener(addInformacionLayout);

        menuitem_backToWork.setOnClickListener(addToMenu);

        int qty = addToCartManager.getTotalQuntity();
        if (qty > 0) {
            DashBoardActivity.showQuntity();
            DashBoardActivity.setQuntity(qty);
        } else {
            DashBoardActivity.hideQuntity();
        }
        setData();
        getMenuItem();
        menuitem_lv.setOnItemClickListener(mOnItemClickListener);
        DashBoardActivity.menuButtonLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DashBoardActivity.popBackStack();
            }
        });

        DashBoardActivity.cartButtonLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int totalSum = addToCartManager.getTotalQuntity();
                if (totalSum > 0) {
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("restaurantModel", b.getSerializable("restaurantListModel"));
                    // bundle.putInt("screenStatus",1);
                    bundle.putInt("idRestaLocal", idRestaLocal);
                    bundle.putString("NombreRestaurante", NombreRestaurante);
                    bundle.putString("NombreLocal", NombreLocal);
                    bundle.putString("TipoComida", TipoComida);
                    bundle.putString("ImagenLocal", ImagenLocal);
                    bundle.putInt("idRestaCategoria", idRestaCategoria);
                    bundle.putString("Nombre", Nombre);

                    Config.orderScreenStatus = 1;

                    DashBoardActivity.cartButtonLayout.setTag(R.id.orderData, bundle);

                    DashBoardActivity.displayFragment(FragmentIDs.OrderFragment_Id, bundle);
                } else {
                    menuitem_noItemHasCart.setVisibility(View.VISIBLE);
                    menuitem_lv.setVisibility(View.GONE);
                }
            }

        });

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        AppController.getInstance().cancelPendingRequests(apiRequestMenuItem);
        AppController.getInstance().cancelPendingRequests(apiProductDetails);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void showProgressBar() {
        menu_progressBar.setVisibility(View.VISIBLE);
        animation = AnimationUtils.loadAnimation(mContext, R.anim.progress_anim);
        animation.setDuration(1000);
        menu_progressBar.startAnimation(animation);
    }

    private void hideProgressbar() {
        if (animation != null) {
            menu_progressBar.clearAnimation();
            menu_progressBar.setVisibility(View.GONE);
        }
    }

    private int getDisplayHeight() {
        int height = 480;
        try {
            DisplayMetrics displaymetrics = new DisplayMetrics();
            getActivity().getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
            height = displaymetrics.heightPixels;
            int width = displaymetrics.widthPixels;
        } catch (NullPointerException e) {
            Log.e(TAG, "" + e.toString());
        }
        return height;
    }

    private void setData() {
        Picasso.with(mContext).load("http://www.deliveryec.com/images/app/" + ImagenLocal)
                .into(info_restImageview, new Callback() {
                    @Override
                    public void onSuccess() {
                    }

                    @Override
                    public void onError() {
                    }
                });

        info_nameOfRestaurant.setText(NombreRestaurante + " - " + NombreLocal);
        info_typeOfFood.setText(TipoComida);

        if (restaurantListModel.getActivo().equals("I")) {
            rest_list_openStatus.setText(mContext.getResources().getString(R.string.CERRADO));

            if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                rest_list_openStatus.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.info_close_status_background));
            } else {
                rest_list_openStatus.setBackground(mContext.getResources().getDrawable(R.drawable.info_close_status_background));
            }
        } else {
            rest_list_openStatus.setText(mContext.getResources().getString(R.string.OPEN));

            if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                rest_list_openStatus.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.info_open_status_background));
            } else {
                rest_list_openStatus.setBackground(mContext.getResources().getDrawable(R.drawable.info_open_status_background));
            }
        }
        try {
            rest_ratingBar.setRating(restaurantListModel.getRating());
        } catch (Exception ec) {
        }
        rest_list_minimo.setText("$ " + restaurantListModel.getMinimo());
        rest_list_costodeenvio.setText("$ " + restaurantListModel.getCostoEnvio());
        rest_list_tempoentrega.setText(restaurantListModel.getTiempoEntrega());
    }

    private void getMenuItem() {
        // String url = API.restaurantItemMenu + "pIdCategoria=" + idRestaCategoria + "&pIdRestaLocal=" + idRestaLocal + "&callback=";

        String url = API.restaurants_list_item + "IdRestaCategoria=" + idRestaCategoria + "&IdRestaLocal=" + idRestaLocal + "&callback=";

        Log.e("Restaurant Item", "" + url);
        showProgressBar();
        GlobalValues.getMethodManagerObj(mContext).makeStringReqWithoutCache(url, apiRequestMenuItem, new MethodListener() {
            @Override
            public void onError() {
            }

            @Override
            public void onError(String response) {
                hideProgressbar();
            }

            @Override
            public void onSuccess(String response) {

                // response = response.substring(1, response.length() - 1);
                try {
                    JSONObject mObj = new JSONObject(response);
                    setMenuItem(mObj);
                } catch (JSONException e) {
                    hideProgressbar();
                    Log.e(TAG, "" + e.toString());
                }
            }
        });
    }

    private void setMenuItem(JSONObject mObj) {
        JSONArray mArray = mObj.optJSONArray("products");
        alItem = new ArrayList<>();

        hideProgressbar();

        if (mArray != null) {
            idRestaProducto = null;
            for (int i = 0; i < mArray.length(); i++) {
                JSONObject object = mArray.optJSONObject(i);
                if (object.optString("Estado").toUpperCase(Locale.ENGLISH).equals("A")) {

                    RestaurentMenuItem restaurentMenuItem = new RestaurentMenuItem();
                    restaurentMenuItem.setIdRestaProducto(object.optInt("idRestaProducto"));

                    idRestaProducto += object.optInt("idRestaProducto") + ",";

                    restaurentMenuItem.setIdRestaCategoria(object.optInt("IdRestaCategoria"));
                    restaurentMenuItem.setIdRestaurante(object.optInt("IdRestaurante"));
                    restaurentMenuItem.setIdRestaLocal(object.optInt("IdRestaLocal"));
                    restaurentMenuItem.setNombre(object.optString("Nombre"));
                    restaurentMenuItem.setDescripcion(object.optString("Descripcion"));
                    restaurentMenuItem.setDiasSemana(object.optString("DiasSemana"));
                    restaurentMenuItem.setDias2Por1(object.optString("Dias2Por1"));

                    restaurentMenuItem.setSiglas(object.optString("Siglas"));
                    restaurentMenuItem.setImagenRestaProducto(object.optString("ImagenRestaProducto"));
                    restaurentMenuItem.setImageValid(true);
                    restaurentMenuItem.setImagenRestaProductoGrande(object.optString("ImagenRestaProductoGrande"));
                    restaurentMenuItem.setEstado(object.optString("Estado"));

                    if (!object.isNull("Precio")) {
                        restaurentMenuItem.setPrecio(Float.parseFloat(object.optString("Precio").replace(",", ".")));
                    } else {
                        restaurentMenuItem.setPrecio(0.0f);
                    }

                    restaurentMenuItem.setValidaMultiPrecios(object.optInt("ValidaMultiPrecios"));
                    restaurentMenuItem.setEsDia2x1(object.optString("EsDia2x1").toUpperCase(Locale.ENGLISH));

                    if (object.optString("EsDia2x1").toUpperCase(Locale.ENGLISH).equals("S")) {
                        restaurentMenuItem.setEs2Status(1);
                    } else {
                        restaurentMenuItem.setEs2Status(0);
                    }

                    restaurentMenuItem.setValidaAdicionales(object.optInt("ValidaAdicionales"));
                    restaurentMenuItem.setNombreCategoria(object.optString("NombreCategoria"));
                    restaurentMenuItem.setNombreLocal(object.optString("NombreLocal"));
                    restaurentMenuItem.setNombreRestaurante(object.optString("NombreRestaurante"));

                    alItem.add(restaurentMenuItem);
                }
            }

            Collections.sort(alItem, new Comparator<RestaurentMenuItem>() {
                @Override
                public int compare(RestaurentMenuItem restaurentMenuItem, RestaurentMenuItem t1) {
                    return restaurentMenuItem.getPrecio().compareTo(t1.getPrecio());
                }
            });

            int height = getDisplayHeight();

            if (idRestaProducto != null && !idRestaProducto.equals("null") && !idRestaProducto.equals("")) {
                idRestaProducto = idRestaProducto.replace("null", "");
                idRestaProducto = idRestaProducto.substring(0, idRestaProducto.length() - 1);
                showProgressBar();
                getProductDetails();

            }

            itemOfRestaurantAdapter =
                    new ItemOfRestaurantAdapter(mContext, alItem, height,
                            ImagenLocal, NombreRestaurante, NombreLocal,
                            TipoComida, fm);
            menuitem_lv.setAdapter(itemOfRestaurantAdapter);
        }
    }

    private void getProductDetails() {
        String url = API.ProductDetails_New + idRestaProducto;
        //Log.e("product_details", "" + url);
        GlobalValues.getMethodManagerObj(mContext).makeStringReqWithoutCache(url, apiProductDetails, new MethodListener() {
            @Override
            public void onError() {
            }

            @Override
            public void onError(String response) {
                hideProgressbar();
            }

            @Override
            public void onSuccess(String response) {
                hideProgressbar();
                try {
                    JSONObject mObj = new JSONObject(response);
                    JSONArray mPrecious = mObj.optJSONArray("ProductoPrecios");
                    JSONArray mAdicionales = mObj.optJSONArray("ProductoAdicionales");

                    if (mPrecious != null) {
                        displayProductPrecious(mPrecious);
                    }

                    if (mAdicionales != null) {
                        getProductoAdicionales(mAdicionales);
                    }
                } catch (JSONException e) {
                    hideProgressbar();
                    Log.e(TAG, "" + e.toString());
                }
            }
        });
    }

    private void displayProductPrecious(JSONArray mPrecious) {
        for (int i = 0; i < mPrecious.length(); i++) {
            JSONArray mArray = mPrecious.optJSONArray(i);
            if (mArray != null && mArray.length() > 0) {
                alPrice = new ArrayList<>();
                for (int j = 0; j < mArray.length(); j++) {
                    JSONObject priceObject = mArray.optJSONObject(j);

                    if (priceObject.optString("Estado").toUpperCase(Locale.ENGLISH).equals("A")) {

                        ItemProductPrice itemProductPrice = new ItemProductPrice();
                        itemProductPrice.setIdRestaProdPrecio(priceObject.optInt("IdRestaProdPrecio"));
                        itemProductPrice.setIdRestaProducto(priceObject.optInt("IdRestaProducto"));
                        itemProductPrice.setGrupo(priceObject.optString("Grupo"));
                        itemProductPrice.setMedida(priceObject.optString("Medida").toUpperCase(Locale.ENGLISH));

                        if (!priceObject.isNull("Precio")) {
                            itemProductPrice.setPrecio(Float.parseFloat(priceObject.optString("Precio").replace(",", ".")));
                        } else {
                            itemProductPrice.setPrecio(0.0f);
                        }
                        itemProductPrice.setPosicion(priceObject.optInt("Posicion"));
                        itemProductPrice.setCantidad(priceObject.optInt("Cantidad"));
                        itemProductPrice.setEstado(priceObject.optString("Estado"));

                        alPrice.add(itemProductPrice);
                    }
                }
            }
        }
    }

    private void getProductoAdicionales(JSONArray mAdicionales) {
        // alItemAdicionales = new TreeMap<>();

        for (int i = 0; i < mAdicionales.length(); i++) {
            JSONArray mArray = mAdicionales.optJSONArray(i);

            if (mArray != null && mArray.length() > 0) {
                //int key = mArray.optJSONObject(0).optInt("IdRestaProducto");

                alProduct = new ArrayList<>();
                for (int j = 0; j < mArray.length(); j++) {
                    JSONObject object = mArray.optJSONObject(j);

                    //  if (object.optString("Estado").toUpperCase(Locale.ENGLISH).equals("A")) {

                    ItemAdditionalProduct product = new ItemAdditionalProduct();

                    product.setIdRelaRestaProduAdiciona(object.optInt("IdRelaRestaProduAdiciona"));
                    product.setIdRestaProducto(object.optInt("IdRestaProducto"));
                    product.setIdRestaExtradicional(object.optInt("IdRestaExtradicional"));
                    product.setNombre(object.optString("Nombre"));
                    product.setGrupo(object.optString("Grupo"));
                    product.setMedida(object.optString("Medida"));
                    product.setPrecio(object.optString("Precio"));
                    product.setPosicion(object.optInt("Posicion"));
                    product.setCantidad(object.optInt("Cantidad"));
                    product.setEstado(object.optString("Estado"));

                    alProduct.add(product);
                    // }

                }
                // alItemAdicionales.put(key, alProduct);
            }
        }
    }

    public void notifyList() {
        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {

                int h0 = 0, h1 = 0;
                if (addToCart_Precioslv != null) {
                    h0 = UtilityMessage.setListViewHeightBasedOnChildren(addToCart_Precioslv);
                }

                if (addToCart_addAdditionallv != null) {
                    addToCart_addAdditionallv.setVisibility(View.VISIBLE);
                    h1 = UtilityMessage.setListViewHeightBasedOnChildren(addToCart_addAdditionallv);

                    int x = addTocart_scrollView.getScrollX();
                    int y = addTocart_scrollView.getScrollY();
                    final int totalY = y + 60;

                    //Log.e("x",""+x);
                    //Log.e("y",""+y);
                    //Log.e("h0",""+h0);
                    //Log.e("h1",""+h1);
                    // Log.e("totalY",""+totalY);

                    addTocart_scrollView.post(new Runnable() {
                        @Override
                        public void run() {
                            addTocart_scrollView.scrollTo(0, totalY);
                        }
                    });
                }
            }
        }, 300);
    }

    private void makeItPrecios(ArrayList<ItemProductPrice> alPrecios) {
        if (alPrecios.size() == 1) {
            isNORMAL = true;
            isBasePrice = Float.valueOf(alPrecios.get(0).getPrecio());
            basePrice = Float.valueOf(alPrecios.get(0).getPrecio());
           // isNormal_ItemProductPrice = alPrecios.get(0);
        } else if (alPrecios.size() > 1) {

            int noOfBase = 0;
            int index =0;
            for(int i=0;i<alPrecios.size();i++){
                boolean isChecked = alPrecios.get(i).getGrupo().startsWith("BASE-");
                if(isChecked){
                    index = i;
                    noOfBase+=1;
                }
            }

            Log.e("index",""+index);
            Log.e("noOfBase",""+noOfBase);
            if(noOfBase==1){
                isNORMAL = true;
                isBasePrice = alPrecios.get(index).getPrecio();
                basePrice = alPrecios.get(index).getPrecio();
                alPrecios.remove(index);
            }


            TreeSet<String> altreeProdcut = new TreeSet<String>();
            for (ItemProductPrice precios : alPrecios) {
                boolean isChecked = precios.getGrupo().startsWith("BASE-");
                if (isChecked) {
                    try {
                        isNORMAL = true;
                        isBasePrice = 0.00f;
                        basePrice = 0.00f;
                       // isNormal_ItemProductPrice = precios;
                    } catch (NumberFormatException e) {
                        Log.e(TAG, "" + e.getMessage());
                    } catch (Exception e) {
                        Log.e(TAG, "" + e.getMessage());
                    }
                }
                altreeProdcut.add(precios.getGrupo());
            }

            Iterator<String> itr = altreeProdcut.iterator();

            while (itr.hasNext()) {
                String group = itr.next();
                ArrayList<ItemProductPrice> alIPP = new ArrayList<ItemProductPrice>();
                for (ItemProductPrice ipp : alPrecios) {
                    String matchGroupo = ipp.getGrupo();
                    if (group.equals(matchGroupo)) {
                        alIPP.add(ipp);
                    }
                }
                alSortPrecios.put(group, alIPP);
            }
        }
    }

    private void makeItAditional(ArrayList<ItemAdditionalProduct> alItemAdditional) {
        TreeSet<String> altreeAditional = new TreeSet<>();

        for (ItemAdditionalProduct aditional : alItemAdditional) {
            altreeAditional.add(aditional.getGrupo());
        }

        Iterator<String> itr = altreeAditional.iterator();

        while (itr.hasNext()) {
            String group = itr.next();
            ArrayList<ItemAdditionalProduct> alIap = new ArrayList<>();
            for (ItemAdditionalProduct aditional : alItemAdditional) {
                String matchGroupo = aditional.getGrupo();
                if (group.equals(matchGroupo)) {
                    alIap.add(aditional);
                }
            }

            Collections.sort(alIap, new Comparator<ItemAdditionalProduct>() {
                @Override
                public int compare(ItemAdditionalProduct itemAdditionalProduct, ItemAdditionalProduct t1) {
                    return itemAdditionalProduct.getPrecio().compareTo(t1.getPrecio());
                }
            });

            additionalProduct.put(group, alIap);
        }
    }

    int insertId;

    public void addTCardService(final String Nombre, final String Descripcion,
                                String ImagenRestaProducto,
                                final int idRestaProducto,
                                final String EsDia2x1, final int position,
                                final String TipoModelo2Por1,
                                final String TipoModelAdicional2Por1,
                                final ArrayList<ItemProductPrice> alPrecios,
                                final ArrayList<ItemAdditionalProduct> alItemAdditional) {

        triggerEveent = 0;

        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(R.layout.addtocart_xml_new);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));

        addToCart_Precioslv = (ListView) dialog.findViewById(R.id.addToCart_Precioslv);
        addToCart_addAdditionallv = (ListView) dialog.findViewById(R.id.addToCart_addAdditionallv);
        addTocart_scrollView = (ScrollView) dialog.findViewById(R.id.addTocart_scrollView);
        View addToCart_linetwo = (View) dialog.findViewById(R.id.addToCart_linetwo);
        ImageView back = (ImageView) dialog.findViewById(R.id.aboutus_toolbar_backButton);
        final GothamBookTextView addToCart_additionaltv = (GothamBookTextView) dialog.findViewById(R.id.addToCart_additionaltv);
        View addToCart_linethree = (View) dialog.findViewById(R.id.addToCart_linethree);

        addToCart_totalPrice = (GothamBookTextView) dialog.findViewById(R.id.addToCart_totalPrice);
        ImageButton addToCart_minus = (ImageButton) dialog.findViewById(R.id.addToCart_minus);
        ImageButton addToCart_plus = (ImageButton) dialog.findViewById(R.id.addToCart_plus);
        addToCart_qty = (GothamBookEditTextView) dialog.findViewById(R.id.addToCart_qty);

        LinearLayout addToCart_agregaralcarrito = (LinearLayout) dialog.findViewById(R.id.addToCart_agregaralcarrito);

        final RoundedImageView menuitem_restImageview1 = (RoundedImageView) dialog.findViewById(R.id.menuitem_restImageview);
        final GothamBoldTextView menuitem_nameOfRestaurant1 = (GothamBoldTextView) dialog.findViewById(R.id.menuitem_nameOfRestaurant);
        final MediumTextView menuitem_typeOfFood1 = (MediumTextView) dialog.findViewById(R.id.menuitem_typeOfFood);
        BebaseNeueTextView menuitem_price1 = (BebaseNeueTextView) dialog.findViewById(R.id.menuitem_price);

        menuitem_nameOfRestaurant1.setText(Nombre);
        menuitem_typeOfFood1.setText(Descripcion);

        Picasso.with(mContext).load("http://www.deliveryec.com/images/app/" + ImagenRestaProducto)
                .into(menuitem_restImageview1, new Callback() {
                    @Override
                    public void onSuccess() {
                        menuitem_restImageview1.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onError() {
                        menuitem_restImageview1.setVisibility(View.GONE);
                        //Add the height for dynamic view.
                        float scale = getContext().getResources().getDisplayMetrics().density;
                        int px = (int) (70 * scale + 0.5f);  // replace 70 with your dimensions

                        RelativeLayout rl = (RelativeLayout) dialog.findViewById(R.id.menuitem_relativelayout);
                        rl.getLayoutParams().height = px;

                       /* RelativeLayout r2 = (RelativeLayout)dialog.findViewById(R.id.menuitem_typeOfFood);
                        r2.getLayoutParams().height = px;*/

                        final RelativeLayout.LayoutParams layoutparams = (RelativeLayout.LayoutParams) menuitem_nameOfRestaurant1.getLayoutParams();
                        layoutparams.setMargins(0, 0, 0, 0);
                        menuitem_nameOfRestaurant1.setLayoutParams(layoutparams);

                        final RelativeLayout.LayoutParams layoutParams1 = (RelativeLayout.LayoutParams) menuitem_typeOfFood1.getLayoutParams();
                        layoutParams1.setMargins(0, 0, 0, 0);
                        menuitem_typeOfFood1.setLayoutParams(layoutParams1);

                        rl.invalidate();
                        //rl.getLayoutParams().width = px;

                    }
                });

        if (isNORMAL) {
            isNORMAL = false;
            totalPrice = Float.parseFloat(decimalFormat.format(basePrice));
        } else {
            totalPrice = Float.parseFloat(decimalFormat.format(totalPrice));
        }
        addToCart_totalPrice.setText("$ " + new DecimalFormat("0.00").format(totalPrice));

        addToCart_qty.setText("" + product_qty);

        //Add to Precious Data
        if (alSortPrecios != null && alSortPrecios.size() > 0) {
            addToCart_Precioslv.setVisibility(View.VISIBLE);
            preciosAdapter = new PreciosAdapter(mContext, alSortPrecios, fm);
            addToCart_Precioslv.setAdapter(preciosAdapter);

        } else {
            if (String.valueOf(basePrice).length() == 3) {
                String show = String.valueOf(basePrice) + "0";
                menuitem_price1.setText("$ " + show);
            } else {
                menuitem_price1.setText("$ " + Float.parseFloat(decimalFormat.format(basePrice)));
            }
            addToCart_Precioslv.setVisibility(View.GONE);
        }

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Config.alhmAditional.clear();
                triggerEveent = 0;
                totalPrice = 0.0f;
                product_qty = 1;
                dialog.dismiss();
            }
        });

        //Add to aditional product
        if (additionalProduct != null && additionalProduct.size() > 0) {
           // f Log.d("additionalproduct", additionalProduct.get(0).get(0).getNombre());
            addToCart_addAdditionallv.setVisibility(View.VISIBLE);
            additionalProductAdapter =
                    new AdditionalProductAdapter(mContext, additionalProduct, fm);

            addToCart_addAdditionallv.setAdapter(additionalProductAdapter);
            additionalProductAdapter.notifyDataSetChanged();

        } else {
            addToCart_addAdditionallv.setVisibility(View.GONE);
            addToCart_linetwo.setVisibility(View.GONE);
            addToCart_additionaltv.setVisibility(View.GONE);
            addToCart_linethree.setVisibility(View.GONE);
        }

        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                if (addToCart_Precioslv.getVisibility() == View.VISIBLE) {
                    UtilityMessage.setListViewHeightBasedOnChildren(addToCart_Precioslv);
                }

                if (addToCart_addAdditionallv.getVisibility() == View.VISIBLE) {
                    UtilityMessage.setListViewHeightBasedOnChildren(addToCart_addAdditionallv);
                }

            }
        }, 1000);


        addToCart_minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    if (product_qty > 1) {
                        product_qty = product_qty - 1;
                        totalPrice = product_qty * (Config.totalAditional + Config.totalPrecios + basePrice);
                        DecimalFormat decimalFormat = doNumberFormatter();
                        totalPrice = Float.parseFloat(decimalFormat.format(totalPrice));
                        addToCart_totalPrice.setText("$ " + new DecimalFormat("0.00").format(totalPrice));
                        addToCart_qty.setText("" + product_qty);
                    }

                } catch (NumberFormatException e) {
                    Log.e(TAG, "" + e.toString());
                } catch (Exception e) {
                    Log.e(TAG, "" + e.toString());
                }
            }
        });

        addToCart_plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                product_qty = product_qty + 1;
                totalPrice = product_qty * (Config.totalAditional + Config.totalPrecios + basePrice);
                DecimalFormat decimalFormat = doNumberFormatter();
                totalPrice = Float.parseFloat(decimalFormat.format(totalPrice));
                addToCart_totalPrice.setText("$ " + new DecimalFormat("0.00").format(totalPrice));
                addToCart_qty.setText("" + product_qty);
            }
        });

        addToCart_agregaralcarrito.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /**
                 * If Precios & adicionales both of then is greate
                 * Mean Precios is greater then 1 and adicional is greater then 0
                 * then add both of then and not any update process.
                 */
                Log.d("wwheu","dio");
                Log.d("totalprice",String.valueOf(totalPrice));
                /**
                 * Case 1
                 */
                if (Config.alhmPrecios != null && alSortPrecios != null && alSortPrecios.size() > 0) {
                    triggerEveent = 1;
                    if (Config.alhmPrecios.size() == 0) {
                        String[] mapKeys = alSortPrecios.keySet().toArray(new String[alSortPrecios.size()]);
                        setMessageErrorDialog("La opcion " + mapKeys[0].toUpperCase(Locale.ENGLISH).replace("BASE-", "") + " es requerida");
                        return;
                    } else {
                        int countOfKeys = 0;
                        StringBuilder key = new StringBuilder();

                        String[] mapKeys = alSortPrecios.keySet().toArray(new String[alSortPrecios.size()]);
                        for (Map.Entry m : Config.alhmPrecios.entrySet()) {
                            key.append("'" + m.getKey() + "'");
                            key.append(",");
                            countOfKeys = countOfKeys + 1;
                        }

                        //String outPutKeys = key.length() > 0 ? key.substring(0, key.length() - 1) : null;
                        //Log.v("outPutKeys",""+outPutKeys);

                        /**
                         * Check the conditions for all validation for true
                         */
                        if (mapKeys.length != countOfKeys) {
                            setMessageErrorDialog("La opcion " + mapKeys[countOfKeys].toUpperCase(Locale.ENGLISH) + " es requerida");
                        } else {
                            /**
                             * Check the adicionales if Precios is available but adicional is not available
                             */

                            // addToCartManager.insertProduct(Nombre, Descripcion, idRestaProducto, product_qty, totalPrice);
                            insertId = (int) addToCartManager.insertProduct(Nombre,
                                    Descripcion, idRestaProducto, product_qty,
                                    totalPrice, TipoModelo2Por1,
                                    TipoModelAdicional2Por1, EsDia2x1);

                            for (Map.Entry m : Config.alhmPrecios.entrySet()) {
                                /**
                                 * Comment: Insert Precios data the value but not update
                                 */
                                ItemProductPrice itm = (ItemProductPrice) m.getValue();
                                String key1 = (String) m.getKey();
                                boolean isBaseChecked = key1.startsWith("BASE-");
                                if (isBaseChecked) {
                                    isBasePrice = itm.getPrecio();
                                }
                                addToCartManager.insertPrecios(itm, 1, insertId);
                            }

                            if (Config.alhmAditional.size() > 0) {

                                Map<Integer, ItemAdditionalProduct> alMap = new HashMap<Integer, ItemAdditionalProduct>();

                                for (int l = 0; l < Config.alhmAditional.size(); l++) {


                                    if (alMap.containsKey(Config.alhmAditional.get(l).getIdRelaRestaProduAdiciona())) {
                                        ItemAdditionalProduct itm = (ItemAdditionalProduct) alMap.get(Config.alhmAditional.get(l).getIdRelaRestaProduAdiciona());
                                        int qty = itm.getAdiciona_qty();
                                        qty = qty + 1;
                                        itm.setAdiciona_qty(qty);

                                        alMap.put(Config.alhmAditional.get(l).getIdRelaRestaProduAdiciona(), itm);

                                    } else {
                                        ItemAdditionalProduct itm = Config.alhmAditional.get(l);
                                        itm.setAdiciona_qty(1);
                                        alMap.put(Config.alhmAditional.get(l).getIdRelaRestaProduAdiciona(), itm);
                                    }
                                }


                                for (Map.Entry m : alMap.entrySet()) {
                                    ItemAdditionalProduct itm = (ItemAdditionalProduct) m.getValue();
                                    addToCartManager.saveAdicionales(itm, insertId);
                                }
                            }
                            addQTY();
                            Config.alhmPrecios.clear();
                            Config.alhmAditional.clear();
                            dialog.dismiss();
                        }
                    }
                }

                /**
                 * Case 2
                 */

                if (Config.alhmAditional != null && triggerEveent == 0) {
                    if (additionalProduct != null && additionalProduct.size() > 0) {
                        triggerEveent = 1;
                        if (Config.alhmAditional.size() == 0 && additionalProduct.size() > 0) {
                            insertId = (int) addToCartManager.insertProduct(Nombre,
                                    Descripcion, idRestaProducto, product_qty,
                                    totalPrice, TipoModelo2Por1,
                                    TipoModelAdicional2Por1, EsDia2x1);
                            addQTY();
                            dialog.dismiss();
                        } else if (Config.alhmAditional.size() > 0 && additionalProduct.size() > 0) {
                            insertId = (int) addToCartManager.insertProduct(Nombre,
                                    Descripcion, idRestaProducto,
                                    product_qty, totalPrice,
                                    TipoModelo2Por1, TipoModelAdicional2Por1, EsDia2x1);

                            Map<Integer, ItemAdditionalProduct> alMap = new HashMap<Integer, ItemAdditionalProduct>();

                            for (int k = 0; k < Config.alhmAditional.size(); k++) {
                                //addToCartManager.insertAdicionales(Config.alhmAditional.get(k),product_qty);

                                if (alMap.containsKey(Config.alhmAditional.get(k).getIdRelaRestaProduAdiciona())) {
                                    ItemAdditionalProduct itm = (ItemAdditionalProduct) alMap.get(Config.alhmAditional.get(k).getIdRelaRestaProduAdiciona());
                                    int qty = itm.getAdiciona_qty();
                                    qty = qty + 1;
                                    itm.setAdiciona_qty(qty);

                                    alMap.put(Config.alhmAditional.get(k).getIdRelaRestaProduAdiciona(), itm);

                                } else {
                                    ItemAdditionalProduct itm = Config.alhmAditional.get(k);
                                    itm.setAdiciona_qty(1);
                                    alMap.put(Config.alhmAditional.get(k).getIdRelaRestaProduAdiciona(), itm);
                                }
                                //Log.v("size", "" + alMap.size());
                            }

                            for (Map.Entry m : alMap.entrySet()) {
                                ItemAdditionalProduct itm = (ItemAdditionalProduct) m.getValue();
                                addToCartManager.saveAdicionales(itm, insertId);
                            }
                            addQTY();
                            dialog.dismiss();
                        }
                    }
                }

                /**
                 * Case 3
                 */

                if (addToCart_Precioslv.getVisibility() == View.GONE
                        && addToCart_additionaltv.getVisibility() == View.GONE
                        && isNORMAL == false) {

                    insertId = addToCartManager.updateProduct(Nombre, Descripcion,
                            idRestaProducto, product_qty, totalPrice,
                            TipoModelo2Por1, TipoModelAdicional2Por1, EsDia2x1);
                    addQTY();
                    dialog.dismiss();
                }

                if (EsDia2x1.equals("S") && isPreciousValidation) {
                    Bundle bundle = new Bundle();

                    //ArrayList<ItemAdditionalProduct> alItemAdditional = alItemAdicionales.get(idRestaProducto);
                    // ArrayList<ItemProductPrice> alPrecios = alItemProdPrecio.get(idRestaProducto);

                    bundle.putParcelableArrayList("alItem", alItem);
                    bundle.putParcelableArrayList("alItemAdditional", alItemAdditional);
                    bundle.putParcelableArrayList("alItemProdPrecio", alPrecios);
                    bundle.putSerializable("restaurantModel", b.getSerializable("restaurantListModel"));
                    // bundle.putInt("screenStatus",1);
                    bundle.putInt("idRestaLocal", idRestaLocal);
                    bundle.putString("NombreRestaurante", NombreRestaurante);
                    bundle.putString("NombreLocal", NombreLocal);
                    bundle.putString("TipoComida", TipoComida);
                    bundle.putString("ImagenLocal", ImagenLocal);
                    bundle.putInt("idRestaCategoria", idRestaCategoria);
                    bundle.putString("Nombre", Nombre);
                    bundle.putFloat("isBasePrice", isBasePrice);
                    bundle.putInt("insertId", insertId);
                    try {
                        int qty = Integer.valueOf(addToCart_qty.getText().toString());
                        bundle.putInt("product_qty", qty);
                    } catch (Exception e) {
                        Log.e(TAG, "" + e.toString());
                    }
                    bundle.putString("TipoModelAdicional2Por1", TipoModelAdicional2Por1);
                    bundle.putString("TipoModelo2Por1", TipoModelo2Por1);
                    bundle.putInt("idRestaProducto", idRestaProducto);

                    isPreciousValidation = false;
                    Log.d("fue", "1305");
                    DashBoardActivity.displayFragment(FragmentIDs.ProductOffersFragment_Id, bundle);
                }
            }
        });

        dialog.show();
    }


    public void addTCardService_withoutadd(final String Nombre,
                                           final String Descripcion,
                                           String ImagenRestaProducto,
                                           final int idRestaProducto,
                                           final String EsDia2x1,
                                           final int position,
                                           final String TipoModelo2Por1,
                                           final String TipoModelAdicional2Por1,
                                           final ArrayList<ItemProductPrice> alPrecios,
                                           final ArrayList<ItemAdditionalProduct> alItemAdditional) {

        triggerEveent = 0;

        final Dialog dialog = new Dialog(new ContextThemeWrapper(mContext, R.style.DialogSlideAnim));
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.addtocart_xml_lessdata);
        addToCart_Precioslv = (ListView) dialog.findViewById(R.id.addToCart_Precioslv);
        addToCart_addAdditionallv = (ListView) dialog.findViewById(R.id.addToCart_addAdditionallv);
        addTocart_scrollView = (ScrollView) dialog.findViewById(R.id.addTocart_scrollView);
        View addToCart_linetwo = (View) dialog.findViewById(R.id.addToCart_linetwo);
        final GothamBookTextView addToCart_additionaltv = (GothamBookTextView) dialog.findViewById(R.id.addToCart_additionaltv);
        View addToCart_linethree = (View) dialog.findViewById(R.id.addToCart_linethree);
        addToCart_totalPrice = (GothamBookTextView) dialog.findViewById(R.id.addToCart_totalPrice);
        ImageButton addToCart_minus = (ImageButton) dialog.findViewById(R.id.addToCart_minus);
        ImageButton addToCart_plus = (ImageButton) dialog.findViewById(R.id.addToCart_plus);
        addToCart_qty = (GothamBookEditTextView) dialog.findViewById(R.id.addToCart_qty);
        GothamBookTextView addToCart_agregaralcarrito = (GothamBookTextView) dialog.findViewById(R.id.addToCart_agregaralcarritotv);
        GothamBookTextView cancel = (GothamBookTextView) dialog.findViewById(R.id.cancel);

        final RoundedImageView menuitem_restImageview1 = (RoundedImageView) dialog.findViewById(R.id.menuitem_restImageview);
        ExtraBoldItalicTextView menuitem_nameOfRestaurant1 = (ExtraBoldItalicTextView) dialog.findViewById(R.id.menuitem_nameOfRestaurant);
        MediumTextView menuitem_typeOfFood1 = (MediumTextView) dialog.findViewById(R.id.menuitem_typeOfFood);
        BebaseNeueTextView menuitem_price1 = (BebaseNeueTextView) dialog.findViewById(R.id.menuitem_price);

        menuitem_nameOfRestaurant1.setText(Nombre);
        menuitem_typeOfFood1.setText(Descripcion);

        Picasso.with(mContext).load("http://www.deliveryec.com/images/app/" + ImagenRestaProducto)
                .into(menuitem_restImageview1, new Callback() {
                    @Override
                    public void onSuccess() {
                        menuitem_restImageview1.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onError() {
                        menuitem_restImageview1.setVisibility(View.GONE);
                    }
                });

        if (isNORMAL) {
            isNORMAL = false;
            totalPrice = Float.parseFloat(decimalFormat.format(basePrice));
        } else {
            totalPrice = Float.parseFloat(decimalFormat.format(basePrice));
            totalPrice = Float.parseFloat(decimalFormat.format(totalPrice));
        }
        addToCart_totalPrice.setText("$ " + new DecimalFormat("0.00").format(totalPrice));

        addToCart_qty.setText("" + product_qty);
        //Add to Precious Data
        if (alSortPrecios != null && alSortPrecios.size() > 0) {
            addToCart_Precioslv.setVisibility(View.VISIBLE);
            preciosAdapter = new PreciosAdapter(mContext, alSortPrecios, fm);
            addToCart_Precioslv.setAdapter(preciosAdapter);

        } else {
            menuitem_price1.setText("$" + " " + new DecimalFormat("0.00").format(basePrice));
            addToCart_Precioslv.setVisibility(View.GONE);
            menuitem_price1.setVisibility(View.VISIBLE);
        }

        //Add to aditional product
        if (additionalProduct != null && additionalProduct.size() > 0) {
            addToCart_addAdditionallv.setVisibility(View.VISIBLE);
            additionalProductAdapter =
                    new AdditionalProductAdapter(mContext, additionalProduct, fm);

            addToCart_addAdditionallv.setAdapter(additionalProductAdapter);
            additionalProductAdapter.notifyDataSetChanged();

        } else {
            addToCart_addAdditionallv.setVisibility(View.GONE);
            addToCart_linetwo.setVisibility(View.GONE);
            addToCart_additionaltv.setVisibility(View.GONE);
            addToCart_linethree.setVisibility(View.GONE);
        }

        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                if (addToCart_Precioslv.getVisibility() == View.VISIBLE) {
                    UtilityMessage.setListViewHeightBasedOnChildren(addToCart_Precioslv);
                }

                if (addToCart_addAdditionallv.getVisibility() == View.VISIBLE) {
                    UtilityMessage.setListViewHeightBasedOnChildren(addToCart_addAdditionallv);
                }

            }
        }, 1000);

        addToCart_minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    if (product_qty > 1) {
                        product_qty = product_qty - 1;
                        totalPrice = product_qty * (Config.totalAditional + Config.totalPrecios + basePrice);
                        DecimalFormat decimalFormat = doNumberFormatter();
                        totalPrice = Float.parseFloat(decimalFormat.format(totalPrice));

                        addToCart_totalPrice.setText("$ " + new DecimalFormat("0.00").format(totalPrice));
                        addToCart_qty.setText("" + product_qty);
                    }

                } catch (NumberFormatException e) {
                    Log.e(TAG, "" + e.toString());
                } catch (Exception e) {
                    Log.e(TAG, "" + e.toString());
                }
            }
        });

        addToCart_plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                product_qty = product_qty + 1;
                totalPrice = product_qty * (Config.totalAditional + Config.totalPrecios + basePrice);
                DecimalFormat decimalFormat = doNumberFormatter();
                totalPrice = Float.parseFloat(decimalFormat.format(totalPrice));

                addToCart_totalPrice.setText("$ " + new DecimalFormat("0.00").format(totalPrice));
                addToCart_qty.setText("" + product_qty);
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Config.alhmAditional.clear();
                triggerEveent = 0;
                totalPrice = 0.0f;
                product_qty = 1;
                isPreciousValidation = false;

                dialog.dismiss();
            }
        });

        addToCart_agregaralcarrito.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                /**
                 * If Precios & adicionales both of then is greate
                 * Mean Precios is greater then 1 and adicional is greater then 0
                 * then add both of then and not any update process.
                 */

                /**
                 * Case 1
                 */
                if (Config.alhmPrecios != null && alSortPrecios != null && alSortPrecios.size() > 0) {
                    triggerEveent = 1;
                    if (Config.alhmPrecios.size() == 0) {
                        String[] mapKeys = alSortPrecios.keySet().toArray(new String[alSortPrecios.size()]);
                        setMessageErrorDialog("La opcion " + mapKeys[0].toUpperCase(Locale.ENGLISH) + " es requerida");
                        return;
                    } else {
                        int countOfKeys = 0;
                        StringBuilder key = new StringBuilder();

                        String[] mapKeys = alSortPrecios.keySet().toArray(new String[alSortPrecios.size()]);

                        for (Map.Entry m : Config.alhmPrecios.entrySet()) {
                            key.append("'" + m.getKey() + "'");
                            key.append(",");
                            countOfKeys = countOfKeys + 1;
                        }

                        // String outPutKeys = key.length() > 0 ? key.substring(0, key.length() - 1) : null;
                        //Log.v("outPutKeys",""+outPutKeys);
                        if (mapKeys.length != countOfKeys) {
                            setMessageErrorDialog("La opcion " + mapKeys[countOfKeys].toUpperCase(Locale.ENGLISH) + " es requerida");
                        } else {

                            /**
                             * Check the adicionales if Precios is available but adicional is not available
                             */

                            // addToCartManager.insertProduct(Nombre, Descripcion, idRestaProducto, product_qty, totalPrice);
                            insertId = (int) addToCartManager.insertProduct(Nombre,
                                    Descripcion, idRestaProducto, product_qty,
                                    totalPrice, TipoModelo2Por1,
                                    TipoModelAdicional2Por1, EsDia2x1);

                            for (Map.Entry m : Config.alhmPrecios.entrySet()) {
                                /**
                                 * Comment: Insert Precios data the value but not update
                                 */
                                ItemProductPrice itm = (ItemProductPrice) m.getValue();
                                String key1 = (String) m.getKey();
                                boolean isBaseChecked = key1.startsWith("BASE-");
                                if (isBaseChecked) {
                                    isBasePrice = itm.getPrecio();
                                }
                                addToCartManager.insertPrecios(itm, 1, insertId);
                            }

                            if (Config.alhmAditional.size() > 0) {

                                Map<Integer, ItemAdditionalProduct> alMap = new HashMap<Integer, ItemAdditionalProduct>();

                                for (int l = 0; l < Config.alhmAditional.size(); l++) {

                                    if (alMap.containsKey(Config.alhmAditional.get(l).getIdRelaRestaProduAdiciona())) {
                                        ItemAdditionalProduct itm = (ItemAdditionalProduct) alMap.get(Config.alhmAditional.get(l).getIdRelaRestaProduAdiciona());
                                        int qty = itm.getAdiciona_qty();
                                        qty = qty + 1;
                                        itm.setAdiciona_qty(qty);

                                        alMap.put(Config.alhmAditional.get(l).getIdRelaRestaProduAdiciona(), itm);

                                    } else {
                                        ItemAdditionalProduct itm = Config.alhmAditional.get(l);
                                        itm.setAdiciona_qty(1);
                                        alMap.put(Config.alhmAditional.get(l).getIdRelaRestaProduAdiciona(), itm);
                                    }
                                }


                                for (Map.Entry m : alMap.entrySet()) {
                                    ItemAdditionalProduct itm = (ItemAdditionalProduct) m.getValue();
                                    addToCartManager.saveAdicionales(itm, insertId);
                                }
                            }
                            addQTY();
                            Config.alhmPrecios.clear();
                            Config.alhmAditional.clear();
                            dialog.dismiss();
                        }
                    }
                }

                /**
                 * Case 2
                 */

                if (Config.alhmAditional != null && triggerEveent == 0) {
                    if (additionalProduct != null && additionalProduct.size() > 0) {
                        triggerEveent = 1;
                        if (Config.alhmAditional.size() == 0 && additionalProduct.size() > 0) {
                            insertId = (int) addToCartManager.insertProduct(Nombre,
                                    Descripcion,
                                    idRestaProducto, product_qty, totalPrice,
                                    TipoModelo2Por1, TipoModelAdicional2Por1, EsDia2x1);
                            addQTY();
                            dialog.dismiss();
                        } else if (Config.alhmAditional.size() > 0 && additionalProduct.size() > 0) {
                            insertId = (int) addToCartManager.insertProduct(Nombre,
                                    Descripcion, idRestaProducto, product_qty,
                                    totalPrice, TipoModelo2Por1,
                                    TipoModelAdicional2Por1, EsDia2x1);

                            Map<Integer, ItemAdditionalProduct> alMap = new HashMap<Integer, ItemAdditionalProduct>();

                            for (int k = 0; k < Config.alhmAditional.size(); k++) {
                                //addToCartManager.insertAdicionales(Config.alhmAditional.get(k),product_qty);

                                if (alMap.containsKey(Config.alhmAditional.get(k).getIdRelaRestaProduAdiciona())) {
                                    ItemAdditionalProduct itm = (ItemAdditionalProduct) alMap.get(Config.alhmAditional.get(k).getIdRelaRestaProduAdiciona());
                                    int qty = itm.getAdiciona_qty();
                                    qty = qty + 1;
                                    itm.setAdiciona_qty(qty);

                                    alMap.put(Config.alhmAditional.get(k).getIdRelaRestaProduAdiciona(), itm);

                                } else {
                                    ItemAdditionalProduct itm = Config.alhmAditional.get(k);
                                    itm.setAdiciona_qty(1);
                                    alMap.put(Config.alhmAditional.get(k).getIdRelaRestaProduAdiciona(), itm);
                                }
                                //Log.v("size", "" + alMap.size());
                            }

                            for (Map.Entry m : alMap.entrySet()) {
                                ItemAdditionalProduct itm = (ItemAdditionalProduct) m.getValue();
                                addToCartManager.saveAdicionales(itm, insertId);
                            }
                            addQTY();
                            dialog.dismiss();
                        }
                    }
                }

                /**
                 * Case 3
                 */

                if (addToCart_Precioslv.getVisibility() == View.GONE
                        && addToCart_additionaltv.getVisibility() == View.GONE
                        && isNORMAL == false) {
                    /**
                     * This one update the cart
                     */
                    UpdateMessage.UpdateMessageListener updateMessageListener = updateMessage.getUpdateMessageListener();
                    addToCartManager.setUpdateMessageListener(updateMessageListener);
                    insertId = addToCartManager.updateProduct(Nombre, Descripcion, idRestaProducto,
                            product_qty, totalPrice, TipoModelo2Por1,
                            TipoModelAdicional2Por1, EsDia2x1);
                    addQTY();
                    dialog.dismiss();
                }

                if (EsDia2x1.equals("S") && isPreciousValidation) {
                    Bundle bundle = new Bundle();
                    bundle.putParcelableArrayList("alItem", alItem);
                    bundle.putParcelableArrayList("alItemAdditional", alItemAdditional);
                    bundle.putParcelableArrayList("alItemProdPrecio", alPrecios);
                    bundle.putSerializable("restaurantModel", b.getSerializable("restaurantListModel"));

                    bundle.putInt("idRestaLocal", idRestaLocal);
                    bundle.putString("NombreRestaurante", NombreRestaurante);
                    bundle.putString("NombreLocal", NombreLocal);
                    bundle.putString("TipoComida", TipoComida);
                    bundle.putString("ImagenLocal", ImagenLocal);
                    bundle.putInt("idRestaCategoria", idRestaCategoria);
                    bundle.putString("Nombre", Nombre);
                    bundle.putFloat("isBasePrice", isBasePrice);
                    bundle.putInt("insertId", insertId);
                    try {
                        int qty = Integer.valueOf(addToCart_qty.getText().toString());
                        bundle.putInt("product_qty", qty);
                    } catch (Exception e) {
                        Log.e(TAG, "" + e.toString());
                    }
                    bundle.putString("TipoModelAdicional2Por1", TipoModelAdicional2Por1);
                    bundle.putString("TipoModelo2Por1", TipoModelo2Por1);
                    bundle.putInt("idRestaProducto", idRestaProducto);

                    isPreciousValidation = false;
                    Log.d("fue", "1659");
                    DashBoardActivity.displayFragment(FragmentIDs.ProductOffersFragment_Id, bundle);
                }
            }
        });

        dialog.show();
    }


    public void addCartPrecios() {
        try {
            totalPrice = product_qty * (Config.totalAditional + Config.totalPrecios + basePrice);
            DecimalFormat decimalFormat = doNumberFormatter();
            totalPrice = Float.parseFloat(decimalFormat.format(totalPrice));
            addToCart_totalPrice.setText("$ " + new DecimalFormat("0.00").format(totalPrice));
        } catch (NumberFormatException e) {
            Log.e(TAG, "" + e.toString());
        } catch (Exception e) {
            Log.e(TAG, "" + e.toString());
        }
    }

    public void addCartAdicionales() {
        totalPrice = product_qty * (Config.totalAditional + Config.totalPrecios + basePrice);
        //DecimalFormat decimalFormat = doNumberFormatter();
        addToCart_totalPrice.setText("$ " + new DecimalFormat("0.00").format(totalPrice));
    }

    private void addQTY() {
        int totalSum = addToCartManager.getTotalQuntity();
        if (totalSum > 0) {
            //Show the quntity
            DashBoardActivity.showQuntity();
        } else {
            //hide the quntity
            DashBoardActivity.hideQuntity();
        }
        //total sum
        DashBoardActivity.setQuntity(totalSum);
        totalPrice = 0.0f;
        product_qty = 1;
    }

    private void clearPrice() {
        Config.totalPrecios = 0;
        Config.totalAditional = 0.0f;
        Config.alhmAditional.clear();
        Config.alhmPrecios.clear();
    }

    private void setMessageErrorDialog(String message) {
        messageDialog = new MessageDialog(mContext,
                android.R.style.Theme_Holo_Light_Dialog,
                message,
                new MessageDialog.okOnClickListener() {
                    @Override
                    public void onButtonClick() {
                        messageDialog.dismiss();
                    }
                });

        messageDialog.setCancelable(false);
        messageDialog.setCanceledOnTouchOutside(false);
        messageDialog.show();
    }

    public DecimalFormat doNumberFormatter() {
        NumberFormat nf = NumberFormat.getNumberInstance(Locale.ENGLISH);
        DecimalFormat dtime = (DecimalFormat) nf;
        dtime.applyPattern("##.##");
        return dtime;
    }

    @Override
    public void onUpdateMessage() {
        setMessageErrorDialog("Se actualizó la cantidad y los valores en el carro de compra");
    }

    private void addToModelo3_WithOutAdicional(final String Nombre,
                                               final String Descripcion,
                                               String ImagenRestaProducto,
                                               final int idRestaProducto,
                                               final String EsDia2x1,
                                               final int position,
                                               final String TipoModelo2Por1,
                                               final String TipoModelAdicional2Por1,
                                               final ArrayList<ItemProductPrice> alPrecios,
                                               final ArrayList<ItemAdditionalProduct> alItemAdditional) {


        triggerEveent = 0;

        final Dialog dialog = new Dialog(new ContextThemeWrapper(mContext, R.style.DialogSlideAnim));
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.addtocart_xml_lessdata);
        addToCart_Precioslv = (ListView) dialog.findViewById(R.id.addToCart_Precioslv);
        addToCart_addAdditionallv = (ListView) dialog.findViewById(R.id.addToCart_addAdditionallv);
        addTocart_scrollView = (ScrollView) dialog.findViewById(R.id.addTocart_scrollView);
        View addToCart_linetwo = (View) dialog.findViewById(R.id.addToCart_linetwo);
        final GothamBookTextView addToCart_additionaltv = (GothamBookTextView) dialog.findViewById(R.id.addToCart_additionaltv);
        View addToCart_linethree = (View) dialog.findViewById(R.id.addToCart_linethree);
        addToCart_totalPrice = (GothamBookTextView) dialog.findViewById(R.id.addToCart_totalPrice);
        ImageButton addToCart_minus = (ImageButton) dialog.findViewById(R.id.addToCart_minus);
        ImageButton addToCart_plus = (ImageButton) dialog.findViewById(R.id.addToCart_plus);
        addToCart_qty = (GothamBookEditTextView) dialog.findViewById(R.id.addToCart_qty);
        GothamBookTextView addToCart_agregaralcarrito = (GothamBookTextView) dialog.findViewById(R.id.addToCart_agregaralcarritotv);
        GothamBookTextView cancel = (GothamBookTextView) dialog.findViewById(R.id.cancel);

        final RoundedImageView menuitem_restImageview1 = (RoundedImageView) dialog.findViewById(R.id.menuitem_restImageview);
        ExtraBoldItalicTextView menuitem_nameOfRestaurant1 = (ExtraBoldItalicTextView) dialog.findViewById(R.id.menuitem_nameOfRestaurant);
        MediumTextView menuitem_typeOfFood1 = (MediumTextView) dialog.findViewById(R.id.menuitem_typeOfFood);
        BebaseNeueTextView menuitem_price1 = (BebaseNeueTextView) dialog.findViewById(R.id.menuitem_price);

        addToCart_Precioslv.setVisibility(View.GONE);
        addToCart_addAdditionallv.setVisibility(View.GONE);
        addToCart_linetwo.setVisibility(View.GONE);
        addToCart_linethree.setVisibility(View.GONE);

        menuitem_nameOfRestaurant1.setText(Nombre);
        menuitem_typeOfFood1.setText(Descripcion);

        Picasso.with(mContext).load("http://www.deliveryec.com/images/app/" + ImagenRestaProducto)
                .into(menuitem_restImageview1, new Callback() {
                    @Override
                    public void onSuccess() {
                        menuitem_restImageview1.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onError() {
                        menuitem_restImageview1.setVisibility(View.GONE);
                    }
                });

        if (isNORMAL) {
            isNORMAL = false;
            totalPrice = Float.parseFloat(decimalFormat.format(basePrice));
        } else {
            totalPrice = Float.parseFloat(decimalFormat.format(basePrice));
            totalPrice = Float.parseFloat(decimalFormat.format(totalPrice));
        }

        menuitem_price1.setText("$" + " " + new DecimalFormat("0.00").format(basePrice));
        addToCart_totalPrice.setText("$ " + new DecimalFormat("0.00").format(totalPrice));

        addToCart_qty.setText("" + product_qty);

        dialog.show();


        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Config.alhmAditional.clear();
                triggerEveent = 0;
                totalPrice = 0.0f;
                product_qty = 1;
                isPreciousValidation = false;

                dialog.dismiss();
            }
        });

        addToCart_minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    if (product_qty > 1) {
                        product_qty = product_qty - 1;
                        totalPrice = product_qty * (Config.totalAditional + Config.totalPrecios + basePrice);
                        DecimalFormat decimalFormat = doNumberFormatter();
                        totalPrice = Float.parseFloat(decimalFormat.format(totalPrice));

                        addToCart_totalPrice.setText("$ " + new DecimalFormat("0.00").format(totalPrice));
                        addToCart_qty.setText("" + product_qty);
                    }

                } catch (NumberFormatException e) {
                    Log.e(TAG, "" + e.toString());
                } catch (Exception e) {
                    Log.e(TAG, "" + e.toString());
                }
            }
        });

        addToCart_plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                product_qty = product_qty + 1;
                totalPrice = product_qty * (Config.totalAditional + Config.totalPrecios + basePrice);
                DecimalFormat decimalFormat = doNumberFormatter();
                totalPrice = Float.parseFloat(decimalFormat.format(totalPrice));

                addToCart_totalPrice.setText("$ " + new DecimalFormat("0.00").format(totalPrice));
                addToCart_qty.setText("" + product_qty);
            }
        });

        addToCart_agregaralcarrito.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (addToCart_Precioslv.getVisibility() == View.GONE
                        && addToCart_additionaltv.getVisibility() == View.GONE
                        && isNORMAL == false) {
                    /**
                     * This one update the cart
                     */

                    long l = addToCartManager.insertProduct(Nombre, Descripcion, idRestaProducto,
                            product_qty, totalPrice, TipoModelo2Por1,
                            TipoModelAdicional2Por1, EsDia2x1);
                    addQTY();
                    dialog.dismiss();
                    if (TipoModelo2Por1.equals("3")) {
                        setMessageErrorDialog(mContext.getResources().getString(R.string.tipo_3));
                    }
                }
            }
        });
    }

    private void addToModelo3_Adicional(final String Nombre,
                                        final String Descripcion,
                                        String ImagenRestaProducto,
                                        final int idRestaProducto,
                                        final String EsDia2x1,
                                        final int position,
                                        final String TipoModelo2Por1,
                                        final String TipoModelAdicional2Por1,
                                        final ArrayList<ItemProductPrice> alPrecios,
                                        final ArrayList<ItemAdditionalProduct> alItemAdditional) {


        triggerEveent = 0;

        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(R.layout.addtocart_xml_new);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));

        addToCart_Precioslv = (ListView) dialog.findViewById(R.id.addToCart_Precioslv);
        addToCart_addAdditionallv = (ListView) dialog.findViewById(R.id.addToCart_addAdditionallv);
        addTocart_scrollView = (ScrollView) dialog.findViewById(R.id.addTocart_scrollView);
        View addToCart_linetwo = (View) dialog.findViewById(R.id.addToCart_linetwo);
        ImageView back = (ImageView) dialog.findViewById(R.id.aboutus_toolbar_backButton);
        final GothamBookTextView addToCart_additionaltv = (GothamBookTextView) dialog.findViewById(R.id.addToCart_additionaltv);
        View addToCart_linethree = (View) dialog.findViewById(R.id.addToCart_linethree);

        addToCart_totalPrice = (GothamBookTextView) dialog.findViewById(R.id.addToCart_totalPrice);
        ImageButton addToCart_minus = (ImageButton) dialog.findViewById(R.id.addToCart_minus);
        ImageButton addToCart_plus = (ImageButton) dialog.findViewById(R.id.addToCart_plus);
        addToCart_qty = (GothamBookEditTextView) dialog.findViewById(R.id.addToCart_qty);

        LinearLayout addToCart_agregaralcarrito = (LinearLayout) dialog.findViewById(R.id.addToCart_agregaralcarrito);

        final RoundedImageView menuitem_restImageview1 = (RoundedImageView) dialog.findViewById(R.id.menuitem_restImageview);
        final GothamBoldTextView menuitem_nameOfRestaurant1 = (GothamBoldTextView) dialog.findViewById(R.id.menuitem_nameOfRestaurant);
        final MediumTextView menuitem_typeOfFood1 = (MediumTextView) dialog.findViewById(R.id.menuitem_typeOfFood);
        BebaseNeueTextView menuitem_price1 = (BebaseNeueTextView) dialog.findViewById(R.id.menuitem_price);

        menuitem_nameOfRestaurant1.setText(Nombre);
        menuitem_typeOfFood1.setText(Descripcion);

        Picasso.with(mContext).load("http://www.deliveryec.com/images/app/" + ImagenRestaProducto)
                .into(menuitem_restImageview1, new Callback() {
                    @Override
                    public void onSuccess() {
                        menuitem_restImageview1.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onError() {
                        menuitem_restImageview1.setVisibility(View.GONE);
                        //Add the height for dynamic view.
                        float scale = getContext().getResources().getDisplayMetrics().density;
                        int px = (int) (70 * scale + 0.5f);  // replace 70 with your dimensions

                        RelativeLayout rl = (RelativeLayout) dialog.findViewById(R.id.menuitem_relativelayout);
                        rl.getLayoutParams().height = px;

                       /* RelativeLayout r2 = (RelativeLayout)dialog.findViewById(R.id.menuitem_typeOfFood);
                        r2.getLayoutParams().height = px;*/

                        final RelativeLayout.LayoutParams layoutparams = (RelativeLayout.LayoutParams) menuitem_nameOfRestaurant1.getLayoutParams();
                        layoutparams.setMargins(0, 0, 0, 0);
                        menuitem_nameOfRestaurant1.setLayoutParams(layoutparams);

                        final RelativeLayout.LayoutParams layoutParams1 = (RelativeLayout.LayoutParams) menuitem_typeOfFood1.getLayoutParams();
                        layoutParams1.setMargins(0, 0, 0, 0);
                        menuitem_typeOfFood1.setLayoutParams(layoutParams1);

                        rl.invalidate();
                        //rl.getLayoutParams().width = px;

                    }
                });

        if (isNORMAL) {
            isNORMAL = false;
            totalPrice = Float.parseFloat(decimalFormat.format(basePrice));
        } else {
            totalPrice = Float.parseFloat(decimalFormat.format(totalPrice));
        }
        addToCart_totalPrice.setText("$ " + new DecimalFormat("0.00").format(totalPrice));

        addToCart_qty.setText("" + product_qty);

        //Add to Precious Data
        if (alSortPrecios != null && alSortPrecios.size() > 0) {
            addToCart_Precioslv.setVisibility(View.VISIBLE);
            preciosAdapter = new PreciosAdapter(mContext, alSortPrecios, fm);
            addToCart_Precioslv.setAdapter(preciosAdapter);

        } else {
            if (String.valueOf(basePrice).length() == 3) {
                String show = String.valueOf(basePrice) + "0";
                menuitem_price1.setText("$ " + show);
            } else {
                menuitem_price1.setText("$ " + Float.parseFloat(decimalFormat.format(basePrice)));
            }
            addToCart_Precioslv.setVisibility(View.GONE);
        }

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Config.alhmAditional.clear();
                triggerEveent = 0;
                totalPrice = 0.0f;
                product_qty = 1;
                dialog.dismiss();
            }
        });

        //Add to aditional product
        if (additionalProduct != null && additionalProduct.size() > 0) {
            addToCart_addAdditionallv.setVisibility(View.VISIBLE);
            additionalProductAdapter =
                    new AdditionalProductAdapter(mContext, additionalProduct, fm);

            addToCart_addAdditionallv.setAdapter(additionalProductAdapter);
            additionalProductAdapter.notifyDataSetChanged();

        } else {
            addToCart_addAdditionallv.setVisibility(View.GONE);
            addToCart_linetwo.setVisibility(View.GONE);
            addToCart_additionaltv.setVisibility(View.GONE);
            addToCart_linethree.setVisibility(View.GONE);
        }

        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                if (addToCart_Precioslv.getVisibility() == View.VISIBLE) {
                    UtilityMessage.setListViewHeightBasedOnChildren(addToCart_Precioslv);
                }

                if (addToCart_addAdditionallv.getVisibility() == View.VISIBLE) {
                    UtilityMessage.setListViewHeightBasedOnChildren(addToCart_addAdditionallv);
                }

            }
        }, 1000);

        addToCart_minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    if (product_qty > 1) {
                        product_qty = product_qty - 1;
                        totalPrice = product_qty * (Config.totalAditional + Config.totalPrecios + basePrice);
                        DecimalFormat decimalFormat = doNumberFormatter();
                        totalPrice = Float.parseFloat(decimalFormat.format(totalPrice));
                        addToCart_totalPrice.setText("$ " + new DecimalFormat("0.00").format(totalPrice));
                        addToCart_qty.setText("" + product_qty);
                    }

                } catch (NumberFormatException e) {
                    Log.e(TAG, "" + e.toString());
                } catch (Exception e) {
                    Log.e(TAG, "" + e.toString());
                }
            }
        });

        addToCart_plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                product_qty = product_qty + 1;
                totalPrice = product_qty * (Config.totalAditional + Config.totalPrecios + basePrice);
                DecimalFormat decimalFormat = doNumberFormatter();
                totalPrice = Float.parseFloat(decimalFormat.format(totalPrice));
                addToCart_totalPrice.setText("$ " + new DecimalFormat("0.00").format(totalPrice));
                addToCart_qty.setText("" + product_qty);
            }
        });

        addToCart_agregaralcarrito.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /**
                 * If Precios & adicionales both of then is greate
                 * Mean Precios is greater then 1 and adicional is greater then 0
                 * then add both of then and not any update process.
                 */

                /**
                 * Case 1
                 */
                if (Config.alhmPrecios != null && alSortPrecios != null && alSortPrecios.size() > 0) {
                    triggerEveent = 1;
                    if (Config.alhmPrecios.size() == 0) {
                        String[] mapKeys = alSortPrecios.keySet().toArray(new String[alSortPrecios.size()]);
                        setMessageErrorDialog("La opcion " + mapKeys[0].toUpperCase(Locale.ENGLISH).replace("BASE-", "") + " es requerida");
                        return;
                    } else {
                        int countOfKeys = 0;
                        StringBuilder key = new StringBuilder();

                        String[] mapKeys = alSortPrecios.keySet().toArray(new String[alSortPrecios.size()]);

                        for (Map.Entry m : Config.alhmPrecios.entrySet()) {
                            key.append("'" + m.getKey() + "'");
                            key.append(",");
                            countOfKeys = countOfKeys + 1;
                        }

                        //String outPutKeys = key.length() > 0 ? key.substring(0, key.length() - 1) : null;
                        //Log.v("outPutKeys",""+outPutKeys);

                        /**
                         * Check the conditions for all validation for true
                         */
                        if (mapKeys.length != countOfKeys) {
                            setMessageErrorDialog("La opcion " + mapKeys[countOfKeys].toUpperCase(Locale.ENGLISH) + " es requerida");
                        } else {
                            /**
                             * Check the adicionales if Precios is available but adicional is not available
                             */

                            // addToCartManager.insertProduct(Nombre, Descripcion, idRestaProducto, product_qty, totalPrice);
                            insertId = (int) addToCartManager.insertProduct(Nombre,
                                    Descripcion, idRestaProducto, product_qty,
                                    totalPrice, TipoModelo2Por1,
                                    TipoModelAdicional2Por1, EsDia2x1);

                            for (Map.Entry m : Config.alhmPrecios.entrySet()) {
                                /**
                                 * Comment: Insert Precios data the value but not update
                                 */
                                ItemProductPrice itm = (ItemProductPrice) m.getValue();
                                String key1 = (String) m.getKey();
                                boolean isBaseChecked = key1.startsWith("BASE-");
                                if (isBaseChecked) {
                                    isBasePrice = itm.getPrecio();
                                }
                                addToCartManager.insertPrecios(itm, 1, insertId);
                            }

                            if (Config.alhmAditional.size() > 0) {

                                Map<Integer, ItemAdditionalProduct> alMap = new HashMap<Integer, ItemAdditionalProduct>();

                                for (int l = 0; l < Config.alhmAditional.size(); l++) {


                                    if (alMap.containsKey(Config.alhmAditional.get(l).getIdRelaRestaProduAdiciona())) {
                                        ItemAdditionalProduct itm = (ItemAdditionalProduct) alMap.get(Config.alhmAditional.get(l).getIdRelaRestaProduAdiciona());
                                        int qty = itm.getAdiciona_qty();
                                        qty = qty + 1;
                                        itm.setAdiciona_qty(qty);

                                        alMap.put(Config.alhmAditional.get(l).getIdRelaRestaProduAdiciona(), itm);

                                    } else {
                                        ItemAdditionalProduct itm = Config.alhmAditional.get(l);
                                        itm.setAdiciona_qty(1);
                                        alMap.put(Config.alhmAditional.get(l).getIdRelaRestaProduAdiciona(), itm);
                                    }
                                }


                                for (Map.Entry m : alMap.entrySet()) {
                                    ItemAdditionalProduct itm = (ItemAdditionalProduct) m.getValue();
                                    addToCartManager.saveAdicionales(itm, insertId);
                                }
                            }
                            addQTY();
                            Config.alhmPrecios.clear();
                            Config.alhmAditional.clear();
                            dialog.dismiss();

                            if (TipoModelo2Por1.equals("3")) {
                                setMessageErrorDialog(mContext.getResources().getString(R.string.tipo_3));
                            }
                        }
                    }
                }


                /**
                 * Case 2
                 */

                if (Config.alhmAditional != null && triggerEveent == 0) {
                    if (additionalProduct != null && additionalProduct.size() > 0) {
                        triggerEveent = 1;
                        if (Config.alhmAditional.size() == 0 && additionalProduct.size() > 0) {
                            insertId = (int) addToCartManager.insertProduct(Nombre,
                                    Descripcion, idRestaProducto, product_qty,
                                    totalPrice, TipoModelo2Por1,
                                    TipoModelAdicional2Por1, EsDia2x1);
                            addQTY();
                            dialog.dismiss();
                        } else if (Config.alhmAditional.size() > 0 && additionalProduct.size() > 0) {
                            insertId = (int) addToCartManager.insertProduct(Nombre,
                                    Descripcion, idRestaProducto,
                                    product_qty, totalPrice,
                                    TipoModelo2Por1, TipoModelAdicional2Por1, EsDia2x1);

                            Map<Integer, ItemAdditionalProduct> alMap = new HashMap<Integer, ItemAdditionalProduct>();

                            for (int k = 0; k < Config.alhmAditional.size(); k++) {
                                //addToCartManager.insertAdicionales(Config.alhmAditional.get(k),product_qty);

                                if (alMap.containsKey(Config.alhmAditional.get(k).getIdRelaRestaProduAdiciona())) {
                                    ItemAdditionalProduct itm = (ItemAdditionalProduct) alMap.get(Config.alhmAditional.get(k).getIdRelaRestaProduAdiciona());
                                    int qty = itm.getAdiciona_qty();
                                    qty = qty + 1;
                                    itm.setAdiciona_qty(qty);

                                    alMap.put(Config.alhmAditional.get(k).getIdRelaRestaProduAdiciona(), itm);

                                } else {
                                    ItemAdditionalProduct itm = Config.alhmAditional.get(k);
                                    itm.setAdiciona_qty(1);
                                    alMap.put(Config.alhmAditional.get(k).getIdRelaRestaProduAdiciona(), itm);
                                }
                                //Log.v("size", "" + alMap.size());
                            }

                            for (Map.Entry m : alMap.entrySet()) {
                                ItemAdditionalProduct itm = (ItemAdditionalProduct) m.getValue();
                                addToCartManager.saveAdicionales(itm, insertId);
                            }
                            addQTY();
                            dialog.dismiss();
                            if (TipoModelo2Por1.equals("3")) {
                                setMessageErrorDialog(mContext.getResources().getString(R.string.tipo_3));
                            }
                        }
                    }
                }

                /**
                 * Case 3
                 */
                if (addToCart_Precioslv.getVisibility() == View.GONE
                        && addToCart_additionaltv.getVisibility() == View.GONE
                        && isNORMAL == false) {
                    /**
                     * This one update the cart
                     */
                    long l = addToCartManager.insertProduct(Nombre, Descripcion, idRestaProducto,
                            product_qty, totalPrice, TipoModelo2Por1,
                            TipoModelAdicional2Por1, EsDia2x1);
                    addQTY();
                    dialog.dismiss();
                    if (TipoModelo2Por1.equals("3")) {
                        setMessageErrorDialog(mContext.getResources().getString(R.string.tipo_3));
                    }
                }
            }
        });

        dialog.show();
    }
}