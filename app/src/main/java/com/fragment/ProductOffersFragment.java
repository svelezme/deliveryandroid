package com.fragment;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.view.ContextThemeWrapper;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.activity.DashBoardActivity;
import com.adapter.ProductOfferAdapter;
import com.adapter.ProductOfferAdditionalAdapter;
import com.adapter.ProductOfferPreciosAdapter;
import com.app.grupodeliveryec.deliveryEC.R;
import com.github.siyamed.shapeimageview.RoundedImageView;
import com.inputview.GothamBookEditTextView;
import com.manager.AddToCartManager;
import com.model.ItemAdditionalProduct;
import com.model.ItemProductPrice;
import com.model.RestaurentMenuItem;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.utility.Config;
import com.utility.JsonParser;
import com.utility.MessageDialog;
import com.utility.UtilityMessage;
import com.view.BebaseNeueTextView;
import com.view.ExtraBoldItalicTextView;
import com.view.GothamBoldTextView;
import com.view.GothamBookTextView;
import com.view.MediumTextView;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;
import java.util.TreeSet;

/**
 * Created by administrator on 29/3/17.
 */

public class ProductOffersFragment extends Fragment {

    private static Context mContext;
    private static FragmentManager fm;
    private static Bundle b;
    private static Fragment fragment;
    String TAG = getClass().getName();
    View view;


    RelativeLayout menuitem_relativelayout;
    RoundedImageView menuitem_restImageview;
    TextView menuitem_nameOfRestaurant;
    TextView menuitem_typeOfFood;
    ListView menuitem_lv;
    LinearLayout menuitem_noItemHasCart;
    TextView menuitem_backToWork;

    int idRestaProductoPrevious;
    int idRestaLocal;
    String NombreRestaurante;
    String NombreLocal;
    String TipoComida;
    String ImagenLocal;
    int idRestaCategoria;
    String Nombre;

    ArrayList<RestaurentMenuItem> alItem1;
    ArrayList<RestaurentMenuItem> alItem;
    ArrayList<ItemAdditionalProduct> alItemAdditional;
    ArrayList<ItemProductPrice> alPrecio1;
    ArrayList<ItemAdditionalProduct> alProduct1;

    ProductOfferAdapter productOfferAdapter;
    JsonParser jsonParser;

    String idRestaProducto;
    int product_qty = 1;
    int validProductQTY=0;
    float totalPrice;


    boolean isNORMAL = false;
    float isBasePrice = 0.0f;
    float basePrice = 0.0f;
    int triggerEveent = 0;

    DecimalFormat decimalFormat;
    MessageDialog messageDialog;

    ProductOfferPreciosAdapter productOfferPreciosAdapter;
    ProductOfferAdditionalAdapter productOfferAdditionalAdapter;

    ListView addToCart_addAdditionallv;
    ListView addToCart_Precioslv;
    ScrollView addTocart_scrollView;
    GothamBookTextView addToCart_totalPrice;
    GothamBookEditTextView addToCart_qty;

    /**
     * final display data
     */
    TreeMap<String, ArrayList<ItemProductPrice>> alSortPrecios;
    TreeMap<String, ArrayList<ItemAdditionalProduct>> additionalProduct;

    AddToCartManager addToCartManager;

    int insertId_ProductId, product_qtybyBundle;
    boolean isBackPress;
    String TipoModelAdicional2Por1, TipoModelo2Por1;

    /**
     * @param context
     * @param fragmentManager
     * @param bundle
     * @return
     */

    public static Fragment getInstance(Context context, FragmentManager fragmentManager, Bundle bundle) {
        fragment = new ProductOffersFragment();
        mContext = context;
        b = bundle;
        fm = fragmentManager;
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.product_offers_xml, container, false);

        addToCartManager = new AddToCartManager(mContext);

        DashBoardActivity.updateTitle(mContext.getResources().getString(R.string.producto2by1));
        DashBoardActivity.hideBackButton();
        DashBoardActivity.hideSearchButton();
        DashBoardActivity.hideInfo();
        DashBoardActivity.hideLogout();
        DashBoardActivity.hideHorizentalBar();
        DashBoardActivity.hideAddDeliveryAddress();
        DashBoardActivity.showCerrar();
        DashBoardActivity.hideMenuPanel();

        jsonParser = new JsonParser();
        decimalFormat = doNumberFormatter();

        idRestaLocal = b.getInt("idRestaLocal");
        NombreRestaurante = b.getString("NombreRestaurante");
        NombreLocal = b.getString("NombreLocal");
        TipoComida = b.getString("TipoComida");
        ImagenLocal = b.getString("ImagenLocal");
        idRestaCategoria = b.getInt("idRestaCategoria");
        Nombre = b.getString("Nombre");
        alItem1 = b.getParcelableArrayList("alItem");
        alItemAdditional = b.getParcelableArrayList("alItemAdditional");
        //alPrecios = b.getParcelableArrayList("alItemProdPrecio");
        isBasePrice = b.getFloat("isBasePrice");
        insertId_ProductId = b.getInt("insertId");
        product_qtybyBundle = b.getInt("product_qty");
        TipoModelAdicional2Por1 = b.getString("TipoModelAdicional2Por1");
        TipoModelo2Por1 = b.getString("TipoModelo2Por1");
        idRestaProductoPrevious = b.getInt("idRestaProducto");


        menuitem_relativelayout = (RelativeLayout) view.findViewById(R.id.menuitem_relativelayout);
        menuitem_restImageview = (RoundedImageView) view.findViewById(R.id.menuitem_restImageview);
        menuitem_nameOfRestaurant = (TextView) view.findViewById(R.id.menuitem_nameOfRestaurant);
        menuitem_typeOfFood = (TextView) view.findViewById(R.id.menuitem_typeOfFood);
        menuitem_lv = (ListView) view.findViewById(R.id.menuitem_lv);
        menuitem_noItemHasCart = (LinearLayout) view.findViewById(R.id.menuitem_noItemHasCart);
        menuitem_backToWork = (TextView) view.findViewById(R.id.menuitem_backToWork);

        menuitem_lv.setOnItemClickListener(addItemClick);

        setData();
        setItem();

        setMessageErrorDialog(getResources().getString(R.string.twobyonemsg));




        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getView() == null) {
            return;
        }

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {

                if (keyEvent.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    // handle back button's click listener
                    if (isBackPress == false) {
                        addToCartManager.deleteTheCartById(insertId_ProductId);
                        DashBoardActivity.popBackStack();
                    }
                    return true;
                }
                return false;
            }
        });
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public void updateProduct() {
        addToCartManager.deleteTheCartById(insertId_ProductId);
    }

    private void setData() {
        Picasso.with(mContext).load("http://www.deliveryec.com/images/app/" + ImagenLocal)
                .into(menuitem_restImageview, new Callback() {
                    @Override
                    public void onSuccess() {
                    }

                    @Override
                    public void onError() {
                    }
                });
        menuitem_nameOfRestaurant.setText(NombreRestaurante + " - " + NombreLocal);
        menuitem_typeOfFood.setText(TipoComida);
    }

    private void setItem() {
        alItem = new ArrayList<>();
        for (int i = 0; i < alItem1.size(); i++) {
            if (alItem1.get(i).getEs2Status() == 1) {
                idRestaProducto += alItem1.get(i).getIdRestaProducto() + ",";
                alItem.add(alItem1.get(i));
            } else {
            }
        }

        if (alItem.size() > 0) {

            if (idRestaProducto != null && !idRestaProducto.equals("null") && !idRestaProducto.equals("")) {
                idRestaProducto = idRestaProducto.replace("null", "");
                idRestaProducto = idRestaProducto.substring(0, idRestaProducto.length() - 1);

            }
            jsonParser.getProductDetails(mContext, idRestaProducto, new ProductInterface() {
                @Override
                public void setProduct(ArrayList<ItemProductPrice> alPrecio,
                                       ArrayList<ItemAdditionalProduct> alProduct) {
                    alPrecio1 = alPrecio;
                    alProduct1 = alProduct;
                }
            });

            new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                @Override
                public void run() {
                    productOfferAdapter = new ProductOfferAdapter(mContext, alItem);
                    //menuitem_lv.setSelection(2);
                    menuitem_lv.setAdapter(productOfferAdapter);
                    //menuitem_lv.setSelection(2);
                }
            }, 200);

        }
    }

    private void makeItPrecios(ArrayList<ItemProductPrice> alPrecios) {
        if (alPrecios.size() == 1) {
            isNORMAL = true;
            basePrice = Float.valueOf(alPrecios.get(0).getPrecio());
        } else if (alPrecios.size() > 1) {
            TreeSet<String> altreeProdcut = new TreeSet<String>();
            for (ItemProductPrice precios : alPrecios) {
                boolean isChecked = precios.getGrupo().startsWith("BASE-");
                if (isChecked) {
                    try {
                        isNORMAL = true;
                        basePrice = Float.valueOf(precios.getPrecio());
                    } catch (NumberFormatException e) {
                        Log.e(TAG, "" + e.getMessage());
                    } catch (Exception e) {
                        Log.e(TAG, "" + e.getMessage());
                    }
                }
                altreeProdcut.add(precios.getGrupo());
            }

            Iterator<String> itr = altreeProdcut.iterator();

            while (itr.hasNext()) {
                String group = itr.next();

                /**
                 * Remove the Group Name - "BASE-PAN"
                 */

                if (!group.equals("BASE-PAN")) {

                    ArrayList<ItemProductPrice> alIPP = new ArrayList<ItemProductPrice>();
                    for (ItemProductPrice ipp : alPrecios) {
                        String matchGroupo = ipp.getGrupo();
                        if (group.equals(matchGroupo)) {
                            alIPP.add(ipp);
                        }
                    }
                    alSortPrecios.put(group, alIPP);
                }
            }
        }

    }

    private void makeItAditional(ArrayList<ItemAdditionalProduct> alItemAdditional) {
        TreeSet<String> altreeAditional = new TreeSet<>();
        for (ItemAdditionalProduct aditional : alItemAdditional) {
            altreeAditional.add(aditional.getGrupo());
        }

        Iterator<String> itr = altreeAditional.iterator();
        while (itr.hasNext()) {
            String group = itr.next();
            ArrayList<ItemAdditionalProduct> alIap = new ArrayList<>();
            for (ItemAdditionalProduct aditional : alItemAdditional) {
                String matchGroupo = aditional.getGrupo();
                if (group.equals(matchGroupo)) {
                    alIap.add(aditional);
                }
            }

            Collections.sort(alIap, new Comparator<ItemAdditionalProduct>() {
                @Override
                public int compare(ItemAdditionalProduct itemAdditionalProduct, ItemAdditionalProduct t1) {
                    return itemAdditionalProduct.getPrecio().compareTo(t1.getPrecio());
                }
            });

            additionalProduct.put(group, alIap);
        }
    }

    private void setMessageErrorDialog(String message) {
        messageDialog = new MessageDialog(mContext,
                android.R.style.Theme_Holo_Light_Dialog,
                message,
                new MessageDialog.okOnClickListener() {
                    @Override
                    public void onButtonClick() {
                        messageDialog.dismiss();
                    }
                });

        messageDialog.setCancelable(false);
        messageDialog.setCanceledOnTouchOutside(false);
        messageDialog.show();
    }

    public DecimalFormat doNumberFormatter() {
        NumberFormat nf = NumberFormat.getNumberInstance(Locale.ENGLISH);
        DecimalFormat dtime = (DecimalFormat) nf;
        dtime.applyPattern("##.##");
        return dtime;
    }

    public void notifyList() {
        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                int h0 = 0, h1 = 0;
                if (addToCart_Precioslv != null) {
                    h0 = UtilityMessage.setListViewHeightBasedOnChildren(addToCart_Precioslv);

                }

                if (addToCart_addAdditionallv != null) {
                    addToCart_addAdditionallv.setVisibility(View.VISIBLE);
                    h1 = UtilityMessage.setListViewHeightBasedOnChildren(addToCart_addAdditionallv);

                    int x = addTocart_scrollView.getScrollX();
                    int y = addTocart_scrollView.getScrollY();
                    final int totalY = y+60;

                    addTocart_scrollView.post(new Runnable() {
                        @Override
                        public void run() {
                            addTocart_scrollView.scrollTo(0,totalY);
                        }
                    });
                }
            }
        }, 300);

    }

    private void clearPrice() {
        Config.totalProductOfferPrecios = 0;
        Config.totalProductOfferAditional = 0.0f;
        Config.alhmProductOfferAditional.clear();
        Config.alhmProductOfferPrecios.clear();
    }


    public void addCartPrecios() {
        try {
            totalPrice = product_qty * (Config.totalProductOfferAditional +
                    Config.totalProductOfferPrecios + basePrice);
            DecimalFormat decimalFormat = doNumberFormatter();
            totalPrice = Float.parseFloat(decimalFormat.format(totalPrice));
            addToCart_totalPrice.setText("$ " + new DecimalFormat("0.00").format(totalPrice));

        } catch (NumberFormatException e) {
            Log.e(TAG, "" + e.toString());
        } catch (Exception e) {
            Log.e(TAG, "" + e.toString());
        }
    }

    public void addCartAdicionales() {
        try {
            totalPrice = product_qty * (Config.totalProductOfferAditional + Config.totalProductOfferPrecios + basePrice);
            addToCart_totalPrice.setText("$ " + new DecimalFormat("0.00").format(totalPrice));

        } catch (NumberFormatException e) {
            Log.e(TAG, "" + e.toString());
        } catch (Exception e) {
            Log.e(TAG, "" + e.toString());
        }
    }


    public void addTCardService_withoutadd(final String Nombre,
                                           final String Descripcion,
                                           String ImagenRestaProducto,
                                           final int idRestaProducto,
                                           final String EsDia2x1,
                                           final int position,
                                           final String TipoModelo2Por1, final String TipoModelAdicional2Por1) {


        triggerEveent = 0;

        final Dialog dialog = new Dialog(new ContextThemeWrapper(mContext, R.style.DialogSlideAnim));
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.addtocart_xml_lessdata);
        //final ListView addToCart_Precioslv = (ListView) dialog.findViewById(R.id.addToCart_Precioslv);
        //addToCart_addAdditionallv = (ListView) dialog.findViewById(R.id.addToCart_addAdditionallv);
        //View addToCart_linetwo = (View) dialog.findViewById(R.id.addToCart_linetwo);
        //final GothamBookTextView addToCart_additionaltv = (GothamBookTextView) dialog.findViewById(R.id.addToCart_additionaltv);
        View addToCart_linethree = (View) dialog.findViewById(R.id.addToCart_linethree);
        addToCart_totalPrice = (GothamBookTextView) dialog.findViewById(R.id.addToCart_totalPrice);
        ImageButton addToCart_minus = (ImageButton) dialog.findViewById(R.id.addToCart_minus);
        ImageButton addToCart_plus = (ImageButton) dialog.findViewById(R.id.addToCart_plus);
        addToCart_qty = (GothamBookEditTextView) dialog.findViewById(R.id.addToCart_qty);
        GothamBookTextView addToCart_agregaralcarrito = (GothamBookTextView) dialog.findViewById(R.id.addToCart_agregaralcarritotv);
        GothamBookTextView cancel = (GothamBookTextView) dialog.findViewById(R.id.cancel);
        addTocart_scrollView = (ScrollView)dialog.findViewById(R.id.addTocart_scrollView);

        final RoundedImageView menuitem_restImageview1 = (RoundedImageView) dialog.findViewById(R.id.menuitem_restImageview);
        ExtraBoldItalicTextView menuitem_nameOfRestaurant1 = (ExtraBoldItalicTextView) dialog.findViewById(R.id.menuitem_nameOfRestaurant);
        MediumTextView menuitem_typeOfFood1 = (MediumTextView) dialog.findViewById(R.id.menuitem_typeOfFood);
        BebaseNeueTextView menuitem_price1 = (BebaseNeueTextView) dialog.findViewById(R.id.menuitem_price);

        addToCart_linethree.setVisibility(View.GONE);

        menuitem_nameOfRestaurant1.setText(Nombre);
        menuitem_typeOfFood1.setText(Descripcion);

        Picasso.with(mContext).load("http://www.deliveryec.com/images/app/" + ImagenRestaProducto)
                .into(menuitem_restImageview1, new Callback() {
                    @Override
                    public void onSuccess() {
                        menuitem_restImageview1.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onError() {
                        menuitem_restImageview1.setVisibility(View.GONE);
                    }
                });

        if (isNORMAL) {
            isNORMAL = false;
            totalPrice = Float.parseFloat(decimalFormat.format(basePrice));
        } else {
            totalPrice = Float.parseFloat(decimalFormat.format(0.0f));
        }
        addToCart_totalPrice.setText("$ " + new DecimalFormat("0.00").format(totalPrice));

        addToCart_qty.setText("" + product_qty);

        menuitem_price1.setText("$" + " " + new DecimalFormat("0.00").format(basePrice));
        menuitem_price1.setVisibility(View.VISIBLE);



        addToCart_minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    if (product_qty > 1) {
                        product_qty = product_qty - 1;
                        totalPrice = product_qty * basePrice;
                        DecimalFormat decimalFormat = doNumberFormatter();
                        totalPrice = Float.parseFloat(decimalFormat.format(totalPrice));
                        addToCart_totalPrice.setText("$ " + new DecimalFormat("0.00").format(totalPrice));
                        addToCart_qty.setText("" + product_qty);
                    }

                } catch (NumberFormatException e) {
                    Log.e(TAG, "" + e.toString());
                } catch (Exception e) {
                    Log.e(TAG, "" + e.toString());
                }
            }
        });

        addToCart_plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                product_qty = product_qty + 1;
                totalPrice = product_qty * basePrice;
                DecimalFormat decimalFormat = doNumberFormatter();
                totalPrice = Float.parseFloat(decimalFormat.format(totalPrice));
                addToCart_totalPrice.setText("$ " + new DecimalFormat("0.00").format(totalPrice));
                addToCart_qty.setText("" + product_qty);
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isBackPress = false;
                totalPrice = 0.0f;
                product_qty = 1;
                triggerEveent = 0;
                dialog.dismiss();
            }
        });


        addToCart_agregaralcarrito.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (product_qty > product_qtybyBundle) {
                    setMessageErrorDialog(mContext.getResources().getString(R.string.limit_message) + " " + product_qtybyBundle);
                } else {
                    dialog.dismiss();

                    /**
                     * Case 3
                     */

                    addToCartManager.updateProductE2x(Nombre, Descripcion,
                            idRestaProducto, product_qty, totalPrice,
                            insertId_ProductId, TipoModelo2Por1, TipoModelAdicional2Por1);

                    validProductQTY += product_qty;
                    showLeftQTY();
                    addQTY();
                    dialog.dismiss();
                }
            }
        });



        dialog.show();
    }


    private void addTCardService(final String Nombre,
                                 final String Descripcion,
                                 String ImagenRestaProducto,
                                 final int idRestaProducto,
                                 final String EsDia2x1,
                                 final int position,
                                 final String TipoModelo2Por1,
                                 final String TipoModelAdicional2Por1,
                                 final float precio) {

        triggerEveent = 0;

        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(R.layout.addtocart_xml_new);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));

        addToCart_Precioslv = (ListView) dialog.findViewById(R.id.addToCart_Precioslv);
        addToCart_addAdditionallv = (ListView) dialog.findViewById(R.id.addToCart_addAdditionallv);
        View addToCart_linetwo = (View) dialog.findViewById(R.id.addToCart_linetwo);
        final TextView addToCart_additionaltv = (TextView) dialog.findViewById(R.id.addToCart_additionaltv);
        View addToCart_linethree = (View) dialog.findViewById(R.id.addToCart_linethree);
        addToCart_totalPrice = (GothamBookTextView) dialog.findViewById(R.id.addToCart_totalPrice);
        ImageButton addToCart_minus = (ImageButton) dialog.findViewById(R.id.addToCart_minus);
        ImageButton addToCart_plus = (ImageButton) dialog.findViewById(R.id.addToCart_plus);
        addToCart_qty = (GothamBookEditTextView) dialog.findViewById(R.id.addToCart_qty);
        ImageView aboutus_toolbar_backButton = (ImageView) dialog.findViewById(R.id.aboutus_toolbar_backButton);
        LinearLayout addToCart_agregaralcarrito = (LinearLayout) dialog.findViewById(R.id.addToCart_agregaralcarrito);
        final RoundedImageView menuitem_restImageview1 = (RoundedImageView) dialog.findViewById(R.id.menuitem_restImageview);
        GothamBoldTextView menuitem_nameOfRestaurant1 = (GothamBoldTextView) dialog.findViewById(R.id.menuitem_nameOfRestaurant);
        MediumTextView menuitem_typeOfFood1 = (MediumTextView) dialog.findViewById(R.id.menuitem_typeOfFood);
        TextView menuitem_price1 = (BebaseNeueTextView) dialog.findViewById(R.id.menuitem_price);
        addTocart_scrollView = (ScrollView)dialog.findViewById(R.id.addTocart_scrollView);

        menuitem_nameOfRestaurant1.setText(Nombre);
        menuitem_typeOfFood1.setText(Descripcion);



        Picasso.with(mContext).load("http://www.deliveryec.com/images/app/" + ImagenRestaProducto)
                .into(menuitem_restImageview1, new Callback() {
                    @Override
                    public void onSuccess() {
                        menuitem_restImageview1.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onError() {
                        menuitem_restImageview1.setVisibility(View.VISIBLE);
                    }
                });


        if (idRestaProductoPrevious == idRestaProducto) {
            basePrice = isBasePrice;
        } else {
            basePrice = precio;
        }

        totalPrice = basePrice;
        addToCart_totalPrice.setText("$ " + new DecimalFormat("0.00").format(totalPrice));
        addToCart_qty.setText("" + product_qty);

        if (alSortPrecios != null && alSortPrecios.size() > 0) {
            addToCart_Precioslv.setVisibility(View.VISIBLE);
            productOfferPreciosAdapter = new ProductOfferPreciosAdapter(mContext, alSortPrecios, fm);
            addToCart_Precioslv.setSelection(2);
            addToCart_Precioslv.setAdapter(productOfferPreciosAdapter);
            addToCart_Precioslv.setActivated(false);



        } else {
            addToCart_Precioslv.setVisibility(View.GONE);

            if (String.valueOf(basePrice).length() == 3) {
                String show = String.valueOf(basePrice) + "0";
                menuitem_price1.setText("$ " + show);
            } else {
                menuitem_price1.setText("$ " + Float.parseFloat(decimalFormat.format(basePrice)));
            }

            menuitem_price1.setVisibility(View.VISIBLE);
        }

        if (additionalProduct != null && additionalProduct.size() > 0) {
            Log.d("ver", "--"+additionalProduct.size());
            productOfferAdditionalAdapter = new
                    ProductOfferAdditionalAdapter(mContext, additionalProduct, fm);

            addToCart_addAdditionallv.setAdapter(productOfferAdditionalAdapter);
            productOfferAdditionalAdapter.notifyDataSetChanged();

        } else {
            addToCart_addAdditionallv.setVisibility(View.GONE);
            addToCart_linetwo.setVisibility(View.GONE);
            addToCart_additionaltv.setVisibility(View.GONE);
            addToCart_linethree.setVisibility(View.GONE);
        }

        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                if (addToCart_Precioslv.getVisibility() == View.VISIBLE) {
                    UtilityMessage.setListViewHeightBasedOnChildren(addToCart_Precioslv);
                }

                if (addToCart_addAdditionallv.getVisibility() == View.VISIBLE) {
                    UtilityMessage.setListViewHeightBasedOnChildren(addToCart_addAdditionallv);
                }
            }
        }, 500);


        addToCart_minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {

                    if (product_qty > 1) {
                        product_qty = product_qty - 1;
                        totalPrice = product_qty * (Config.totalProductOfferAditional + Config.totalProductOfferPrecios + basePrice);
                        DecimalFormat decimalFormat = doNumberFormatter();
                        totalPrice = Float.parseFloat(decimalFormat.format(totalPrice));
                        addToCart_totalPrice.setText("$ " + new DecimalFormat("0.00").format(totalPrice));
                        //addToCart_totalPrice.setText("$ " + totalPrice);
                        addToCart_qty.setText("" + product_qty);
                    }

                } catch (NumberFormatException e) {
                    Log.e(TAG, "" + e.toString());
                } catch (Exception e) {
                    Log.e(TAG, "" + e.toString());
                }
            }
        });

        addToCart_plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                product_qty = product_qty + 1;
                totalPrice = product_qty * (Config.totalProductOfferAditional + Config.totalProductOfferPrecios + basePrice);
                DecimalFormat decimalFormat = doNumberFormatter();
                totalPrice = Float.parseFloat(decimalFormat.format(totalPrice));
                addToCart_totalPrice.setText("$ " + new DecimalFormat("0.00").format(totalPrice));
                addToCart_qty.setText("" + product_qty);
            }
        });


        aboutus_toolbar_backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isBackPress = false;
                totalPrice = 0.0f;
                product_qty = 1;
                triggerEveent = 0;
                dialog.dismiss();
            }
        });

        addToCart_agregaralcarrito.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("ae","ahora si debo de quitar el precio");

                if (product_qty > product_qtybyBundle) {
                    setMessageErrorDialog(mContext.getResources().getString(R.string.limit_message) + " " + product_qtybyBundle);
                } else {

                    /**
                     * If Precios & adicionales both of then is greate
                     * Mean Precios is greater then 1 and adicional is greater then 0
                     * then add both of then and not any update process.
                     */

                    /**
                     * Case 1
                     */
                    if (Config.alhmProductOfferPrecios != null && alSortPrecios != null && alSortPrecios.size() > 1) {
                        Log.d(TAG, "entro 1");
                        triggerEveent = 1;
                        if (Config.alhmProductOfferPrecios.size() == 0) {
                            String[] mapKeys = alSortPrecios.keySet().toArray(new String[alSortPrecios.size()]);
                            setMessageErrorDialog("La opcion " + mapKeys[0].toUpperCase(Locale.ENGLISH) + " es requerida");
                            return;
                        } else {
                            int countOfKeys = 0;
                            StringBuilder key = new StringBuilder();

                            String[] mapKeys = alSortPrecios.keySet().toArray(new String[alSortPrecios.size()]);

                            for (Map.Entry m : Config.alhmProductOfferPrecios.entrySet()) {
                                key.append("'" + m.getKey() + "'");
                                key.append(",");
                                countOfKeys = countOfKeys + 1;
                            }

                            String outPutKeys = key.length() > 0 ? key.substring(0, key.length() - 1) : null;
                            //Log.v("outPutKeys",""+outPutKeys);
                            if (mapKeys.length != countOfKeys) {
                                setMessageErrorDialog("La opcion " + mapKeys[countOfKeys].toUpperCase(Locale.ENGLISH) + " es requerida");
                            } else {

                                int insertId = (int) addToCartManager.insertProductE2XOffers(Nombre, Descripcion,
                                        idRestaProducto, product_qty,
                                        totalPrice, insertId_ProductId,
                                        TipoModelo2Por1,
                                        TipoModelAdicional2Por1);


                                for (Map.Entry m : Config.alhmProductOfferPrecios.entrySet()) {

                                    ItemProductPrice itm = (ItemProductPrice) m.getValue();
                                    addToCartManager.insertPreciosE2X(itm, 1, insertId);

                                }

                                if (Config.alhmProductOfferAditional.size() > 0) {

                                    Map<Integer, ItemAdditionalProduct> alMap = new HashMap<Integer, ItemAdditionalProduct>();

                                    for (int l = 0; l < Config.alhmProductOfferAditional.size(); l++) {

                                        if (alMap.containsKey(Config.alhmProductOfferAditional.get(l).getIdRelaRestaProduAdiciona())) {
                                            ItemAdditionalProduct itm = (ItemAdditionalProduct) alMap.get(Config.alhmProductOfferAditional.get(l).getIdRelaRestaProduAdiciona());
                                            int qty = itm.getAdiciona_qty();
                                            qty = qty + 1;
                                            itm.setAdiciona_qty(qty);

                                            alMap.put(Config.alhmProductOfferAditional.get(l).getIdRelaRestaProduAdiciona(), itm);

                                        } else {
                                            ItemAdditionalProduct itm = Config.alhmProductOfferAditional.get(l);
                                            itm.setAdiciona_qty(1);
                                            alMap.put(Config.alhmProductOfferAditional.get(l).getIdRelaRestaProduAdiciona(), itm);
                                        }
                                    }


                                    for (Map.Entry m : alMap.entrySet()) {
                                        ItemAdditionalProduct itm = (ItemAdditionalProduct) m.getValue();
                                        addToCartManager.saveAdicionalesE2x(itm, insertId);
                                    }
                                }

                                validProductQTY += product_qty;
                                showLeftQTY();

                                addQTY();
                                Config.alhmProductOfferPrecios.clear();
                                Config.alhmProductOfferAditional.clear();
                                dialog.dismiss();

                            }
                        }
                    }

                    /**
                     * case 2
                     */

                    if (Config.alhmProductOfferAditional != null && triggerEveent == 0) {
                        Log.d(TAG, "entro 2");
                        Log.d("totalprice",String.valueOf(totalPrice));
                        totalPrice= totalPrice-basePrice;
                        if (additionalProduct != null && additionalProduct.size() > 0) {
                            triggerEveent = 1;
                            if (Config.alhmProductOfferAditional.size() == 0 && additionalProduct.size() > 0) {

                                //int insertId = (int) addToCartManager.insertProduct(Nombre, Descripcion, idRestaProducto, product_qty, totalPrice);

                                int insertId = (int) addToCartManager.insertProductE2XOffers(Nombre, Descripcion,
                                        idRestaProducto, product_qty,
                                        totalPrice, insertId_ProductId,
                                        TipoModelo2Por1,
                                        TipoModelAdicional2Por1);

                                addQTY();
                                dialog.dismiss();
                                isBackPress = true;
                                DashBoardActivity.popBackStack();


                            } else if (Config.alhmProductOfferAditional.size() > 0 && additionalProduct.size() > 0) {

                                int insertId = (int) addToCartManager.insertProductE2XOffers(Nombre,
                                        Descripcion, idRestaProducto,
                                        product_qty, totalPrice,
                                        insertId_ProductId,
                                        TipoModelo2Por1,
                                        TipoModelAdicional2Por1);



                                Map<Integer, ItemAdditionalProduct> alMap = new HashMap<Integer, ItemAdditionalProduct>();

                                for (int k = 0; k < Config.alhmProductOfferAditional.size(); k++) {

                                    if (alMap.containsKey(Config.alhmProductOfferAditional.get(k).getIdRelaRestaProduAdiciona())) {
                                        ItemAdditionalProduct itm = (ItemAdditionalProduct) alMap.get(Config.alhmProductOfferAditional.get(k).getIdRelaRestaProduAdiciona());
                                        int qty = itm.getAdiciona_qty();
                                        qty = qty + 1;
                                        itm.setAdiciona_qty(qty);

                                        alMap.put(Config.alhmProductOfferAditional.get(k).getIdRelaRestaProduAdiciona(), itm);

                                    } else {
                                        ItemAdditionalProduct itm = Config.alhmProductOfferAditional.get(k);
                                        itm.setAdiciona_qty(1);
                                        alMap.put(Config.alhmProductOfferAditional.get(k).getIdRelaRestaProduAdiciona(), itm);
                                    }
                                    //Log.v("size", "" + alMap.size());
                                }

                                for (Map.Entry m : alMap.entrySet()) {
                                    ItemAdditionalProduct itm = (ItemAdditionalProduct) m.getValue();
                                    addToCartManager.saveAdicionalesE2x(itm, insertId);
                                }

                                validProductQTY += product_qty;

                                showLeftQTY();
                                addQTY();
                                dialog.dismiss();

                            }
                        }
                    }
                }
            }
        });



        dialog.show();
    }


    AdapterView.OnItemClickListener addItemClick = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, final int i, long l) {

            new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                @Override
                public void run() {
                    clearPrice();
                    final int key = alItem.get(i).getIdRestaProducto();
                    final String Nombre = alItem.get(i).getNombre();
                    final String Descripcion = alItem.get(i).getDescripcion();
                    final String ImagenRestaProducto = alItem.get(i).getImagenRestaProducto();
                    final String EsDia2x1 = alItem.get(i).getEsDia2x1();
                    final float precio = 0f;//alItem.get(i).getPrecio();

                    alSortPrecios = new TreeMap<>();
                    additionalProduct = new TreeMap<>();

                    basePrice = 0.0f;

                    ArrayList<ItemProductPrice> alPrecios = new ArrayList<>();
                    String nvd= String.valueOf(isBasePrice);
                    if(alPrecio1!=null) {
                        for (int j = 0; j < alPrecio1.size(); j++) {
                            //Log.d("alprecio1", Arrays.toString(alPrecio1));
                            if (alPrecio1.get(j).getIdRestaProducto() == key ) {
                                Log.d("alprecio1",String.valueOf(j)+".-"+alPrecio1.get(j).getGrupo());
                                Log.d("alprecio1", String.format("%s.-%s", String.valueOf(j), alPrecio1.get(j).getPrecio()));

                              //  if (!alPrecio1.get(j).getGrupo().startsWith("BASE-")) {
                                alPrecios.add(alPrecio1.get(j));

                                    //alPrecio1.
                               // }
                            }

                        }
                        //alPrecios.set(2, alPrecio1.get(2));

                    }

                    if (alPrecios != null && alPrecios.size() > 0 && !alPrecios.equals("")) {
                        makeItPrecios(alPrecios);
                    } else {
                        alSortPrecios.clear();
                    }

                    ArrayList<ItemAdditionalProduct> alItemAdditional = new ArrayList<>();

                    if(alProduct1!=null) {
                        for (int k = 0; k < alProduct1.size(); k++) {
                            if (alProduct1.get(k).getIdRestaProducto() == key) {

                                alItemAdditional.add(alProduct1.get(k));
                            }
                        }
                    }

                    if (alItemAdditional != null && alItemAdditional.size() > 0 && !alItemAdditional.equals("")) {
                        makeItAditional(alItemAdditional);
                    } else {
                        additionalProduct.clear();
                    }



                    if (alSortPrecios != null && alSortPrecios.size() == 0 && additionalProduct != null && additionalProduct.size() == 0) {
                        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                addTCardService_withoutadd(Nombre, Descripcion, ImagenRestaProducto,
                                        key, EsDia2x1, i, TipoModelo2Por1, TipoModelAdicional2Por1);

                                showMessage();
                            }
                        },200);
                    } else if (alSortPrecios != null && alSortPrecios.size() > 0 && additionalProduct != null && additionalProduct.size() == 0) {

                        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                addTCardService(Nombre, Descripcion, ImagenRestaProducto,
                                        key, EsDia2x1, i, TipoModelo2Por1, TipoModelAdicional2Por1
                                        , precio);
                                //showMessage();
                            }
                        },200);


                    }

                    if (additionalProduct != null && additionalProduct.size() > 0) {

                        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                addTCardService(Nombre, Descripcion, ImagenRestaProducto,
                                        key, EsDia2x1, i, TipoModelo2Por1, TipoModelAdicional2Por1,
                                        precio);
                            }
                        },200);
                    }
                }
            }, 500);
        }
    };

    public interface ProductInterface {
        public void setProduct(ArrayList<ItemProductPrice> alPrecio,
                               ArrayList<ItemAdditionalProduct> alProduct);
    }

    private void addQTY() {
        int totalSum = addToCartManager.getTotalQuntity();
        if (totalSum > 0) {
            //Show the quntity
            DashBoardActivity.showQuntity();
        } else {
            //hide the quntity
            DashBoardActivity.hideQuntity();
        }
        //total sum
        DashBoardActivity.setQuntity(totalSum);
        totalPrice = 0.0f;
        product_qty = 1;
    }

    private void showLeftQTY(){
        isBackPress = true;
        DashBoardActivity.popBackStack();
        /*if(product_qtybyBundle==validProductQTY){

        }else{
            int leftQty = product_qtybyBundle-validProductQTY;
            setMessageErrorDialog(mContext.getResources().getString(R.string.left_qty)+" "+
                    leftQty+" más");
        }*/
    }

    private void showMessage(){
        if(TipoModelo2Por1.equals("1") && TipoModelAdicional2Por1.equals("1")){
            setMessageErrorDialog(mContext.getResources().getString(R.string.tipo_1_adi_1));
        }

        if(TipoModelo2Por1.equals("2") && TipoModelAdicional2Por1.equals("1")){
            setMessageErrorDialog(mContext.getResources().getString(R.string.tipo_2_adi_1));
        }

        if(TipoModelo2Por1.equals("1") && TipoModelAdicional2Por1.equals("2")){
            setMessageErrorDialog(mContext.getResources().getString(R.string.tipo_1_adi_2));
        }

        if(TipoModelo2Por1.equals("2") && TipoModelAdicional2Por1.equals("2")){
            setMessageErrorDialog(mContext.getResources().getString(R.string.tipo_2_adi_2));
        }
    }


}
