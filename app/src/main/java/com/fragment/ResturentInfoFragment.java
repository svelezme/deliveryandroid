package com.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.activity.DashBoardActivity;
import com.app.grupodeliveryec.deliveryEC.R;
import com.github.siyamed.shapeimageview.RoundedImageView;
import com.model.RestaurantListModel;
import com.squareup.picasso.Picasso;

/**
 * Created by administrator on 25/1/17.
 */

public class ResturentInfoFragment extends Fragment {

    String TAG = getClass().getName();
    protected static Context context;
    protected static Fragment fragment;
    protected static FragmentManager fragmentManager;
    protected static Bundle b;
    protected RestaurantListModel restaurantListModel;

    RoundedImageView info_restImageview;
    TextView info_nameOfRestaurant,info_typeOfFood;
    TextView info_openCloseStatus,rest_list_minimumPrice;
    TextView rest_list_shippingCost,rest_list_deliveryTime;
    TextView info_instagram,info_facebook,info_twitter;
    TextView info_website,info_HoursOfAttention;
    TextView info_Address,info_Tipoffood,info_general;

    TextView info_siteWebTV,info_twitterTv;
    TextView info_facebookTv,info_instagramTv;
    View info_siteWebLine,info_twitterLine;
    View info_facebookLine,info_instagramLine;

    View view;

    public static Fragment getInstance(Context mContext, FragmentManager fragmentManager, Bundle bundle) {
        fragment = new ResturentInfoFragment();
        context = mContext;
        b = bundle;

        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        DashBoardActivity.updateTitle(getResources().getString(R.string.Informacion));
        DashBoardActivity.hideBackButton();
        DashBoardActivity.hideSearchButton();
        DashBoardActivity.showCerrar();
        DashBoardActivity.hideInfo();
        DashBoardActivity.hideLogout();
        DashBoardActivity.hideAddDeliveryAddress();
        DashBoardActivity.hideMenuPanel();

        if(view==null){
            view = inflater.inflate(R.layout.resturent_info_xml,container,false);
            restaurantListModel = (RestaurantListModel) b.getSerializable("restaurantModel");

            info_restImageview = (RoundedImageView)view.findViewById(R.id.info_restImageview);
            info_nameOfRestaurant = (TextView)view.findViewById(R.id.info_nameOfRestaurant);
            info_typeOfFood = (TextView)view.findViewById(R.id.info_typeOfFood);
            info_openCloseStatus = (TextView)view.findViewById(R.id.info_openCloseStatus);
            rest_list_minimumPrice = (TextView)view.findViewById(R.id.rest_list_minimumPrice);
            rest_list_shippingCost = (TextView)view.findViewById(R.id.rest_list_shippingCost);
            rest_list_deliveryTime = (TextView)view.findViewById(R.id.rest_list_deliveryTime);
            info_instagram = (TextView)view.findViewById(R.id.info_instagram);
            info_facebook = (TextView)view.findViewById(R.id.info_facebook);
            info_twitter = (TextView)view.findViewById(R.id.info_twitter);
            info_website = (TextView)view.findViewById(R.id.info_website);
            info_HoursOfAttention = (TextView)view.findViewById(R.id.info_HoursOfAttention);
            info_Address = (TextView)view.findViewById(R.id.info_Address);
            info_Tipoffood = (TextView)view.findViewById(R.id.info_Tipoffood);

            info_siteWebTV = (TextView)view.findViewById(R.id.info_siteWebTV);
            info_siteWebLine = (View)view.findViewById(R.id.info_siteWebLine);

            info_twitterTv = (TextView)view.findViewById(R.id.info_twitterTv);
            info_twitterLine = (View)view.findViewById(R.id.info_twitterLine);

            info_facebookTv = (TextView)view.findViewById(R.id.info_facebookTv);
            info_facebookLine = (View)view.findViewById(R.id.info_facebookLine);

            info_instagramTv = (TextView)view.findViewById(R.id.info_instagramTv);
            info_instagramLine = (View)view.findViewById(R.id.info_instagramLine);

            info_general = (TextView)view.findViewById(R.id.info_general);

            setData();


           // String Direccion = restaurantListModel.getDireccion();
            //Log.e("Direccion",""+Direccion);
        }
        return view;
    }

  /*  @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }*/

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void setData(){
        Picasso.with(context)
                .load("http://www.deliveryec.com/images/app/"
                +restaurantListModel.getImagenLocal())
                .error(getResources().getDrawable(R.drawable.default_image))
                .placeholder(getResources().getDrawable(R.drawable.default_image))
                .into(info_restImageview);

        if(!restaurantListModel.getNombreRestaurante().equals("")){
            info_nameOfRestaurant.setText(restaurantListModel.getNombreRestaurante()
            +" - "+restaurantListModel.getNombreLocal());
        }else{
            info_nameOfRestaurant.setVisibility(View.GONE);
        }


        if(!restaurantListModel.getInfoGeneral().equals("")){
            info_general.setText(restaurantListModel.getInfoGeneral());
            info_general.setVisibility(View.VISIBLE);
        }else{
            info_general.setVisibility(View.GONE);
        }

        if(!restaurantListModel.getTipoComida().equals("")){
            info_typeOfFood.setText(restaurantListModel.getTipoComida());
            info_Tipoffood.setText(restaurantListModel.getTipoComida());
            info_Tipoffood.setVisibility(View.VISIBLE);
            info_typeOfFood.setVisibility(View.VISIBLE);
        }else{
            info_Tipoffood.setVisibility(View.GONE);
            info_typeOfFood.setVisibility(View.GONE);
        }

        if(restaurantListModel.getActivo().equals("I")){
            info_openCloseStatus.setText(getResources().getString(R.string.CERRADO));
            info_openCloseStatus.setBackground(getResources()
                    .getDrawable(R.drawable.info_close_status_background));
            info_openCloseStatus.setTextColor(getResources().getColor(R.color.dark_red));
        }else{
            info_openCloseStatus.setText(getResources().getString(R.string.OPEN));
            info_openCloseStatus.setBackground(getResources()
                    .getDrawable(R.drawable.info_open_status_background));
            info_openCloseStatus.setTextColor(getResources().getColor(R.color.light_green));
        }

        if(!restaurantListModel.getMinimo().equals("")){
            rest_list_minimumPrice.setText("$ "+restaurantListModel.getMinimo());
        }else{
            rest_list_minimumPrice.setVisibility(View.INVISIBLE);
        }

        if(!restaurantListModel.getCostoEnvio().equals("")){
            rest_list_shippingCost.setText("$ "+restaurantListModel.getCostoEnvio());
        }else{
            rest_list_shippingCost.setVisibility(View.INVISIBLE);
        }

        if(!restaurantListModel.getTiempoEntrega().equals("")){
            rest_list_deliveryTime.setText(restaurantListModel.getTiempoEntrega());
        }else{
            rest_list_deliveryTime.setVisibility(View.GONE);
        }

        if(!restaurantListModel.getDireccion().equals("")){
            info_Address.setText(restaurantListModel.getDireccion());
        }else{
            info_Address.setVisibility(View.GONE);
        }

        if(!restaurantListModel.getHoraApertura().equals("")
                && !restaurantListModel.getHoraCierre().equals("")){

            info_HoursOfAttention.setText(restaurantListModel.getHoraApertura()
                    +" - " +restaurantListModel.getHoraCierre());
        }else{
            info_HoursOfAttention.setVisibility(View.GONE);
        }

        if(!restaurantListModel.getDireccionWeb().equals("")){
            info_website.setText(restaurantListModel.getDireccionWeb());
        }else{
            info_website.setVisibility(View.GONE);
            info_siteWebTV.setVisibility(View.GONE);
            info_siteWebLine.setVisibility(View.GONE);
        }

        if(!restaurantListModel.getTwitter().equals("")){
            info_twitter.setText(restaurantListModel.getTiempoEntrega());
        }else{
            info_twitter.setVisibility(View.GONE);
            info_twitterTv.setVisibility(View.GONE);
            info_twitterLine.setVisibility(View.GONE);
        }

        if(!restaurantListModel.getFacebook().equals("")){
            info_facebook.setText(restaurantListModel.getFacebook());
            info_facebookTv.setVisibility(View.VISIBLE);
        }else{
            info_facebook.setVisibility(View.GONE);
            info_facebookTv.setVisibility(View.GONE);
            info_siteWebLine.setVisibility(View.GONE);
        }

        if(!restaurantListModel.getInstagram().equals("")){
            info_instagram.setText(restaurantListModel.getInstagram());
            info_instagramTv.setVisibility(View.VISIBLE);
        }else{
            info_instagram.setVisibility(View.GONE);
            info_instagramTv.setVisibility(View.GONE);
            info_instagramLine.setVisibility(View.GONE);
        }

    }
}
