package com.fragment;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.view.ContextThemeWrapper;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.TextView;
import com.activity.DashBoardActivity;
import com.app.grupodeliveryec.deliveryEC.R;
import com.bruce.pickerview.LoopScrollListener;
import com.bruce.pickerview.LoopView;
import com.com.rest.API;
import com.com.rest.GlobalValues;
import com.com.rest.MethodListener;
import com.model.AreaModel;
import com.model.CityModel;
import com.utility.AppController;
import com.utility.FragmentIDs;
import com.utility.MessageDialog;
import com.utility.SessionManager;
import com.view.BebaseNeueTextView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;



/**
 * Created by administrator on 20/1/17.
 */

public class ResturentSearchFragment extends Fragment implements NumberPicker.OnValueChangeListener {

    String TAG = getClass().getName();
    String apiGetLocals = "apiGetLocals";
    protected static Context context;
    protected static Fragment fragment;
    protected static FragmentManager fragmentManager;
    LoopView loopView;
    BebaseNeueTextView resturent_city, resturent_area;
    LinearLayout searchBtn;
    ImageView resturent_progressBar;
    MessageDialog.okOnClickListener okOnClickListener;
    MessageDialog messageDialog;
    Animation animation;
    ArrayList<CityModel> alCityList;
    ArrayList<AreaModel> alAreaList;
    ArrayList<AreaModel> alFilterAreaList;
    int IdCiudad, IdSector;
    View view;

    public static Fragment getInstance(Context mContext, FragmentManager fm) {
        fragment = new ResturentSearchFragment();
        context = mContext;
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        DashBoardActivity.updateTitle(getResources().getString(R.string.DeliveryEC));
        DashBoardActivity.hideSearchButton();
        DashBoardActivity.hideBackButton();
        DashBoardActivity.hideCerrar();
        DashBoardActivity.hideInfo();
        DashBoardActivity.showHorizentalBar();
        DashBoardActivity.hideLogout();
        DashBoardActivity.hideAddDeliveryAddress();
        DashBoardActivity.hideMenuPanel();
        DashBoardActivity.hideUserPerfilIcon();

        DashBoardActivity.setBackgroundColor(getResources().getColor(R.color.hint_color_1),
                Color.TRANSPARENT,
                Color.TRANSPARENT,
                Color.TRANSPARENT);


        view = inflater.inflate(R.layout.resturent_search_frg, container, false);
        resturent_city = (BebaseNeueTextView) view.findViewById(R.id.resturent_city);
        resturent_area = (BebaseNeueTextView) view.findViewById(R.id.resturent_area);
        searchBtn = (LinearLayout) view.findViewById(R.id.searchBtn);
        resturent_progressBar = (ImageView) view.findViewById(R.id.resturent_progressBar);

        resturent_city.setOnClickListener(addCitySearch);
        resturent_area.setOnClickListener(addAreaSearch);
        searchBtn.setOnClickListener(addSearchResult);

        resturent_city.setEnabled(false);
        resturent_city.setClickable(false);
        getGetLocals();

        return view;

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        AppController.getInstance().cancelPendingRequests(apiGetLocals);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    private void getGetLocals() {
        resturent_city.setEnabled(true);
        resturent_city.setClickable(true);

        String url = API.localList;
        showProgressBar();
        GlobalValues.getMethodManagerObj(context).makeStringReq(url, apiGetLocals, new MethodListener() {
            @Override
            public void onError() {
                hideProgressbar();
            }

            @Override
            public void onError(String response) {
                hideProgressbar();
            }

            @Override
            public void onSuccess(String response) {
                hideProgressbar();
                try {
                    JSONObject mObj = new JSONObject(response);
                    JSONArray mCity = mObj.optJSONArray("city");
                    if (mCity != null) {
                        getParseLocalList(mCity);
                    }
                } catch (JSONException e) {
                    Log.e(TAG, "" + e.toString());
                }
            }
        });
    }

    private void getParseLocalList(JSONArray mCity) {
        alCityList = new ArrayList<>();
        alAreaList = new ArrayList<>();

        for (int i = 0; i < mCity.length(); i++) {
            JSONObject object = mCity.optJSONObject(i);

            CityModel cityModel = new CityModel();
            cityModel.setIdCiudad(object.optInt("IdCiudad"));
            cityModel.setNombre(object.optString("Nombre"));
            cityModel.setSiglas(object.optString("Siglas"));
            cityModel.setEstado(object.optString("Estado"));

            JSONArray msectors = object.optJSONArray("sectors");
            getSectors(msectors);

            alCityList.add(cityModel);
        }
    }

    private void getSectors(JSONArray msectors) {
        for (int i = 0; i < msectors.length(); i++) {
            JSONObject object = msectors.optJSONObject(i);
            AreaModel areaModel = new AreaModel();
            areaModel.setIdCiudad(object.optInt("IdCiudad"));
            areaModel.setIdSector(object.optInt("IdSector"));
            areaModel.setNombre(object.optString("Nombre"));
            areaModel.setSiglas(object.optString("Siglas"));
            areaModel.setEstado(object.optString("Estado"));
            alAreaList.add(areaModel);
        }
    }
    private void openCityDialog() {
        if (alCityList == null) {
            getGetLocals();
        } else if (alCityList != null && alCityList.size() == 0) {
            getGetLocals();
        } else {

            resturent_city.setEnabled(true);
            resturent_city.setClickable(true);

            final Dialog cityDialog = new Dialog(new ContextThemeWrapper(context, R.style.DialogSlideAnim));
            cityDialog.getWindow().setGravity(Gravity.BOTTOM);
            cityDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            cityDialog.setContentView(R.layout.areadialog_xml2);
            final LoopView cityDialog_cityPicker = (LoopView) cityDialog.findViewById(R.id.loop_view);
            final TextView cityDialog_cancel = (TextView) cityDialog.findViewById(R.id.areaDialog_cancel);
            final TextView cityDialog_accept = (TextView) cityDialog.findViewById(R.id.areaDialog_accept);

            //cityDialog_cityPicker.setInitPosition(0);
            cityDialog_cityPicker.setCanLoop(false);

            ArrayList<String> area_list = new ArrayList<>();
            for (int i = 0; i < alCityList.size(); i++) {
                area_list.add(i, alCityList.get(i).getNombre());
            }

            cityDialog_cityPicker.setTextSize(22);//must be called before setDateList
            cityDialog_cityPicker.setDataList(area_list);


            cityDialog_cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (cityDialog != null) {
                        cityDialog.dismiss();
                    }
                }
            });
            cityDialog_accept.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (cityDialog != null) {
                        IdCiudad = alCityList.get(cityDialog_cityPicker.getSelectedItem()).getIdCiudad();
                        SessionManager.save_cityid_Ciudad(getActivity() , ""+IdCiudad);
                        resturent_city.setText("" + alCityList.get(cityDialog_cityPicker.getSelectedItem()).getNombre());
                        cityDialog.dismiss();


                        alFilterAreaList = new ArrayList<AreaModel>();
                        for (int i = 0; i < alAreaList.size(); i++) {
                            if (alAreaList.get(i).getIdCiudad() == IdCiudad) {
                                alFilterAreaList.add(alAreaList.get(i));
                            }
                        }
                    }

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            resturent_area.setText("");
                            resturent_area.setHint(context.getResources().getString(R.string.SELECCIONA_TU_SECTOR));
                        }
                    },200);
                }
            });
            cityDialog.show();
        }
    }

    private void openAreaDialog() {

        if (alFilterAreaList != null) {
            if (alFilterAreaList.size() > 0) {
                final Dialog areaDialog = new Dialog(new ContextThemeWrapper(context, R.style.DialogSlideAnim));
                areaDialog.getWindow().setGravity(Gravity.BOTTOM);
                areaDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                areaDialog.setContentView(R.layout.areadialog_xml2);

                TextView areaDialog_cancel = (TextView) areaDialog.findViewById(R.id.areaDialog_cancel);
                TextView areaDialog_accept = (TextView) areaDialog.findViewById(R.id.areaDialog_accept);
                loopView = (LoopView) areaDialog.findViewById(R.id.loop_view);

                loopView.setInitPosition(0);
                loopView.setCanLoop(false);
                loopView.setLoopListener(new LoopScrollListener() {
                    @Override
                    public void onItemSelect(int item) {
                    }
                });

                ArrayList<String> area_list = new ArrayList<>();
                for (int i = 0; i < alFilterAreaList.size(); i++) {
                    area_list.add(i, alFilterAreaList.get(i).getNombre());
                }

                loopView.setTextSize(22);
                loopView.setDataList(area_list);
                areaDialog_cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (areaDialog != null) {
                            areaDialog.dismiss();
                        }
                    }
                });

                areaDialog_accept.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (areaDialog != null) {
                            IdSector = alFilterAreaList.get(loopView.getSelectedItem()).getIdSector();
                            SessionManager.save_areaid_sector(getActivity() , ""+IdSector);
                            resturent_area.setText("" + alFilterAreaList.get(loopView.getSelectedItem()).getNombre());
                            areaDialog.dismiss();
                        }
                    }
                });
                areaDialog.show();
            }
        }
    }

    private void showProgressBar() {
        resturent_progressBar.setVisibility(View.VISIBLE);
        animation = AnimationUtils.loadAnimation(context, R.anim.progress_anim);
        animation.setDuration(1000);
        resturent_progressBar.startAnimation(animation);
    }

    private void hideProgressbar() {
        if (animation != null) {
            resturent_progressBar.clearAnimation();
            resturent_progressBar.setVisibility(View.GONE);
        }
    }

    private void setMessageErrorDialog(String message) {
        messageDialog = new MessageDialog(context,
                android.R.style.Theme_Holo_Light_Dialog,
                message,
                new MessageDialog.okOnClickListener() {
                    @Override
                    public void onButtonClick() {
                        messageDialog.dismiss();
                    }
                });

        messageDialog.setCancelable(false);
        messageDialog.setCanceledOnTouchOutside(false);
        messageDialog.show();
    }


    View.OnClickListener addCitySearch = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            resturent_city.setEnabled(false);
            resturent_city.setClickable(false);
            openCityDialog();
        }
    };

    View.OnClickListener addAreaSearch = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (resturent_city.getText().toString().equals("")) {
                setMessageErrorDialog(getResources().getString(R.string.PRIMERO_DEBE_SELECCIONAR_SU_CLUDAD));
            } else {

                alFilterAreaList = new ArrayList<AreaModel>();
                for (int i = 0; i < alAreaList.size(); i++) {
                    if (alAreaList.get(i).getIdCiudad() == IdCiudad) {
                        alFilterAreaList.add(alAreaList.get(i));
                    }
                }
                openAreaDialog();
            }
        }
    };

    View.OnClickListener addSearchResult = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (resturent_city.getText().toString().equals("") &&
                    resturent_area.getText().toString().equals("")) {
                String message1 = "Debe escoger ciudad";
                String message2 = "Debe escoger sector";
                String message = message1 + "\n" + message2;

                setMessageErrorDialog(message);

            } else if (resturent_city.getText().toString().equals("")) {
                String message = "Debe escoger ciudad";
                setMessageErrorDialog(message);
            } else if (resturent_area.getText().toString().equals("")) {
                String message = "Debe escoger sector";
                setMessageErrorDialog(message);
            } else {
                /***
                 * Call the web service to search restaurant
                 */

                //API.volleyClearCache();

                Bundle b = new Bundle();
                b.putInt("IdCiudad", IdCiudad);
                b.putInt("IdSector", IdSector);
                SessionManager.save_rastaurant_id(context , ""+IdSector);
                DashBoardActivity.displayFragment(FragmentIDs.ResturentSearchResultFragment_Id, b);
            }
        }
    };

    @Override
    public void onValueChange(NumberPicker numberPicker, int i, int i1) {
    }

}
