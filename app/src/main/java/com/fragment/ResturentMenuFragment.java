package com.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.ScrollView;
import android.widget.TextView;
import com.activity.DashBoardActivity;
import com.adapter.OpinionAdap;
import com.adapter.Opinions_adapter;
import com.adapter.RestraurantMenuAdapter;
import com.app.grupodeliveryec.deliveryEC.R;
import com.bean.OpinionModel;
import com.com.rest.API;
import com.com.rest.GlobalValues;
import com.com.rest.MethodListener;
import com.github.siyamed.shapeimageview.RoundedImageView;
import com.manager.AddToCartManager;
import com.model.RestaurantListModel;
import com.model.RestaurantMenuList;
import com.squareup.picasso.Picasso;
import com.utility.ClearCart;
import com.utility.Config;
import com.utility.FragmentIDs;
import com.view.BebaseNeueTextView;
import com.view.ExoBoldItalicTextView;
import com.view.ExtraBoldItalicTextView;
import com.view.GothamBoldTextView;
import com.view.GothamBookTextView;
import com.view.MediumTextView;
import com.view.RegularTextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 * Created by administrator on 27/1/17.
 */

public class ResturentMenuFragment extends Fragment {

    final int sdk = android.os.Build.VERSION.SDK_INT;
    private static Context mContext;
    private static FragmentManager fragmentManager;
    private static Bundle b;
    private static Fragment fragment;
    protected RestaurantListModel restaurantListModel;
    String TAG = getClass().getName();
    String apiTAG = "RestaurentMenu";
    View view;
    RoundedImageView info_restImageview;
    ExtraBoldItalicTextView info_nameOfRestaurant;
    MediumTextView info_typeOfFood;
    ListView resturent_lv;
    LinearLayout menuitem_noItemHasCart;
    ImageView menu_progressBar;
    TextView menuitem_backToWork;
    Animation animation;
    RestraurantMenuAdapter restraurantMenuAdapter;
    ArrayList<RestaurantMenuList> arrayList;
    ClearCart.cartOnClickListener cartOnClickListener;
    ClearCart clearCart;
    AddToCartManager addToCartManager;
    ExoBoldItalicTextView rest_list_openStatus;
    BebaseNeueTextView rest_list_minimo,rest_list_costodeenvio,rest_list_tempoentrega;
    LinearLayout menu_LinearMenu,menu_LinearOpiniones,menu_Linearinformacion;
    LinearLayout menu_layout,opinion_layout;
    ScrollView information_layout;
    View menu_MenuLine,menu_OpinionesLine,menu_InformacionLine;
    RecyclerView opnionrecyclerview;
    OpinionAdap opinionAdap;
    RatingBar  ratingBar;
    BebaseNeueTextView text_rating;
    ArrayList<OpinionModel> opinion_list;
    RegularTextView comment_text;
    LinearLayout menu_linearLayout;

    GothamBookTextView info_instagram,info_facebook,info_twitter;
    GothamBookTextView info_website,info_HoursOfAttention;
    GothamBookTextView info_Address,info_Tipoffood,info_general;
    GothamBoldTextView info_siteWebTV,info_twitterTv;

    GothamBoldTextView info_facebookTv,info_instagramTv;
    View info_siteWebLine,info_twitterLine;
    View info_facebookLine,info_instagramLine,line_hoursofatten;
    RatingBar rest_ratingBar;

    View.OnClickListener addToMenu = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            menuitem_noItemHasCart.setVisibility(View.GONE);
            resturent_lv.setVisibility(View.VISIBLE);
        }
    };

    View.OnClickListener addMenuLayout = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            menu_MenuLine.setVisibility(View.VISIBLE);
            menu_OpinionesLine.setVisibility(View.GONE);
            menu_InformacionLine.setVisibility(View.GONE);

            menu_layout.setVisibility(View.VISIBLE);
            opinion_layout.setVisibility(View.GONE);
            information_layout.setVisibility(View.GONE);
        }
    };

    View.OnClickListener addInformacionLayout = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            menu_MenuLine.setVisibility(View.GONE);
            menu_OpinionesLine.setVisibility(View.GONE);
            menu_InformacionLine.setVisibility(View.VISIBLE);

            menu_layout.setVisibility(View.GONE);
            opinion_layout.setVisibility(View.GONE);
            information_layout.setVisibility(View.VISIBLE);
        }
    };

    View.OnClickListener addOpinionesLayout = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            menu_MenuLine.setVisibility(View.GONE);
            menu_OpinionesLine.setVisibility(View.VISIBLE);
            menu_InformacionLine.setVisibility(View.GONE);

            menu_layout.setVisibility(View.GONE);
            opinion_layout.setVisibility(View.VISIBLE);
            information_layout.setVisibility(View.GONE);
            if (opinion_list == null){
                get_opinions();
            }
        }
    };

    AdapterView.OnItemClickListener mOnItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

            if (arrayList.size() > 0) {
                int idRestaLocal = restaurantListModel.getIdRestaLocal();
                String NombreRestaurante = restaurantListModel.getNombreRestaurante();
                String NombreLocal = restaurantListModel.getNombreLocal();
                String TipoComida = restaurantListModel.getTipoComida();
                String ImagenLocal = restaurantListModel.getImagenLocal();

                int idRestaCategoria = arrayList.get(i).getIdRestaCategoria();
                String Nombre = arrayList.get(i).getNombre();

                Config.idRestaCategoria = idRestaCategoria;
                Config.Nombre = Nombre;

                Bundle bundle = new Bundle();
                bundle.putInt("idRestaLocal", idRestaLocal);
                bundle.putString("NombreRestaurante", NombreRestaurante);
                bundle.putString("NombreLocal", NombreLocal);
                bundle.putString("TipoComida", TipoComida);
                bundle.putString("ImagenLocal", ImagenLocal);
                bundle.putInt("idRestaCategoria", idRestaCategoria);
                bundle.putString("Nombre", Nombre);
                bundle.putSerializable("restaurantListModel", restaurantListModel);
                DashBoardActivity.displayFragment(FragmentIDs.ResturentMenuItemFragment_Id, bundle);
            }
        }
    };

    public static Fragment getInstance(Context context, FragmentManager fragmentManager, Bundle bundle) {
        fragment = new ResturentMenuFragment();
        mContext = context;
        b = bundle;
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        try {
            addToCartManager = new AddToCartManager(mContext);
            restaurantListModel = (RestaurantListModel) b.getSerializable("restaurantModel");

            // DashBoardActivity.updateTitle(restaurantListModel.getNombreRestaurante());
            DashBoardActivity.updateTitle(mContext.getResources().getString(R.string.DELIVERYEC));
            DashBoardActivity.showBackButton();
            DashBoardActivity.hideCerrar();
            DashBoardActivity.hideSearchButton();
            DashBoardActivity.hideInfo();
            DashBoardActivity.hideHorizentalBar();
            DashBoardActivity.hideLogout();
            DashBoardActivity.hideAddDeliveryAddress();
            DashBoardActivity.showMenuPanel();
            DashBoardActivity.hideUserPerfilIcon();

            view = inflater.inflate(R.layout.resturent_menu_xml, container, false);

            resturent_lv = (ListView) view.findViewById(R.id.resturent_lv);
            info_nameOfRestaurant = (ExtraBoldItalicTextView) view.findViewById(R.id.info_nameOfRestaurant);
            info_typeOfFood = (MediumTextView) view.findViewById(R.id.info_typeOfFood);
            info_restImageview = (RoundedImageView) view.findViewById(R.id.info_restImageview);
            menu_progressBar = (ImageView) view.findViewById(R.id.menu_progressBar);
            menuitem_noItemHasCart = (LinearLayout) view.findViewById(R.id.menuitem_noItemHasCart);
            menuitem_backToWork = (TextView) view.findViewById(R.id.menuitem_backToWork);
            rest_list_openStatus = (ExoBoldItalicTextView)view.findViewById(R.id.rest_list_openStatus);
            rest_list_minimo = (BebaseNeueTextView)view.findViewById(R.id.rest_list_minimo);
            rest_list_costodeenvio = (BebaseNeueTextView)view.findViewById(R.id.rest_list_costodeenvio);
            rest_list_tempoentrega = (BebaseNeueTextView)view.findViewById(R.id.rest_list_tempoentrega);

            menu_linearLayout = (LinearLayout) view.findViewById(R.id.menu_linearLayout);

            menu_LinearMenu = (LinearLayout)view.findViewById(R.id.menu_LinearMenu);
            menu_LinearOpiniones = (LinearLayout)view.findViewById(R.id.menu_LinearOpiniones);
            menu_Linearinformacion = (LinearLayout)view.findViewById(R.id.menu_Linearinformacion);
            ratingBar = (RatingBar) view.findViewById(R.id.ratingBar);
            text_rating = (BebaseNeueTextView) view.findViewById(R.id.text_ratting);
            comment_text = (RegularTextView) view.findViewById(R.id.comment_text);

            menu_layout = (LinearLayout)view.findViewById(R.id.menu_layout);
            opinion_layout = (LinearLayout)view.findViewById(R.id.opinion_layout);
            information_layout = (ScrollView) view.findViewById(R.id.information_layout);

            opnionrecyclerview = (RecyclerView) view.findViewById(R.id.opnion_recyclerview);

            menu_MenuLine = (View)view.findViewById(R.id.menu_MenuLine);
            menu_OpinionesLine = (View)view.findViewById(R.id.menu_OpinionesLine);
            menu_InformacionLine = (View)view.findViewById(R.id.menu_InformacionLine);
            line_hoursofatten= (View)view.findViewById(R.id.line_hoursofatten);

            rest_ratingBar = (RatingBar) view.findViewById(R.id.rest_ratingBar);


            info_instagram = (GothamBookTextView)view.findViewById(R.id.info_instagram);
            info_facebook = (GothamBookTextView)view.findViewById(R.id.info_facebook);
            info_twitter = (GothamBookTextView)view.findViewById(R.id.info_twitter);
            info_website = (GothamBookTextView)view.findViewById(R.id.info_website);
            info_HoursOfAttention = (GothamBookTextView)view.findViewById(R.id.info_HoursOfAttention);
            info_Address = (GothamBookTextView)view.findViewById(R.id.info_Address);
            info_Tipoffood = (GothamBookTextView)view.findViewById(R.id.info_Tipoffood);

            info_siteWebTV = (GothamBoldTextView)view.findViewById(R.id.info_siteWebTV);
            info_siteWebLine = (View)view.findViewById(R.id.info_siteWebLine);

            info_twitterTv = (GothamBoldTextView)view.findViewById(R.id.info_twitterTv);
            info_twitterLine = (View)view.findViewById(R.id.info_twitterLine);

            info_facebookTv = (GothamBoldTextView)view.findViewById(R.id.info_facebookTv);
            info_facebookLine = (View)view.findViewById(R.id.info_facebookLine);

            info_instagramTv = (GothamBoldTextView)view.findViewById(R.id.info_instagramTv);
            info_instagramLine = (View)view.findViewById(R.id.info_instagramLine);

            info_general = (GothamBookTextView)view.findViewById(R.id.info_general);

            menu_MenuLine.setVisibility(View.VISIBLE);

            resturent_lv.setOnItemClickListener(mOnItemClickListener);
            menuitem_backToWork.setOnClickListener(addToMenu);

            menu_LinearMenu.setOnClickListener(addMenuLayout);
            menu_LinearOpiniones.setOnClickListener(addOpinionesLayout);
            menu_Linearinformacion.setOnClickListener(addInformacionLayout);

            arrayList = restaurantListModel.getArrayList();

            opnionrecyclerview.setHasFixedSize(true);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mContext);
            opnionrecyclerview.setLayoutManager(mLayoutManager);
            opnionrecyclerview.setItemAnimator(new DefaultItemAnimator());

            ArrayList<OpinionModel> opinion_list = new ArrayList<>();
            opinionAdap = new OpinionAdap(mContext , opinion_list);
            opnionrecyclerview.setAdapter(opinionAdap);

            setMenuData();
            DashBoardActivity.menuButtonLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    menuitem_noItemHasCart.setVisibility(View.GONE);
                }
            });

            DashBoardActivity.cartButtonLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int totalSum = addToCartManager.getTotalQuntity();
                    if (totalSum > 0) {
                        int idRestaLocal = restaurantListModel.getIdRestaLocal();
                        String NombreRestaurante = restaurantListModel.getNombreRestaurante();
                        String NombreLocal = restaurantListModel.getNombreLocal();
                        String TipoComida = restaurantListModel.getTipoComida();
                        String ImagenLocal = restaurantListModel.getImagenLocal();

                        Bundle bundle = new Bundle();
                        bundle.putSerializable("restaurantModel", restaurantListModel);
                        // bundle.putInt("screenStatus",1);
                        bundle.putInt("idRestaLocal", idRestaLocal);
                        bundle.putString("NombreRestaurante", NombreRestaurante);
                        bundle.putString("NombreLocal", NombreLocal);
                        bundle.putString("TipoComida", TipoComida);
                        bundle.putString("ImagenLocal", ImagenLocal);
                        bundle.putInt("idRestaCategoria", Config.idRestaCategoria);
                        bundle.putString("Nombre", Config.Nombre);

                        Config.orderScreenStatus = 1;

                        Config.orderScreenStatus = 1;
                        DashBoardActivity.displayFragment(FragmentIDs.OrderFragment_Id, bundle);

                    } else {
                        menuitem_noItemHasCart.setVisibility(View.VISIBLE);
                    }
                }
            });
            setData();

        }catch (Exception e){
            e.printStackTrace();
        }
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        int totalSum = addToCartManager.getTotalQuntity();
        if (totalSum > 0) {
            DashBoardActivity.showQuntity();
            DashBoardActivity.setQuntity(totalSum);

            onBack();
        } else {
            DashBoardActivity.hideQuntity();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    private void setData(){

        if(!restaurantListModel.getNombreRestaurante().equals("")){
            info_nameOfRestaurant.setText(restaurantListModel.getNombreRestaurante()
                    +" - "+restaurantListModel.getNombreLocal());
        }else{
            info_nameOfRestaurant.setVisibility(View.GONE);
        }

        if(!restaurantListModel.getInfoGeneral().equals("")){
            info_general.setText(restaurantListModel.getInfoGeneral());
            info_general.setVisibility(View.VISIBLE);
        }else{
            info_general.setVisibility(View.GONE);
        }

        if(!restaurantListModel.getTipoComida().equals("")){
            info_typeOfFood.setText(restaurantListModel.getTipoComida());
            info_Tipoffood.setText(restaurantListModel.getTipoComida());
            info_Tipoffood.setVisibility(View.VISIBLE);
            info_typeOfFood.setVisibility(View.VISIBLE);
        }else{
            info_Tipoffood.setVisibility(View.GONE);
            info_typeOfFood.setVisibility(View.GONE);
        }

//        condition to show only open resaturant menu opinion and information IMP CONDITION

        if(restaurantListModel.getActivo().equals("I")){
            menu_linearLayout.setVisibility(View.GONE);
            menu_layout.setVisibility(View.GONE);
            opinion_layout.setVisibility(View.GONE);
            information_layout.setVisibility(View.VISIBLE);
            DashBoardActivity.hideMenuPanel();
        }else {
            menu_linearLayout.setVisibility(View.VISIBLE);
            menu_layout.setVisibility(View.VISIBLE);
            opinion_layout.setVisibility(View.GONE);
            information_layout.setVisibility(View.GONE);
            DashBoardActivity.showMenuPanel();
        }

        ///////////**************************////////////////////

        try {
            rest_ratingBar.setRating(restaurantListModel.getRating());
        }catch (Exception ec){
        }

        if(!restaurantListModel.getDireccion().equals("")){
            info_Address.setText(restaurantListModel.getDireccion());
        }else{
            info_Address.setVisibility(View.GONE);
        }

        if(!restaurantListModel.getHoraApertura().equals("")
                && !restaurantListModel.getHoraCierre().equals("")){

            info_HoursOfAttention.setText(restaurantListModel.getHoraApertura()
                    +" - " +restaurantListModel.getHoraCierre());
        }else{
            info_HoursOfAttention.setVisibility(View.GONE);
            line_hoursofatten.setVisibility(View.GONE);
        }

        if(!restaurantListModel.getDireccionWeb().equals("")){
            info_website.setText(restaurantListModel.getDireccionWeb());
        }else{
            info_website.setVisibility(View.GONE);
            info_siteWebTV.setVisibility(View.GONE);
            info_siteWebLine.setVisibility(View.GONE);
        }

        if(!restaurantListModel.getTwitter().equals("")){
            info_twitter.setText(restaurantListModel.getTwitter());
        }else{
            info_twitter.setVisibility(View.GONE);
            info_twitterTv.setVisibility(View.GONE);
            info_twitterLine.setVisibility(View.GONE);
        }

        if(!restaurantListModel.getFacebook().equals("")){
            info_facebook.setText(restaurantListModel.getFacebook());
            info_facebookTv.setVisibility(View.VISIBLE);
        }else{
            info_facebook.setVisibility(View.GONE);
            info_facebookTv.setVisibility(View.GONE);
           // info_siteWebLine.setVisibility(View.GONE);
            info_facebookLine.setVisibility(View.GONE);
        }

        if(!restaurantListModel.getInstagram().equals("")){
            info_instagram.setText(restaurantListModel.getInstagram());
            info_instagramTv.setVisibility(View.VISIBLE);
        }else{
            info_instagram.setVisibility(View.GONE);
            info_instagramTv.setVisibility(View.GONE);
            info_instagramLine.setVisibility(View.GONE);
        }

    }

    public void onBack() {
        if (getView() == null) {
            return;
        }

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {

                if (keyEvent.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    // handle back button's click listener

                    clearCart = new ClearCart(mContext, android.R.style.Theme_Holo_Light_Dialog, new ClearCart.cartOnClickListener() {
                        @Override
                        public void yesOnButtonClick() {
                            clearCart.dismiss();
                            addToCartManager.dropCart();
                            DashBoardActivity.onBackViaKey();
                        }

                        @Override
                        public void noOnButtonClick() {
                            clearCart.dismiss();
                        }
                    });

                    clearCart.setCanceledOnTouchOutside(false);
                    clearCart.setCancelable(false);
                    clearCart.show();
                    return true;
                }
                return false;
            }
        });
    }

    private void setMenuData() {

        Picasso.with(mContext)
                .load(restaurantListModel.getBaseUrl() + restaurantListModel.getImagenLocal())
                .error(getResources().getDrawable(R.drawable.default_image))
                .placeholder(getResources().getDrawable(R.drawable.default_image))
                .into(info_restImageview);


        Log.d(TAG, "setMenuData: "+restaurantListModel.getBaseUrl() + restaurantListModel.getImagenLocal());
        info_nameOfRestaurant.setText(restaurantListModel.getNombreRestaurante() + " - " + restaurantListModel.getNombreLocal());
        info_typeOfFood.setText("( "+restaurantListModel.getTipoComida()+" )");

        try {
            ratingBar.setRating(restaurantListModel.getRating());
            text_rating.setText(""+new DecimalFormat("0.0").format(restaurantListModel.getRating()));
        }catch (Exception e){
        }


        if(restaurantListModel.getActivo().equals("I")){
            rest_list_openStatus.setText(mContext.getResources().getString(R.string.CERRADO));

            if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                rest_list_openStatus.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.info_close_status_background));
            } else {
                rest_list_openStatus.setBackground(mContext.getResources().getDrawable(R.drawable.info_close_status_background));
            }
        }else{
            rest_list_openStatus.setText(mContext.getResources().getString(R.string.OPEN));
            if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                rest_list_openStatus.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.info_open_status_background));
            } else {
                rest_list_openStatus.setBackground(mContext.getResources().getDrawable(R.drawable.info_open_status_background));
            }
        }

        rest_list_minimo.setText("$ "+restaurantListModel.getMinimo());
        rest_list_costodeenvio.setText("$ "+restaurantListModel.getCostoEnvio());
        rest_list_tempoentrega.setText(restaurantListModel.getTiempoEntrega());

        restraurantMenuAdapter = new RestraurantMenuAdapter(mContext, arrayList);
        resturent_lv.setAdapter(restraurantMenuAdapter);
    }

    private void showProgressBar() {
        menu_progressBar.setVisibility(View.VISIBLE);
        animation = AnimationUtils.loadAnimation(mContext, R.anim.progress_anim);
        animation.setDuration(1000);
        menu_progressBar.startAnimation(animation);
    }

    private void hideProgressbar() {
        if (animation != null) {
            menu_progressBar.clearAnimation();
            menu_progressBar.setVisibility(View.GONE);
        }
    }

    public void showRestaurants() {
        Bundle bundle = new Bundle();
        bundle.putSerializable("restaurantModel", restaurantListModel);
        DashBoardActivity.displayFragment(FragmentIDs.ResturentInfoFragment_Id, bundle);
    }

    private void get_opinions() {

         String url = API.opinion_url /*+ "id_restalocal=4";*/+ "id_restalocal=" + ""+restaurantListModel.getIdRestaLocal();

         showProgressBar();

        GlobalValues.getMethodManagerObj(mContext).makeStringReqWithoutCache(url, "GetReviews", new MethodListener() {
            @Override
            public void onError() {
                hideProgressbar();
            }

            @Override
            public void onError(String response) {
                hideProgressbar();
            }

            @Override
            public void onSuccess(String response) {
                hideProgressbar();
                try {
                    JSONObject mObj = new JSONObject(response);
                    JSONArray mArray = mObj.optJSONArray("Reviews");
                     opinion_list = new ArrayList<OpinionModel>();

                    for (int i=0 ; i< mArray.length() ; i++){
                        JSONObject jsonObject = mArray.getJSONObject(i);
                        OpinionModel opinionModel = new OpinionModel();
                        opinionModel.setId_review(jsonObject.optString("id_review"));
                        opinionModel.setFecha_de_revision(jsonObject.optString("fecha_de_revision"));
                        opinionModel.setId_restalocal(jsonObject.optInt("id_restalocal"));
                        opinionModel.setResta_average_rate(jsonObject.optString("clasificacion"));
                        opinionModel.setTx_apellidos(jsonObject.optString("tx_apellidos"));
                        opinionModel.setRevision(jsonObject.optString("revision"));
                        opinionModel.setTx_nombres(jsonObject.optString("tx_nombres"));
                        opinion_list.add(opinionModel);
                    }
                    opinionAdap = new OpinionAdap(mContext , opinion_list);
                    opnionrecyclerview.setAdapter(opinionAdap);
                    comment_text.setText("Basado on "+opinion_list.size()+" comentarios");

                } catch (JSONException e) {
                    Log.e(TAG, "" + e.toString());
                }
            }
        });

    }
}
