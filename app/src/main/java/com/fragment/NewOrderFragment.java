package com.fragment;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.view.ContextThemeWrapper;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.activity.DashBoardActivity;
import com.app.grupodeliveryec.deliveryEC.R;
import com.bean.AddToCartItem;
import com.bean.OrderData;
import com.bruce.pickerview.LoopScrollListener;
import com.bruce.pickerview.LoopView;
import com.com.rest.API;
import com.com.rest.GlobalValues;
import com.com.rest.MethodListener;
import com.inputview.ExoSemiEditTextView;
import com.manager.AddToCartManager;
import com.model.AreaModel;
import com.model.CityModel;
import com.model.ItemAdditionalProduct;
import com.model.ItemProductPrice;
import com.model.RestaurantListModel;
import com.utility.Config;
import com.utility.EmailValidation;
import com.utility.FragmentIDs;
import com.utility.MessageDialog;
import com.utility.SessionManager;
import com.utility.UtilityMessage;
import com.view.GothamBoldTextView;
import com.view.GothamBookTextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by administrator on 21/3/17.
 */

public class NewOrderFragment extends Fragment {

    String TAG = getClass().getName();
   // final int sdk = android.os.Build.VERSION.SDK_INT;
    private static Context mContext;
    private static FragmentManager fm;
    public static Bundle b;
    private static Fragment fragment;
    public static boolean isFromNewOrder = false;

    View view;

    ExoSemiEditTextView nombre, perfil_email, apellidos, cellular, contrasena, confirm_contrasena;
    ExoSemiEditTextView nombreestadirection, ciudad, sector, direction1, direction2, telefono, referencia;
    ExoSemiEditTextView nombra_pera, razonsocial, cadulaoruk, direction, telefono2;
    GothamBoldTextView total;
    ExoSemiEditTextView newOrder_formdePago;
    RestaurantListModel restaurantModel;
    AddToCartManager addToCartManager;

    String pForma;
    String minimo;
    String costEncio;
    float sumOfPrice = 0.0f;
    float totalPrice = 0.0f;
    ArrayList<AddToCartItem> alOrderItem;


    ImageView resturent_progressBar;
    Animation animation;
    LinearLayout order_finishOrder;
    ArrayList<CityModel> alCityList;
    ArrayList<AreaModel> alAreaList;
    LoopView loopView;
    int IdCiudad, IdSector;
    ArrayList<AreaModel> alFilterAreaList;
    String apiGetLocals = "apiGetLocals";
    MessageDialog messageDialog;
    GothamBookTextView facturar_checl_uncheck, acurdo_checl_uncheck;
    public static int facturar_checl_uncheck_STATUS = 0;
    public static int acurdo_checl_uncheck_STATUS = 0;
    String order_commentBox;

    ProgressDialog pDialog;

    LinearLayout showidfactu, delivery_address;


    public static Fragment getInstance(Context context, FragmentManager fragmentManager, Bundle bundle) {
        fragment = new NewOrderFragment();
        mContext = context;
        fm = fragmentManager;
        b = bundle;
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        //DashBoardActivity.order_back_status = 1;
        DashBoardActivity.hideHorizentalBar();
        DashBoardActivity.hideInfo();
        DashBoardActivity.hideLogout();
        DashBoardActivity.hideSearchButton();
        DashBoardActivity.hideCerrar();
        DashBoardActivity.showBackButton();
        DashBoardActivity.showMenuPanel();

        NewOrderFragment.isFromNewOrder = false;
        Log.d("Sam", "estoy en new order fragment");
        view = inflater.inflate(R.layout.new_order, container, false);

        nombre = (ExoSemiEditTextView) view.findViewById(R.id.nombre);
        perfil_email = (ExoSemiEditTextView) view.findViewById(R.id.perfil_email);
        apellidos = (ExoSemiEditTextView) view.findViewById(R.id.apellidos);
        cellular = (ExoSemiEditTextView) view.findViewById(R.id.cellular);
        contrasena = (ExoSemiEditTextView) view.findViewById(R.id.contrasena);
        confirm_contrasena = (ExoSemiEditTextView) view.findViewById(R.id.confirm_contrasena);

        nombreestadirection = (ExoSemiEditTextView) view.findViewById(R.id.nombreestadirection);
        ciudad = (ExoSemiEditTextView) view.findViewById(R.id.ciudad);
        sector = (ExoSemiEditTextView) view.findViewById(R.id.sector);
        direction1 = (ExoSemiEditTextView) view.findViewById(R.id.direction1);
        direction2 = (ExoSemiEditTextView) view.findViewById(R.id.direction2);
        telefono = (ExoSemiEditTextView) view.findViewById(R.id.telefono);
        referencia = (ExoSemiEditTextView) view.findViewById(R.id.referencia);

        nombra_pera = (ExoSemiEditTextView) view.findViewById(R.id.nombra_pera);
        razonsocial = (ExoSemiEditTextView) view.findViewById(R.id.razonsocial);
        cadulaoruk = (ExoSemiEditTextView) view.findViewById(R.id.cadulaoruk);
        direction = (ExoSemiEditTextView) view.findViewById(R.id.direction);
        telefono2 = (ExoSemiEditTextView) view.findViewById(R.id.telefono2);

        facturar_checl_uncheck = (GothamBookTextView) view.findViewById(R.id.facturar_checl_uncheck);
        acurdo_checl_uncheck = (GothamBookTextView) view.findViewById(R.id.acurdo_checl_uncheck);
        total = (GothamBoldTextView) view.findViewById(R.id.total);
        resturent_progressBar = (ImageView) view.findViewById(R.id.resturent_progressBar);

        order_finishOrder = (LinearLayout) view.findViewById(R.id.order_finishOrder);
        showidfactu = (LinearLayout) view.findViewById(R.id.showidfactu);
        delivery_address = (LinearLayout) view.findViewById(R.id.delivery_address);
        newOrder_formdePago = (ExoSemiEditTextView)view.findViewById(R.id.newOrder_formdePago);

        DashBoardActivity.cartButtonLayout.setClickable(false);

        restaurantModel = (RestaurantListModel) b.getSerializable("restaurantModel");
        order_commentBox = b.getString("order_commentBox");
        addToCartManager = new AddToCartManager(mContext);

        order_finishOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int pIdRestaLocal = restaurantModel.getIdRestaLocal();
                String url = API.ValidaLocalAbierto+"pIdRestaLocal="+pIdRestaLocal+"&callback=";
                GlobalValues.getMethodManagerObj(mContext).makeStringReqWithoutCache(url, "apiValidaLocalAbierto", new MethodListener() {
                    @Override
                    public void onError() {
                    }

                    @Override
                    public void onError(String response) {
                    }

                    @Override
                    public void onSuccess(String response) {
                        //({"data":[{"respuesta":"NO"}]})

                        try{
                            response = response.substring(1,response.length()-1);
                            JSONObject mObj = new JSONObject(response);

                            JSONArray array = mObj.optJSONArray("data");
                            for(int i= 0 ;i<array.length();i++){
                                JSONObject object = array.optJSONObject(i);
                                if(object.optString("respuesta").equals("NO")){
                                    setMessageErrorDialog(mContext.getResources().getString(R.string.validaLocalabierto_message));
                                }else{
                                    orderValidation();
                                }
                            }
                        }catch (Exception e){
                            Log.e(TAG,""+e.toString());
                        }
                    }
                });
            }
        });

        addToCartManager = new AddToCartManager(mContext);

        ciudad.setOnClickListener(addCitySearch);
        sector.setOnClickListener(addAreaSearch);

        facturar_checl_uncheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (facturar_checl_uncheck_STATUS == 0) {
                    facturar_checl_uncheck_STATUS = 1;
                    showidfactu.setVisibility(View.VISIBLE);
                    facturar_checl_uncheck.setCompoundDrawablesWithIntrinsicBounds(R.drawable.checkbox_active, 0, 0, 0);
                } else {
                    showidfactu.setVisibility(View.GONE);
                    facturar_checl_uncheck_STATUS = 0;
                    facturar_checl_uncheck.setCompoundDrawablesWithIntrinsicBounds(R.drawable.checkbox_inactive, 0, 0, 0);
                }
            }
        });

        acurdo_checl_uncheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (acurdo_checl_uncheck_STATUS == 0) {
                    acurdo_checl_uncheck_STATUS = 1;
                    acurdo_checl_uncheck.setCompoundDrawablesWithIntrinsicBounds(R.drawable.checkbox_active, 0, 0, 0);
                } else {
                    acurdo_checl_uncheck_STATUS = 0;
                    acurdo_checl_uncheck.setCompoundDrawablesWithIntrinsicBounds(R.drawable.checkbox_inactive, 0, 0, 0);
                }
            }
        });
        delivery_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                if (SessionManager.getUserId(mContext) != 0) {
                    DashBoardActivity.displayFragment(FragmentIDs.PerfilListFragment_Id, bundle);
                } else {
                    DashBoardActivity.removePopupBackstack(FragmentIDs.OrderFragment_Tag);
                    DashBoardActivity.removePopupBackstack(FragmentIDs.NewOrderFragment_Tag);
                    NewOrderFragment.isFromNewOrder = true;
                    DashBoardActivity.login_back_status = 1;
                    DashBoardActivity.displayFragment(FragmentIDs.PerfilFragment_Id, bundle);
                }
            }
        });

        newOrder_formdePago.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showListaFormapago();
            }
        });

        alOrderItem = getOrderRecord();
        minimo = restaurantModel.getMinimo();
        try {
            if (minimo.length() == 3) {
                minimo = minimo + "0";
            }
        } catch (Exception e) {
            Log.e(TAG,""+e.toString());
        }
        costEncio = restaurantModel.getCostoEnvio().replace(",", ".");

        try {
            totalPrice = sumOfPrice + Float.parseFloat(costEncio); //Float.parseFloat(minimo)
            total.setText("$ " + new DecimalFormat("0.00").format(totalPrice));
        } catch (NumberFormatException e) {
            total.setText(" $ " + totalPrice);
            Log.e(TAG, "" + e.toString());
        } catch (Exception e) {
            total.setText(" $ " + totalPrice);
            Log.e(TAG, "" + e.toString());
        }

        return view;
    }

    View.OnClickListener addCitySearch = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            ciudad.setEnabled(false);
            ciudad.setClickable(false);
            openCityDialog();
        }
    };

    View.OnClickListener addAreaSearch = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (ciudad.getText().toString().equals("")) {
                setMessageErrorDialog(getResources().getString(R.string.PRIMERO_DEBE_SELECCIONAR_SU_CLUDAD));
            } else {

                alFilterAreaList = new ArrayList<AreaModel>();
                for (int i = 0; i < alAreaList.size(); i++) {
                    if (alAreaList.get(i).getIdCiudad() == IdCiudad) {
                        alFilterAreaList.add(alAreaList.get(i));
                    }
                }
                openAreaDialog();
            }
        }
    };

    private void setMessageErrorDialog(String message) {
        messageDialog = new MessageDialog(getActivity(),
                android.R.style.Theme_Holo_Light_Dialog,
                message,
                new MessageDialog.okOnClickListener() {
                    @Override
                    public void onButtonClick() {
                        messageDialog.dismiss();
                    }
                });

        messageDialog.setCancelable(false);
        messageDialog.setCanceledOnTouchOutside(false);
        messageDialog.show();
    }

    private void getParseLocalList(JSONArray mCity) {
        alCityList = new ArrayList<>();
        alAreaList = new ArrayList<>();

        for (int i = 0; i < mCity.length(); i++) {
            JSONObject object = mCity.optJSONObject(i);

            CityModel cityModel = new CityModel();
            cityModel.setIdCiudad(object.optInt("IdCiudad"));
            cityModel.setNombre(object.optString("Nombre"));
            cityModel.setSiglas(object.optString("Siglas"));
            cityModel.setEstado(object.optString("Estado"));

            JSONArray msectors = object.optJSONArray("sectors");
            getSectors(msectors);

            alCityList.add(cityModel);
        }

    }

    private void getSectors(JSONArray msectors) {
        for (int i = 0; i < msectors.length(); i++) {
            JSONObject object = msectors.optJSONObject(i);
            AreaModel areaModel = new AreaModel();
            areaModel.setIdCiudad(object.optInt("IdCiudad"));
            areaModel.setIdSector(object.optInt("IdSector"));
            areaModel.setNombre(object.optString("Nombre"));
            areaModel.setSiglas(object.optString("Siglas"));
            areaModel.setEstado(object.optString("Estado"));
            alAreaList.add(areaModel);
        }
    }

    private void openCityDialog() {
        if (alCityList == null) {
            getGetLocals();
        } else if (alCityList != null && alCityList.size() == 0) {
            getGetLocals();
        } else {

            ciudad.setEnabled(true);
            ciudad.setClickable(true);

            final Dialog cityDialog = new Dialog(new ContextThemeWrapper(getActivity(), R.style.DialogSlideAnim));
            cityDialog.getWindow().setGravity(Gravity.BOTTOM);
            cityDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            cityDialog.setContentView(R.layout.areadialog_xml2);
            final LoopView cityDialog_cityPicker = (LoopView) cityDialog.findViewById(R.id.loop_view);
            final TextView cityDialog_cancel = (TextView) cityDialog.findViewById(R.id.areaDialog_cancel);
            final TextView cityDialog_accept = (TextView) cityDialog.findViewById(R.id.areaDialog_accept);

            //cityDialog_cityPicker.setInitPosition(0);
            cityDialog_cityPicker.setCanLoop(false);

            ArrayList<String> area_list = new ArrayList<>();
            for (int i = 0; i < alCityList.size(); i++) {
                area_list.add(i, alCityList.get(i).getNombre());
            }

            cityDialog_cityPicker.setTextSize(22);//must be called before setDateList
            cityDialog_cityPicker.setDataList(area_list);


            cityDialog_cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (cityDialog != null) {
                        cityDialog.dismiss();
                    }
                }
            });
            cityDialog_accept.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (cityDialog != null) {
                        IdCiudad = alCityList.get(cityDialog_cityPicker.getSelectedItem()).getIdCiudad();
                        SessionManager.save_cityid_Ciudad(getActivity(), "" + IdCiudad);
                        // IdSector = loopView.getSelectedItem().getIdSector();
                        ciudad.setText("" + alCityList.get(cityDialog_cityPicker.getSelectedItem()).getNombre());
                        cityDialog.dismiss();

                        alFilterAreaList = new ArrayList<AreaModel>();
                        for (int i = 0; i < alAreaList.size(); i++) {
                            if (alAreaList.get(i).getIdCiudad() == IdCiudad) {
                                alFilterAreaList.add(alAreaList.get(i));
                            }
                        }
                    }
                }
            });
            cityDialog.show();
        }
    }

    private void openAreaDialog() {

        if (alFilterAreaList != null) {
            if (alFilterAreaList.size() > 0) {
                final Dialog areaDialog = new Dialog(new ContextThemeWrapper(getActivity(), R.style.DialogSlideAnim));
                areaDialog.getWindow().setGravity(Gravity.BOTTOM);
                areaDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                areaDialog.setContentView(R.layout.areadialog_xml2);

                TextView areaDialog_cancel = (TextView) areaDialog.findViewById(R.id.areaDialog_cancel);
                TextView areaDialog_accept = (TextView) areaDialog.findViewById(R.id.areaDialog_accept);
                loopView = (LoopView) areaDialog.findViewById(R.id.loop_view);

                loopView.setInitPosition(0);
                loopView.setCanLoop(false);
                loopView.setLoopListener(new LoopScrollListener() {
                    @Override
                    public void onItemSelect(int item) {
                    }
                });

                ArrayList<String> area_list = new ArrayList<>();
                for (int i = 0; i < alFilterAreaList.size(); i++) {
                    area_list.add(i, alFilterAreaList.get(i).getNombre());
                }

                loopView.setTextSize(22);
                loopView.setDataList(area_list);

                areaDialog_cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (areaDialog != null) {
                            areaDialog.dismiss();
                        }
                    }
                });

                areaDialog_accept.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (areaDialog != null) {
                            IdSector = alFilterAreaList.get(loopView.getSelectedItem()).getIdSector();
                            SessionManager.save_areaid_sector(getActivity(), "" + IdSector);
                            sector.setText("" + alFilterAreaList.get(loopView.getSelectedItem()).getNombre());
                            areaDialog.dismiss();
                        }
                    }
                });
                areaDialog.show();
            }
        }
    }

    private void getGetLocals() {
        ciudad.setEnabled(true);
        ciudad.setClickable(true);

        String url = API.localList;
        //showProgressBar();
        GlobalValues.getMethodManagerObj(getActivity()).makeStringReq(url, apiGetLocals, new MethodListener() {
            @Override
            public void onError() {
                //  hideProgressbar();
            }

            @Override
            public void onError(String response) {
                //hideProgressbar();
            }

            @Override
            public void onSuccess(String response) {
                //hideProgressbar();
                try {
                    JSONObject mObj = new JSONObject(response);
                    JSONArray mCity = mObj.optJSONArray("city");
                    if (mCity != null) {
                        getParseLocalList(mCity);
                    }
                } catch (JSONException e) {
                    Log.e(TAG, "" + e.toString());
                }
            }
        });
    }

    private void show_ProgressDialog() {
        pDialog = new ProgressDialog(getActivity());
        pDialog.setMessage("Please Wait...");
        pDialog.setCancelable(false);
        pDialog.setCanceledOnTouchOutside(false);
        pDialog.setIndeterminate(true);
        pDialog.show();
    }

    private void hide_ProgressDialog() {
        if (pDialog != null) {
            pDialog.dismiss();
        }
    }

    private void orderValidation(){
        if (nombre.getText().toString().equals("")) {
            setMessageErrorDialog("Debe ingresar nombres");
        } else if (apellidos.getText().toString().trim().equals("")) {
            String message = "Debe ingresar apellidos";
            setMessageErrorDialog(message);
        } else if (perfil_email.getText().toString().trim().equals("")) {
            String message3 = "Debe ingresar el correo electronico";
            setMessageErrorDialog(message3);
        }/*else if(!EmailValidation.isValidEmail(perfil_email.getText().toString())){
            String message3 = "Debes ingresar el correo electrónico correcto";
            setMessageErrorDialog(message3);
        }*/ else if (cellular.getText().toString().trim().equals("")) {
            String message4 = "Debe ingresar celular";
            setMessageErrorDialog(message4);
        } else if (contrasena.getText().toString().trim().equals("")) {
            String message5 = "Debe ingresar contraseña";
            setMessageErrorDialog(message5);
        } else if (confirm_contrasena.getText().toString().trim().equals("")) {
            String message5 = "Debe ingresar confirmar contraseña";
            setMessageErrorDialog(message5);
        }else if(!contrasena.getText().toString().trim().equals(confirm_contrasena.getText().toString().trim())){
            String message5 = "Su contraseña y la contraseña de confirmación no coinciden";
            setMessageErrorDialog(message5);
        } else if (nombreestadirection.getText().toString().trim().equals("")) {
            String message5 = "Debe ingresar description";
            setMessageErrorDialog(message5);
        } else if (ciudad.getText().toString().trim().equals("")) {
            setMessageErrorDialog("Debe escoser ciudad");
        } else if (sector.getText().toString().trim().equals("")) {
            setMessageErrorDialog("Debe escoser sectir");
        } else if (direction1.getText().toString().trim().equals("")) {
            setMessageErrorDialog("Debe ingresar dirección1");
        } else if (direction2.getText().toString().trim().equals("")) {
            setMessageErrorDialog("Debe ingresar dirección2");
        } else if (telefono.getText().toString().trim().equals("")) {
            setMessageErrorDialog("Debe ingresar telefono");
        } else if (referencia.getText().toString().trim().equals("")) {
            setMessageErrorDialog("Debe ingresar referencia");
        }else if (acurdo_checl_uncheck_STATUS == 0) {
            String message13 = "Debe acetpar los términos y condiciones";
            setMessageErrorDialog("" + message13);
        }else if (pForma == null){
            setMessageErrorDialog(mContext.getResources().getString(R.string.select_form_de_pago));
        } else if (facturar_checl_uncheck_STATUS == 1) {
            if (nombra_pera.getText().toString().trim().equals("")) {
                setMessageErrorDialog("Debe ingresar nombre para estos datos");
            } else if (razonsocial.getText().toString().trim().equals("")) {
                setMessageErrorDialog("Debe ingresar razón social");
            } else if (cadulaoruk.getText().toString().trim().equals("")) {
                setMessageErrorDialog("Debe ingresar cedula o ruc");
            } else if (direction.getText().toString().trim().equals("")) {
                setMessageErrorDialog("Debe ingresar dirección");
            } else if (telefono2.getText().toString().trim().equals("")) {
                setMessageErrorDialog("Debe ingresar teléfono");
            }  else {
                registerOrder();
            }
        } else if (facturar_checl_uncheck_STATUS == 0) {
            registerOrder();
        }
    }


    private void registerOrder() {
        show_ProgressDialog();
        String _data = null;

        for(int i=0;i<alOrderItem.size();i++){
            _data += (i+1)+"**";
            _data += alOrderItem.get(i).getIdRestaProducto() + "**";
            _data += alOrderItem.get(i).getNombre().replace(" ", "%20") + "**";
            _data += alOrderItem.get(i).getQty() + "**";
            _data += alOrderItem.get(i).getPrice() + "**";

            StringBuilder sb = new StringBuilder();

            ArrayList<ItemProductPrice> ipp = alOrderItem.get(i).getAlPriceo();
            ArrayList<ItemAdditionalProduct> iap = alOrderItem.get(i).getAladiciona();

           // if (iap == null && iap == null) {
                sb.append(alOrderItem.get(i).getDescripcion().replace(" ", "%20"));
           // }

            if (ipp != null && ipp.size() > 0) {
                for (int j = 0; j < ipp.size(); j++) {
                    sb.append(ipp.get(j).getGrupo() + " " + ipp.get(j).getMedida());

                    if (iap != null && iap.size() > 0) {
                        sb.append(",");
                    }
                }
            }

            if (iap != null && iap.size() > 0) {
                sb.append("ADICIONALES: ");
                for (int k = 0; k < iap.size(); k++) {
                    sb.append("(" + iap.get(k).getAdiciona_qty() + ")");
                    sb.append(iap.get(k).getNombre() + "; ");
                }
            }


            String observation = sb.toString().replace(" ", "%20");

            _data += observation + "**";
            _data += "" + "**";
            _data += "" + "*--*";

            /** 2x1 Product **/

            if(alOrderItem.get(i).getQtyE2x1()!=null && alOrderItem.get(i).getNombreE2x1()!=null){
                _data += (i+1) + "**";
                _data += alOrderItem.get(i).getIdRestaProducto() + "**";
                _data += alOrderItem.get(i).getNombre().replace(" ", "%20") + "**";
                _data += alOrderItem.get(i).getQty() + "**";
                _data += alOrderItem.get(i).getPriceE2x1() + "**";

                StringBuilder sb2X = new StringBuilder();

                ArrayList<ItemProductPrice> ipp2X = alOrderItem.get(i).getAlPriceo();
                ArrayList<ItemAdditionalProduct> iap2X = alOrderItem.get(i).getAladiciona();

                //if (ipp2X == null || iap2X == null) {
                    sb2X.append(alOrderItem.get(i).getDescripcion().replace(" ", "%20"));
               // }

                if (ipp2X != null && ipp2X.size() > 0) {
                    for (int k = 0; k < ipp2X.size(); k++) {
                        sb2X.append(ipp2X.get(k).getGrupo() + " " + ipp2X.get(k).getMedida());

                        if (iap2X != null && iap2X.size() > 0) {
                            sb2X.append(",");
                        }
                    }
                }

                if (iap2X != null && iap2X.size() > 0) {
                    sb2X.append("ADICIONALES: ");
                    for (int l = 0; l < iap2X.size(); l++) {
                        sb2X.append("(" + iap2X.get(l).getAdiciona_qty() + ")");
                        sb2X.append(iap2X.get(l).getNombre() + " ");
                    }
                }


                String observation1 = sb2X.toString().replace(" ", "%20");

                _data += observation1 + "**";
                _data += "" + "**";
                _data += "" + "*--*";

            }

        }

        if (_data != null && !_data.equals("null") && !_data.equals("")) {
            _data = _data.replace("null", "");
        }

        int pIdRestaLocal = restaurantModel.getIdRestaLocal();
        String pIdToken = SessionManager.getSaveToken(mContext);
        String pTipoCelular = Config.deviceType;

        String url;

        if (facturar_checl_uncheck_STATUS == 1) {
            url = API.RegistraClienteNew + "pNombres=" + nombre.getText().toString().replace(" ", "%20")
                    + "&pApellidos=" + apellidos.getText().toString().replace(" ", "%20")
                    + "&pGenero=" + "M"//register_gender.getText().toString()
                    + "&pIdentificacion=" + ""
                    + "&pEmail=" + perfil_email.getText().toString().replace(" ", "%20")
                    + "&pContra=" + contrasena.getText().toString().replace(" ", "%20")
                    + "&pTelefono=" + "" + cellular.getText().toString().replace(" ", "%20")
                    + "&pCelular=" + cellular.getText().toString().replace(" ", "%20")
                    + "&pToken=" + SessionManager.getSaveToken(mContext)
                    + "&pOS=" + Config.deviceType
                    + "&pNombreDispo=" + UtilityMessage.deviceInfo().replace(" ", "%20")
                    + "&pUUID=" + UtilityMessage.deviceUUID(mContext)
                    + "&callback="

                    + "&addpIdCiudad=" + SessionManager.get_city_id_Ciudad(mContext)
                    + "&addpIdSector=" + SessionManager.get_city_idSector(mContext)
                    + "&addpTipo=" + "AD"
                    + "&addpDescripcion=" + nombreestadirection.getText().toString().replace(" ", "%20")
                    + "&addpDireccion2=" + direction2.getText().toString().replace(" ", "%20")
                    + "&addpDireccion=" + direction1.getText().toString().replace(" ", "%20")
                    + "&addpTelefono=" + telefono.getText().toString().replace(" ", "%20")
                    + "&addpCelular=" + telefono.getText().toString().replace(" ", "%20")
                    + "&addpReferencia=" + referencia.getText().toString().replace(" ", "%20")

                    + "&billpRazonSocial=" + razonsocial.getText().toString().replace(" ", "%20")
                    + "&billpIdentificacion=" + cadulaoruk.getText().toString().replace(" ", "%20")
                    + "&billpDescripcion=" + "" + nombreestadirection.getText().toString().replace(" ", "%20")
                    + "&billpDireccion=" + direction.getText().toString().replace(" ", "%20")
                    + "&billpTelefono=" + telefono2.getText().toString().replace(" ", "%20")

                    + "&ordpIdRestaLocal=" + pIdRestaLocal
                    + "&ordpObservacion=" + order_commentBox.replace(" ", "%20")
                    + "&ordpIdDirec1=" + "1"          //Static
                    + "&ordpIdDirec2=" + "2"          //Static
                    + "&ordpForma=" + pForma.replace(" ", "%20")
                    + "&ordpRecibe=" + "Domicilio"   //Static
                    + "&ordpIdToken=" + pIdToken
                    + "&ordpTipoCelular=" + pTipoCelular
                    + "&ordpSubTotal=" + sumOfPrice
                    + "&ordpTotal=" + totalPrice
                    + "&ordpCostoEnvio=" + costEncio
                    + "&ordpData=" + _data
                    + "&facturar_status=" + "1";


        } else {
            url = API.RegistraClienteNew + "pNombres=" + nombre.getText().toString().replace(" ", "%20")
                    + "&pApellidos=" + apellidos.getText().toString().replace(" ", "%20")
                    + "&pGenero=" + "M"//register_gender.getText().toString()
                    + "&pIdentificacion=" + ""
                    + "&pEmail=" + perfil_email.getText().toString().replace(" ", "%20")
                    + "&pContra=" + contrasena.getText().toString().replace(" ", "%20")
                    + "&pTelefono=" + "" + cellular.getText().toString().replace(" ", "%20")
                    + "&pCelular=" + cellular.getText().toString().replace(" ", "%20")
                    + "&pToken=" + SessionManager.getSaveToken(mContext)
                    + "&pOS=" + Config.deviceType
                    + "&pNombreDispo=" + UtilityMessage.deviceInfo().replace(" ", "%20")
                    + "&pUUID=" + UtilityMessage.deviceUUID(mContext)
                    + "&callback="

                    + "&addpIdCiudad=" + SessionManager.get_city_id_Ciudad(mContext)
                    + "&addpIdSector=" + SessionManager.get_city_idSector(mContext)
                    + "&addpTipo=" + "AD"
                    + "&addpDescripcion=" + nombreestadirection.getText().toString().replace(" ", "%20")
                    + "&addpDireccion2=" + direction2.getText().toString().replace(" ", "%20")
                    + "&addpDireccion=" + direction1.getText().toString().replace(" ", "%20")
                    + "&addpTelefono=" + telefono.getText().toString().replace(" ", "%20")
                    + "&addpCelular=" + telefono.getText().toString().replace(" ", "%20")
                    + "&addpReferencia=" + referencia.getText().toString().replace(" ", "%20")

                    + "&billpRazonSocial=" + referencia.getText().toString().replace(" ", "%20")
                    + "&billpIdentificacion=" + ""
                    + "&billpDescripcion=" + "" + nombreestadirection.getText().toString().replace(" ", "%20")
                    + "&billpDireccion=" + direction1.getText().toString().replace(" ", "%20")
                    + "&billpTelefono=" + telefono2.getText().toString().replace(" ", "%20")

                    + "&ordpIdRestaLocal=" + pIdRestaLocal
                    + "&ordpObservacion=" + order_commentBox.replace(" ", "%20")
                    + "&ordpIdDirec1=" + "1"                            ////Static
                    + "&ordpIdDirec2=" + "2"                           ////Static
                    + "&ordpForma=" + pForma.replace(" ", "%20")
                    + "&ordpRecibe=" + "Domicilio".replace(" ", "%20") //Static
                    + "&ordpIdToken=" + pIdToken
                    + "&ordpTipoCelular=" + pTipoCelular.replace(" ", "%20")
                    + "&ordpSubTotal=" + sumOfPrice
                    + "&ordpTotal=" + totalPrice
                    + "&ordpCostoEnvio=" + costEncio
                    + "&ordpData=" + _data
                    + "&facturar_status=" + "0";
        }

        Log.e("url", "" + url);
        GlobalValues.getMethodManagerObj(mContext).makeStringReqWithoutCache(url, "apiOrder", new MethodListener() {
            @Override
            public void onError() {
                hide_ProgressDialog();
            }

            @Override
            public void onError(String response) {
                hide_ProgressDialog();
                //({"data":[{"respuesta":"NO","Error":"Correo ElectrÃ³nico ya existe!"}]})
                showErrorMessage(response);
            }

            @Override
            public void onSuccess(String response) {
                //({"data":[{"respuesta":"OK"}]})
                //   hideProgressbar();
                hide_ProgressDialog();

                try {
                    JSONObject mObj = new JSONObject(response);
                    JSONArray mArray = mObj.optJSONArray("data");
                    if (mArray != null) {
                        for (int i = 0; i < mArray.length(); i++) {
                            JSONObject object = mArray.optJSONObject(i);
                            if (object.has("respuesta")) {
                                if (object.optString("respuesta").equals("OK")) {
                                  /*  *
                                     * Show the congratulations screen
                                     **/
                                    Bundle bundle = new Bundle();
                                    DashBoardActivity.displayFragment(FragmentIDs.CongratulationsFragment_Id, bundle);
                                } else if (object.optString("respuesta").equals("NO")) {
                                    setMessageErrorDialog("" + object.optString("Error"));
                                }
                            }
                        }
                    }
                } catch (JSONException e) {
                    hide_ProgressDialog();
                    Log.e(TAG, "" + e.toString());
                    showErrorMessage(response);
                }
            }
        });
    }

    private void showErrorMessage(String response){
        try{
            String outPut = response.substring(1,response.length()-1);
            JSONObject mObj = new JSONObject(outPut);
            JSONArray mArray = mObj.getJSONArray("data");
            if(mArray!=null && mArray.length()>0){
                String error = mArray.optJSONObject(0).optString("Error");
                setMessageErrorDialog(error);
            }
        }catch (JSONException e){
            Log.e(TAG,""+e.toString());
        }catch (Exception e){
            Log.e(TAG,""+e.toString());
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 701) {
            if (resultCode == getActivity().RESULT_OK) {
            }
        }

        if (requestCode == 702) {
            if (resultCode == getActivity().RESULT_OK) {

            }
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }




    private void showListaFormapago() {

        final Dialog cityDialog = new Dialog(new ContextThemeWrapper(mContext, R.style.DialogSlideAnim));
        cityDialog.getWindow().setGravity(Gravity.BOTTOM);
        cityDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        cityDialog.setContentView(R.layout.preciousadapter_citydialog_xml);
        final LoopView cityDialog_cityPicker = (LoopView) cityDialog.findViewById(R.id.loop_view);
        final TextView cityDialog_cancel = (TextView) cityDialog.findViewById(R.id.cityDialog_cancel);
        final TextView cityDialog_accept = (TextView) cityDialog.findViewById(R.id.cityDialog_accept);

        cityDialog_cityPicker.setInitPosition(0);
        cityDialog_cityPicker.setCanLoop(false);

        ArrayList<String> area_list = new ArrayList<>();
        for (int i = 0; i < Config.alListPagos.size(); i++) {
            area_list.add(i, (Config.alListPagos.get(i).getNombre()));
        }

        cityDialog_cityPicker.setTextSize(22);
        cityDialog_cityPicker.setDataList(area_list);

        cityDialog_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (cityDialog != null) {
                    cityDialog.dismiss();
                    pForma = null;
                    newOrder_formdePago.setText("");
                }
            }
        });

        cityDialog_accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (cityDialog != null) {
                    cityDialog.dismiss();
                }
                int value = cityDialog_cityPicker.getSelectedItem();
                pForma = Config.alListPagos.get(value).getNombre();
                newOrder_formdePago.setText(Config.alListPagos.get(value).getNombre());
            }
        });

        cityDialog.show();
    }


    public ArrayList<AddToCartItem> getOrderRecord() {
        totalPrice = sumOfPrice = 0.0f;

        ArrayList<OrderData> alOrderData = addToCartManager.getOrderData();

        ArrayList<AddToCartItem> alOrderItem = new ArrayList<>();

        if (alOrderData.size() > 0) {

            ArrayList<AddToCartItem> alOrderItem1 = getTipoModelo2(alOrderData);
            if(alOrderItem1.size()>0){
                for(int i=0;i<alOrderItem1.size();i++){
                    alOrderItem.add(alOrderItem1.get(i));
                }
            }

            for (int l = 0; l < alOrderData.size(); l++) {
                if (alOrderData.get(l).getEsDia2x1().equals("S")) {

                    if (alOrderData.get(l).getTipoModelo2Por1_P().equals("1")
                            && alOrderData.get(l).getTipoModelAdicional2Por1().equals("1")) {

                        AddToCartItem addToCartItem = addToCartManager.getProductItemModelAdicional2Por1_1(alOrderData.get(l));
                        alOrderItem.add(addToCartItem);

                        sumOfPrice += (addToCartItem.getPrice() + addToCartItem.getPriceE2x1());
                    }

                    if (alOrderData.get(l).getTipoModelo2Por1_P().equals("1")
                            && alOrderData.get(l).getTipoModelAdicional2Por1().equals("2")) {

                        AddToCartItem addToCartItem = addToCartManager.getProductItem(alOrderData.get(l));
                        alOrderItem.add(addToCartItem);

                        sumOfPrice += (addToCartItem.getPrice() + addToCartItem.getPriceE2x1());

                    }

                    //--------------------------------------------------------//

                    if (alOrderData.get(l).getTipoModelo2Por1_P().equals("3")
                            && (alOrderData.get(l).getTipoModelAdicional2Por1().equals("2")
                            || alOrderData.get(l).getTipoModelAdicional2Por1().equals("1"))) {

                        AddToCartItem addToCartItem = new AddToCartItem();
                        addToCartItem = addToCartManager.callProductTable(addToCartItem, alOrderData.get(l));
                        alOrderItem.add(addToCartItem);

                        sumOfPrice += addToCartItem.getPrice();

                    }
                }

                if (alOrderData.get(l).getEsDia2x1().equals("N")) {
                    AddToCartItem addToCartItem = new AddToCartItem();
                    addToCartItem = addToCartManager.callProductTable(addToCartItem, alOrderData.get(l));
                    alOrderItem.add(addToCartItem);

                    sumOfPrice += addToCartItem.getPrice();
                }
            }
        }
        return alOrderItem;
    }

    private ArrayList<AddToCartItem> getTipoModelo2(ArrayList<OrderData> alOrderData){

        ArrayList<AddToCartItem> alOrderItem = new ArrayList<>();
        ArrayList<OrderData> alOrderDataCopyOf = new ArrayList<>();

        for (int l = 0; l < alOrderData.size(); l++) {
            if (alOrderData.get(l).getEsDia2x1().equals("S")) {
                if (alOrderData.get(l).getTipoModelo2Por1_P().equals("2")
                        && alOrderData.get(l).getTipoModelAdicional2Por1().equals("1")) {

                    alOrderDataCopyOf.add(alOrderData.get(l));

                }

                if (alOrderData.get(l).getTipoModelo2Por1_P().equals("2")
                        && alOrderData.get(l).getTipoModelAdicional2Por1().equals("2")) {
                    alOrderDataCopyOf.add(alOrderData.get(l));
                }
            }
        }

        if(alOrderDataCopyOf.size()>0){
            Collections.sort(alOrderDataCopyOf, new Comparator<OrderData>() {
                @Override
                public int compare(OrderData t0, OrderData t1) {
                    return Float.compare(t1.getProductPrice(), t0.getProductPrice());
                }
            });

            if (alOrderDataCopyOf.get(0).getTipoModelo2Por1_P().equals("2")
                    && alOrderDataCopyOf.get(0).getTipoModelAdicional2Por1().equals("1")) {


                int size = alOrderDataCopyOf.size();
                for (int i = 0; i < size/2; i++) {
                    AddToCartItem addToCartItem = addToCartManager.getOrderItemTipoModelo2_Modelo1(alOrderDataCopyOf.get(i),1);
                    alOrderItem.add(addToCartItem);
                }

                for (int i = size/2; i < size; i++) {
                    AddToCartItem addToCartItem = addToCartManager.getOrderItemTipoModelo2_Modelo1(alOrderDataCopyOf.get(i),2);
                    alOrderItem.add(addToCartItem);
                }

                for (int i = 0; i < alOrderItem.size() / 2; i++) {
                    sumOfPrice += alOrderItem.get(i).getPrice();
                }
            }


            if (alOrderDataCopyOf.get(0).getTipoModelo2Por1_P().equals("2")
                    && alOrderDataCopyOf.get(0).getTipoModelAdicional2Por1().equals("2")) {

                int size = alOrderDataCopyOf.size();
                for (int i = 0; i < size/2; i++) {
                    AddToCartItem addToCartItem = addToCartManager.getOrderItemTipoModelo2_Modelo2(alOrderDataCopyOf.get(i));
                    alOrderItem.add(addToCartItem);
                }

                for (int i = size/2; i < size; i++) {
                    AddToCartItem addToCartItem = addToCartManager.getOrderItemTipoModelo2_Modelo2_ChargeOfAdicional(alOrderDataCopyOf.get(i));
                    alOrderItem.add(addToCartItem);
                }

                for (int i = 0; i < alOrderItem.size(); i++) {
                    sumOfPrice += alOrderItem.get(i).getPrice();
                }
            }
        }
        return alOrderItem;
    }
}
