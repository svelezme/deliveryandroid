package com.fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.activity.DashBoardActivity;
import com.adapter.OrderItemAdapter;
import com.app.grupodeliveryec.deliveryEC.R;
import com.bean.MyOrderBean;
import com.bean.MyOrderDetails;
import com.com.rest.API;
import com.com.rest.GlobalValues;
import com.com.rest.MethodListener;
import com.github.siyamed.shapeimageview.RoundedImageView;
import com.github.siyamed.shapeimageview.mask.PorterShapeImageView;
import com.inputview.BebaseNeueEditTextView;
import com.inputview.RegularEditTextView;
import com.model.ItemAdditionalProduct;
import com.model.ItemProductPrice;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.utility.Config;
import com.utility.FragmentIDs;
import com.utility.KeyboadSetting;
import com.utility.SessionManager;
import com.utility.UtilityMessage;
import com.view.BebaseNeueTextView;
import com.view.ExoBoldItalicTextView;
import com.view.ExoBoldTextView;
import com.view.ExoSemiBoldTextView;
import com.view.ExtraBoldItalicTextView;
import com.view.GothamBookTextView;
import com.view.MediumTextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 * Created by administrator on 18/2/17.
 */

public class MyOrderDetailsFragment extends Fragment {

    private static Context context;
    private static FragmentManager fm;
    private static Bundle b;
    private static Fragment fragment;
    View view;
    RecyclerView myorderDetails_lv;
    RoundedImageView info_restImageview;
    ExtraBoldItalicTextView info_nameOfRestaurant;
    MediumTextView info_typeOfFood;
    RegularEditTextView orderitem_commentBox;
    //Casa
    ExoBoldTextView orderitem_DirecEntrega;
    ExoSemiBoldTextView orderitem_DirecEntregaTipo;
    ExoSemiBoldTextView orderitem_DirecEntregaCiudad;
    ExoSemiBoldTextView orderitem_DirecEntregaDireccion;
    ExoSemiBoldTextView orderitem_DirecEntregaDireccion2;
    ExoSemiBoldTextView orderitem_DirecEntregaTelefono;
    ExoSemiBoldTextView DirecEntregaReferencia;

    /**
     * Personal
     */
    ExoBoldTextView orderitem_DirecFactura;
    ExoSemiBoldTextView orderitem_DirecFacturaRazonSocial;
    ExoSemiBoldTextView orderitem_DirecFacturaIdentificacion;
    ExoSemiBoldTextView orderitem_DirecFacturaDireccion;
    ExoSemiBoldTextView orderitem_DirecFacturaTelefono;

    BebaseNeueTextView myorderDetails_Subtotal;

    ExoBoldItalicTextView rateus;


    ExoBoldTextView formdepage,recibe,subtotoal,order_total;

    MyOrderBean myOrderBean;
    ArrayList<MyOrderDetails> alOrderDetails;

    OrderItemAdapter orderItemAdapter;
    public String TAG = getClass().getName();
    String ratedValue;


    public static Fragment getInstance(Context mContext, FragmentManager fragmentManager, Bundle bundle) {
        fragment = new MyOrderDetailsFragment();
        context = mContext;
        b = bundle;
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.myorder_details_xml, container, false);

        DashBoardActivity.updateTitle(getResources().getString(R.string.pedidos));
        DashBoardActivity.showCerrar();
        DashBoardActivity.hideInfo();
        DashBoardActivity.hideSearchButton();
        DashBoardActivity.hideLogout();
        DashBoardActivity.showBackButton();
        DashBoardActivity.showHorizentalBar();
        DashBoardActivity.hideMenuPanel();
        myorderDetails_lv = (RecyclerView) view.findViewById(R.id.myorderDetails_lv);
        info_restImageview = (RoundedImageView) view.findViewById(R.id.info_restImageview);
        info_nameOfRestaurant = (ExtraBoldItalicTextView) view.findViewById(R.id.info_nameOfRestaurant);
        info_typeOfFood = (MediumTextView) view.findViewById(R.id.info_typeOfFood);

        //Direccion de entrega
        orderitem_DirecEntrega = (ExoBoldTextView) view.findViewById(R.id.orderitem_DirecEntrega);
        orderitem_DirecEntregaTipo = (ExoSemiBoldTextView) view.findViewById(R.id.orderitem_DirecEntregaTipo);
        orderitem_DirecEntregaCiudad = (ExoSemiBoldTextView) view.findViewById(R.id.orderitem_DirecEntregaCiudad);
        orderitem_DirecEntregaDireccion = (ExoSemiBoldTextView) view.findViewById(R.id.orderitem_DirecEntregaDireccion);
        orderitem_DirecEntregaDireccion2 = (ExoSemiBoldTextView) view.findViewById(R.id.orderitem_DirecEntregaDireccion2);
        orderitem_DirecEntregaTelefono = (ExoSemiBoldTextView) view.findViewById(R.id.orderitem_DirecEntregaTelefono);
        DirecEntregaReferencia = (ExoSemiBoldTextView) view.findViewById(R.id.DirecEntregaReferencia);

        orderitem_DirecFactura = (ExoBoldTextView) view.findViewById(R.id.orderitem_DirecFactura);
        orderitem_DirecFacturaRazonSocial = (ExoSemiBoldTextView) view.findViewById(R.id.orderitem_DirecFacturaRazonSocial);
        orderitem_DirecFacturaIdentificacion = (ExoSemiBoldTextView) view.findViewById(R.id.orderitem_DirecFacturaIdentificacion);
        orderitem_DirecFacturaDireccion = (ExoSemiBoldTextView) view.findViewById(R.id.orderitem_DirecFacturaDireccion);
        orderitem_DirecFacturaTelefono = (ExoSemiBoldTextView) view.findViewById(R.id.orderitem_DirecFacturaTelefono);

        myorderDetails_Subtotal = (BebaseNeueTextView) view.findViewById(R.id.myorderDetails_Subtotal);

        orderitem_commentBox = (RegularEditTextView) view.findViewById(R.id.orderitem_commentBox);
        rateus = (ExoBoldItalicTextView) view.findViewById(R.id.rateus);

        formdepage = (ExoBoldTextView) view.findViewById(R.id.formadepago);
        recibe = (ExoBoldTextView) view.findViewById(R.id.recibe);
        subtotoal = (ExoBoldTextView) view.findViewById(R.id.subtotoal);
        order_total = (ExoBoldTextView) view.findViewById(R.id.order_total);


        myOrderBean = (MyOrderBean) b.getSerializable("orderDetails");
        alOrderDetails = myOrderBean.getAlOrderDetails();

        setData();

        rateus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showForgotPW();
            }
        });

        return view;
    }


    private void showForgotPW() {
        final Dialog forgotDialog = new Dialog(context);
        forgotDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        forgotDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        forgotDialog.setCancelable(false);
        forgotDialog.setCanceledOnTouchOutside(true);
        forgotDialog.setContentView(R.layout.rateus_dialog);
        ExoBoldTextView forgot_email = (ExoBoldTextView) forgotDialog.findViewById(R.id.cancel_action);
        final RegularEditTextView review = (RegularEditTextView) forgotDialog.findViewById(R.id.review_commet_tv);
        RatingBar forgot_accept = (RatingBar) forgotDialog.findViewById(R.id.ratingBar1);
        final ImageView progressBar = (ImageView) forgotDialog.findViewById(R.id.progressBar);

        ratedValue = "";

        forgot_accept.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating,
                                        boolean fromUser) {

                ratedValue = String.valueOf(ratingBar.getRating());
                // rateMessage.setText("Rating : " + ratedValue + "/5");
            }
        });

        forgot_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ratedValue == "") {
                    showalert("¿Como te fue con este pedido?");
                } else if (review.getText().toString().trim().equals("")) {
                    showalert("¿Como te fue con este pedido?");
                } else {
                    rate_and_review(review.getText().toString().trim(), progressBar, forgotDialog);
                }
            }
        });

        forgotDialog.show();
    }

    public void showalert(final String mesasages) {
        final Dialog dialog = new Dialog(context);
         dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setContentView(R.layout.alert);
        dialog.show();
        final TextView message = (TextView) dialog.findViewById(R.id.fsdfasdf);

        TextView ok = (TextView) dialog.findViewById(R.id.ok);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        message.setText(""+mesasages);
    }

    private void rate_and_review(String reviews, final ImageView imageView, final Dialog dialog) {
        int pedido_id = 0;
        int id_restalocal = 0;

        if (myOrderBean != null) {
            pedido_id = myOrderBean.getIdPedido();
            id_restalocal = myOrderBean.getIdRestaLocal();
        }


        String url = API.RateandReview
                + "rating=" + ratedValue
                + "&review=" + reviews.replace(" ","%20")
                + "&id_cliente=" + SessionManager.getUserId(context)
                + "&id_pedido=" + pedido_id
                + "&id_restalocal=" + id_restalocal;


        //  http://deliveryecnew.azurewebsites.net/DeliveryAPI.asmx/PostReview?rating=4&review=good&id_cliente=82168&id_pedido=103122&id_restalocal=4

        // showProgressBar();

        GlobalValues.getMethodManagerObj(context).makeStringReqWithoutCache(url, "PostReview", new MethodListener() {
            @Override
            public void onError() {
                //hideProgressbar();
                imageView.setVisibility(View.GONE);
                dialog.dismiss();
            }

            @Override
            public void onError(String response) {
                //hideProgressbar();
                imageView.setVisibility(View.GONE);
                dialog.dismiss();
            }

            @Override
            public void onSuccess(String response) {
                //({"data":[{"respuesta":"OK"}]})
                //hideProgressbar();
                dialog.dismiss();
                imageView.setVisibility(View.GONE);
                //  response = response.substring(1, response.length() - 1);
                try {
                    JSONObject mObj = new JSONObject(response);

                    if (mObj.optString("status").equals("True")) {
                        showalert("Tu calificación ha sido enviada satisfactoriamente!");
                    }

                    rateus.setVisibility(View.GONE);
                } catch (JSONException e) {
                    // hideProgressbar();
                    Log.e(TAG, "" + e.toString());
                }
            }
        });

    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void setData() {

        myorderDetails_lv.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
        myorderDetails_lv.setLayoutManager(mLayoutManager);
        //promociones_recycleView.addItemDecoration(new DividerItemDecoration(context, LinearLayoutManager.VERTICAL));
        myorderDetails_lv.setItemAnimator(new DefaultItemAnimator());

        orderItemAdapter = new OrderItemAdapter(context, alOrderDetails);
        myorderDetails_lv.setAdapter(orderItemAdapter);
        // UtilityMessage.setListViewHeightBasedOnChildren(myorderDetails_lv);

        //NombreRestaurante - NombreLocal
        info_nameOfRestaurant.setText(myOrderBean.getNombreRestaurante() + " - " + myOrderBean.getNombreLocal());
        info_typeOfFood.setText(myOrderBean.getTipoComida());

        String url = "http://www.deliveryec.com/images/app/" + myOrderBean.getImagenLocal();
        Picasso.with(context).load(url).into(info_restImageview, new Callback() {
            @Override
            public void onSuccess() {
            }

            @Override
            public void onError() {
            }
        });

        if (myOrderBean.getRevision_status().equals("0")){
            rateus.setVisibility(View.VISIBLE);
        }else {
            rateus.setVisibility(View.GONE);
        }

        formdepage.setText(myOrderBean.getFormaPago());
        recibe.setText(myOrderBean.getRecibe());
        order_total.setText("$ " + new DecimalFormat("0.00").format(myOrderBean.getTotal()));
        subtotoal.setText("$ " + new DecimalFormat("0.00").format(myOrderBean.getServicio()));

        //Direccion de entrega
        orderitem_DirecEntrega.setText(myOrderBean.getDirecEntrega());
        orderitem_DirecEntregaTipo.setText(myOrderBean.getDirecEntregaTipo());
        orderitem_DirecEntregaCiudad.setText(myOrderBean.getDirecEntregaCiudad() + " - " +
                myOrderBean.getDirecEntregaSector());

        orderitem_DirecEntregaDireccion.setText(myOrderBean.getDirecEntregaDireccion());

        orderitem_DirecEntregaDireccion2.setText(myOrderBean.getDirecEntregaDireccion2());

        orderitem_DirecEntregaTelefono.setText(myOrderBean.getDirecEntregaTelefono());

        DirecEntregaReferencia.setText(myOrderBean.getDirecEntregaReferencia());

        orderitem_DirecFactura.setText(myOrderBean.getDirecFactura());

        orderitem_DirecFacturaRazonSocial.setText(myOrderBean.getDirecFacturaRazonSocial());

        orderitem_DirecFacturaIdentificacion.setText(myOrderBean.getDirecFacturaIdentificacion());

        orderitem_DirecFacturaDireccion.setText(myOrderBean.getDirecFacturaDireccion());

        orderitem_DirecFacturaTelefono.setText(myOrderBean.getDirecFacturaTelefono());

        myorderDetails_Subtotal.setText("$ " + new DecimalFormat("0.00").format(myOrderBean.getSubtotal()));

        orderitem_commentBox.setText(myOrderBean.getObservacion());

        //myorderDetails_Subtotal.setText("$" + " " + myOrderBean.getTotal());
    }
}
