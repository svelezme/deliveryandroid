package com.fragment;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.activity.AddBillingAddressActivity;
import com.activity.AddDeliveryAddressActivity;
import com.activity.DashBoardActivity;
import com.adapter.OrderListAdapter;
import com.app.grupodeliveryec.deliveryEC.R;
import com.bean.AddToCartItem;
import com.bean.AddressListBean;
import com.bean.BillingBean;
import com.bean.OrderData;
import com.bruce.pickerview.LoopView;
import com.com.rest.API;
import com.com.rest.GlobalValues;
import com.com.rest.MethodListener;
import com.github.siyamed.shapeimageview.RoundedImageView;
import com.inputview.RegularEditTextView;
import com.manager.AddToCartManager;
import com.model.ItemAdditionalProduct;
import com.model.ItemProductPrice;
import com.model.RestaurantListModel;
import com.squareup.picasso.Picasso;
import com.utility.Config;
import com.utility.FragmentIDs;
import com.utility.MessageDialog;
import com.utility.PullToData;
import com.utility.SessionManager;
import com.view.BebaseNeueTextView;
import com.view.ExoBoldItalicTextView;
import com.view.ExoBoldTextView;
import com.view.ExtraBoldItalicTextView;
import com.view.MediumTextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import static com.utility.SessionManager.getUserId;

/**
 * Created by administrator on 21/3/17.
 */

public class OrderFragment extends Fragment {

    String TAG = getClass().getName();
    final int sdk = android.os.Build.VERSION.SDK_INT;
    private static Context mContext;
    private static FragmentManager fm;
    public static Bundle b;
    private static Fragment fragment;
    View view;
    RestaurantListModel restaurantModel;
    LinearLayout order_orderDetailsLayout;
    RoundedImageView menuitem_restImageview;
    ExtraBoldItalicTextView menuitem_nameOfRestaurant;
    MediumTextView menuitem_typeOfFood;
    RatingBar rest_ratingBar;
    ExoBoldItalicTextView rest_list_openStatus;
    BebaseNeueTextView rest_list_minimo, rest_list_costodeenvio, rest_list_tempoentrega;
    TextView menuitem_backToWork;
    Handler handler;
    RegularEditTextView order_commentBox;
    ExoBoldTextView order_minimo;
    ExoBoldTextView order_costo_de_envio;
    ExoBoldTextView order_subtotalAgain;
    ExoBoldTextView order_total;
    ImageView order_addDeliveryAddress, order_addBillingAddress;
    LinearLayout order_SectionPedido;
    LinearLayout order_SectionConfirmPedido;
    LinearLayout order_finishOrder, order_pideAhore;
    LinearLayout order_detailsOfDelivery, order_billingAddressLayout;
    ExoBoldTextView order_minimoAgain;
    ExoBoldTextView order_costo_de_envioAgain;
    ExoBoldTextView order_subtotal;
    ExoBoldTextView order_totalAgain;
    RegularEditTextView order_selectDeliveryAddress, order_selectBillingAddress;
    RegularEditTextView order_commentBoxAgain;
    RegularEditTextView order_selectCard;
    ImageView resturent_progressBar;
    LinearLayout menuitem_noItemHasCart;
    RecyclerView order_recycler_view;

    int pIdRestaurante;
    float totalPrice = 0.0f;
    String costEncio;
    float sumOfPrice = 0.0f;
    String minimo;
    public static String commentOnOrder;
    public static String commentOnOrderForLogin;
    AddToCartManager addToCartManager;
    int pIdDirec1;
    int pIdDirec2;
    String pForma;
    Animation animation;
    MessageDialog.okOnClickListener okOnClickListener;
    MessageDialog messageDialog;
    OrderListAdapter orderListAdapter;

    public static boolean isConfirm = false;

    public static Fragment getInstance(Context context, FragmentManager fragmentManager, Bundle bundle) {
        fragment = new OrderFragment();
        mContext = context;
        fm = fragmentManager;
        if (NewOrderFragment.isFromNewOrder) {
        } else {
            b = bundle;
        }
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        //DashBoardActivity.order_back_status = 1;
        DashBoardActivity.hideHorizentalBar();
        DashBoardActivity.hideInfo();
        DashBoardActivity.hideLogout();
        DashBoardActivity.hideSearchButton();
        DashBoardActivity.hideCerrar();
        DashBoardActivity.showBackButton();
        DashBoardActivity.showMenuPanel();
        handler = new Handler(Looper.getMainLooper());

        view = inflater.inflate(R.layout.order_xml, container, false);

        addToCartManager = new AddToCartManager(mContext);


        View.OnClickListener addToMenu = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                menuitem_noItemHasCart.setVisibility(View.GONE);
                DashBoardActivity.removePopupBackstack(FragmentIDs.OrderFragment_Tag);
                addToCartManager.dropCart();
            }
        };

        try {
            addToCartManager.backupDatabase();
        } catch (Exception e) {
            Log.v(TAG, "" + e.toString());
        }

        restaurantModel = (RestaurantListModel) b.getSerializable("restaurantModel");
        pIdRestaurante = restaurantModel.getIdRestaurante();
        getDetails();


        order_orderDetailsLayout = (LinearLayout) view.findViewById(R.id.order_orderDetailsLayout);
        menuitem_restImageview = (RoundedImageView) view.findViewById(R.id.info_restImageview);
        menuitem_nameOfRestaurant = (ExtraBoldItalicTextView) view.findViewById(R.id.info_nameOfRestaurant);
        menuitem_typeOfFood = (MediumTextView) view.findViewById(R.id.info_typeOfFood);
        rest_ratingBar = (RatingBar) view.findViewById(R.id.rest_ratingBar);
        rest_list_openStatus = (ExoBoldItalicTextView) view.findViewById(R.id.rest_list_openStatus);
        rest_list_minimo = (BebaseNeueTextView) view.findViewById(R.id.rest_list_minimo);
        rest_list_costodeenvio = (BebaseNeueTextView) view.findViewById(R.id.rest_list_costodeenvio);
        rest_list_tempoentrega = (BebaseNeueTextView) view.findViewById(R.id.rest_list_tempoentrega);
        menuitem_backToWork = (TextView) view.findViewById(R.id.menuitem_backToWork);
        order_recycler_view = (RecyclerView) view.findViewById(R.id.order_recycler_view);
        menuitem_noItemHasCart = (LinearLayout) view.findViewById(R.id.menuitem_noItemHasCart);
        order_commentBoxAgain = (RegularEditTextView) view.findViewById(R.id.order_commentBoxAgain);
        order_commentBox = (RegularEditTextView) view.findViewById(R.id.order_commentBox);
        order_minimo = (ExoBoldTextView) view.findViewById(R.id.order_minimo);
        order_subtotal = (ExoBoldTextView) view.findViewById(R.id.order_subtotal);
        order_costo_de_envio = (ExoBoldTextView) view.findViewById(R.id.order_costo_de_envio);
        order_total = (ExoBoldTextView) view.findViewById(R.id.order_total);
        order_SectionPedido = (LinearLayout) view.findViewById(R.id.order_SectionPedido);
        order_SectionConfirmPedido = (LinearLayout) view.findViewById(R.id.order_SectionConfirmPedido);
        order_finishOrder = (LinearLayout) view.findViewById(R.id.order_finishOrder);
        order_pideAhore = (LinearLayout) view.findViewById(R.id.order_pideAhore);

        order_subtotalAgain = (ExoBoldTextView) view.findViewById(R.id.order_subtotalAgain);

        order_selectDeliveryAddress = (RegularEditTextView) view.findViewById(R.id.order_selectDeliveryAddress);
        order_selectBillingAddress = (RegularEditTextView) view.findViewById(R.id.order_selectBillingAddress);
        order_selectCard = (RegularEditTextView) view.findViewById(R.id.order_selectCard);
        order_minimoAgain = (ExoBoldTextView) view.findViewById(R.id.order_minimoAgain);
        order_costo_de_envioAgain = (ExoBoldTextView) view.findViewById(R.id.order_costo_de_envioAgain);
        order_totalAgain = (ExoBoldTextView) view.findViewById(R.id.order_totalAgain);

        order_detailsOfDelivery = (LinearLayout) view.findViewById(R.id.order_detailsOfDelivery);
        order_billingAddressLayout = (LinearLayout) view.findViewById(R.id.order_billingAddressLayout);

        order_addDeliveryAddress = (ImageView) view.findViewById(R.id.order_addDeliveryAddress);
        order_addBillingAddress = (ImageView) view.findViewById(R.id.order_addBillingAddress);
        resturent_progressBar = (ImageView) view.findViewById(R.id.resturent_progressBar);

        order_recycler_view.setVisibility(View.VISIBLE);
        order_recycler_view.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mContext);
        order_recycler_view.setLayoutManager(mLayoutManager);
        order_recycler_view.setItemAnimator(new DefaultItemAnimator());


        if (Config.orderScreenStatus == 1) {

            DashBoardActivity.order_back_status = 1;
            DashBoardActivity.showMenuPanel();
            order_SectionPedido.setVisibility(View.VISIBLE);
            order_SectionConfirmPedido.setVisibility(View.GONE);
            setOrderDetails();
        }

        if (Config.orderScreenStatus == 2) {
            DashBoardActivity.order_back_status = 2;
            DashBoardActivity.hideMenuPanel();
            order_SectionPedido.setVisibility(View.GONE);
            order_SectionConfirmPedido.setVisibility(View.VISIBLE);
            DashBoardActivity.updateTitle(mContext.getResources().getString(R.string.confirmar_pedido));
            setUpFinalPedido();
            setOrderDetails();
        }

        order_finishOrder.setOnClickListener(addFinishOrder);
        order_pideAhore.setOnClickListener(addpideAhore);
        order_selectDeliveryAddress.setOnClickListener(addSelectDeliveryAddress);
        order_selectBillingAddress.setOnClickListener(addBillingAddress);
        order_selectCard.setOnClickListener(addSelectCard);
        order_addDeliveryAddress.setOnClickListener(mDeliveryAddress);
        order_addBillingAddress.setOnClickListener(mBillingAddress);
        menuitem_backToWork.setOnClickListener(addToMenu);

        DashBoardActivity.menuButtonLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle b1 = new Bundle();
                b1.putSerializable("restaurantModel", restaurantModel);
                DashBoardActivity.removePopupBackstack(FragmentIDs.OrderFragment_Tag);
                DashBoardActivity.displayFragment(FragmentIDs.ResturentMenuFragment_Id, b1);
            }
        });

        DashBoardActivity.cartButtonLayout.setClickable(false);


        if (NewOrderFragment.isFromNewOrder) {
            isConfirm = true;
            DashBoardActivity.updateTitle(mContext.getResources().getString(R.string.confirmar_pedido));
            order_SectionConfirmPedido.setVisibility(View.VISIBLE);
            order_SectionPedido.setVisibility(View.GONE);
            setUpFinalPedido();
            DashBoardActivity.order_back_status = 4;
            setOrderDetails();

            order_commentBox.setText(OrderFragment.commentOnOrderForLogin);
            NewOrderFragment.isFromNewOrder = false;
        }


        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (Config.orderScreenStatus == 2) {
            if (getView() == null) {
                return;
            }

            getView().setFocusableInTouchMode(true);
            getView().requestFocus();
            getView().setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {

                    if (keyEvent.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {

                        Bundle bundle = new Bundle();
                        bundle.putInt("idRestaLocal", b.getInt("idRestaLocal"));
                        bundle.putString("NombreRestaurante", b.getString("NombreRestaurante"));
                        bundle.putString("NombreLocal", b.getString("NombreLocal"));
                        bundle.putString("TipoComida", b.getString("TipoComida"));
                        bundle.putString("ImagenLocal", b.getString("ImagenLocal"));
                        bundle.putInt("idRestaCategoria", b.getInt("idRestaCategoria"));
                        bundle.putString("Nombre", b.getString("Nombre"));
                        bundle.putSerializable("restaurantListModel", restaurantModel);

                        DashBoardActivity.removeOrderFragment(FragmentIDs.OrderFragment_Tag);
                        DashBoardActivity.displayFragment(FragmentIDs.ResturentMenuItemFragment_Id, bundle);
                        return true;
                    }
                    return false;
                }
            });
        }
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 701) {
            if (resultCode == getActivity().RESULT_OK) {
            }
        }

        if (requestCode == 702) {
            if (resultCode == getActivity().RESULT_OK) {

            }
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    private void setOrderDetails() {

        ArrayList<AddToCartItem> alOrderItem = getOrderRecord();
        Log.d("asd", String.valueOf(alOrderItem));
        orderListAdapter = new OrderListAdapter(mContext, alOrderItem, new DeleteItemfromCartListener(), DashBoardActivity.order_back_status);
        order_recycler_view.setAdapter(orderListAdapter);

        setData();
    }

    public class DeleteItemfromCartListener {
        public void delete() {

            ArrayList<OrderData> alOrderData = addToCartManager.getOrderData();

            try {
                addToCartManager.backupDatabase();
            } catch (Exception e) {
                Log.v(TAG, "" + e.toString());
            }

            DashBoardActivity.order_back_status = 1;

            if (alOrderData.size() == 0) {
                DashBoardActivity.hideQuntity();
                menuitem_noItemHasCart.setVisibility(View.VISIBLE);
                order_SectionPedido.setVisibility(View.GONE);
                order_recycler_view.setVisibility(View.GONE);
            } else {
                int qty = addToCartManager.getTotalQuntity();
                DashBoardActivity.setQuntity(qty);
                menuitem_noItemHasCart.setVisibility(View.GONE);
                setOrderDetails();
            }
        }
    }

    public void setData() {

        DashBoardActivity.updateTitle(mContext.getResources().getString(R.string.pedidos));

        Picasso.with(mContext)
                .load(restaurantModel.getBaseUrl() + restaurantModel.getImagenLocal())
                .error(getResources().getDrawable(R.drawable.default_image))
                .placeholder(getResources().getDrawable(R.drawable.default_image))
                .into(menuitem_restImageview);

        menuitem_nameOfRestaurant.setText(restaurantModel.getNombreRestaurante() + " - " + restaurantModel.getNombreLocal());
        menuitem_typeOfFood.setText(restaurantModel.getTipoComida());
        try {
            rest_ratingBar.setRating(restaurantModel.getRating());
        } catch (Exception ec) {
        }
        if (restaurantModel.getActivo().equals("I")) {

            rest_list_openStatus.setText(mContext.getResources().getString(R.string.CERRADO));

            if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                rest_list_openStatus.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.info_close_status_background));
            } else {
                rest_list_openStatus.setBackground(mContext.getResources().getDrawable(R.drawable.info_close_status_background));
            }

        } else {

            rest_list_openStatus.setText(mContext.getResources().getString(R.string.OPEN));

            if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                rest_list_openStatus.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.info_open_status_background));
            } else {
                rest_list_openStatus.setBackground(mContext.getResources().getDrawable(R.drawable.info_open_status_background));
            }
        }

        try {
            String headerMinimo = restaurantModel.getMinimo();
            if (headerMinimo.length() == 3) {
                headerMinimo = headerMinimo + "0";
            }
            rest_list_minimo.setText("$ " + new DecimalFormat("00.00").format(headerMinimo));
        } catch (Exception e) {
            rest_list_minimo.setText("$ " + restaurantModel.getMinimo());
            e.printStackTrace();
        }
        rest_list_costodeenvio.setText("$ " + restaurantModel.getCostoEnvio().replace(",", "."));
        rest_list_tempoentrega.setText(restaurantModel.getTiempoEntrega());

        minimo = restaurantModel.getMinimo();
        try {

            if (minimo.length() == 3) {
                minimo = minimo + "0";
            }
            order_minimo.setText("$ " + new DecimalFormat("0.00").format(Float.parseFloat(minimo)));

        } catch (Exception e) {
            order_minimo.setText("$ " + restaurantModel.getMinimo());
            e.printStackTrace();
        }

        try {
            order_subtotal.setText("$ " + new DecimalFormat("0.00").format(sumOfPrice));
        } catch (Exception e) {
            order_subtotal.setText("$ " + addToCartManager.getSumOfPrice());
            e.printStackTrace();
        }

        costEncio = restaurantModel.getCostoEnvio().replace(",", ".");
        order_costo_de_envio.setText(" $ " + costEncio);

        showTotalPrice();
    }

    private void showTotalPrice() {
        totalPrice = 0.0f;
        try {
            totalPrice = sumOfPrice + Float.parseFloat(costEncio);
            order_total.setText("$ " + new DecimalFormat("0.00").format(totalPrice));
        } catch (NumberFormatException e) {
            order_total.setText(" $ " + totalPrice);
            Log.e(TAG, "" + e.toString());
        } catch (Exception e) {
            order_total.setText(" $ " + totalPrice);
            Log.e(TAG, "" + e.toString());
        }
    }

    private void setUpFinalPedido() {

        order_SectionPedido.setVisibility(View.GONE);
        order_SectionConfirmPedido.setVisibility(View.VISIBLE);

        try {
            if (String.valueOf(sumOfPrice).length() == 3) {
                order_subtotalAgain.setText("$ " + new DecimalFormat("0.00").format(sumOfPrice));
            } else {
                order_subtotalAgain.setText("$ " + new DecimalFormat("0.00").format(sumOfPrice));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            minimo = restaurantModel.getMinimo();
            if (minimo.length() == 3) {
                minimo = minimo + "0";
            }

            order_minimoAgain.setText("$ " + new DecimalFormat("0.00").format(Float.parseFloat(minimo)));
        } catch (Exception e) {
            e.printStackTrace();
        }
        order_costo_de_envioAgain.setText(" $ " + costEncio);
        try {
            order_totalAgain.setText("$ " + new DecimalFormat("0.00").format(totalPrice));
        } catch (Exception e) {
            order_totalAgain.setText("$ " + totalPrice);
            e.printStackTrace();
        }

        try {
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (NewOrderFragment.isFromNewOrder) {
                    } else {
                        order_commentBoxAgain.setText(order_commentBox.getText().toString());
                        OrderFragment.commentOnOrder = order_commentBoxAgain.getText().toString();
                    }
                }
            }, 300);
        } catch (Exception e) {
            Log.e(TAG, "" + e.toString());
        }
    }

    View.OnClickListener addFinishOrder = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            Log.d("ta","este es el fin");
            /**
             * If i am login the show the three fields Delivery Address(DIRECCION DE ENTREGA), Billing Address(DIRECCION DE FACTURACION), WAY TO PAY(Form de pago)
             */

            /**
             * if i am not login the go the New screen "NewOrderFragment"
             */

            Float minimo1 = Float.valueOf(minimo);
            if (sumOfPrice >= minimo1) {
                int clientId = SessionManager.getUserId(mContext);
                if (clientId != 0) {
                    isConfirm = true;
                    DashBoardActivity.updateTitle(mContext.getResources().getString(R.string.confirmar_pedido));
                    order_SectionConfirmPedido.setVisibility(View.VISIBLE);
                    order_SectionPedido.setVisibility(View.GONE);
                    setUpFinalPedido();
                    DashBoardActivity.order_back_status = 4;
                    setOrderDetails();
                } else {

                    OrderFragment.commentOnOrderForLogin = order_commentBox.getText().toString();
                    OrderFragment.commentOnOrder = order_commentBox.getText().toString();
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("restaurantModel", restaurantModel);
                    bundle.putString("order_commentBox", "" + order_commentBox.getText().toString());
                    DashBoardActivity.displayFragment(FragmentIDs.NewOrderFragment_Id, bundle);
                }
            } else {
                setMessageErrorDialog(mContext.getResources().getString(R.string.minimum_price_validation));
            }
        }
    };


    public void updateUi() {
        DashBoardActivity.order_back_status = 1;
        DashBoardActivity.showMenuPanel();
        // setData();
        order_SectionPedido.setVisibility(View.VISIBLE);
        order_SectionConfirmPedido.setVisibility(View.GONE);
        DashBoardActivity.order_back_status = 1;
        setOrderDetails();
    }


    View.OnClickListener addpideAhore = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            /**
             * Call to finish order
             */
            order_pideAhore.setEnabled(false);
            OrderFragment.commentOnOrder = order_commentBoxAgain.getText().toString();

            int pIdRestaLocal = restaurantModel.getIdRestaLocal();
            String url = API.ValidaLocalAbierto + "pIdRestaLocal=" + pIdRestaLocal + "&callback=";

            GlobalValues.getMethodManagerObj(mContext).makeStringReqWithoutCache(url, "apiValidaLocalAbierto", new MethodListener() {
                @Override
                public void onSuccess(String response) {
                    //({"data":[{"respuesta":"NO"}]})
                    try {
                        response = response.substring(1, response.length() - 1);
                        JSONObject mObj = new JSONObject(response);

                        JSONArray array = mObj.optJSONArray("data");
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject object = array.optJSONObject(i);
                            if (object.optString("respuesta").equals("NO")) {
                                setMessageErrorDialog(mContext.getResources().getString(R.string.validaLocalabierto_message));
                            } else {
                                Float minimo1 = Float.valueOf(minimo);
                                if (sumOfPrice >= minimo1) {
                                    order_pideAhore.setEnabled(false);
                                    Log.d(TAG, "onSuccess: PEDIDO");
                                    registerOrder();
                                } else {
                                    setMessageErrorDialog(mContext.getResources().getString(R.string.minimum_price_validation));
                                    order_pideAhore.setEnabled(true);
                                }
                            }
                        }
                    } catch (Exception e) {
                        Log.e(TAG, "" + e.toString());
                    }
                }

                @Override
                public void onError(String response) {
                }

                @Override
                public void onError() {
                }
            });
        }
    };

    View.OnClickListener addSelectDeliveryAddress = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (Config.alDeliveryAddress.size() != 0) {
                showDeliveryAddress();
            }
        }
    };

    View.OnClickListener addBillingAddress = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (Config.alBilling.size() != 0) {
                showBiilingAddress();
            }
        }
    };

    View.OnClickListener addSelectCard = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (Config.alListPagos.size() != 0) {
                showListaFormapago();
            }
        }
    };

    View.OnClickListener mDeliveryAddress = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent intent = new Intent(mContext, AddDeliveryAddressActivity.class);
            getActivity().overridePendingTransition(R.anim.animation_enter, R.anim.animation_leave);
            startActivityForResult(intent, 701);
        }
    };

    View.OnClickListener mBillingAddress = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent intent = new Intent(mContext, AddBillingAddressActivity.class);
            getActivity().overridePendingTransition(R.anim.animation_enter, R.anim.animation_leave);
            startActivityForResult(intent, 702);
        }
    };

    private void showDeliveryAddress() {

        final Dialog cityDialog = new Dialog(new ContextThemeWrapper(mContext, R.style.DialogSlideAnim));
        cityDialog.getWindow().setGravity(Gravity.BOTTOM);
        cityDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        cityDialog.setContentView(R.layout.preciousadapter_citydialog_xml);

        final LoopView cityDialog_cityPicker = (LoopView) cityDialog.findViewById(R.id.loop_view);
        final TextView cityDialog_cancel = (TextView) cityDialog.findViewById(R.id.cityDialog_cancel);
        final TextView cityDialog_accept = (TextView) cityDialog.findViewById(R.id.cityDialog_accept);

        cityDialog_cityPicker.setInitPosition(0);
        cityDialog_cityPicker.setCanLoop(false);

        ArrayList<String> area_list = new ArrayList<>();
        for (int i = 0; i < Config.alDeliveryAddress.size(); i++) {
            area_list.add(i, (Config.alDeliveryAddress.get(i).getDescripcion()));
        }

        cityDialog_cityPicker.setTextSize(22);
        cityDialog_cityPicker.setDataList(area_list);

        cityDialog_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (cityDialog != null) {
                    cityDialog.dismiss();
                }
            }
        });

        cityDialog_accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (cityDialog != null) {
                    cityDialog.dismiss();
                }
                int value = cityDialog_cityPicker.getSelectedItem();
                String dispalyItem = Config.alDeliveryAddress.get(value).getDescripcion();
                order_selectDeliveryAddress.setText("" + dispalyItem);

                /**
                 * Show to view of Details
                 */

                if (Config.alDeliveryAddress.get(value).getIdDirecciones() != 0) {
                    order_detailsOfDelivery.setVisibility(View.VISIBLE);

                    order_detailsOfDelivery.removeAllViews();

                    LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View view1 = inflater.inflate(R.layout.delivery_address_inflayout, null, false);

                    TextView order_deliveryInf_descripcion = (TextView) view1.findViewById(R.id.order_deliveryInf_descripcion);
                    TextView order_deliveryInf_nombreperfile = (TextView) view1.findViewById(R.id.order_deliveryInf_nombreperfile);
                    TextView order_deliveryInf_nombreCiudad_or_sector = (TextView) view1.findViewById(R.id.order_deliveryInf_nombreCiudad_or_sector);
                    TextView order_deliveryInf_tipo = (TextView) view1.findViewById(R.id.order_deliveryInf_tipo);
                    TextView order_deliveryInf_direccion1 = (TextView) view1.findViewById(R.id.order_deliveryInf_direccion1);
                    TextView order_deliveryInf_direccion2 = (TextView) view1.findViewById(R.id.order_deliveryInf_direccion2);
                    TextView order_deliveryInf_telephone = (TextView) view1.findViewById(R.id.order_deliveryInf_telephone);
                    TextView order_deliveryInf_celular = (TextView) view1.findViewById(R.id.order_deliveryInf_celular);
                    TextView order_deliveryInf_referencia = (TextView) view1.findViewById(R.id.order_deliveryInf_referencia);

                    AddressListBean al = Config.alDeliveryAddress.get(value);

                    pIdDirec1 = al.getIdDirecciones();

                    order_deliveryInf_descripcion.setText(al.getDescripcion());
                    order_deliveryInf_nombreperfile.setText(al.getNombrePerfil());
                    order_deliveryInf_nombreCiudad_or_sector.
                            setText(al.getNombreCiudad() + " "
                                    + al.getNombreSector());

                    order_deliveryInf_tipo.setText(al.getTipo());

                    order_deliveryInf_direccion1.setText(al.getDireccion());
                    order_deliveryInf_direccion2.setText(al.getDireccion2());
                    order_deliveryInf_telephone.setText(al.getTelefono());
                    order_deliveryInf_celular.setText(al.getCelular());
                    order_deliveryInf_referencia.setText(al.getReferencia());

                    order_detailsOfDelivery.addView(view1);

                } else if (dispalyItem.equals("RECOGE EN LOCAL")) {
                    pIdDirec1 = -1;
                } else {
                    pIdDirec1 = 0;
                    order_detailsOfDelivery.setVisibility(View.GONE);
                }
            }
        });
        cityDialog.show();
    }

    public void showprevious_fragment() {
        order_SectionPedido.setVisibility(View.VISIBLE);
        order_SectionConfirmPedido.setVisibility(View.GONE);
        DashBoardActivity.showMenuPanel();
        DashBoardActivity.order_back_status = 1;
        setOrderDetails();
    }

    private void showBiilingAddress() {
        final Dialog cityDialog = new Dialog(new ContextThemeWrapper(mContext, R.style.DialogSlideAnim));
        cityDialog.getWindow().setGravity(Gravity.BOTTOM);
        cityDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        cityDialog.setContentView(R.layout.preciousadapter_citydialog_xml);
        final LoopView cityDialog_cityPicker = (LoopView) cityDialog.findViewById(R.id.loop_view);
        final TextView cityDialog_cancel = (TextView) cityDialog.findViewById(R.id.cityDialog_cancel);
        final TextView cityDialog_accept = (TextView) cityDialog.findViewById(R.id.cityDialog_accept);

        cityDialog_cityPicker.setInitPosition(0);
        cityDialog_cityPicker.setCanLoop(false);

        ArrayList<String> area_list = new ArrayList<>();
        for (int i = 0; i < Config.alBilling.size(); i++) {
            area_list.add(i, (Config.alBilling.get(i).getDescripcion()));
        }

        cityDialog_cityPicker.setTextSize(22);
        cityDialog_cityPicker.setDataList(area_list);


        cityDialog_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (cityDialog != null) {
                    cityDialog.dismiss();
                }
            }
        });

        cityDialog_accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (cityDialog != null) {
                    cityDialog.dismiss();
                }

                int value = cityDialog_cityPicker.getSelectedItem();
                String dispalyItem = Config.alBilling.get(value).getDescripcion();
                order_selectBillingAddress.setText("" + dispalyItem);

                BillingBean bb = Config.alBilling.get(value);
                if (bb.getIdDireccionFactura() != 0) {
                    pIdDirec2 = bb.getIdDireccionFactura();
                    order_billingAddressLayout.setVisibility(View.VISIBLE);
                    order_billingAddressLayout.removeAllViews();

                    LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                    View v = inflater.inflate(R.layout.billing_address_inflater, null);
                    TextView order_biiling_descripcion = (TextView) v.findViewById(R.id.order_biiling_descripcion);
                    TextView order_biiling_razonsocial = (TextView) v.findViewById(R.id.order_biiling_razonsocial);
                    TextView order_biiling_identificacion = (TextView) v.findViewById(R.id.order_biiling_identificacion);
                    TextView order_biiling_direccion = (TextView) v.findViewById(R.id.order_biiling_direccion);
                    TextView order_biiling_telephono = (TextView) v.findViewById(R.id.order_biiling_telephono);

                    order_biiling_descripcion.setText(bb.getDescripcion());
                    order_biiling_razonsocial.setText(bb.getRazonSocial());
                    order_biiling_identificacion.setText(bb.getIdentificacion());
                    order_biiling_direccion.setText(bb.getDireccion());
                    order_biiling_telephono.setText(bb.getTelefono());

                    order_billingAddressLayout.addView(v);

                } else if (dispalyItem.equals("CONSUMIDOR FINAL")) {
                    pIdDirec2 = -1;
                } else {
                    pIdDirec2 = 0;
                    order_billingAddressLayout.setVisibility(View.GONE);
                }
            }
        });

        cityDialog.show();
    }

    private void showListaFormapago() {

        final Dialog cityDialog = new Dialog(new ContextThemeWrapper(mContext, R.style.DialogSlideAnim));
        cityDialog.getWindow().setGravity(Gravity.BOTTOM);
        cityDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        cityDialog.setContentView(R.layout.preciousadapter_citydialog_xml);
        final LoopView cityDialog_cityPicker = (LoopView) cityDialog.findViewById(R.id.loop_view);
        final TextView cityDialog_cancel = (TextView) cityDialog.findViewById(R.id.cityDialog_cancel);
        final TextView cityDialog_accept = (TextView) cityDialog.findViewById(R.id.cityDialog_accept);

        cityDialog_cityPicker.setInitPosition(0);
        cityDialog_cityPicker.setCanLoop(false);

        ArrayList<String> area_list = new ArrayList<>();
        for (int i = 0; i < Config.alListPagos.size(); i++) {
            area_list.add(i, (Config.alListPagos.get(i).getNombre()));
        }

        cityDialog_cityPicker.setTextSize(22);
        cityDialog_cityPicker.setDataList(area_list);

        cityDialog_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (cityDialog != null) {
                    cityDialog.dismiss();
                    order_selectCard.setText("");
                }
            }
        });

        cityDialog_accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (cityDialog != null) {
                    cityDialog.dismiss();
                }
                int value = cityDialog_cityPicker.getSelectedItem();
                pForma = Config.alListPagos.get(value).getNombre();
                order_selectCard.setText(Config.alListPagos.get(value).getNombre());
            }
        });

        cityDialog.show();
    }

    public void getDetails() {
        /**
         * Call for Intent service for fetching the data.
         */
        Intent synIntent = new Intent(Intent.ACTION_SYNC, null, mContext, PullToData.class);
        synIntent.putExtra("syncId", 1);
        synIntent.putExtra("pIdRestaurante", pIdRestaurante);
        mContext.startService(synIntent);
    }

    private void registerOrder() {

        ArrayList<AddToCartItem> alOrderItem = getOrderRecord();

        String _data = null;

        for (int i = 0; i < alOrderItem.size(); i++) {
            _data += (i + 1) + "**";
            _data += alOrderItem.get(i).getIdRestaProducto() + "**";
            _data += alOrderItem.get(i).getNombre().replace(" ", "%20") + "**";
            _data += alOrderItem.get(i).getQty() + "**";
            _data += alOrderItem.get(i).getPrice() + "**";

            StringBuilder sb = new StringBuilder();

            ArrayList<ItemProductPrice> ipp = alOrderItem.get(i).getAlPriceo();
            ArrayList<ItemAdditionalProduct> iap = alOrderItem.get(i).getAladiciona();

            // if (iap == null && iap == null) {
            sb.append(alOrderItem.get(i).getDescripcion().replace(" ", "%20"));
            //}

            if (ipp != null && ipp.size() > 0) {
                for (int j = 0; j < ipp.size(); j++) {
                    sb.append(ipp.get(j).getGrupo() + " " + ipp.get(j).getMedida());

                    if (iap != null && iap.size() > 0) {
                        sb.append(",");
                    }
                }
            }

            if (iap != null && iap.size() > 0) {
                sb.append(" ADICIONALES: ");
                for (int k = 0; k < iap.size(); k++) {
                    sb.append("(" + iap.get(k).getAdiciona_qty() + ")");
                    sb.append(iap.get(k).getNombre() + "");
                }
            }


            String observation = sb.toString().replace(" ", "%20");

            _data += observation + "**";
            _data += "" + "**";
            _data += "" + "*--*";

            /** 2x1 Product **/

            if (alOrderItem.get(i).getQtyE2x1() != null && alOrderItem.get(i).getNombreE2x1() != null) {
                _data += (i + 1) + "**";
                _data += alOrderItem.get(i).getIdRestaProducto() + "**";
                _data += alOrderItem.get(i).getNombre().replace(" ", "%20") + "**";
                _data += alOrderItem.get(i).getQty() + "**";
                _data += alOrderItem.get(i).getPriceE2x1() + "**";

                StringBuilder sb2X = new StringBuilder();

                ArrayList<ItemProductPrice> ipp2X = alOrderItem.get(i).getAlPriceo();
                ArrayList<ItemAdditionalProduct> iap2X = alOrderItem.get(i).getAladiciona();

                // if (ipp2X == null || iap2X == null) {
                sb2X.append(alOrderItem.get(i).getDescripcion().replace(" ", "%20"));
                //}

                if (ipp2X != null && ipp2X.size() > 0) {
                    for (int k = 0; k < ipp2X.size(); k++) {
                        sb2X.append(ipp2X.get(k).getGrupo() + " " + ipp2X.get(k).getMedida());

                        if (iap2X != null && iap2X.size() > 0) {
                            sb2X.append(",");
                        }
                    }
                }

                if (iap2X != null && iap2X.size() > 0) {
                    sb2X.append(" ADICIONALES: ");
                    for (int l = 0; l < iap2X.size(); l++) {
                        sb2X.append("(" + iap2X.get(l).getAdiciona_qty() + ")");
                        sb2X.append(iap2X.get(l).getNombre() + "");
                    }
                }


                String observation1 = sb2X.toString().replace(" ", "%20");

                _data += observation1 + "**";
                _data += "" + "**";
                _data += "" + "*--*";

            }

        }

        if (_data != null && !_data.equals("null") && !_data.equals("")) {
            _data = _data.replace("null", "");
        }

        int pIdCliente = getUserId(mContext);
        int pIdRestaLocal = restaurantModel.getIdRestaLocal();

        String pObservacion = "";
        if (OrderFragment.commentOnOrder != null && !OrderFragment.commentOnOrder.equals("") && !OrderFragment.commentOnOrder.equals("null")) {
            pObservacion = OrderFragment.commentOnOrder.replace(" ", "%20");
        }
        String pRecibe;
        String pIdToken = SessionManager.getSaveToken(mContext);
        String pTipoCelular = Config.deviceType;
        float pSubTotal = sumOfPrice;

        showTotalPrice();
        float pTotal = totalPrice;

        String pCostoEnvio = restaurantModel.getCostoEnvio();

        if (pIdDirec1 != 0) {
            pRecibe = "Domicilio";
        } else {
            pRecibe = "Restaurant";
        }

        if (pIdDirec1 == 0) {
            setMessageErrorDialog(mContext.getResources().getString(R.string.select_entrega));
            return;
        }

        if (pForma == null) {
            setMessageErrorDialog(mContext.getResources().getString(R.string.select_form_de_pago));
            return;
        }

        String url = API.RegistraPedido
                + "pIdCliente=" + pIdCliente
                + "&pIdRestaLocal=" + pIdRestaLocal
                + "&pObservacion=" + pObservacion
                + "&pIdDirec1=" + pIdDirec1
                + "&pIdDirec2=" + pIdDirec2
                + "&pForma=" + pForma.replace(" ", "%20")
                + "&pRecibe=" + pRecibe
                + "&pIdToken=" + pIdToken
                + "&pTipoCelular=" + pTipoCelular
                + "&pSubTotal=" + pSubTotal
                + "&pTotal=" + pTotal
                + "&pCostoEnvio=" + pCostoEnvio
                + "&pData=" + _data
                + "&callback=" + "";

        Log.e("url", "" + url);

        showProgressBar();

        GlobalValues.getMethodManagerObj(mContext).makeStringReqWithoutCache(url, "apiOrder", new MethodListener() {
            @Override
            public void onError() {
                hideProgressbar();
            }

            @Override
            public void onError(String response) {
                hideProgressbar();
            }

            @Override
            public void onSuccess(String response) {
                //({"data":[{"respuesta":"OK"}]})
                //{"data":[{"respuesta":"OK"}]}
                hideProgressbar();
                //response = response.substring(1, response.length() - 1);
                try {
                    JSONObject mObj = new JSONObject(response);
                    JSONArray mArray = mObj.optJSONArray("data");
                    if (mArray != null) {
                        for (int i = 0; i < mArray.length(); i++) {
                            JSONObject object = mArray.optJSONObject(i);
                            if (object.has("respuesta")) {
                                if (object.optString("respuesta").equals("OK")) {
                                  /* *
                                     * Show the congratulations screen
                                     **/
                                    Bundle bundle = new Bundle();
                                    DashBoardActivity.displayFragment(FragmentIDs.CongratulationsFragment_Id, bundle);
                                }
                            }
                        }
                    }
                } catch (JSONException e) {
                    hideProgressbar();
                    Log.e(TAG, "" + e.toString());
                }
            }
        });

    }

    private void showProgressBar() {
        resturent_progressBar.setVisibility(View.VISIBLE);
        animation = AnimationUtils.loadAnimation(mContext, R.anim.progress_anim);
        animation.setDuration(1000);
        resturent_progressBar.startAnimation(animation);
    }

    private void hideProgressbar() {
        if (animation != null) {
            resturent_progressBar.clearAnimation();
            resturent_progressBar.setVisibility(View.GONE);
        }
    }

    private void setMessageErrorDialog(String message) {
        messageDialog = new MessageDialog(mContext,
                android.R.style.Theme_Holo_Light_Dialog,
                message,
                new MessageDialog.okOnClickListener() {
                    @Override
                    public void onButtonClick() {
                        messageDialog.dismiss();
                    }
                });

        messageDialog.setCancelable(false);
        messageDialog.setCanceledOnTouchOutside(false);
        messageDialog.show();
    }


    public ArrayList<AddToCartItem> getOrderRecord() {
        totalPrice = sumOfPrice = 0.0f;

        ArrayList<OrderData> alOrderData = addToCartManager.getOrderData();

        ArrayList<AddToCartItem> alOrderItem = new ArrayList<>();

        if (alOrderData.size() > 0) {

            ArrayList<AddToCartItem> alOrderItem1 = getTipoModelo2(alOrderData);
            if (alOrderItem1.size() > 0) {
                for (int i = 0; i < alOrderItem1.size(); i++) {
                    alOrderItem.add(alOrderItem1.get(i));
                }
            }

            for (int l = 0; l < alOrderData.size(); l++) {
                if (alOrderData.get(l).getEsDia2x1().equals("S")) {

                    if (alOrderData.get(l).getTipoModelo2Por1_P().equals("1")
                            && alOrderData.get(l).getTipoModelAdicional2Por1().equals("1")) {

                        AddToCartItem addToCartItem = addToCartManager.getProductItemModelAdicional2Por1_1(alOrderData.get(l));
                        alOrderItem.add(addToCartItem);

                        sumOfPrice += (addToCartItem.getPrice() + addToCartItem.getPriceE2x1());
                    }

                    if (alOrderData.get(l).getTipoModelo2Por1_P().equals("1")
                            && alOrderData.get(l).getTipoModelAdicional2Por1().equals("2")) {

                        AddToCartItem addToCartItem = addToCartManager.getProductItem(alOrderData.get(l));
                        alOrderItem.add(addToCartItem);

                        sumOfPrice += (addToCartItem.getPrice() + addToCartItem.getPriceE2x1());

                    }

                    //--------------------------------------------------------//

                    if (alOrderData.get(l).getTipoModelo2Por1_P().equals("3")
                            && (alOrderData.get(l).getTipoModelAdicional2Por1().equals("2")
                            || alOrderData.get(l).getTipoModelAdicional2Por1().equals("1"))) {

                    /*    AddToCartItem addToCartItem = new AddToCartItem();
                        addToCartItem = addToCartManager.callProductTable(addToCartItem, alOrderData.get(l));
                        alOrderItem.add(addToCartItem);

                        AddToCartItem addToCartItem2 = new AddToCartItem();

                        addToCartItem2 = addToCartManager.callProductTable(addToCartItem2, alOrderData.get(l));
                        addToCartItem2.setPrice(0f);
                        alOrderItem.add(addToCartItem2);

                        sumOfPrice += addToCartItem.getPrice();
*/
                        if(alOrderData.get(l).getTipoModelAdicional2Por1().equals("1")){
                            AddToCartItem addToCartItem = new AddToCartItem();
                            addToCartItem = addToCartManager.callProductTable(addToCartItem, alOrderData.get(l));
                            alOrderItem.add(addToCartItem);
                            AddToCartItem addToCartItem2 = new AddToCartItem();

                            addToCartItem2 = addToCartManager.callProductTable(addToCartItem2, alOrderData.get(l));
                            addToCartItem2.setPrice(0f);
                            ArrayList<ItemAdditionalProduct> lsvacia = new ArrayList<>();
                            addToCartItem2.setAladiciona(lsvacia);
                            alOrderItem.add(addToCartItem2);

                            sumOfPrice += addToCartItem.getPrice();

                        }
                        if(alOrderData.get(l).getTipoModelAdicional2Por1().equals("2")){
                            AddToCartItem addToCartItem = new AddToCartItem();
                            addToCartItem = addToCartManager.callProductTable(addToCartItem, alOrderData.get(l));
                            alOrderItem.add(addToCartItem);
                            AddToCartItem addToCartItem2 = new AddToCartItem();

                            addToCartItem2 = addToCartManager.callProductTable(addToCartItem2, alOrderData.get(l));
                            addToCartItem2.setPrice(0f);
                            alOrderItem.add(addToCartItem2);

                            sumOfPrice += addToCartItem.getPrice();

                        }

                    }
                }

                if (alOrderData.get(l).getEsDia2x1().equals("N")) {
                    AddToCartItem addToCartItem = new AddToCartItem();
                    addToCartItem = addToCartManager.callProductTable(addToCartItem, alOrderData.get(l));
                    alOrderItem.add(addToCartItem);

                    sumOfPrice += addToCartItem.getPrice();
                }
            }
        }
        return alOrderItem;
    }

    private ArrayList<AddToCartItem> getTipoModelo2(ArrayList<OrderData> alOrderData) {

        ArrayList<AddToCartItem> alOrderItem = new ArrayList<>();
        ArrayList<OrderData> alOrderDataCopyOf = new ArrayList<>();

        for (int l = 0; l < alOrderData.size(); l++) {
            if (alOrderData.get(l).getEsDia2x1().equals("S")) {
                if (alOrderData.get(l).getTipoModelo2Por1_P().equals("2")
                        && alOrderData.get(l).getTipoModelAdicional2Por1().equals("1")) {

                    alOrderDataCopyOf.add(alOrderData.get(l));

                }

                if (alOrderData.get(l).getTipoModelo2Por1_P().equals("2")
                        && alOrderData.get(l).getTipoModelAdicional2Por1().equals("2")) {
                    alOrderDataCopyOf.add(alOrderData.get(l));
                }
            }
        }

        if (alOrderDataCopyOf.size() > 0) {
            Collections.sort(alOrderDataCopyOf, new Comparator<OrderData>() {
                @Override
                public int compare(OrderData t0, OrderData t1) {
                    return Float.compare(t1.getProductPrice(), t0.getProductPrice());
                }
            });

            if (alOrderDataCopyOf.get(0).getTipoModelo2Por1_P().equals("2")
                    && alOrderDataCopyOf.get(0).getTipoModelAdicional2Por1().equals("1")) {


                int size = alOrderDataCopyOf.size();
                for (int i = 0; i < size / 2; i++) {
                    AddToCartItem addToCartItem = addToCartManager.getOrderItemTipoModelo2_Modelo1(alOrderDataCopyOf.get(i), 1);
                    alOrderItem.add(addToCartItem);
                }

                for (int i = size / 2; i < size; i++) {
                    AddToCartItem addToCartItem = addToCartManager.getOrderItemTipoModelo2_Modelo1(alOrderDataCopyOf.get(i), 2);
                    alOrderItem.add(addToCartItem);
                }

                for (int i = 0; i < alOrderItem.size() / 2; i++) {
                    sumOfPrice += alOrderItem.get(i).getPrice();
                }
            }


            if (alOrderDataCopyOf.get(0).getTipoModelo2Por1_P().equals("2")
                    && alOrderDataCopyOf.get(0).getTipoModelAdicional2Por1().equals("2")) {

                int size = alOrderDataCopyOf.size();
                for (int i = 0; i < size / 2; i++) {
                    AddToCartItem addToCartItem = addToCartManager.getOrderItemTipoModelo2_Modelo2(alOrderDataCopyOf.get(i));
                    alOrderItem.add(addToCartItem);
                }

                for (int i = size / 2; i < size; i++) {
                    AddToCartItem addToCartItem = addToCartManager.getOrderItemTipoModelo2_Modelo2_ChargeOfAdicional(alOrderDataCopyOf.get(i));
                    alOrderItem.add(addToCartItem);
                }

                for (int i = 0; i < alOrderItem.size(); i++) {
                    sumOfPrice += alOrderItem.get(i).getPrice();
                }
            }
        }
        return alOrderItem;
    }
}
