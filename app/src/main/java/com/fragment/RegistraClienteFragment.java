package com.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.view.ContextThemeWrapper;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import com.activity.DashBoardActivity;
import com.android.internal.http.multipart.MultipartEntity;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.grupodeliveryec.deliveryEC.R;
import com.bruce.pickerview.LoopView;
import com.com.rest.API;
import com.com.rest.GlobalValues;
import com.com.rest.MethodListener;
import com.utility.AppController;
import com.utility.Config;
import com.utility.FragmentIDs;
import com.utility.KeyboadSetting;
import com.utility.MessageDialog;
import com.utility.SessionManager;
import com.utility.UtilityMessage;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by administrator on 8/2/17.
 */

/**
 * Process of Registration
 * 1. Fill the Registration form
 * 2. Fill the Delivery Address
 */

public class RegistraClienteFragment extends Fragment {

    String TAG = getClass().getName();
    String apiRegistraCliente = "apiRegistraCliente";
    private static Bundle b;
    protected static Context context;
    protected static Fragment fragment;
    protected static FragmentManager fragmentManager;
    View view;

    MessageDialog.okOnClickListener okOnClickListener;
    MessageDialog messageDialog;

    EditText register_name, register_surname;
    EditText register_gender, register_identificationCard;
    EditText register_email, register_phone;
    EditText register_mobile, register_password;
    EditText register_terms_and_conditions;
    LinearLayout register_regContinue;
    ImageView resturent_progressBar;

    String profile_pic;
    Animation animation;

    boolean isChecked = false;

    String[] genderId = new String[]{"0", "M", "F"};
    String genderids;

    public static Fragment getInstance(Context mContext, FragmentManager fragmentManager, Bundle bundle) {
        fragment = new RegistraClienteFragment();
        context = mContext;
        b = bundle;
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.register_client_xml, container, false);

        DashBoardActivity.hideInfo();
        DashBoardActivity.hideCerrar();
        DashBoardActivity.hideSearchButton();
        DashBoardActivity.showBackButton();
        DashBoardActivity.hideLogout();
        DashBoardActivity.hideHorizentalBar();
        DashBoardActivity.hideAddDeliveryAddress();
        DashBoardActivity.updateTitle(getResources().getString(R.string.registro));
        DashBoardActivity.hideMenuPanel();

        register_name = (EditText) view.findViewById(R.id.register_name);
        register_surname = (EditText) view.findViewById(R.id.register_surname);
        register_gender = (EditText) view.findViewById(R.id.register_gender);
        register_identificationCard = (EditText) view.findViewById(R.id.register_identificationCard);
        register_email = (EditText) view.findViewById(R.id.register_email);
        register_phone = (EditText) view.findViewById(R.id.register_phone);
        register_mobile = (EditText) view.findViewById(R.id.register_mobile);
        register_password = (EditText) view.findViewById(R.id.register_password);
        register_terms_and_conditions = (EditText) view.findViewById(R.id.register_terms_and_conditions);
        register_regContinue = (LinearLayout) view.findViewById(R.id.register_regContinue);
        resturent_progressBar = (ImageView) view.findViewById(R.id.resturent_progressBar);

        register_regContinue.setOnClickListener(addToRegistration);
        register_terms_and_conditions.setOnClickListener(addToTermsConditions);
        register_gender.setOnClickListener(addToGender);

        String id = b.containsKey("id") ? b.getString("id") : null;
        String name = b.containsKey("name") ? b.getString("name") : null;
        String first_name = b.containsKey("first_name") ? b.getString("first_name") : null;
        String last_name = b.containsKey("last_name") ? b.getString("last_name") : null;
        String email = b.containsKey("email") ? b.getString("email") : null;
        String gender = b.containsKey("gender") ? b.getString("gender") : null;

        profile_pic = b.containsKey("profilepic") ? b.getString("profilepic") : null;

        if (first_name != null) {
            register_name.setText(first_name);
        }
        if (last_name != null) {
            register_surname.setText(last_name);
        }

        if (email != null) {
            register_email.setText(email);
        }


        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        try {
            final InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
        }catch (Exception e){

        }

    }

    @Override
    public void onStop() {
        super.onStop();

    }

    @Override
    public void onDestroy() {

        super.onDestroy();

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        AppController.getInstance().cancelPendingRequests(apiRegistraCliente);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void setMessageErrorDialog(String message) {
        messageDialog = new MessageDialog(context,
                android.R.style.Theme_Holo_Light_Dialog,
                message,
                new MessageDialog.okOnClickListener() {
                    @Override
                    public void onButtonClick() {
                        messageDialog.dismiss();
                    }
                });

        messageDialog.setCancelable(false);
        messageDialog.setCanceledOnTouchOutside(false);
        messageDialog.show();
    }

    private void showProgressBar() {
        resturent_progressBar.setVisibility(View.VISIBLE);
        animation = AnimationUtils.loadAnimation(context, R.anim.progress_anim);
        animation.setDuration(1000);
        resturent_progressBar.startAnimation(animation);
    }

    private void hideProgressbar() {
        if (animation != null) {
            resturent_progressBar.clearAnimation();
            resturent_progressBar.setVisibility(View.GONE);
        }
    }

    private void clearView() {
        register_name.setText("");
        register_surname.setText("");
        register_gender.setText(getResources().getString(R.string.Seleccione_genero));
        register_identificationCard.setText("");
        register_email.setText("");
        register_phone.setText("");
        register_mobile.setText("");
        register_password.setText("");
        register_terms_and_conditions.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.unchecked, 0);
    }

    private void userRegister() {
        if (register_name.getText().toString().equals("") && register_surname.getText().toString().equals("")
                && register_gender.getText().toString().equals("") && register_identificationCard.getText().toString().equals("")
                && register_email.getText().toString().equals("") && register_phone.getText().toString().equals("")
                && register_mobile.getText().toString().equals("") && register_password.getText().toString().equals("")
                && isChecked == true) {

            String message = getResources().getString(R.string.enter_Debe_ingresar_nombres) + "\n"
                    + getResources().getString(R.string.enter_Debe_ingresar_apellidos) + "\n"
                    + getResources().getString(R.string.enter_Debe_seleccionar_genero) + "\n"
                    + getResources().getString(R.string.enter_Debe_ingresar_identificacion) + "\n"
                    + getResources().getString(R.string.enter_Debe_ingresar_correo_electronico) + "\n"
                    + getResources().getString(R.string.enter_Debe_ingresar_telefono) + "\n"
                    + getResources().getString(R.string.enter_Debe_ingresar_celular) + "\n"
                    + getResources().getString(R.string.enter_Debe_ingresar_contrasena) + "\n"
                    + getResources().getString(R.string.enter_debe_aceptar_los_terminos_y_condiciones);

            setMessageErrorDialog(message);
        } else if (register_name.getText().toString().equals("")) {
            String message = getResources().getString(R.string.enter_Debe_ingresar_nombres);
            setMessageErrorDialog(message);
        } else if (register_surname.getText().toString().equals("")) {
            String message = getResources().getString(R.string.enter_Debe_ingresar_apellidos);
            setMessageErrorDialog(message);
        } else if (register_gender.getText().toString().equals("")) {
            String message = getResources().getString(R.string.enter_Debe_seleccionar_genero);
            setMessageErrorDialog(message);
        } else if (register_identificationCard.getText().toString().equals("")) {
            String message = getResources().getString(R.string.enter_Debe_ingresar_identificacion);
            setMessageErrorDialog(message);
        } else if (register_email.getText().toString().equals("")) {
            String message = getResources().getString(R.string.enter_Debe_ingresar_correo_electronico);
            setMessageErrorDialog(message);
        } else if (register_phone.getText().toString().equals("")) {
            String message = getResources().getString(R.string.enter_Debe_ingresar_telefono);
            setMessageErrorDialog(message);
        } else if (register_mobile.getText().toString().equals("")) {
            String message = getResources().getString(R.string.enter_Debe_ingresar_celular);
            setMessageErrorDialog(message);
        } else if (register_password.getText().toString().equals("")) {
            String message = getResources().getString(R.string.enter_Debe_ingresar_contrasena);
            setMessageErrorDialog(message);
        } else if (isChecked == true) {
            String message = getResources().getString(R.string.enter_debe_aceptar_los_terminos_y_condiciones);
            setMessageErrorDialog(message);
        } else if (genderids.equals("0")) {
            String message = getResources().getString(R.string.enter_Debe_seleccionar_genero);
            setMessageErrorDialog(message);
        } else {
            /**
             * Call the web service
             */

            register();
           /* AsyncTaskRunner runner = new AsyncTaskRunner();
            runner.execute();*/

            /*String url = API.RegistraCliente+"pNombres="+register_name.getText().toString().replace(" ","%20")
                    +"&pApellidos="+register_surname.getText().toString().replace(" ","%20")
                    +"&pGenero="+genderids//register_gender.getText().toString()
                    +"&pIdentificacion="+register_identificationCard.getText().toString().replace(" ","%20")
                    +"&pEmail="+register_email.getText().toString().replace(" ","%20")
                    +"&pContra="+register_phone.getText().toString().replace(" ","%20")
                    +"&pTelefono="+register_phone.getText().toString().replace(" ","%20")
                    +"&pCelular="+register_mobile.getText().toString().replace(" ","%20")
                    +"&pToken="+ SessionManager.getSaveToken(context)
                    +"&pOS="+ Config.deviceType
                    +"&pNombreDispo="+ UtilityMessage.deviceInfo().replace(" ","%20")
                    +"&pUUID="+UtilityMessage.deviceUUID(context)
                    +"&callback=";
*/
            showProgressBar();

            /*GlobalValues.getMethodManagerObj(context).makeStringReq(url, apiRegistraCliente, new MethodListener() {
                @Override
                public void onError() {
                    hideProgressbar();
                }

                @Override
                public void onError(String response) {
                    Log.e(TAG,""+response);
                    hideProgressbar();
                }

                @Override
                public void onSuccess(String response) {
                    hideProgressbar();
                    response  = response.substring(1,response.length()-1);
                    Log.e(TAG,""+response);

                    try{
                        JSONObject obj = new JSONObject(response);
                        JSONArray mArray = obj.optJSONArray("data");
                        if(mArray!=null){
                            for(int i=0;i<mArray.length();i++){
                                JSONObject object = mArray.optJSONObject(i);
                                if(object.has("respuesta")){
                                    if(object.optString("respuesta").equals("OK")){
                                        clearView();

                                        *//**
             * Move to DIRECCION DE ENTREGA
             *//*

                                        try{
                                            String idcliente = object.optString("idcliente");
                                            String fullName = register_name.getText().toString()+" "+register_surname.getText().toString();
                                            int Id = Integer.parseInt(idcliente);

                                            SessionManager.saveUserLogin(context,Id,register_email.getText().toString(),fullName,register_identificationCard.getText().toString());

                                            Bundle bundle = new Bundle();
                                            bundle.putString("idcliente",idcliente);
                                            bundle.putInt("addressType", Config.addRegistrationAddressType);
                                            DashBoardActivity.displayFragment(FragmentIDs.DeliveryAddressFragment_Id,bundle);
                                        }catch (NumberFormatException e){
                                            Log.e(TAG,""+e.toString());
                                        }catch (Exception e){
                                            Log.e(TAG,""+e.toString());
                                        }
                                    }else if(object.optString("respuesta").equals("NO")){
                                        setMessageErrorDialog(object.optString("Error"));
                                    }
                                }
                            }
                        }
                    }catch (JSONException e){
                        Log.e(TAG,""+e.toString());
                    }

                }
            });*/
        }
    }
    private void register(){

      // String url = "https://deliveryec.azurewebsites.net/ServiciosWeb/WebRestauranteLogin.asmx/EliminarClienteDireccion?pIdDirecciones="+idDirecciones+"&pIdCliente="+ SessionManager.getUserId(mContext)+"&callback=";




        String url = API.new_registration_url + "?pNombres="+""+register_name.getText().toString().replace(" ", "%20")+"&pApellidos="
                +""+register_surname.getText().toString().replace(" ", "%20")+"&pGenero="+genderids+"&pIdentificacion="+
                register_identificationCard.getText().toString().replace(" ", "%20")
                +"&pEmail="+register_email.getText().toString().replace(" ", "%20")+"&pContra="+
                register_password.getText().toString().replace(" ", "%20")+"&pTelefono="
                +register_phone.getText().toString().replace(" ", "%20")+
                "&pCelular="+register_mobile.getText().toString().replace(" ", "%20")+
                "&pToken="+SessionManager.getSaveToken(context)+"&pOS="+Config.deviceType
                +"&pNombreDispo="+UtilityMessage.deviceInfo().replace(" ", "%20")+
                "&pUUID="+UtilityMessage.deviceUUID(context)+"&fb_image="+profile_pic.replace("&","_@_")+"&callback="+"";

        //String url = API.deliveryAddress+"pIdCliente="+ SessionManager.getUserId(context)+"&callback=";
        //https://deliveryec.azurewebsites.net/ServiciosWeb/WebRestauranteLogin.asmx/ListarClienteDireccion?pIdCliente=10004&callback=
        //showProgressBar();

        GlobalValues.getMethodManagerObj(context).makeStringReqWithoutCache(url, "RegistraCliente", new MethodListener() {
            @Override
            public void onError() {
                hideProgressbar();
            }

            @Override
            public void onError(String response) {
                hideProgressbar();
            }

            @Override
            public void onSuccess(String response) {
                hideProgressbar();
                try{
                    response = response.substring(1,response.length()-1);

                    JSONObject obj = new JSONObject(response);
                    JSONArray mArray = obj.optJSONArray("data");
                    if(mArray!=null){
                        for(int i=0;i<mArray.length();i++){
                            JSONObject object = mArray.optJSONObject(i);
                            if(object.has("respuesta")){
                                if(object.optString("respuesta").equals("OK")){

                                    try{
                                        String idcliente = object.optString("idcliente");
                                        String fullName = register_name.getText().toString()+" "+register_surname.getText().toString();
                                        int Id = Integer.parseInt(idcliente);
                                        SessionManager.save_fb_image(context , profile_pic);
                                        SessionManager.save_username(context , fullName);
                                        SessionManager.saveUserLogin(context,Id,register_email.getText().toString(),fullName,register_identificationCard.getText().toString());
                                        clearView();
                                        Bundle bundle = new Bundle();
                                        bundle.putString("idcliente",idcliente);
                                        bundle.putInt("addressType", Config.addRegistrationAddressType);
                                        DashBoardActivity.displayFragment(FragmentIDs.DeliveryAddressFragment_Id,bundle);
                                    }catch (NumberFormatException e){
                                        Log.e(TAG,""+e.toString());
                                    }catch (Exception e){
                                        Log.e(TAG,""+e.toString());
                                    }
                                }else if(object.optString("respuesta").equals("NO")){
                                    setMessageErrorDialog(object.optString("Error"));
                                }
                            }
                        }
                    }
                }catch (JSONException e){
                    Log.e(TAG,""+e.toString());
                }
            }
        });
    }


    private class AsyncTaskRunner extends AsyncTask<String, String, String> {

        private String resp;
        List<NameValuePair> nameValuePair;

        @Override
        protected void onPreExecute() {
            nameValuePair = new ArrayList<NameValuePair>();
            nameValuePair.add(new BasicNameValuePair("pNombres", register_name.getText().toString().replace(" ", "%20")));
            nameValuePair.add(new BasicNameValuePair("pApellidos", register_surname.getText().toString().replace(" ", "%20")));
            nameValuePair.add(new BasicNameValuePair("pGenero", genderids));
            nameValuePair.add(new BasicNameValuePair("pIdentificacion", register_identificationCard.getText().toString().replace(" ", "%20")));
            nameValuePair.add(new BasicNameValuePair("pEmail", register_email.getText().toString().replace(" ", "%20")));
            nameValuePair.add(new BasicNameValuePair("pContra", register_password.getText().toString().replace(" ", "%20")));
            nameValuePair.add(new BasicNameValuePair("pTelefono", register_phone.getText().toString().replace(" ", "%20")));
            nameValuePair.add(new BasicNameValuePair("pCelular", register_mobile.getText().toString().replace(" ", "%20")));
            nameValuePair.add(new BasicNameValuePair("pToken", SessionManager.getSaveToken(context)));
            nameValuePair.add(new BasicNameValuePair("pOS", Config.deviceType));
            nameValuePair.add(new BasicNameValuePair("pNombreDispo", UtilityMessage.deviceInfo().replace(" ", "%20")));
            nameValuePair.add(new BasicNameValuePair("pUUID", UtilityMessage.deviceUUID(context)));
            nameValuePair.add(new BasicNameValuePair("fb_image", profile_pic));
            nameValuePair.add(new BasicNameValuePair("callback", ""));
        }

        @Override
        protected String doInBackground(String... params) {
            String response;
            InputStream is = null;
            try {
                DefaultHttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(API.new_registration_url);
                try {
                    httpPost.setEntity(new UrlEncodedFormEntity(nameValuePair));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }

                HttpResponse httpResponse = httpClient.execute(httpPost);
                HttpEntity httpEntity = httpResponse.getEntity();
                is = httpEntity.getContent();
                int statusCode = httpResponse.getStatusLine().getStatusCode();
                Log.e("Status Code <><><<><><>", "cod = " + statusCode);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        is, "iso-8859-1"), 8);
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                is.close();
                response = sb.toString();
            } catch (Exception e) {
                Log.e("Buffer Error", "Error converting result " + e.toString());
                response = null;
            }
            return response;
        }

        @Override
        protected void onPostExecute(String response) {
            hideProgressbar();
         //   response  = response.substring(1,response.length()-1);
            Log.e(TAG,""+response);
            try{
                JSONObject obj = new JSONObject(response);
                JSONArray mArray = obj.optJSONArray("data");
                if(mArray!=null){
                    for(int i=0;i<mArray.length();i++){
                        JSONObject object = mArray.optJSONObject(i);
                        if(object.has("respuesta")){
                            if(object.optString("respuesta").equals("OK")){

                                try{
                                    String idcliente = object.optString("idcliente");
                                    String fullName = register_name.getText().toString()+" "+register_surname.getText().toString();
                                    int Id = Integer.parseInt(idcliente);
                                    SessionManager.save_fb_image(context , profile_pic);
                                    SessionManager.save_username(context , fullName);
                                    SessionManager.saveUserLogin(context,Id,register_email.getText().toString(),fullName,register_identificationCard.getText().toString());
                                    clearView();
                                    Bundle bundle = new Bundle();
                                    bundle.putString("idcliente",idcliente);
                                    bundle.putInt("addressType", Config.addRegistrationAddressType);
                                    DashBoardActivity.displayFragment(FragmentIDs.DeliveryAddressFragment_Id,bundle);
                                }catch (NumberFormatException e){
                                    Log.e(TAG,""+e.toString());
                                }catch (Exception e){
                                    Log.e(TAG,""+e.toString());
                                }
                            }else if(object.optString("respuesta").equals("NO")){
                                setMessageErrorDialog(object.optString("Error"));
                            }
                        }
                    }
                }
            }catch (JSONException e){
                Log.e(TAG,""+e.toString());
            }
            // execution of result of Long time consuming operation
        }

        @Override
        protected void onProgressUpdate(String... text) {
        }
    }

    private void getGender() {
        final String[] genderArray = new String[]{"Seleccione Genero", "Masculino", "Femenino"};
        final Dialog cityDialog = new Dialog(new ContextThemeWrapper(context, R.style.DialogSlideAnim));
        cityDialog.getWindow().setGravity(Gravity.BOTTOM);
        cityDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        cityDialog.setContentView(R.layout.areadialog_xml2);
        final LoopView cityDialog_cityPicker = (LoopView) cityDialog.findViewById(R.id.loop_view);
        final TextView cityDialog_cancel = (TextView) cityDialog.findViewById(R.id.areaDialog_cancel);
        final TextView cityDialog_accept = (TextView) cityDialog.findViewById(R.id.areaDialog_accept);

        /*final NumberPicker cityDialog_cityPicker = (NumberPicker) cityDialog.findViewById(R.id.cityDialog_cityPicker);
        final TextView cityDialog_cancel = (TextView) cityDialog.findViewById(R.id.cityDialog_cancel);
        final TextView cityDialog_accept = (TextView) cityDialog.findViewById(R.id.cityDialog_accept);*/
        cityDialog_cityPicker.setCanLoop(false);
       /* cityDialog_cityPicker.setMinValue(0);
        cityDialog_cityPicker.setMaxValue(genderArray.length - 1);
       */ // String[] displayValues = new String[genderArray.length];
        /*cityDialog_cityPicker.setDisplayedValues(genderArray);
        setCityPickerDividerColour(cityDialog_cityPicker);*/

        ArrayList<String> area_list = new ArrayList<>();
        for (int i = 0; i < genderArray.length; i++) {
            area_list.add(i, ""+genderArray[i]);
        }

        cityDialog_cityPicker.setTextSize(22);//must be called before setDateList
        cityDialog_cityPicker.setDataList(area_list);

        cityDialog_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cityDialog.dismiss();
            }
        });

        cityDialog_accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cityDialog.dismiss();
                register_gender.setText(genderArray[cityDialog_cityPicker.getSelectedItem()]);
                genderids = genderId[cityDialog_cityPicker.getSelectedItem()];
            }
        });

        cityDialog.show();
    }

    Handler openGenderHandler;

    private void openGender() {
        View v = ((Activity) context).getCurrentFocus();
        //View v = getActivity().getCurrentFocus();
        KeyboadSetting.hideKeyboadUsingView(v, context);
        //KeyboadSetting.hideKeyboard(context,register_surname);

        openGenderHandler = new Handler(Looper.getMainLooper());
        openGenderHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                getGender();
            }
        }, 500);
    }

    View.OnClickListener addToGender = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            openGender();
        }
    };

    View.OnClickListener addToRegistration = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            userRegister();
        }
    };

    View.OnClickListener addToTermsConditions = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (isChecked == false) {
                isChecked = true;
                register_terms_and_conditions.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.checked, 0);
            } else {
                isChecked = false;
                register_terms_and_conditions.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.unchecked, 0);
            }

        }
    };
}
