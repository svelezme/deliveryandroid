package com.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import com.activity.DashBoardActivity;
import com.adapter.MyOrderAdapter;
import com.app.grupodeliveryec.deliveryEC.R;
import com.bean.MyOrderBean;
import com.bean.MyOrderDetails;
import com.com.rest.API;
import com.com.rest.GlobalValues;
import com.com.rest.MethodListener;
import com.utility.AppController;
import com.utility.FragmentIDs;
import com.utility.RecyclerTouchListener;
import com.utility.SessionManager;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.Vector;

/**
 * Created by administrator on 17/2/17.
 */

public class MyOrdersFragment extends Fragment{

    String apiRequest = "apiMyOrder";
    String TAG = getClass().getName();
    private static Context context;
    private static FragmentManager fm;
    private static Bundle bundle;
    private static Fragment fragment;
    View view;
    RecyclerView myorder_recycleView;
    ImageView resturent_progressBar;
    Animation animation;
    Vector<MyOrderBean> alOrder;
    MyOrderAdapter myOrderAdapter;

    public static Fragment getInstance(Context mContext, FragmentManager fragmentManager, Bundle bundle){
        fragment = new MyOrdersFragment();
        context = mContext;
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        DashBoardActivity.updateTitle(getResources().getString(R.string.mis_pedidos));
        DashBoardActivity.showBackButton();
        DashBoardActivity.hideInfo();
        DashBoardActivity.hideLogout();
        DashBoardActivity.hideSearchButton();
        DashBoardActivity.hideCerrar();
        DashBoardActivity.showHorizentalBar();
        DashBoardActivity.hideMenuPanel();

        view = inflater.inflate(R.layout.myorders_xml,container,false);
        myorder_recycleView = (RecyclerView)view.findViewById(R.id.myorder_recycleView);
        resturent_progressBar = (ImageView)view.findViewById(R.id.resturent_progressBar);

        myorder_recycleView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
        myorder_recycleView.setLayoutManager(mLayoutManager);
        myorder_recycleView.setItemAnimator(new DefaultItemAnimator());

        myorder_recycleView.addOnItemTouchListener(new RecyclerTouchListener(context, myorder_recycleView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                if(alOrder!=null && alOrder.size()>0){
                    MyOrderBean myOrderBean = alOrder.get(position);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("orderDetails",myOrderBean);
                    DashBoardActivity.displayFragment(FragmentIDs.MyOrderDetailsFragment_Id,bundle);
                }
            }

            @Override
            public void onLongClick(View view, int position) {
            }
        }));
        getMyOrder();

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        AppController.getInstance().cancelPendingRequests(apiRequest);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void showProgressBar(){
        resturent_progressBar.setVisibility(View.VISIBLE);
        animation = AnimationUtils.loadAnimation(context, R.anim.progress_anim);
        animation.setDuration(1000);
        resturent_progressBar.startAnimation(animation);
    }
    private void hideProgressbar(){
        if(animation!=null){
            resturent_progressBar.clearAnimation();
            resturent_progressBar.setVisibility(View.GONE);
        }
    }

    private void readyToOrderList(JSONArray mData){
        alOrder = new Vector<>();
        for(int i=0;i<mData.length();i++){
            JSONObject mObj = mData.optJSONObject(i);
            MyOrderBean myOrderBean = new MyOrderBean();
            myOrderBean.setIdPedido(mObj.optInt("IdPedido"));
            myOrderBean.setIdRestaLocal(mObj.optInt("IdRestaLocal"));
            myOrderBean.setIdCliente(mObj.optInt("IdCliente"));
            myOrderBean.setObservacion(mObj.optString("Observacion"));
            myOrderBean.setSubtotal(mObj.optDouble("Subtotal"));
            myOrderBean.setIVA(mObj.optDouble("IVA"));
            myOrderBean.setServicio(mObj.optDouble("Servicio"));
            myOrderBean.setTotal(mObj.optDouble("Total"));
            myOrderBean.setRecibe(mObj.optString("Recibe"));
            myOrderBean.setFormaPago(mObj.optString("FormaPago"));
            myOrderBean.setFechaRegistro(mObj.optString("FechaRegistro"));
            myOrderBean.setEstado(mObj.optString("Estado"));
            myOrderBean.setNombres(mObj.optString("Nombres"));
            myOrderBean.setApellidos(mObj.optString("Apellidos"));
            myOrderBean.setIdentificacion(mObj.optString("Identificacion"));
            myOrderBean.setNombreCompletoCliente(mObj.optString("NombreCompletoCliente"));
            myOrderBean.setNombreRestaurante(mObj.optString("NombreRestaurante"));
            myOrderBean.setNombreLocal(mObj.optString("NombreLocal"));
            myOrderBean.setDescripcionLocal(mObj.optString("DescripcionLocal"));
            myOrderBean.setTipoComida(mObj.optString("TipoComida"));
            myOrderBean.setImagenLocal(mObj.optString("ImagenLocal"));
            myOrderBean.setDirecEntrega(mObj.optString("DirecEntrega"));
            myOrderBean.setDirecEntregaTipo(mObj.optString("DirecEntregaTipo"));
            myOrderBean.setDirecEntregaDireccion(mObj.optString("DirecEntregaDireccion"));
            myOrderBean.setDirecEntregaDireccion2(mObj.optString("DirecEntregaDireccion2"));
            myOrderBean.setDirecEntregaCiudad(mObj.optString("DirecEntregaCiudad"));
            myOrderBean.setDirecEntregaSector(mObj.optString("DirecEntregaSector"));
            myOrderBean.setDirecEntregaTelefono(mObj.optString("DirecEntregaTelefono"));
            myOrderBean.setDirecEntregaReferencia(mObj.optString("DirecEntregaReferencia"));
            myOrderBean.setRevision_status(mObj.optString("revision_status"));

            myOrderBean.setDirecFactura(mObj.optString("DirecFactura"));
            myOrderBean.setDirecFacturaRazonSocial(mObj.optString("DirecFacturaRazonSocial"));
            myOrderBean.setDirecFacturaIdentificacion(mObj.optString("DirecFacturaIdentificacion"));
            myOrderBean.setDirecFacturaDireccion(mObj.optString("DirecFacturaDireccion"));
            myOrderBean.setDirecFacturaTelefono(mObj.optString("DirecFacturaTelefono"));

            JSONArray PedidoDetalleArray = mObj.optJSONArray("PedidoDetalle");
            if(PedidoDetalleArray!=null){
                ArrayList<MyOrderDetails> alOrderDetails = new ArrayList<>();
                for(int j=0;j<PedidoDetalleArray.length();j++){
                    JSONObject orderDetails = PedidoDetalleArray.optJSONObject(j);
                    MyOrderDetails myOrderDetails = new MyOrderDetails();
                    myOrderDetails.setIdPedidoDetalle(orderDetails.optInt("IdPedidoDetalle"));
                    myOrderDetails.setIdPedido(orderDetails.optInt("IdPedido"));
                    myOrderDetails.setIdRestaProducto(orderDetails.optInt("IdRestaProducto"));
                    myOrderDetails.setNombreProducto(orderDetails.optString("NombreProducto"));
                    myOrderDetails.setObservacion(orderDetails.optString("Observacion"));
                    myOrderDetails.setPrecio(orderDetails.optDouble("Precio"));
                    myOrderDetails.setValor(orderDetails.optString("Valor"));
                    myOrderDetails.setCantidad(orderDetails.optInt("Cantidad"));

                    alOrderDetails.add(myOrderDetails);
                }

                myOrderBean.setAlOrderDetails(alOrderDetails);
            }

            alOrder.add(myOrderBean);

        }
        myOrderAdapter = new MyOrderAdapter(context,alOrder);
        myorder_recycleView.setAdapter(myOrderAdapter);
    }

    private void getMyOrder(){
        String url = API.myOrder+"pIdCliente="+ SessionManager.getUserId(context)+"&callback=";

        showProgressBar();

        GlobalValues.getMethodManagerObj(context).makeStringReqWithoutCache(url, apiRequest, new MethodListener() {
            @Override
            public void onError() {
                hideProgressbar();
            }

            @Override
            public void onError(String response) {
                hideProgressbar();
            }

            @Override
            public void onSuccess(String response) {
                hideProgressbar();
                try{
                    //response = response.substring(1,response.length()-1);
                    JSONObject mobj = new JSONObject(response);
                    JSONArray mData = mobj.optJSONArray("data");
                    if(mData!=null){
                        readyToOrderList(mData);
                    }
                }catch (JSONException e){
                    Log.e(TAG,""+e.toString());
                }catch (Exception e){
                    Log.e(TAG,""+e.toString());
                }
            }
        });
    }
}
