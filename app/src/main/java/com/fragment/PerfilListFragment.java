package com.fragment;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.activity.AboutUSActivity;
import com.activity.DashBoardActivity;
import com.activity.TermsConditionActivity;
import com.app.grupodeliveryec.deliveryEC.R;
import com.squareup.picasso.Picasso;
import com.utility.FragmentIDs;
import com.utility.SessionManager;
import com.view.BebaseNeueTextView;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by administrator on 8/2/17.
 */

public class PerfilListFragment extends Fragment {

    protected static Context context;
    protected static Fragment fragment;
    protected static FragmentManager fragmentManager;
    View view;
    BebaseNeueTextView user_name;
    ListView perfile_listview;
    CircleImageView roundedImageView;
    Picasso picasso;
    BebaseNeueTextView restaurant;

    /*String[] perfileList = new String[]{
            "Datos Basicos","Dirección De Entrega","Dirección De Facturación",
            "Contrasena","Dispositivos","Mis Pedidos","Terminos Y Condiciones","Politica De Datos"
    };*/
    String[] perfileList = new String[]{
            "Datos Básicos", "Dirección de Entrega", "Dirección de Facturación", "Contraseña", "Dispositivos", "Mis Pedidos", "Términos y Condiciones", "Política de Datos"};


    public static Fragment getInstance(Context mContext, FragmentManager fragmentManager, Bundle bundle) {
        fragment = new PerfilListFragment();
        context = mContext;
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.perfil_list, container, false);
        picasso = Picasso.with(getActivity());
        DashBoardActivity.hideCerrar();
        DashBoardActivity.hideSearchButton();
        DashBoardActivity.hideInfo();
        DashBoardActivity.showBackButton();
        DashBoardActivity.updateTitle(getResources().getString(R.string.PERFIL).toUpperCase());
        DashBoardActivity.hideHorizentalBar();
        DashBoardActivity.hideAddDeliveryAddress();
        DashBoardActivity.showLogout();
        DashBoardActivity.hideMenuPanel();
        DashBoardActivity.hideUserPerfilIcon();

        perfile_listview = (ListView) view.findViewById(R.id.perfile_listview);
        user_name = (BebaseNeueTextView) view.findViewById(R.id.user_name);
        roundedImageView = (CircleImageView) view.findViewById(R.id.perfil_list_UserProfile);
        restaurant = (BebaseNeueTextView)  view.findViewById(R.id.restaurants);



        if (SessionManager.get_username(context).equals("")){
            roundedImageView.setImageDrawable(getResources().getDrawable(R.drawable.user_profile_default));
            user_name.setText("nombre" );
        }else {

           if(!SessionManager.get_fb_image(context).equals("")) {
               picasso.load(SessionManager.get_fb_image(context))
                       .into(roundedImageView);
           }
            user_name.setText("" + SessionManager.get_username(context));
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, R.layout.perfile_list_item, R.id.perfile_list_tv, perfileList);

        perfile_listview.setAdapter(adapter);
        perfile_listview.setOnItemClickListener(mOnItemClickListener);

        restaurant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle   bundle = new Bundle();
                DashBoardActivity.displayFragment(FragmentIDs.ResturentSearchResultFragment_Id, bundle);
                DashBoardActivity.removePopupBackstack(FragmentIDs.PerfilListFragment_Tag);

            }
        });

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public void showLogoutDialog(final FragmentManager framanger) {

        final Dialog dialog = new Dialog(context);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.logout_xml);

        TextView logout_no = (TextView) dialog.findViewById(R.id.logout_no);
        TextView logout_yes = (TextView) dialog.findViewById(R.id.logout_yes);

        logout_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        logout_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                SessionManager.clearLoginUser(context);
                SessionManager.save_fb_image(context , null);
                roundedImageView.setImageDrawable(getResources().getDrawable(R.drawable.user_profile_default));
                //picasso.load(SessionManager.get_fb_image(context)).error(R.drawable.user_profile_default).into(roundedImageView);
                SessionManager.save_username(context,"");
                user_name.setText("nombre" );
                framanger.popBackStack();
               /* DashBoardActivity.removePopupBackstack(FragmentIDs.PerfilListFragment_Tag);
                Bundle bundle = new Bundle();
                DashBoardActivity.displayFragment(FragmentIDs.PerfilFragment_Id, bundle);*/
            }
        });
        dialog.show();
    }

    private void switchProfile(int position) {
        Bundle bundle;
        switch (position) {
            case 0:
                /**
                 * Datos Basicos
                 */
                bundle = new Bundle();
                DashBoardActivity.displayFragment(FragmentIDs.UserProfileFragment_Id, bundle);
                break;
            case 1:
                /**
                 * Direccion de Entrega
                 */

                bundle = new Bundle();
                DashBoardActivity.displayFragment(FragmentIDs.DeliveryAddressListFragment_Id, bundle);

                break;
            case 2:
                /**
                 * Direccion de Facturacion
                 */
                bundle = new Bundle();
                DashBoardActivity.displayFragment(FragmentIDs.BillingAddressListFragment_Id, bundle);

                break;
            case 3:
                /**
                 * Contrasena
                 */
                bundle = new Bundle();
                DashBoardActivity.displayFragment(FragmentIDs.ChangePasswordFragment_Id, bundle);
                break;
            case 4:
                /**
                 * Dispositivos
                 */
                bundle = new Bundle();
                DashBoardActivity.displayFragment(FragmentIDs.DispositivosListFragment_Id, bundle);
                break;
            case 5:
                /**
                 * Mis Pedidos
                 */
                bundle = new Bundle();
                DashBoardActivity.displayFragment(FragmentIDs.MyOrdersFragment_Id, bundle);
                break;

            case 6:
                /**
                 *Terminos Y Condiciones
                 */
                try {
                    Intent I = new Intent(context, TermsConditionActivity.class);
                    I.putExtra("url", "http://deliveryec.azurewebsites.net/Nosotros/Terminos.html");
                    //I.putExtra("headingTitle","");
                    startActivity(I);
                } catch (Exception e) {
                    Log.e("asdf", "" + e.toString());
                }
                break;

            case 7:
                /**
                 *Politica De Datos
                 */
                try {
                    Intent I = new Intent(context, AboutUSActivity.class);
                    I.putExtra("url", "http://deliveryec.azurewebsites.net/Nosotros/Politica.html");
                    // I.putExtra("headingTitle","");
                    startActivity(I);
                } catch (Exception e) {
                    Log.e("asdf", "" + e.toString());
                }
                break;
        }
    }

    AdapterView.OnItemClickListener mOnItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            switchProfile(i);
        }
    };
}
