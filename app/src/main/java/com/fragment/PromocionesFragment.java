package com.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.activity.DashBoardActivity;
import com.adapter.PromocionesAdapter;
import com.app.grupodeliveryec.deliveryEC.R;
import com.bean.PromocionesBean;
import com.com.rest.API;
import com.com.rest.GlobalValues;
import com.com.rest.MethodListener;
import com.utility.DividerItemDecoration;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Vector;

/**
 * Created by administrator on 15/4/17.
 */

public class PromocionesFragment extends Fragment{

    protected static Context context;
    protected static Fragment fragment;
    protected static FragmentManager fragmentManager;
    protected static Bundle b;
    String TAG = getClass().getName();
    View view;

    ImageView promociones_progressBar;
    RecyclerView promociones_recycleView;

    Vector<PromocionesBean> alPromociones;

    PromocionesAdapter promocionesAdapter;
    int idSector;

    Animation animation;

    public static Fragment getInstance(Context mContext, FragmentManager fragmentManager, Bundle bundle){
        fragment = new PromocionesFragment();
        context = mContext;
        b = bundle;
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.promociones_xml,container,false);

        idSector = b.getInt("idSector");

        DashBoardActivity.updateTitle(getResources().getString(R.string.DeliveryEC));
        DashBoardActivity.hideSearchButton();
        DashBoardActivity.showBackButton();
        DashBoardActivity.hideCerrar();
        DashBoardActivity.hideInfo();
        DashBoardActivity.showHorizentalBar();
        DashBoardActivity.hideLogout();
        DashBoardActivity.hideAddDeliveryAddress();
        DashBoardActivity.hideMenuPanel();
        DashBoardActivity.hideUserPerfilIcon();

        promociones_progressBar = (ImageView)view.findViewById(R.id.promociones_progressBar);
        promociones_recycleView = (RecyclerView)view.findViewById(R.id.promociones_recycleView);

        promociones_recycleView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
        promociones_recycleView.setLayoutManager(mLayoutManager);
        //promociones_recycleView.addItemDecoration(new DividerItemDecoration(context, LinearLayoutManager.VERTICAL));
        promociones_recycleView.setItemAnimator(new DefaultItemAnimator());

        getPromociones();

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void showProgressBar(){
        promociones_progressBar.setVisibility(View.VISIBLE);
        animation = AnimationUtils.loadAnimation(context, R.anim.progress_anim);
        animation.setDuration(1000);
        promociones_progressBar.startAnimation(animation);
    }
    private void hideProgressbar(){
        if(animation!=null){
            promociones_progressBar.clearAnimation();
            promociones_progressBar.setVisibility(View.GONE);
        }
    }

    private void getPromociones(){

        showProgressBar();

        String url = API.ListaPromociones+"idsector="+idSector+"&callback=";
        GlobalValues.getMethodManagerObj(context).makeStringReq(url, "apiListPromociones", new MethodListener() {
            @Override
            public void onError() {
                hideProgressbar();
            }

            @Override
            public void onError(String response) {
                Log.v(TAG,""+response);
                hideProgressbar();
            }

            @Override
            public void onSuccess(String response) {
                hideProgressbar();
                try {
                    response = response.substring(1,response.length()-1);
                    JSONObject mObj = new JSONObject(response);
                    JSONArray mArray = mObj.optJSONArray("data");

                    alPromociones = new Vector<PromocionesBean>();

                    if(mArray!=null){
                        for(int i=0;i<mArray.length();i++){
                            JSONObject object = mArray.optJSONObject(i);
                            PromocionesBean pb = new PromocionesBean();
                            pb.setIdRestaPromocion(object.optLong("IdRestaPromocion"));
                            pb.setIdRestaurante(object.optLong("IdRestaurante"));
                            pb.setIdCiudad(object.optLong("IdCiudad"));
                            pb.setIdSector(object.optLong("IdSector"));
                            pb.setNombre(object.optString("Nombre"));
                            pb.setPublicidad(object.optString("Publicidad"));
                            pb.setImagen(object.optString("Imagen"));
                            pb.setNombreRestaurante(object.optString("NombreRestaurante"));

                            alPromociones.add(pb);
                        }

                        promocionesAdapter = new PromocionesAdapter(context,alPromociones);
                        promociones_recycleView.setAdapter(promocionesAdapter);
                    }
                }catch (JSONException e){
                    Log.e(TAG,""+e.toString());
                }catch (Exception e){
                    Log.e(TAG,""+e.toString());
                }
            }
        });
    }
}
