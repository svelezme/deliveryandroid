package com.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.activity.DashBoardActivity;
import com.app.grupodeliveryec.deliveryEC.R;
import com.bean.BillingBean;
import com.com.rest.API;
import com.com.rest.GlobalValues;
import com.com.rest.MethodListener;
import com.utility.AppController;
import com.utility.FragmentIDs;
import com.utility.KeyboadSetting;
import com.utility.MessageDialog;
import com.utility.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by administrator on 13/2/17.
 */

public class BillingAddressFragment extends Fragment{

    String TAG = getClass().getName();
    String apiRequest = "BillingAddress";
    private static Context context;
    private  FragmentManager fm;
    private static Bundle b;
    protected static Fragment fragment;
    View view;
    MessageDialog.okOnClickListener okOnClickListener;
    MessageDialog messageDialog;
    EditText billing_descripcion,billing_socialAddress;
    EditText billing_cedula,billing_direccion;
    EditText billing_telefono;
    LinearLayout billing_termsconditionsLayout;
    LinearLayout billing_update,billing_cancel;
    ImageView resturent_progressBar;
    Animation animation;
    BillingBean billingBean;

    public static Fragment getInstance(Context mContext, FragmentManager fragmentManager, Bundle bundle){
        fragment = new BillingAddressFragment();
        b = bundle;
        context = mContext;
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        DashBoardActivity.hideAddDeliveryAddress();
        DashBoardActivity.showBackButton();
        DashBoardActivity.hideInfo();
        DashBoardActivity.hideLogout();
        DashBoardActivity.hideSearchButton();
        DashBoardActivity.hideCerrar();
        DashBoardActivity.hideMenuPanel();

        DashBoardActivity.updateTitle(getResources().getString(R.string.direccion_de_facturacion));

        view = inflater.inflate(R.layout.billing_xml,container,false);
        billing_descripcion = (EditText)view.findViewById(R.id.billing_descripcion);
        billing_socialAddress = (EditText)view.findViewById(R.id.billing_socialAddress);
        billing_cedula = (EditText)view.findViewById(R.id.billing_cedula);
        billing_direccion = (EditText)view.findViewById(R.id.billing_direccion);
        billing_telefono = (EditText)view.findViewById(R.id.billing_telefono);
        billing_termsconditionsLayout = (LinearLayout)view.findViewById(R.id.billing_termsconditionsLayout);
        resturent_progressBar = (ImageView)view.findViewById(R.id.resturent_progressBar);
        billing_update = (LinearLayout)view.findViewById(R.id.billing_update);
        billing_cancel = (LinearLayout)view.findViewById(R.id.billing_cancel);

        billing_termsconditionsLayout.setOnClickListener(addTermsConditions);
        billing_update.setOnClickListener(addUpdate);
        billing_cancel.setOnClickListener(addCancel);

        billingBean = (BillingBean)b.getSerializable("billingBean");

        if(b.getInt("billingType")==2 || b.getInt("billingType")==3){
            //Show the Updaet & Cancel Billing Address Button
            DashBoardActivity.showHorizentalBar();
            billing_update.setVisibility(View.VISIBLE);
            billing_cancel.setVisibility(View.VISIBLE);
            billing_termsconditionsLayout.setVisibility(View.GONE);
            if(b.getInt("billingType")==3){
                /**
                 * Get the data from List.
                 */
                setUI();
            }
        }

        if(b.getInt("billingType")==1){
            // Add a Billing Address at time of Registration
            billing_update.setVisibility(View.GONE);
            billing_cancel.setVisibility(View.GONE);
            billing_termsconditionsLayout.setVisibility(View.VISIBLE);
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                billing_descripcion.setCursorVisible(true);
                billing_descripcion.requestFocus();
                KeyboadSetting.showKeyboard(context,billing_descripcion);
            }
        },200);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        try {
            final InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
        }catch (Exception e){
            Log.e(TAG,""+e.toString());
        }
    }
    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        AppController.getInstance().cancelPendingRequests(apiRequest);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void setUI(){
        billing_descripcion.setText(billingBean.getDescripcion());
        billing_socialAddress.setText(billingBean.getRazonSocial());
        billing_cedula.setText(billingBean.getIdentificacion());
        billing_direccion.setText(billingBean.getDireccion());
        billing_telefono.setText(billingBean.getTelefono());
    }

    private void showProgressBar(){
        resturent_progressBar.setVisibility(View.VISIBLE);
        animation = AnimationUtils.loadAnimation(context, R.anim.progress_anim);
        animation.setDuration(1000);
        resturent_progressBar.startAnimation(animation);
    }
    private void hideProgressbar(){
        if(animation!=null){
            resturent_progressBar.clearAnimation();
            resturent_progressBar.setVisibility(View.GONE);
        }
    }

    private void setMessageErrorDialog(String message){
        messageDialog = new MessageDialog(context,
                android.R.style.Theme_Holo_Light_Dialog,
                message,
                new MessageDialog.okOnClickListener() {
                    @Override
                    public void onButtonClick() {
                        messageDialog.dismiss();
                    }
                });

        messageDialog.setCancelable(false);
        messageDialog.setCanceledOnTouchOutside(false);
        messageDialog.show();
    }

    private void doBillingAddres(){
        if(billing_descripcion.getText().toString().equals("")
                && billing_socialAddress.getText().toString().equals("")
                && billing_cedula.getText().toString().equals("")
                && billing_direccion.getText().toString().equals("")
                && billing_telefono.getText().toString().equals("")){

            String message = getResources().getString(R.string.empty_Debe_ingresar_descripcion)
                            +"\n"+getResources().getString(R.string.empty_Debe_ingresar_razon_social)
                            +"\n"+getResources().getString(R.string.enter_Debe_ingresar_identificacion)
                            +"\n"+getResources().getString(R.string.empty_Debe_ingresar_direccion)
                            +"\n"+getResources().getString(R.string.empty_Debe_ingresar_telefono);
            setMessageErrorDialog(message);

        }else if(billing_descripcion.getText().toString().equals("")){
            String message = getResources().getString(R.string.empty_Debe_ingresar_descripcion);
            setMessageErrorDialog(message);
        }else if(billing_socialAddress.getText().toString().equals("")){
            String message = getResources().getString(R.string.empty_Debe_ingresar_razon_social);
            setMessageErrorDialog(message);
        }else if(billing_cedula.getText().toString().equals("")){
            String message = getResources().getString(R.string.enter_Debe_ingresar_identificacion);
            setMessageErrorDialog(message);
        }else if(billing_direccion.getText().toString().equals("")){
            String message = getResources().getString(R.string.empty_Debe_ingresar_direccion);
            setMessageErrorDialog(message);
        }else if(billing_telefono.getText().toString().equals("")){
            String message = getResources().getString(R.string.empty_Debe_ingresar_telefono);
            setMessageErrorDialog(message);
        }else{
            /**
             * Call the Terms & Conditions WS
             */
            String pDescripcion = billing_descripcion.getText().toString().replace(" ","%20");
            String pRazonSocial = billing_socialAddress.getText().toString().replace(" ","%20");
            String pIdentificacion = billing_cedula.getText().toString().replace(" ","%20");
            String pDireccion = billing_direccion.getText().toString().replace(" ","%20");
            String pTelefono = billing_telefono.getText().toString().replace(" ","%20");

            String url = null;

            if(b.getInt("billingType")==3){
                // Update the billing Address details
                url = API.updateBillingAddress+"pIdDirecciones="+billingBean.getIdDireccionFactura()
                        +"&pIdCliente="+SessionManager.getUserId(context)
                        +"&pRazonSocial="+pRazonSocial
                        +"&pIdentificacion="+pIdentificacion
                        +"&pDescripcion="+pDescripcion
                        +"&pDireccion="+pDireccion
                        +"&pTelefono="+pTelefono
                        +"&callback=";
            }

            if(b.getInt("billingType")==2 || b.getInt("billingType")==1){
                // Add the billing Address
                url = API.billingAddress+"pIdCliente="+ SessionManager.getUserId(context)
                        +"&pRazonSocial="+pRazonSocial
                        +"&pIdentificacion="+pIdentificacion
                        +"&pDescripcion="+pDescripcion
                        +"&pDireccion="+pDireccion
                        +"&pTelefono="+pTelefono
                        +"&callback=";
            }

            //Log.e(TAG,""+url);
            showProgressBar();

            GlobalValues.getMethodManagerObj(context).makeStringReqWithoutCache(url, apiRequest, new MethodListener() {
                @Override
                public void onError() {
                    hideProgressbar();
                }

                @Override
                public void onError(String response) {
                    hideProgressbar();
                }

                @Override
                public void onSuccess(String response) {
                    hideProgressbar();
                    //Log.e("response",""+response);
                    try{
                        response = response.substring(1,response.length()-1);
                        JSONObject mObj = new JSONObject(response);
                        JSONArray mArray = mObj.optJSONArray("data");
                        if(mArray!=null){
                            for(int i=0;i<mArray.length();i++){
                                JSONObject object = mArray.optJSONObject(i);
                                if(object.has("respuesta")){
                                    if(object.optString("respuesta").equals("OK")){


                                        if(b.getInt("billingType")==2 || b.getInt("billingType")==3){
                                            //Back to the Billing Address List
                                            DashBoardActivity.popBackStack();
                                        }


                                        if(b.getInt("billingType")==1){
                                            //Go to the user Dashboard
                                            setMessageErrorDialog(getResources().getString(R.string.registration_complete));
                                            Bundle bundle = new Bundle();
                                            DashBoardActivity.displayFragment(FragmentIDs.PerfilListFragment_Id,bundle);
                                        }



                                    }else{
                                        setMessageErrorDialog(object.optString("Error"));
                                    }
                                }
                            }
                        }
                    }catch (JSONException e){
                        Log.e(TAG,""+e.toString());
                    }
                }
            });

            /**
             *
             https://deliveryec.azurewebsites.net/ServiciosWeb/WebRestauranteLogin.asmx/
             RegistraClienteDireccionFactura?
             pIdCliente=82190
             &pRazonSocial=JV Complex
             &pIdentificacion= JV Complex
             &pDescripcion=Race cource road
             &pDireccion=Race cource road
             &pTelefono=9981939393
             &callback=9981939390
             */
        }
    }

    View.OnClickListener addTermsConditions = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            View v = ((Activity) context).getCurrentFocus();
            //View v = getActivity().getCurrentFocus();
            KeyboadSetting.hideKeyboadUsingView(v,context);
            //KeyboadSetting.hideKeyboard(context,register_surname);
            doBillingAddres();
        }
    };

    View.OnClickListener addUpdate = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            View v = ((Activity) context).getCurrentFocus();
            //View v = getActivity().getCurrentFocus();
            KeyboadSetting.hideKeyboadUsingView(v,context);
            //KeyboadSetting.hideKeyboard(context,register_surname);

            doBillingAddres();
        }
    };

    View.OnClickListener addCancel = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            View v = ((Activity) context).getCurrentFocus();
            //View v = getActivity().getCurrentFocus();
            KeyboadSetting.hideKeyboadUsingView(v,context);
            //KeyboadSetting.hideKeyboard(context,register_surname);
            DashBoardActivity.popBackStack();
        }
    };
}
