package com.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.activity.DashBoardActivity;
import com.adapter.DeviceListAdapter;
import com.app.grupodeliveryec.deliveryEC.R;
import com.bean.DeviceBean;
import com.com.rest.API;
import com.com.rest.GlobalValues;
import com.com.rest.MethodListener;
import com.utility.AppController;
import com.utility.DividerItemDecoration;
import com.utility.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Vector;

/**
 * Created by administrator on 16/2/17.
 */

public class DispositivosListFragment extends Fragment {

    String TAG = getClass().getName();
    String apiDeviceList = "apiDeviceList";
    View view;
    private static Context context;
    private static FragmentManager fm;
    private static Bundle bundle;
    private static Fragment fragment;

    RecyclerView dispositivos_recycleView;
    ImageView resturent_progressBar;
    Animation animation;

    Vector<DeviceBean> alDevice;
    DeviceListAdapter deviceListAdapter;

    public static Fragment getInstance(Context mContext, FragmentManager fragmentManager, Bundle bundle) {
        fragment = new DispositivosListFragment();
        context = mContext;
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        DashBoardActivity.updateTitle(getResources().getString(R.string.dispositivos));
        DashBoardActivity.hideCerrar();
        DashBoardActivity.hideLogout();
        DashBoardActivity.hideInfo();
        DashBoardActivity.hideSearchButton();
        DashBoardActivity.showBackButton();
        DashBoardActivity.showHorizentalBar();
        DashBoardActivity.hideMenuPanel();

        view = inflater.inflate(R.layout.dispositivos_xml, container, false);
        dispositivos_recycleView = (RecyclerView) view.findViewById(R.id.dispositivos_recycleView);
        resturent_progressBar = (ImageView) view.findViewById(R.id.resturent_progressBar);

        dispositivos_recycleView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
        dispositivos_recycleView.setLayoutManager(mLayoutManager);
        dispositivos_recycleView.addItemDecoration(new DividerItemDecoration(context, LinearLayoutManager.VERTICAL));
        dispositivos_recycleView.setItemAnimator(new DefaultItemAnimator());

        getDeviceList();
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        AppController.getInstance().cancelPendingRequests(apiDeviceList);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void showProgressBar() {
        resturent_progressBar.setVisibility(View.VISIBLE);
        animation = AnimationUtils.loadAnimation(context, R.anim.progress_anim);
        animation.setDuration(1000);
        resturent_progressBar.startAnimation(animation);
    }

    private void hideProgressbar() {
        if (animation != null) {
            resturent_progressBar.clearAnimation();
            resturent_progressBar.setVisibility(View.GONE);
        }
    }

    private void getDeviceList() {
        String url = API.deviceList + "pIdCliente=" + SessionManager.getUserId(context) + "&callback=";

        showProgressBar();

        GlobalValues.getMethodManagerObj(context).makeStringReq(url, apiDeviceList, new MethodListener() {
            @Override
            public void onError() {
                hideProgressbar();
            }

            @Override
            public void onError(String response) {
                hideProgressbar();
                Log.e(TAG, "" + response);
            }

            @Override
            public void onSuccess(String response) {
                hideProgressbar();
                try {
                    response = response.substring(1, response.length() - 1);
                    JSONObject object = new JSONObject(response);
                    JSONArray dArray = object.optJSONArray("data");
                    if (dArray != null) {
                        alDevice = new Vector<DeviceBean>();
                        for (int i = 0; i < dArray.length(); i++) {
                            JSONObject mObj = dArray.optJSONObject(i);

                            if (mObj.has("IdUUID")) {
                                if (!mObj.optString("IdUUID").equals("anonymous")) {
                                    DeviceBean deviceBean = new DeviceBean();
                                    deviceBean.setIdClienteDispositivo(mObj.optInt("IdClienteDispositivo"));
                                    deviceBean.setIdCliente(mObj.optInt("IdCliente"));
                                    deviceBean.setNombreDispositivo(mObj.optString("NombreDispositivo"));
                                    deviceBean.setTipoCelular(mObj.optString("TipoCelular"));
                                    deviceBean.setIdUUID(mObj.optString("IdUUID"));
                                    deviceBean.setFechaRegistro(mObj.optString("FechaRegistro"));

                                    alDevice.add(deviceBean);
                                }
                            }
                        }

                        deviceListAdapter = new DeviceListAdapter(context, alDevice);
                        dispositivos_recycleView.setAdapter(deviceListAdapter);
                    }
                } catch (JSONException e) {
                    Log.e(TAG, "" + e.toString());
                } catch (Exception e) {
                    Log.e(TAG, "" + e.toString());
                }
            }
        });
        //ServiciosWeb/WebRestauranteLogin.asmx/ListarClienteDispositivos?pIdCliente=10004&callback=
    }
}
