package com.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import com.activity.DashBoardActivity;
import com.adapter.BillingAddressListAdapter;
import com.app.grupodeliveryec.deliveryEC.R;
import com.bean.BillingBean;
import com.com.rest.API;
import com.com.rest.GlobalValues;
import com.com.rest.MethodListener;
import com.utility.AppController;
import com.utility.DividerItemDecoration;
import com.utility.SessionManager;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.Vector;

/**
 * Created by administrator on 15/2/17.
 */

public class BillingAddressListFragment extends Fragment{

    String TAG = getClass().getName();
    String apiRequest = "apiBillingList";
    protected static Context context;
    protected static FragmentManager fragmentManager;
    protected static Bundle b;
    protected static Fragment fragment;
    View view;
    Animation animation;
    ImageView resturent_progressBar;
    private RecyclerView address_recycleView;
    BillingAddressListAdapter billingAddressListAdapter;
    Vector<BillingBean> alBilling;

    public static Fragment getInstance(Context mContext, FragmentManager fragmentManager, Bundle bundle){
        fragment = new BillingAddressListFragment();
        b = bundle;
        context = mContext;
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        DashBoardActivity.updateTitle(getResources().getString(R.string.direccion_de_facturacion));
        DashBoardActivity.hideLogout();
        DashBoardActivity.showBackButton();
        DashBoardActivity.hideInfo();
        DashBoardActivity.hideSearchButton();
        DashBoardActivity.hideCerrar();
        DashBoardActivity.showAddDeliveryAddress();
        DashBoardActivity.showHorizentalBar();
        DashBoardActivity.hideMenuPanel();

        view = inflater.inflate(R.layout.delivery_address_list,container,false);
        resturent_progressBar = (ImageView)view.findViewById(R.id.resturent_progressBar);
        address_recycleView = (RecyclerView)view.findViewById(R.id.address_recycleView);

        address_recycleView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
        address_recycleView.setLayoutManager(mLayoutManager);
        address_recycleView.addItemDecoration(new DividerItemDecoration(context, LinearLayoutManager.VERTICAL));
        address_recycleView.setItemAnimator(new DefaultItemAnimator());

        getBillingAddress();

       /* address_recycleView.addOnItemTouchListener(new RecyclerTouchListener(context, address_recycleView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                BillingBean billingBean = alBilling.get(position);
                Bundle b = new Bundle();
                b.putInt("billingType",Config.updateListBillingType);
                b.putSerializable("billingBean",billingBean);
                DashBoardActivity.displayFragment(FragmentIDs.BillingAddressFragment_Id,b);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
*/
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        AppController.getInstance().cancelPendingRequests(apiRequest);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void showProgressBar(){
        resturent_progressBar.setVisibility(View.VISIBLE);
        animation = AnimationUtils.loadAnimation(context, R.anim.progress_anim);
        animation.setDuration(1000);
        resturent_progressBar.startAnimation(animation);
    }
    private void hideProgressbar(){
        if(animation!=null){
            resturent_progressBar.clearAnimation();
            resturent_progressBar.setVisibility(View.GONE);
        }
    }

    private void getBillingAddress(){
        //https://deliveryec.azurewebsites.net/ServiciosWeb/WebRestauranteLogin.asmx/ListarClienteDireccionFactura?pIdCliente=10004&callback=
//        String url = API.billingAddressList+"pIdCliente="+ SessionManager.getUserId(context)+"&callback=";

        String url = API.new_billing_address_url+"id_cliente="+ SessionManager.getUserId(context)+"&callback=";

        showProgressBar();

        GlobalValues.getMethodManagerObj(context).makeStringReqWithoutCache(url, apiRequest, new MethodListener() {
            @Override
            public void onError() {
                hideProgressbar();
            }

            @Override
            public void onError(String response) {
                hideProgressbar();
            }

            @Override
            public void onSuccess(String response) {
                hideProgressbar();
                try{
                   // response = response.substring(1,response.length()-1);
                    JSONObject object = new JSONObject(response);
                    JSONArray mArray = object.optJSONArray("data");
                    if(mArray!=null){

                        alBilling = new Vector<BillingBean>();

                        for(int i=0;i<mArray.length();i++){
                            JSONObject mObj = mArray.optJSONObject(i);
                            BillingBean billingBean = new BillingBean();
                            billingBean.setIdDireccionFactura(mObj.optInt("IdDireccionFactura"));
                            billingBean.setIdCliente(mObj.optInt("IdCliente"));
                            billingBean.setRazonSocial(mObj.optString("RazonSocial"));
                            billingBean.setIdentificacion(mObj.optString("Identificacion"));
                            billingBean.setDescripcion(mObj.optString("Descripcion"));
                            billingBean.setDireccion(mObj.optString("Direccion"));
                            billingBean.setTelefono(mObj.optString("Telefono"));
                            billingBean.setNombrePerfil(mObj.optString("NombrePerfil"));

                            alBilling.add(billingBean);
                        }

                        billingAddressListAdapter = new BillingAddressListAdapter(context,alBilling);
                        address_recycleView.setAdapter(billingAddressListAdapter);
                    }
                }catch (JSONException e){
                    Log.e(TAG,""+e.toString());
                }catch (Exception e){
                    Log.e(TAG,""+e.toString());
                }
            }
        });
    }
}
