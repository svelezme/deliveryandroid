package com.receiver;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.utility.SessionManager;

import java.util.List;

/**
 * Created by administrator on 10/7/17.
 */

public class ThirtyMinutesReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        // Toast.makeText(context, "start alarm.", Toast.LENGTH_SHORT).show();
        if (isAppOnForeground(context) == false) {
            SessionManager.clearLoginUser(context);
            SessionManager.save_fb_image(context, "");
            //Toast.makeText(context, "i am calling.", Toast.LENGTH_SHORT).show();
        }
    }

    private boolean isAppOnForeground(Context context) {
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> appProcesses = activityManager.getRunningAppProcesses();
        if (appProcesses == null) {
            return false;
        }
        final String packageName = context.getPackageName();
        for (ActivityManager.RunningAppProcessInfo appProcess : appProcesses) {
            if (appProcess.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND && appProcess.processName.equals(packageName)) {
                return true;
            }
        }
        return false;
    }
}
