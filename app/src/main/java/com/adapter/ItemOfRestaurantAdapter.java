package com.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Toast;

import com.app.grupodeliveryec.deliveryEC.R;
import com.github.siyamed.shapeimageview.RoundedImageView;
import com.model.RestaurentMenuItem;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.view.ExoBoldTextView;
import com.view.ExoSemiBoldTextView;
import com.view.GothamBookTextView;
import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 * Created by administrator on 1/2/17.
 */

public class ItemOfRestaurantAdapter extends BaseAdapter {

    String TAG = getClass().getName();
    Context mContext;
    ArrayList<RestaurentMenuItem> alItem;
    int height;
    String ImagenLocal;
    String NombreRestaurante;
    String NombreLocal;
    String TipoComida;
    FragmentManager fm;

    public ItemOfRestaurantAdapter(Context mContext, ArrayList<RestaurentMenuItem> alItem, int height, String ImagenLocal, String NombreRestaurante, String NombreLocal, String TipoComida, FragmentManager fm) {
        this.mContext = mContext;
        this.alItem = alItem;
        this.height = height;
        this.ImagenLocal = ImagenLocal;
        this.NombreRestaurante = NombreRestaurante;
        this.NombreLocal = NombreLocal;
        this.TipoComida = TipoComida;
        this.fm = fm;
    }

    @Override
    public int getCount() {
        return alItem.size();
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        final ViewHolder viewHolder;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.item_of_resturent_xml, viewGroup, false);
            viewHolder = new ViewHolder();
            viewHolder.itemOfRest_Nombre = (ExoSemiBoldTextView) view.findViewById(R.id.itemOfRest_Nombre);
            viewHolder.itemOfRest_ImagenRestaProducto = (RoundedImageView) view.findViewById(R.id.itemOfRest_ImagenRestaProducto);
            viewHolder.itemOfRest_Descripcion = (GothamBookTextView) view.findViewById(R.id.itemOfRest_Descripcion);
            viewHolder.itemOfRest_Descripcion2 = (GothamBookTextView) view.findViewById(R.id.itemOfRest_Descripcion2);
            viewHolder.itemOfRest_price = (ExoBoldTextView) view.findViewById(R.id.itemOfRest_price);

            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        if (alItem.get(i).getImagenRestaProducto().equals("") || alItem.get(i).getImagenRestaProducto()==null){
            //viewHolder.itemOfRest_ImagenRestaProducto.setVisibility(View.GONE);
            viewHolder.itemOfRest_ImagenRestaProducto.setVisibility(View.GONE);
        }else {
           // viewHolder.itemOfRest_ImagenRestaProducto.setVisibility(View.VISIBLE);

            if((alItem.get(i).getImagenRestaProducto().contains("png")||
                    alItem.get(i).getImagenRestaProducto().contains("jpg")||
                    alItem.get(i).getImagenRestaProducto().contains("jpeg")) && alItem.get(i).isImageValid()){

                viewHolder.itemOfRest_ImagenRestaProducto.setVisibility(View.VISIBLE);

                Picasso.with(mContext).
                        load("http://www.deliveryec.com/images/app/" + alItem.get(i).getImagenRestaProducto()).

                        into(viewHolder.itemOfRest_ImagenRestaProducto, new Callback() {
                            @Override
                            public void onSuccess() {
                                viewHolder.itemOfRest_ImagenRestaProducto.setVisibility(View.VISIBLE);
                                RestaurentMenuItem restaurentMenuItem =alItem.get(i);
                                restaurentMenuItem.setImageValid(true);
                                alItem.set(i,restaurentMenuItem);
                        /*viewHolder.itemOfRest_ImagenRestaProducto.setVisibility(View.VISIBLE);
                        viewHolder.itemOfRest_Descripcion2.setVisibility(View.GONE);
                        viewHolder.itemOfRest_Descripcion.setVisibility(View.VISIBLE);*/
                            }

                            @Override
                            public void onError() {
                                //Toast.makeText(mContext,"Error",Toast.LENGTH_LONG).show();
                                RestaurentMenuItem restaurentMenuItem =alItem.get(i);
                                restaurentMenuItem.setImageValid(false);
                                alItem.set(i,restaurentMenuItem);
                                viewHolder.itemOfRest_ImagenRestaProducto.setVisibility(View.GONE);
                       /* viewHolder.itemOfRest_ImagenRestaProducto.setVisibility(View.GONE);
                        viewHolder.itemOfRest_Descripcion2.setVisibility(View.VISIBLE);
                        viewHolder.itemOfRest_Descripcion.setVisibility(View.INVISIBLE);*/
                            }
                        });
            }else{
                viewHolder.itemOfRest_ImagenRestaProducto.setVisibility(View.GONE);
            }
        }





        viewHolder.itemOfRest_Nombre.setText(alItem.get(i).getNombre());
        viewHolder.itemOfRest_Descripcion.setText(alItem.get(i).getDescripcion());
        //viewHolder.itemOfRest_Descripcion2.setText(alItem.get(i).getDescripcion());

        try {
            viewHolder.itemOfRest_price.setText("$ " + new DecimalFormat("0.00").format(alItem.get(i).getPrecio()));
        }catch (Exception e){
            Log.e("pr",""+e);
        }

//        viewHolder.itemOfRest_price.setText("$ " + alItem.get(i).getPrecio());


        return view;
    }

    static class ViewHolder {
        ExoSemiBoldTextView itemOfRest_Nombre;
        RoundedImageView itemOfRest_ImagenRestaProducto;
        GothamBookTextView itemOfRest_Descripcion;
        ExoBoldTextView itemOfRest_price;
        GothamBookTextView itemOfRest_Descripcion2;
    }
}