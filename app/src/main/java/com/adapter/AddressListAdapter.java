package com.adapter;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.activity.DashBoardActivity;
import com.app.grupodeliveryec.deliveryEC.R;
import com.bean.AddressListBean;
import com.com.rest.GlobalValues;
import com.com.rest.MethodListener;
import com.utility.Config;
import com.utility.FragmentIDs;
import com.utility.SessionManager;
import java.util.Vector;
/**
 * Created by administrator on 14/2/17.
 */

public class AddressListAdapter extends RecyclerView.Adapter<AddressListAdapter.MyViewHolder> implements View.OnClickListener{

    Context mContext;
    Vector<AddressListBean> alList;
    public static int deletestatus = 0;

    public AddressListAdapter(Context mContext, Vector<AddressListBean> alList){
        this.mContext = mContext;
        this.alList = alList;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public com.view.ExoSemiBoldTextView delivery_list_Descripcion, delivery_list_NombrePerfil;
        public com.view.ExoSemiBoldTextView delivery_list_Tipo;
        public com.view.ExoSemiBoldTextView delivery_list_NombreCiudad_NombreSector;
        public com.view.ExoSemiBoldTextView delivery_list_Direccion,delivery_list_Direccion2;
        public com.view.ExoSemiBoldTextView delivery_list_Telefono,delivery_list_Celular;
        public com.view.ExoSemiBoldTextView delivery_list_Referencia;
        public ImageView delete;
        public LinearLayout item;

        public MyViewHolder(final View view) {
            super(view);
            delivery_list_Descripcion = (com.view.ExoSemiBoldTextView) view.findViewById(R.id.delivery_list_Descripcion);
            delivery_list_NombrePerfil = (com.view.ExoSemiBoldTextView) view.findViewById(R.id.delivery_list_NombrePerfil);
            delivery_list_Tipo = (com.view.ExoSemiBoldTextView) view.findViewById(R.id.delivery_list_Tipo);
            delivery_list_NombreCiudad_NombreSector = (com.view.ExoSemiBoldTextView)view.findViewById(R.id.delivery_list_NombreCiudad_NombreSector);
            delivery_list_Direccion = (com.view.ExoSemiBoldTextView)view.findViewById(R.id.delivery_list_Direccion);
            delivery_list_Direccion2 = (com.view.ExoSemiBoldTextView)view.findViewById(R.id.delivery_list_Direccion2);
            delivery_list_Telefono = (com.view.ExoSemiBoldTextView)view.findViewById(R.id.delivery_list_Telefono);
            delivery_list_Celular = (com.view.ExoSemiBoldTextView)view.findViewById(R.id.delivery_list_Celular);
            delivery_list_Referencia = (com.view.ExoSemiBoldTextView)view.findViewById(R.id.delivery_list_Referencia);
            item = (LinearLayout) view.findViewById(R.id.item);
            delete = (ImageView) view.findViewById(R.id.delted);
        }
    }
    @Override
    public void onClick(View v) {
    }
    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public int getItemCount() {
        return alList.size();
    }

    @Override
    public int getItemViewType(int position) {
        super.getItemViewType(position);
        return position;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.delivery_address_item, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder,final int position) {
        AddressListBean addressListBean = alList.get(position);

        holder.delivery_list_Descripcion.setText(addressListBean.getDescripcion());
        holder.delivery_list_NombrePerfil.setText(addressListBean.getNombrePerfil());
        holder.delivery_list_Tipo.setText(addressListBean.getTipo());
        String str =  addressListBean.getNombreCiudad()+" - " +addressListBean.getNombreSector();

        //holder.delivery_list_NombreCiudad_NombreSector.setText(Html.fromHtml(str));

        /*try {
            byte[] strByte = str.getBytes("UTF-8");
            String myStr = new String(strByte, "UTF-8");
            holder.delivery_list_NombreCiudad_NombreSector.setText(myStr);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }*/

        holder.delivery_list_NombreCiudad_NombreSector.setText(str);

        holder.delivery_list_Direccion.setText(
                addressListBean.getDireccion()
        );

        holder.delivery_list_Direccion2.setText(addressListBean.getDireccion2());
        holder.delivery_list_Telefono.setText(
                addressListBean.getTelefono()
        );

        holder.delivery_list_Celular.setText(
                addressListBean.getCelular()
        );

        holder.delivery_list_Referencia.setText(
                addressListBean.getReferencia()
        );

        holder.item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddressListBean addressListBean = alList.get(position);
                Bundle b = new Bundle();
                b.putInt("addressType", Config.updateListAddressType);
                b.putSerializable("addressListBean",addressListBean);
                DashBoardActivity.displayFragment(FragmentIDs.DeliveryAddressFragment_Id,b);
            }
        });

        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                are_your_sure(position);
            }
        });

    }

    public void are_your_sure(final int position){
        final Dialog dialog = new Dialog(mContext);
        dialog.setCancelable(false);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_entrega_delete);
        dialog.show();

        TextView cancel_tv = (TextView) dialog.findViewById(R.id.cancletv);
        TextView question = (TextView) dialog.findViewById(R.id.question);

        question.setText("¿Está seguro que desea eliminar estea dirección de entrega?");
        cancel_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });

        TextView ok_tv = (TextView) dialog.findViewById(R.id.oktv);

        ok_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    delete_item(alList.get(position).getIdDirecciones() , dialog , position);

            }
        });
    }

    private void delete_item(int idDirecciones ,final Dialog dialog ,final int post){

        String url = "https://deliveryec.azurewebsites.net/ServiciosWeb/WebRestauranteLogin.asmx/EliminarClienteDireccion?pIdDirecciones="+idDirecciones+"&pIdCliente="+SessionManager.getUserId(mContext)+"&callback=";
                //String url = API.deliveryAddress+"pIdCliente="+ SessionManager.getUserId(context)+"&callback=";
        //https://deliveryec.azurewebsites.net/ServiciosWeb/WebRestauranteLogin.asmx/ListarClienteDireccion?pIdCliente=10004&callback=
        //showProgressBar();

        GlobalValues.getMethodManagerObj(mContext).makeStringReqWithoutCache(url, "delete_item", new MethodListener() {
            @Override
            public void onError() {
                //hideProgressbar();
                dialog.cancel();
            }

            @Override
            public void onError(String response) {
                //hideProgressbar();
                dialog.cancel();
            }

            @Override
            public void onSuccess(String response) {
                dialog.cancel();
                alList.remove(post);
                notifyDataSetChanged();
            }
        });
    }
}
