package com.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import com.app.grupodeliveryec.deliveryEC.R;
import com.bean.OpinionModel;
import com.model.RestaurantListModel;
import com.view.GothamBoldTextView;
import com.view.SemiBoldItalicTextView;
import java.util.ArrayList;

/**
 * Created by administrator on 23/5/17.
 */

public class Opinions_adapter extends RecyclerView.Adapter<Opinions_adapter.MyViewHolder> {

    Context mContext;
    ArrayList<OpinionModel> restaurantListModels;

    LayoutInflater mLayoutInflater;

    public Opinions_adapter(Context mContext, ArrayList<OpinionModel> restaurantListModels) {
        this.mContext = mContext;
        this.restaurantListModels = restaurantListModels;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public GothamBoldTextView name;
        public GothamBoldTextView description;
        public SemiBoldItalicTextView date;
        public RatingBar rating;

        public MyViewHolder(View view) {
            super(view);
            name = (GothamBoldTextView) view.findViewById(R.id.name);
            description = (GothamBoldTextView) view.findViewById(R.id.description);
            date = (SemiBoldItalicTextView) view.findViewById(R.id.date);
            rating = (RatingBar) view.findViewById(R.id.ratingBar1);
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.opinion_item, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public int getItemViewType(int position) {
        super.getItemViewType(position);
        return position;
    }
    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }
    @Override
    public void onBindViewHolder(Opinions_adapter.MyViewHolder holder, int position) {

        OpinionModel restaurantListModel = restaurantListModels.get(position);

        holder.name.setText(""+restaurantListModel.getRevision());
        holder.date.setText(""+restaurantListModel.getFecha_de_revision());
        holder.description.setText(""+restaurantListModel.getTx_apellidos());
        //holder.name.setText(""+restaurantListModel.getRevision());
    }

    @Override
    public int getItemCount() {
        return restaurantListModels.size();
    }
}
