package com.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.grupodeliveryec.deliveryEC.R;
import com.bean.MyOrderBean;
import com.bean.MyOrderDetails;
import com.github.siyamed.shapeimageview.mask.PorterShapeImageView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.view.BebaseNeueTextView;
import com.view.ExoBoldTextView;
import com.view.ExoSemiBoldTextView;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Vector;

/**
 * Created by administrator on 17/2/17.
 */

public class OrderItemAdapter extends RecyclerView.Adapter<OrderItemAdapter.MyViewHolder> {

    Context mContext;
    ArrayList<MyOrderDetails> alOrderDetails;

    public OrderItemAdapter(Context mContext, ArrayList<MyOrderDetails> alOrderDetails){
        this.mContext = mContext;
        this.alOrderDetails = alOrderDetails;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        ExoSemiBoldTextView NombreProducto;
        ExoBoldTextView Observacion;
        BebaseNeueTextView Precio;

        public MyViewHolder(View view) {
            super(view);
            NombreProducto = (ExoSemiBoldTextView)view.findViewById(R.id.orderitem_NombreProducto);
            Observacion = (ExoBoldTextView)view.findViewById(R.id.orderitem_Observacion);
            Precio = (BebaseNeueTextView)view.findViewById(R.id.orderitem_Precio);
        }
    }

    @Override
    public int getItemCount() {
        return alOrderDetails.size();
    }

    @Override
    public int getItemViewType(int position) {
        super.getItemViewType(position);
        return position;
    }

    @Override
    public long getItemId(int position) {
        super.getItemId(position);
        return position;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.order_item, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder viewHolder, int i) {
        viewHolder.NombreProducto.setText(alOrderDetails.get(i).getCantidad()+" * "+
                alOrderDetails.get(i).getNombreProducto());

        viewHolder.Observacion.setText(Html.fromHtml(alOrderDetails.get(i).getObservacion()));

        viewHolder.Precio.setText("$ " + new DecimalFormat("0.00").format(alOrderDetails.get(i).getPrecio()));

        //viewHolder.Precio.setText(" $ "+alOrderDetails.get(i).getPrecio());
    }
}
