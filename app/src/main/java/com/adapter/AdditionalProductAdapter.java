package com.adapter;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.app.grupodeliveryec.deliveryEC.R;
import com.bruce.pickerview.LoopView;
import com.fragment.ResturentMenuItemFragment;
import com.inputview.GothamBookEditTextView;
import com.model.ItemAdditionalProduct;
import com.utility.Config;
import com.utility.FragmentIDs;
import com.utility.MessageDialog;
import com.view.GothamBookTextView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import android.support.v7.view.ContextThemeWrapper;


/**
 * Created by administrator on 3/2/17.
 */

public class AdditionalProductAdapter extends BaseAdapter {

    String TAG = getClass().getName();
    Context mContext;
    TreeMap<String, ArrayList<ItemAdditionalProduct>> additionalProduct;
    private String[] mapKeys;
    Map<Integer, ArrayList<ItemAdditionalProduct>> selectedItem;
    private FragmentManager fm;
    ViewHolderItem viewHolderItem;
    int value;
    boolean[] isDialog;

    public AdditionalProductAdapter(Context mContext, TreeMap<String,
            ArrayList<ItemAdditionalProduct>> additionalProduct,
                                    FragmentManager fm) {
        this.mContext = mContext;
        this.additionalProduct = additionalProduct;
        this.fm = fm;
        mapKeys = additionalProduct.keySet().toArray(new String[additionalProduct.size()]);
        selectedItem = new HashMap<>();

        isDialog = new boolean[additionalProduct.size()];
        Arrays.fill(isDialog, Boolean.FALSE);
    }

    @Override
    public int getCount() {
        return additionalProduct.size();
    }

    @Override
    public ArrayList<ItemAdditionalProduct> getItem(int i) {
        return additionalProduct.get(mapKeys[i]);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }


    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {

        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.additional_product_item, viewGroup, false);

            viewHolderItem = new ViewHolderItem();

            viewHolderItem.additional_groupName = (GothamBookTextView) view.findViewById(R.id.additional_groupName);
            viewHolderItem.additional_group_item = (GothamBookEditTextView) view.findViewById(R.id.additional_group_item);
            viewHolderItem.additional_group_add = (ImageView) view.findViewById(R.id.additional_group_add);
            viewHolderItem.addToCart_addAdditional_selectedItem = (LinearLayout) view.findViewById(R.id.addToCart_addAdditional_selectedItem);

            view.setTag(viewHolderItem);

        } else {
            viewHolderItem = (ViewHolderItem) view.getTag();
        }

        viewHolderItem.additional_group_item.setTag(i);
        viewHolderItem.additional_group_item.setTag(viewHolderItem);

        viewHolderItem.additional_group_add.setTag(i);
        viewHolderItem.additional_group_add.setTag(viewHolderItem);

        final ArrayList<ItemAdditionalProduct> additionalPro = getItem(i);
        String key = mapKeys[i];

        final ArrayList<ItemAdditionalProduct> alhmItem = selectedItem.get(i);

        if (alhmItem != null) {

            viewHolderItem.addToCart_addAdditional_selectedItem.removeAllViews();

            for (int j = 0; j < alhmItem.size(); j++) {
                final LinearLayout childLayout = new LinearLayout(mContext);
                childLayout.setOrientation(LinearLayout.HORIZONTAL);
                childLayout.setPadding(5, 5, 5, 10);
                childLayout.setBackgroundColor(mContext.getResources().getColor(R.color.white));

                TextView tvGrupo = new TextView(mContext);
                TextView tvPriceo = new TextView(mContext);
                ImageButton tvDelete = new ImageButton(mContext);

                tvGrupo.setLayoutParams(new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT, 1f));

                tvPriceo.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));

                tvDelete.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));

                tvGrupo.setTextSize(14);
                tvGrupo.setPadding(5, 0, 0, 0);

                Typeface face = Typeface.createFromAsset(mContext.getAssets(), "fonts/Gotham-Book.otf");
                tvGrupo.setTypeface(face);
                tvGrupo.setTextColor(mContext.getResources().getColor(R.color.fuscous_gray));
                tvGrupo.setText(alhmItem.get(j).getNombre());

                tvPriceo.setTextSize(18);
                tvPriceo.setPadding(0, 0, 5, 0);

                Typeface face1 = Typeface.createFromAsset(mContext.getAssets(), "fonts/BebasNeue.otf");
                tvPriceo.setTypeface(face1);
                tvPriceo.setTextColor(mContext.getResources().getColor(R.color.spicy_mix));
                tvPriceo.setText("$ " + alhmItem.get(j).getPrecio().replace(",", "."));

                tvDelete.setPadding(5, 5, 5, 5);
                tvDelete.setImageResource(R.drawable.minus_icon);
                tvDelete.setId(j);
                tvDelete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        int id = view.getId();
                        //String message = mContext.getResources().getString(R.string.adiciional);
                        setMessageErrorDialog(id,alhmItem);
                    }
                });

                childLayout.addView(tvDelete, 0);
                childLayout.addView(tvPriceo, 0);
                childLayout.addView(tvGrupo, 0);


                viewHolderItem.addToCart_addAdditional_selectedItem.addView(childLayout);
            }
        }

        viewHolderItem.additional_groupName.setText(key);

        viewHolderItem.additional_group_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                isDialog[i] = false;

                final ViewHolderItem holderItem = (ViewHolderItem) view.getTag();

                final Dialog additionalProduct = new Dialog(new ContextThemeWrapper(mContext, R.style.DialogSlideAnim));
                additionalProduct.getWindow().setGravity(Gravity.BOTTOM);
                additionalProduct.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                additionalProduct.setCancelable(false);
                additionalProduct.setCanceledOnTouchOutside(false);
                additionalProduct.setContentView(R.layout.preciousadapter_citydialog_xml);
                final LoopView cityDialog_cityPicker = (LoopView) additionalProduct.findViewById(R.id.loop_view);
                final TextView cityDialog_cancel = (TextView) additionalProduct.findViewById(R.id.cityDialog_cancel);
                final TextView cityDialog_accept = (TextView) additionalProduct.findViewById(R.id.cityDialog_accept);

                cityDialog_cityPicker.setInitPosition(0);
                cityDialog_cityPicker.setCanLoop(false);

                ArrayList<String> area_list = new ArrayList<>();
                for (int i = 0; i < additionalPro.size(); i++) {
                    area_list.add(i, (additionalPro.get(i).getNombre() + "  $ " + additionalPro.get(i).getPrecio().replace(",", ".")));
                }

                cityDialog_cityPicker.setTextSize(22);
                cityDialog_cityPicker.setDataList(area_list);

                additionalProduct.show();

                cityDialog_cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        isDialog[i] = false;
                        additionalProduct.dismiss();
                    }
                });

                cityDialog_accept.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        isDialog[i] = true;

                        additionalProduct.dismiss();
                        value = cityDialog_cityPicker.getSelectedItem();
                        String dispalyItem = additionalPro.get(value).getNombre() + "  $" + additionalPro.get(value).getPrecio().replace(",", ".");
                        //Log.e("pos",""+i);
                        holderItem.additional_group_item.setText("" + dispalyItem);
                    }
                });

            }
        });

        viewHolderItem.additional_group_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Toast.makeText(mContext,""+isDialog[i]+","+i,Toast.LENGTH_LONG).show();

                if (isDialog[i]) {
                    updateLocalList(i, additionalPro);

                    /**
                     * Add the price into total amount.
                     */

                    if (fm != null) {
                        ResturentMenuItemFragment resturentMenuItemFragment =
                                (ResturentMenuItemFragment) fm.findFragmentByTag(FragmentIDs.ResturentMenuItemFragment_Tag);

                        Config.totalAditional = 0;
                        Config.alhmAditional.clear();

                        try {
                            for (Map.Entry m : selectedItem.entrySet()) {
                                ArrayList<ItemAdditionalProduct> alhm = (ArrayList<ItemAdditionalProduct>) m.getValue();
                                for (int i = 0; i < alhm.size(); i++) {
                                    Config.totalAditional = Config.totalAditional + Float.parseFloat(alhm.get(i).getPrecio().replace(",", "."));
                                    Config.alhmAditional.add(alhm.get(i));
                                }
                            }
                        } catch (NumberFormatException e) {
                            Log.e(TAG, "" + e.toString());
                        } catch (Exception e) {
                            Log.e(TAG, "" + e.toString());
                        }

                        notifyDataSetChanged();
                        resturentMenuItemFragment.notifyList();

                        resturentMenuItemFragment.addCartAdicionales();
                    }
                }
            }
        });

        return view;
    }

    static class ViewHolderItem {
        GothamBookTextView additional_groupName;
        GothamBookEditTextView additional_group_item;
        ImageView additional_group_add;
        LinearLayout addToCart_addAdditional_selectedItem;
    }


    private void updateLocalList(int position, ArrayList<ItemAdditionalProduct> additionalPro) {
        /**
         * Update the arrayList
         */

        try {
            ArrayList<ItemAdditionalProduct> alhm = new ArrayList<ItemAdditionalProduct>();

            if (selectedItem.containsKey(position)) {
                alhm = selectedItem.get(position);
                alhm.add(additionalPro.get(value));
            } else {
                alhm.add(additionalPro.get(value));
            }
            selectedItem.put(position, alhm);
        } catch (Exception e) {
            Log.e(TAG, "" + e.toString());
        }
    }


    private void setMessageErrorDialog(final int id,
                                       final ArrayList<ItemAdditionalProduct> alhmItem) {
        final Dialog cityDialog = new Dialog(mContext);
        cityDialog.getWindow().setGravity(Gravity.CENTER);
        cityDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        cityDialog.setCanceledOnTouchOutside(false);
        cityDialog.setContentView(R.layout.delete_adicional_xml);

        final TextView clear_adicional_no = (TextView) cityDialog.findViewById(R.id.clear_adicional_no);
        final TextView clear_adicional_yes = (TextView) cityDialog.findViewById(R.id.clear_adicional_yes);

        clear_adicional_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (cityDialog != null) {
                    cityDialog.dismiss();
                }
            }
        });

        clear_adicional_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (cityDialog != null) {
                    cityDialog.dismiss();
                }

                if(alhmItem.size()>0){
                    int IdRelaRestaProduAdiciona = alhmItem.get(id).getIdRelaRestaProduAdiciona();
                    alhmItem.remove(id);

                    for(int i=0;i<Config.alhmAditional.size();i++){

                        if(Config.alhmAditional.get(i).getIdRelaRestaProduAdiciona()==IdRelaRestaProduAdiciona){
                            // Log.e("size-0",""+Config.alhmAditional.size());
                            String price = Config.alhmAditional.get(i).getPrecio();
                            Config.totalAditional = Config.totalAditional - Float.parseFloat(price.replace(",", "."));
                            // Log.e("price",""+price);
                            Config.alhmAditional.remove(i);
                            break;
                            //Log.e("size-1",""+Config.alhmAditional.size());
                            // Log.e("id",""+i);
                        }

                    }
                    if (fm != null) {
                        ResturentMenuItemFragment resturentMenuItemFragment =
                                (ResturentMenuItemFragment) fm.findFragmentByTag(FragmentIDs.ResturentMenuItemFragment_Tag);

                        notifyDataSetChanged();
                        resturentMenuItemFragment.notifyList();
                        resturentMenuItemFragment.addCartAdicionales();
                    }
                }
            }
        });
        cityDialog.show();
    }
}
