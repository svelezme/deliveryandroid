package com.adapter;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import com.app.grupodeliveryec.deliveryEC.R;
import com.bean.DeviceBean;
import com.com.rest.GlobalValues;
import com.com.rest.MethodListener;
import com.utility.SessionManager;

import java.util.Vector;

/**
 * Created by administrator on 16/2/17.
 */

public class DeviceListAdapter extends RecyclerView.Adapter<DeviceListAdapter.MyViewHolder>{

    Context mContext;
    Vector<DeviceBean> alDevice;
    public DeviceListAdapter(Context mContext, Vector<DeviceBean> alDevice){
        this.mContext = mContext;
        this.alDevice = alDevice;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{

        ImageView dispositivos_logo;
        TextView dispositivos_NombreDispositivo;
        TextView dispositivos_TipoCelular;
        ImageView delet;

        public MyViewHolder(View view){
            super(view);
            dispositivos_logo = (ImageView)view.findViewById(R.id.dispositivos_logo);
            dispositivos_NombreDispositivo = (TextView)view.findViewById(R.id.dispositivos_NombreDispositivo);
            dispositivos_TipoCelular = (TextView)view.findViewById(R.id.dispositivos_TipoCelular);
            delet = (ImageView)view.findViewById(R.id.delted);
        }
    }

    @Override
    public int getItemCount() {
        return alDevice.size();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.dispositivos_list_item_xml,parent,false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        DeviceBean deviceBean = alDevice.get(position);

        if(deviceBean.getTipoCelular().equals("AD")){
            holder.dispositivos_logo.setBackgroundResource(R.drawable.android_icon);
            holder.dispositivos_TipoCelular.setText("Android");
        }else{
            holder.dispositivos_logo.setBackgroundResource(R.drawable.ios_icon);
            holder.dispositivos_TipoCelular.setText("iOS");
        }
        holder.dispositivos_NombreDispositivo.setText(deviceBean.getNombreDispositivo());
        holder.delet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                are_your_sure(position);
            }
        });
    }

    public void are_your_sure(final int position){
        final Dialog dialog = new Dialog(mContext);
        dialog.setCancelable(false);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_entrega_delete);

        dialog.show();

        TextView cancel_tv = (TextView) dialog.findViewById(R.id.cancletv);

        TextView question = (TextView) dialog.findViewById(R.id.question);

        question.setText("¿Está seguro que desea eliminar estea Dispositivos?");

        cancel_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });

        TextView ok_tv = (TextView) dialog.findViewById(R.id.oktv);

        ok_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
                delete_item(alDevice.get(position).getIdClienteDispositivo() , dialog , position);
            }
        });
    }
    private void delete_item(int idDirecciones ,final Dialog dialog ,final int post){

        String url = "https://deliveryec.azurewebsites.net/ServiciosWeb/WebRestauranteLogin.asmx/EliminaClienteDispositivo?pIdClienteDispositivo="+idDirecciones+"&pIdCliente="+ SessionManager.getUserId(mContext)+"&callback=";

        GlobalValues.getMethodManagerObj(mContext).makeStringReqWithoutCache(url, "delete_dev", new MethodListener() {
            @Override
            public void onError() {
                //hideProgressbar();
            }

            @Override
            public void onError(String response) {
                //hideProgressbar();
            }

            @Override
            public void onSuccess(String response) {
                dialog.cancel();
                alDevice.remove(post);
                notifyItemRemoved(post);
            }
        });
    }
}
