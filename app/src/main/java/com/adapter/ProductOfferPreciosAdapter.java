package com.adapter;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.app.FragmentManager;
import android.support.v7.view.ContextThemeWrapper;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.TextView;

import com.app.grupodeliveryec.deliveryEC.R;
import com.bruce.pickerview.LoopView;
import com.fragment.ProductOffersFragment;
import com.inputview.GothamBookEditTextView;
import com.manager.AddToCartManager;
import com.model.ItemProductPrice;
import com.utility.Config;
import com.utility.FragmentIDs;
import com.view.GothamBookTextView;

import java.lang.reflect.Field;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import static com.utility.Config.alhmPrecios;
import static com.utility.Config.alhmProductOfferPrecios;
import static com.utility.Config.totalProductOfferPrecios;

/**
 * Created by administrator on 30/3/17.
 */

public class ProductOfferPreciosAdapter extends BaseAdapter{

    Context mContext;
    Map<String, ItemProductPrice> alMap = new HashMap<>();
    TreeMap<String, ArrayList<ItemProductPrice>> sorted;
    private String[] mapKeys;
    private FragmentManager fm;
    AddToCartManager addToCartManager;

    public ProductOfferPreciosAdapter(Context mContext, TreeMap<String, ArrayList<ItemProductPrice>> sorted, FragmentManager fm){
        this.mContext = mContext;
        this.sorted = sorted;
        this.fm = fm;
        mapKeys = sorted.keySet().toArray(new String[sorted.size()]);
    }

    @Override
    public int getCount() {
        return sorted.size();
    }

    @Override
    public ArrayList<ItemProductPrice> getItem(int i) {
        return sorted.get(mapKeys[i]);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.pricasos_item_xml, viewGroup, false);
        final GothamBookTextView pricesos_group = (GothamBookTextView) v.findViewById(R.id.pricesos_group);
        final GothamBookEditTextView pricesos_group_item = (GothamBookEditTextView) v.findViewById(R.id.pricesos_group_item);


        final ArrayList<ItemProductPrice> arrayListEntry = getItem(i);
        String key = mapKeys[i];
        //String key = arrayListEntry.getKey();
        pricesos_group.setText(key);


        pricesos_group_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final Dialog additionalProduct = new Dialog(new ContextThemeWrapper(mContext, R.style.DialogSlideAnim));
                additionalProduct.getWindow().setGravity(Gravity.BOTTOM);
                additionalProduct.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                additionalProduct.setCancelable(false);
                additionalProduct.setCanceledOnTouchOutside(false);
                additionalProduct.setContentView(R.layout.preciousadapter_citydialog_xml);

                final LoopView cityDialog_cityPicker = (LoopView) additionalProduct.findViewById(R.id.loop_view);
                final TextView cityDialog_cancel = (TextView) additionalProduct.findViewById(R.id.cityDialog_cancel);
                final TextView cityDialog_accept = (TextView) additionalProduct.findViewById(R.id.cityDialog_accept);

                cityDialog_cityPicker.setInitPosition(0);
                cityDialog_cityPicker.setCanLoop(false);

                ArrayList<String> area_list = new ArrayList<>();
                for (int i = 0; i < arrayListEntry.size(); i++) {
                    area_list.add(i, (arrayListEntry.get(i).getMedida() + "  $ " + new DecimalFormat("0.00").format(arrayListEntry.get(i).getPrecio()).replace(",",".")));
                }

                cityDialog_cityPicker.setTextSize(22);
                cityDialog_cityPicker.setDataList(area_list);

                additionalProduct.show();

                cityDialog_cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        additionalProduct.dismiss();
                    }
                });

                cityDialog_accept.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        additionalProduct.dismiss();
                        int value = cityDialog_cityPicker.getSelectedItem();
                        String dispalyItem = arrayListEntry.get(value).getMedida() + "  $" + ""+new DecimalFormat("0.00").format(arrayListEntry.get(value).getPrecio()).replace(",",".");
                        pricesos_group_item.setText("" + dispalyItem);
                        String mKey = mapKeys[i];

                        if (alMap.containsKey(mKey)) {
                            alMap.put(mKey, arrayListEntry.get(value));
                            alhmProductOfferPrecios.put(mKey,arrayListEntry.get(value));
                        } else {
                            alMap.put(mKey, arrayListEntry.get(value));
                            alhmProductOfferPrecios.put(mKey,arrayListEntry.get(value));
                        }

                        Config.totalProductOfferPrecios = 0;

                        for (Map.Entry m : alMap.entrySet()) {
                            ItemProductPrice ite = (ItemProductPrice) m.getValue();
                            Config.totalProductOfferPrecios = Config.totalProductOfferPrecios + ite.getPrecio();
                        }

                        if (fm != null) {
                            ProductOffersFragment productOffersFragment = (ProductOffersFragment)
                                    fm.findFragmentByTag(FragmentIDs.ProductOffersFragment_Tag);

                            if(productOffersFragment!=null && productOffersFragment.isVisible()){
                                productOffersFragment.addCartPrecios();
                            }
                        }

                    }
                });

            }
        });

        return v;
    }

    private void setCityPickerDividerColour(NumberPicker number_picker, int size) {

        // final int count = number_picker.getChildCount();
        for (int i = 0; i < size; i++) {
            View child = number_picker.getChildAt(i);

            try {
                Field dividerField = number_picker.getClass().getDeclaredField("mSelectionDivider");
                dividerField.setAccessible(true);
                ColorDrawable colorDrawable = new ColorDrawable(mContext.getResources().getColor(R.color
                        .hint_color_1));
                dividerField.set(number_picker, colorDrawable);

                if (child instanceof EditText) {

                    Field selectorWheelPaintField = number_picker.getClass()
                            .getDeclaredField("mSelectorWheelPaint");
                    selectorWheelPaintField.setAccessible(true);
                    ((Paint) selectorWheelPaintField.get(number_picker)).setColor(mContext.getResources().getColor(R.color.navigation_bg));
                    ((EditText) child).setTextSize(18.0f);
                    ((EditText) child).setTextColor(mContext.getResources().getColor(R.color.navigation_bg));
                    Log.d("","lalalalalallalalalalalalalal");
                    number_picker.invalidate();
                }
            } catch (NoSuchFieldException e) {
                Log.w("PickerTextColor", e);
            } catch (IllegalAccessException e) {
                Log.w("PickerTextColor", e);
            } catch (IllegalArgumentException e) {
                Log.w("PickerTextColor", e);
            }
        }
    }


}
