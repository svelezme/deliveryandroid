package com.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.TextView;

import com.app.grupodeliveryec.deliveryEC.R;
import com.github.siyamed.shapeimageview.RoundedImageView;
import com.model.ItemAdditionalProduct;
import com.model.RestaurentMenuItem;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.view.ExoBoldTextView;
import com.view.ExoSemiBoldTextView;
import com.view.GothamBookTextView;

import java.lang.reflect.Field;
import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 * Created by administrator on 1/2/17.
 */

public class ProductOfferAdapter extends BaseAdapter {


    String TAG = getClass().getName();
    Context mContext;
    ArrayList<RestaurentMenuItem> alItem;



    public ProductOfferAdapter(Context mContext,
                               ArrayList<RestaurentMenuItem> alItem
                              ) {
        this.mContext = mContext;
        this.alItem = alItem;

    }

    @Override
    public int getCount() {
        return alItem.size();
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        final ViewHolder viewHolder;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.item_of_resturent_xml, viewGroup, false);
            viewHolder = new ViewHolder();
            viewHolder.itemOfRest_Nombre = (ExoSemiBoldTextView) view.findViewById(R.id.itemOfRest_Nombre);
            viewHolder.itemOfRest_ImagenRestaProducto = (RoundedImageView) view.findViewById(R.id.itemOfRest_ImagenRestaProducto);
            viewHolder.itemOfRest_Descripcion = (GothamBookTextView) view.findViewById(R.id.itemOfRest_Descripcion);
            viewHolder.itemOfRest_price = (ExoBoldTextView) view.findViewById(R.id.itemOfRest_price);


            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        Picasso.with(mContext).
                load("http://www.deliveryec.com/images/app/" + alItem.get(i).getImagenRestaProducto()).
                into(viewHolder.itemOfRest_ImagenRestaProducto, new Callback() {
                    @Override
                    public void onSuccess() {
                        viewHolder.itemOfRest_ImagenRestaProducto.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onError() {
                        viewHolder.itemOfRest_ImagenRestaProducto.setVisibility(View.GONE);
                    }
                });

        viewHolder.itemOfRest_Nombre.setText(alItem.get(i).getNombre());
        viewHolder.itemOfRest_Descripcion.setText(alItem.get(i).getDescripcion());

        viewHolder.itemOfRest_price.setText("$ " + new DecimalFormat("0.00").format(alItem.get(i).getPrecio()));
       /* if (String.valueOf(alItem.get(i).getPrecio()).length()==3){
            viewHolder.itemOfRest_price.setText("$ " + new DecimalFormat("0.00").format(alItem.get(i).getPrecio()));
        }else {
            viewHolder.itemOfRest_price.setText("$ " + alItem.get(i).getPrecio());
        }*/



        return view;
    }

    public void updateAdapter(ArrayList<RestaurentMenuItem> alItem) {
        this.alItem = alItem;
        notifyDataSetChanged();
    }


    static class ViewHolder {
        ExoSemiBoldTextView itemOfRest_Nombre;
        RoundedImageView itemOfRest_ImagenRestaProducto;
        GothamBookTextView itemOfRest_Descripcion;
        ExoBoldTextView itemOfRest_price;
    }
}
