package com.adapter;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.app.FragmentManager;
import android.support.v7.view.ContextThemeWrapper;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.TextView;

import com.app.grupodeliveryec.deliveryEC.R;
import com.bruce.pickerview.LoopView;
import com.fragment.ProductOffersFragment;
import com.fragment.ResturentMenuItemFragment;
import com.inputview.GothamBookEditTextView;
import com.model.ItemAdditionalProduct;
import com.utility.Config;
import com.utility.FragmentIDs;
import com.utility.MessageDialog;
import com.view.GothamBookTextView;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by administrator on 30/3/17.
 */

public class ProductOfferAdditionalAdapter extends BaseAdapter{

    String TAG = getClass().getName();
    Context mContext;
    TreeMap<String, ArrayList<ItemAdditionalProduct>> additionalProduct;
    private String[] mapKeys;
    Map<Integer, ArrayList<ItemAdditionalProduct>> selectedItem;
    private FragmentManager fm;

    int value;
    ViewHolderItem viewHolderItem;
    boolean[] isDialog;

    public ProductOfferAdditionalAdapter(Context mContext, TreeMap<String, ArrayList<ItemAdditionalProduct>> additionalProduct, FragmentManager fm){

        this.mContext = mContext;
        this.additionalProduct = additionalProduct;
        this.fm = fm;
        mapKeys = additionalProduct.keySet().toArray(new String[additionalProduct.size()]);
        selectedItem = new HashMap<>();

        isDialog = new boolean[additionalProduct.size()];
        Arrays.fill(isDialog, Boolean.FALSE);

    }

    @Override
    public int getCount() {
        return additionalProduct.size();
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public ArrayList<ItemAdditionalProduct> getItem(int i) {
        return additionalProduct.get(mapKeys[i]);
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.additional_product_item, viewGroup, false);

            viewHolderItem = new ViewHolderItem();

            viewHolderItem.additional_groupName = (GothamBookTextView) view.findViewById(R.id.additional_groupName);
            viewHolderItem.additional_group_item = (GothamBookEditTextView) view.findViewById(R.id.additional_group_item);
            viewHolderItem.additional_group_add = (ImageView) view.findViewById(R.id.additional_group_add);
            viewHolderItem.addToCart_addAdditional_selectedItem = (LinearLayout) view.findViewById(R.id.addToCart_addAdditional_selectedItem);

            view.setTag(viewHolderItem);

        } else {
            viewHolderItem = (ViewHolderItem) view.getTag();
        }

        viewHolderItem.additional_group_item.setTag(i);
        viewHolderItem.additional_group_item.setTag(viewHolderItem);

        viewHolderItem.additional_group_add.setTag(i);
        viewHolderItem.additional_group_add.setTag(viewHolderItem);

        final ArrayList<ItemAdditionalProduct> additionalPro = getItem(i);
        String key = mapKeys[i];

        final ArrayList<ItemAdditionalProduct> alhmItem = selectedItem.get(i);

        if (alhmItem != null) {

            viewHolderItem.addToCart_addAdditional_selectedItem.removeAllViews();

            for (int j = 0; j < alhmItem.size(); j++) {
                LinearLayout childLayout = new LinearLayout(mContext);
                childLayout.setOrientation(LinearLayout.HORIZONTAL);
                childLayout.setPadding(5, 5, 5, 5);
                childLayout.setBackgroundColor(mContext.getResources().getColor(R.color.white));

                TextView tvGrupo = new TextView(mContext);
                TextView tvPriceo = new TextView(mContext);
                ImageButton tvDelete = new ImageButton(mContext);

                tvGrupo.setLayoutParams(new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT, 1f));

                tvPriceo.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                tvDelete.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));

                tvGrupo.setTextSize(14);
                tvGrupo.setPadding(5, 0, 0, 0);
                Typeface face = Typeface.createFromAsset(mContext.getAssets(),
                        "fonts/Gotham-Book.otf");
                //tv.setTypeface(face);
                tvGrupo.setTypeface(face);
                tvGrupo.setTextColor(mContext.getResources().getColor(R.color.fuscous_gray));
                //tvGrupo.setGravity(Gravity.LEFT | Gravity.CENTER);
                tvGrupo.setText(alhmItem.get(j).getNombre());

                tvPriceo.setTextSize(18);
                tvPriceo.setPadding(0, 0, 5, 0);
                Typeface face1 = Typeface.createFromAsset(mContext.getAssets(),
                        "fonts/BebasNeue.otf");
                tvPriceo.setTypeface(face1);
                tvPriceo.setTextColor(mContext.getResources().getColor(R.color.spicy_mix));
                //tvPriceo.setGravity(Gravity.RIGHT | Gravity.CENTER);
                tvPriceo.setText("$ "+alhmItem.get(j).getPrecio().replace("," , "."));

                tvDelete.setPadding(5, 5, 5, 5);
                tvDelete.setImageResource(R.drawable.minus_icon);
                tvDelete.setId(j);
                tvDelete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        int id = view.getId();
                        String message = mContext.getResources().getString(R.string.adiciional);
                        setMessageErrorDialog(id,alhmItem);
                    }
                });

                childLayout.addView(tvDelete, 0);

                childLayout.addView(tvPriceo, 0);
                childLayout.addView(tvGrupo, 0);

                viewHolderItem.addToCart_addAdditional_selectedItem.addView(childLayout);
            }
        }

        viewHolderItem.additional_groupName.setText(key);

        viewHolderItem.additional_group_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                isDialog[i] = false;

                final ViewHolderItem holderItem = (ViewHolderItem) view.getTag();

                final Dialog additionalProduct = new Dialog(new ContextThemeWrapper(mContext, R.style.DialogSlideAnim));
                additionalProduct.getWindow().setGravity(Gravity.BOTTOM);
                additionalProduct.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                additionalProduct.setCancelable(false);
                additionalProduct.setCanceledOnTouchOutside(false);
                additionalProduct.setContentView(R.layout.preciousadapter_citydialog_xml);
                /*final NumberPicker cityDialog_cityPicker = (NumberPicker) additionalProduct.findViewById(R.id.cityDialog_cityPicker);
                final TextView cityDialog_cancel = (TextView) additionalProduct.findViewById(R.id.cityDialog_cancel);
                final TextView cityDialog_accept = (TextView) additionalProduct.findViewById(R.id.cityDialog_accept);*/
                final LoopView cityDialog_cityPicker = (LoopView) additionalProduct.findViewById(R.id.loop_view);
                final TextView cityDialog_cancel = (TextView) additionalProduct.findViewById(R.id.cityDialog_cancel);
                final TextView cityDialog_accept = (TextView) additionalProduct.findViewById(R.id.cityDialog_accept);

              /*  cityDialog_cityPicker.setMinValue(0);
                cityDialog_cityPicker.setMaxValue(additionalPro.size() - 1);*/
                cityDialog_cityPicker.setInitPosition(0);
                cityDialog_cityPicker.setCanLoop(false);


                ArrayList<String> area_list = new ArrayList<>();
                // String[] displayValues = new String[alFilterAreaList.size()];
                for (int i = 0; i < additionalPro.size(); i++) {
                    area_list.add(i, (additionalPro.get(i).getNombre() + "  $ " + additionalPro.get(i).getPrecio().replace(",",".")));
                    // displayValues[i] = alFilterAreaList.get(i).getNombre();
                }

                cityDialog_cityPicker.setTextSize(22);
                cityDialog_cityPicker.setDataList(area_list);

                /*final String[] displayValues = new String[additionalPro.size()];
                for (int i = 0; i < additionalPro.size(); i++) {
                    displayValues[i] = additionalPro.get(i).getNombre() + "  $ " + additionalPro.get(i).getPrecio();
                }

                cityDialog_cityPicker.setDisplayedValues(displayValues);
                setCityPickerDividerColour(cityDialog_cityPicker, additionalPro.size());*/

                additionalProduct.show();

                cityDialog_cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        isDialog[i] = false;
                        additionalProduct.dismiss();
                    }
                });

                cityDialog_accept.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        isDialog[i] = true;

                        additionalProduct.dismiss();
                        value = cityDialog_cityPicker.getSelectedItem();
                        String dispalyItem = additionalPro.get(value).getNombre() + "  $" + additionalPro.get(value).getPrecio().replace(",",".");
                        //Log.e("pos",""+i);
                        holderItem.additional_group_item.setText("" + dispalyItem);
                    }
                });
            }
        });

        viewHolderItem.additional_group_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(isDialog[i]){
                    try {
                        updateLocalList(i,additionalPro);
                    }catch (Exception e){
                        Log.e(TAG,""+e.toString());
                    }
                    if(fm!=null){
                        ProductOffersFragment productOffersFragment = (ProductOffersFragment)
                                fm.findFragmentByTag(FragmentIDs.ProductOffersFragment_Tag);
                        if(productOffersFragment!=null && productOffersFragment.isVisible()){

                            notifyDataSetChanged();
                            productOffersFragment.notifyList();

                            Config.totalProductOfferAditional = 0;
                            Config.alhmProductOfferAditional.clear();

                            try{
                                for(Map.Entry m:selectedItem.entrySet()){
                                    //Log.e("Log:","key "+m.getKey()+","+m.getValue());
                                    //  group = mapKeys[(Integer) m.getKey()];
                                    ArrayList<ItemAdditionalProduct> alhm = (ArrayList<ItemAdditionalProduct>) m.getValue();
                                    for(int i=0;i<alhm.size();i++){
                                        //Log.e("price",""+alhm.get(i).getPrecio());
                                        Config.totalProductOfferAditional = Config.totalProductOfferAditional+Float.parseFloat(alhm.get(i).getPrecio());
                                        Config.alhmProductOfferAditional.add(alhm.get(i));
                                    }
                                }
                            }catch (NumberFormatException e){
                                Log.e(TAG,""+e.toString());
                            }catch (Exception e){
                                Log.e(TAG,""+e.toString());
                            }

                            productOffersFragment.addCartAdicionales();
                        }
                    }
                }
            }
        });
        return view;
    }

    private void updateLocalList(int position,ArrayList<ItemAdditionalProduct> additionalPro){
        /**
         * Update the arrayList
         */

        try{
            ArrayList<ItemAdditionalProduct> alhm = new ArrayList<ItemAdditionalProduct>();

            if (selectedItem.containsKey(position)) {
                alhm = selectedItem.get(position);
                alhm.add(additionalPro.get(value));
            } else {
                alhm.add(additionalPro.get(value));
            }
            selectedItem.put(position, alhm);
        }catch (Exception e){
            Log.e(TAG,""+e.toString());
        }
    }

    static class ViewHolderItem {
        GothamBookTextView additional_groupName;
        GothamBookEditTextView additional_group_item;
        ImageView additional_group_add;
        LinearLayout addToCart_addAdditional_selectedItem;
    }



    private void setMessageErrorDialog(final int id,
                                       final ArrayList<ItemAdditionalProduct> alhmItem) {
        final Dialog cityDialog = new Dialog(mContext);
        cityDialog.getWindow().setGravity(Gravity.CENTER);
        cityDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        cityDialog.setCanceledOnTouchOutside(false);
        cityDialog.setContentView(R.layout.delete_adicional_xml);

        final TextView clear_adicional_no = (TextView) cityDialog.findViewById(R.id.clear_adicional_no);
        final TextView clear_adicional_yes = (TextView) cityDialog.findViewById(R.id.clear_adicional_yes);

        clear_adicional_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (cityDialog != null) {
                    cityDialog.dismiss();
                }
            }
        });

        clear_adicional_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (cityDialog != null) {
                    cityDialog.dismiss();
                }

                if(alhmItem.size()>0){
                    int IdRelaRestaProduAdiciona = alhmItem.get(id).getIdRelaRestaProduAdiciona();
                    alhmItem.remove(id);

                    for(int i=0;i<Config.alhmProductOfferAditional.size();i++){

                        if(Config.alhmProductOfferAditional.get(i).getIdRelaRestaProduAdiciona()==IdRelaRestaProduAdiciona){

                            String precio = Config.alhmProductOfferAditional.get(i).getPrecio();
                            Config.totalProductOfferAditional = Config.totalProductOfferAditional-Float.parseFloat(precio);
                            Config.alhmProductOfferAditional.remove(i);
                        }
                    }

                    if(fm!=null) {
                        ProductOffersFragment productOffersFragment = (ProductOffersFragment)
                                fm.findFragmentByTag(FragmentIDs.ProductOffersFragment_Tag);
                        if (productOffersFragment != null && productOffersFragment.isVisible()) {

                            notifyDataSetChanged();
                            productOffersFragment.notifyList();
                            productOffersFragment.addCartAdicionales();
                        }
                    }
                }
            }
        });

        cityDialog.show();
    }
}
