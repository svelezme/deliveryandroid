package com.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;

import com.app.grupodeliveryec.deliveryEC.R;
import com.github.siyamed.shapeimageview.RoundedImageView;
import com.model.RestaurantListModel;
import com.squareup.picasso.Picasso;
import com.view.BebaseNeueTextView;
import com.view.BlackItalicTextView;
import com.view.ExoBoldItalicTextView;
import com.view.ExtraBoldItalicTextView;
import com.view.MediumTextView;
import com.view.RegularTextView;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by administrator on 7/4/17.
 */

public class RestaurantResultAdapter extends BaseAdapter {

    final int sdk = android.os.Build.VERSION.SDK_INT;
    Context mContext;
    ArrayList<RestaurantListModel> arraylist;
    ArrayList<RestaurantListModel> restaurantListModels;
    ViewHolder viewHolder;

    public RestaurantResultAdapter(Context mContext, ArrayList<RestaurantListModel> restaurantListModels) {
        this.mContext = mContext;
        this.restaurantListModels = restaurantListModels;
        this.arraylist = new ArrayList<>();
        this.arraylist.addAll(restaurantListModels);
    }

    @Override
    public int getCount() {
        return restaurantListModels.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.rest_list_item_layout, viewGroup, false);

            viewHolder = new ViewHolder();

            viewHolder.rest_list_imageview = (RoundedImageView) view.findViewById(R.id.rest_list_imageview);
            viewHolder.rest_list_nameOfRestaurant = (ExtraBoldItalicTextView) view.findViewById(R.id.rest_list_nameOfRestaurant);
            viewHolder.rest_list_typeOfFood = (MediumTextView) view.findViewById(R.id.rest_list_typeOfFood);
            viewHolder.rest_ratingBar = (RatingBar) view.findViewById(R.id.rest_ratingBar);
            viewHolder.rest_list_item_Activo = (ImageView) view.findViewById(R.id.rest_list_item_Activo);
            viewHolder.rest_minioprice = (BebaseNeueTextView) view.findViewById(R.id.rest_minioprice);
            viewHolder.rest_list_openStatus = (ExoBoldItalicTextView) view.findViewById(R.id.rest_list_openStatus);
            viewHolder.prog = (ProgressBar) view.findViewById(R.id.prog);

            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        viewHolder.rest_list_imageview.setTag(position);

        viewHolder.rest_list_nameOfRestaurant.setText(
                restaurantListModels.get(position).getNombreRestaurante()
                        + "-" +
                        restaurantListModels.get(position).getNombreLocal() + " ");

        viewHolder.rest_list_typeOfFood.setText("( " + restaurantListModels.get(position).
                getTipoComida() + " )");
        try {
            viewHolder.rest_ratingBar.setRating(restaurantListModels.get(position).getRating());
        } catch (Exception ec) {
        }

        String imgUrl = "http://www.deliveryec.com/images/app/" + restaurantListModels.get(position).getImagenLocal();

        Picasso.with(mContext).load(imgUrl)
                .error(mContext.getResources().getDrawable(R.drawable.default_image))
                .placeholder(mContext.getResources().getDrawable(R.drawable.default_image))
                .noFade()
                .fit()
                .into(viewHolder.rest_list_imageview, new com.squareup.picasso.Callback() {
                    @Override
                    public void onSuccess() {
                        viewHolder.prog.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {
                        viewHolder.prog.setVisibility(View.GONE);
                    }
                });


        if (restaurantListModels.get(position).getActivo().equals("I")) {
            viewHolder.rest_list_item_Activo.setImageResource(R.drawable.cross_red);
            viewHolder.rest_list_openStatus.setText(mContext.getResources().getString(R.string.CERRADO));

            if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                viewHolder.rest_list_openStatus.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.info_close_status_background));
            } else {
                viewHolder.rest_list_openStatus.setBackground(mContext.getResources().getDrawable(R.drawable.info_close_status_background));
            }

        } else {
            viewHolder.rest_list_item_Activo.setImageResource(R.drawable.checked_green);
            viewHolder.rest_list_openStatus.setText(mContext.getResources().getString(R.string.OPEN));

            if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                viewHolder.rest_list_openStatus.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.info_open_status_background));
            } else {
                viewHolder.rest_list_openStatus.setBackground(mContext.getResources().getDrawable(R.drawable.info_open_status_background));
            }
        }

        viewHolder.rest_minioprice.setText("$ " + "" + new DecimalFormat().format(Double.parseDouble(restaurantListModels.get(position).getMinimo())));

        return view;
    }


    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        restaurantListModels.clear();
        if (charText.length() == 0) {
            restaurantListModels.addAll(arraylist);
        } else {
            for (RestaurantListModel wp : arraylist) {
                if (wp.getNombreRestaurante().toLowerCase(Locale.getDefault()).contains(charText)) {
                    restaurantListModels.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }

    class ViewHolder {
        RoundedImageView rest_list_imageview;
        ExtraBoldItalicTextView rest_list_nameOfRestaurant;
        MediumTextView rest_list_typeOfFood;
        RatingBar rest_ratingBar;
        ImageView rest_list_item_Activo;
        BebaseNeueTextView rest_minioprice;
        ExoBoldItalicTextView rest_list_openStatus;
        ProgressBar prog;
    }
}
