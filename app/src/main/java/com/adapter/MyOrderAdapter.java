package com.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.app.grupodeliveryec.deliveryEC.R;
import com.bean.MyOrderBean;
import com.github.siyamed.shapeimageview.mask.PorterShapeImageView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.view.BebaseNeueTextView;
import java.text.DecimalFormat;
import java.util.Vector;

/**
 * Created by administrator on 17/2/17.
 */

public class MyOrderAdapter extends RecyclerView.Adapter<MyOrderAdapter.MyViewHolder>{

    Context mContext;
    Vector<MyOrderBean> alOrder;
    public MyOrderAdapter(Context mContext, Vector<MyOrderBean> alOrder){
        this.mContext = mContext;
        this.alOrder = alOrder;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{

        PorterShapeImageView myorder_ImagenLocal;
        com.view.ExoSemiBoldTextView myorder_NombreRestaurante;
        com.view.RegularTextView TipoComida;
        com.view.RegularTextView myorder_FechaRegistro;
        BebaseNeueTextView  myorder_Total;
        com.view.SemiBoldItalicTextView  Estado;//myorder_Estado

        public MyViewHolder(View view){
            super(view);
            myorder_ImagenLocal = (PorterShapeImageView)view.findViewById(R.id.myorder_ImagenLocal);
            myorder_NombreRestaurante = (com.view.ExoSemiBoldTextView)view.findViewById(R.id.myorder_NombreRestaurante);
            TipoComida = (com.view.RegularTextView)view.findViewById(R.id.TipoComida);
            myorder_FechaRegistro = (com.view.RegularTextView)view.findViewById(R.id.myorder_FechaRegistro);
            myorder_Total = (com.view.BebaseNeueTextView)view.findViewById(R.id.myorder_Total);
          //  myorder_Estado = (com.view.SemiBoldItalicTextView)view.findViewById(R.id.myorder_Estado);
            Estado = (com.view.SemiBoldItalicTextView)view.findViewById(R.id.Estado);
        }

    }

    @Override
    public int getItemCount() {
        return alOrder.size();
    }

    @Override
    public int getItemViewType(int position) {
        super.getItemViewType(position);
        return position;
    }

    @Override
    public long getItemId(int position) {
        super.getItemId(position);
        return position;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.myorder_item,parent,false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        MyOrderBean orderBean = alOrder.get(position);

        holder.myorder_NombreRestaurante.setText(orderBean.getNombreRestaurante()+"" +
                " - "+orderBean.getNombreLocal());

        holder.TipoComida.setText(orderBean.getTipoComida());

        holder.myorder_FechaRegistro.setText(orderBean.getFechaRegistro());
        //double price = orderBean.getTotal();
        holder.myorder_Total.setText("$ " + new DecimalFormat("0.00").format(orderBean.getTotal()));

        if(orderBean.getEstado().equals("P")){
            holder.Estado.setText(mContext.getResources().getString(R.string.procesado));
            holder.Estado.setTextColor(mContext.getResources().getColor(R.color.light_green));
        }else if (orderBean.getEstado().equals("C")){
            holder.Estado.setText("Cancelado");
            holder.Estado.setTextColor(mContext.getResources().getColor(R.color.red));
        }else  if (orderBean.getEstado().equals("A")){
            holder.Estado.setText("Esperando confirmación del restaurante"); //Aceptado
            holder.Estado.setTextColor(mContext.getResources().getColor(R.color.red));
        }


       String url = "http://www.deliveryec.com/images/app/"+orderBean.getImagenLocal();
        Picasso.with(mContext).load(url).into(holder.myorder_ImagenLocal, new Callback() {
            @Override
            public void onSuccess() {
            }

            @Override
            public void onError() {
            }
        });
    }
}
