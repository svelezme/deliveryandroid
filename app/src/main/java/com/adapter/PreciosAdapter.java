package com.adapter;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.app.FragmentManager;
import android.support.v7.view.ContextThemeWrapper;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.TextView;
import com.app.grupodeliveryec.deliveryEC.R;
import com.bruce.pickerview.LoopScrollListener;
import com.bruce.pickerview.LoopView;
import com.fragment.ResturentMenuItemFragment;
import com.inputview.GothamBookEditTextView;
import com.model.ItemProductPrice;
import com.utility.Config;
import com.utility.FragmentIDs;
import com.view.GothamBookTextView;
import java.lang.reflect.Field;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import static com.utility.Config.alhmPrecios;

/**
 * Created by administrator on 7/3/17.
 */

public class PreciosAdapter extends BaseAdapter {

    Context mContext;
    Map<String, ItemProductPrice> alMap = new HashMap<>();

    TreeMap<String, ArrayList<ItemProductPrice>> sorted;
    private String[] mapKeys;
    private FragmentManager fm;


    public PreciosAdapter(Context mContext, TreeMap<String,
            ArrayList<ItemProductPrice>> sorted,
                          FragmentManager fm) {
        this.mContext = mContext;
        this.sorted = sorted;
        this.fm = fm;
        mapKeys = sorted.keySet().toArray(new String[sorted.size()]);
    }

    @Override
    public int getCount() {
        return sorted.size();
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public ArrayList<ItemProductPrice> getItem(int i) {
        return sorted.get(mapKeys[i]);
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {

        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.pricasos_item_xml, viewGroup, false);
        GothamBookTextView pricesos_group = (GothamBookTextView) v.findViewById(R.id.pricesos_group);
        final GothamBookEditTextView pricesos_group_item = (GothamBookEditTextView) v.findViewById(R.id.pricesos_group_item);

        final ArrayList<ItemProductPrice> arrayListEntry = getItem(i);
        String key = mapKeys[i];
        if(key.contains("BASE-")){
            pricesos_group.setText(key.substring(5));
        }else {
            pricesos_group.setText(key);
        }

        pricesos_group_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final Dialog additionalProduct = new Dialog(new ContextThemeWrapper(mContext, R.style.DialogSlideAnim));
                additionalProduct.getWindow().setGravity(Gravity.BOTTOM);
                additionalProduct.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                additionalProduct.setCancelable(false);
                additionalProduct.setCanceledOnTouchOutside(false);
                additionalProduct.setContentView(R.layout.preciousadapter_citydialog_xml);
                final LoopView cityDialog_cityPicker = (LoopView) additionalProduct.findViewById(R.id.loop_view);
                final TextView cityDialog_cancel = (TextView) additionalProduct.findViewById(R.id.cityDialog_cancel);
                final TextView cityDialog_accept = (TextView) additionalProduct.findViewById(R.id.cityDialog_accept);

                cityDialog_cityPicker.setInitPosition(0);
                cityDialog_cityPicker.setCanLoop(false);
                cityDialog_cityPicker.setLoopListener(new LoopScrollListener() {
                    @Override
                    public void onItemSelect(int item) {
                    }
                });

                ArrayList<String> area_list = new ArrayList<>();
                // String[] displayValues = new String[alFilterAreaList.size()];
                for (int i = 0; i < arrayListEntry.size(); i++) {
                    area_list.add(i, (arrayListEntry.get(i).getMedida() + "  $ " + new DecimalFormat("0.00").format(arrayListEntry.get(i).getPrecio())));
                    // displayValues[i] = alFilterAreaList.get(i).getNombre();
                }

                cityDialog_cityPicker.setTextSize(22);
                cityDialog_cityPicker.setDataList(area_list);
                additionalProduct.show();

                cityDialog_cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        additionalProduct.dismiss();
                    }
                });

                cityDialog_accept.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        additionalProduct.dismiss();
                        int value = cityDialog_cityPicker.getSelectedItem();
                        String dispalyItem = arrayListEntry.get(value).getMedida() + "  $" + String.valueOf(new DecimalFormat("0.00").format(arrayListEntry.get(value).getPrecio()).replace(",","."));
                        pricesos_group_item.setText("" + dispalyItem);

                        String mKey = mapKeys[i];

                        //addToCartManager.insertPrecios(arrayListEntry.get(value));

                        if (alMap.containsKey(mKey)) {
                            //Toast.makeText(mContext,"Update Key"+mKey,Toast.LENGTH_LONG).show();
                            alMap.put(mKey, arrayListEntry.get(value));

                            alhmPrecios.put(mKey,arrayListEntry.get(value));

                        } else {
                            //Toast.makeText(mContext,"Insert Key"+mKey,Toast.LENGTH_LONG).show();
                            alMap.put(mKey, arrayListEntry.get(value));

                            alhmPrecios.put(mKey,arrayListEntry.get(value));
                        }

                        Config.totalPrecios = 0;

                        for (Map.Entry m :
                                alMap.entrySet()) {
                            ItemProductPrice ite = (ItemProductPrice) m.getValue();
                            Config.totalPrecios = Config.totalPrecios + ite.getPrecio();
                        }

                        /**
                         * Add the price into the total amount
                         */

                        if (fm != null) {
                            ResturentMenuItemFragment resturentMenuItemFragment =
                                    (ResturentMenuItemFragment) fm.findFragmentByTag(FragmentIDs.ResturentMenuItemFragment_Tag);

                            if (resturentMenuItemFragment != null && resturentMenuItemFragment.isVisible()) {
                                //ItemProductPrice itemProductPrice = arrayListEntry.get(value);
                                resturentMenuItemFragment.addCartPrecios();
                            }
                        }

                    }
                });
            }
        });

        return v;
    }
}
