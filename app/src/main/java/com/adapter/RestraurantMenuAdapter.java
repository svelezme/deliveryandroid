package com.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.app.grupodeliveryec.deliveryEC.R;
import com.model.RestaurantMenuList;
import com.view.GothamBoldTextView;

import java.util.ArrayList;

/**
 * Created by administrator on 30/1/17.
 */

public class RestraurantMenuAdapter extends BaseAdapter {

    Context mContext;
    ArrayList<RestaurantMenuList> arrayList;

    public RestraurantMenuAdapter(Context mContext, ArrayList<RestaurantMenuList> arrayList) {
        this.mContext = mContext;
        this.arrayList = arrayList;
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolderItem viewHolder;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.menu_item_xml, viewGroup, false);

            viewHolder = new ViewHolderItem();
            viewHolder.menu_name = (GothamBoldTextView)view.findViewById(R.id.menu_item_MenuName);

            view.setTag(viewHolder);


        } else {
            viewHolder = (ViewHolderItem) view.getTag();
        }

        viewHolder.menu_name.setText(arrayList.get(i).getNombre());
        return view;
    }

    static class ViewHolderItem {
        GothamBoldTextView menu_name;
    }
}
