package com.adapter;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Environment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.grupodeliveryec.deliveryEC.R;
import com.bean.AddToCartItem;
import com.fragment.OrderFragment;
import com.manager.AddToCartManager;
import com.manager.DeleteManager;
import com.model.ItemAdditionalProduct;
import com.model.ItemProductPrice;
import com.view.ExoBoldTextView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;
import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 * Created by administrator on 17/4/17.
 */

public class OrderListAdapter extends RecyclerView.Adapter<OrderListAdapter.OrderListViewHolder> {

    Context mContext;
    ArrayList<AddToCartItem> alOrders;
    OrderFragment.DeleteItemfromCartListener deleteItemfromCartListener;
    int status;
    DeleteManager deleteManager;

    public OrderListAdapter(Context mContext, ArrayList<AddToCartItem> alOrders, OrderFragment.DeleteItemfromCartListener deleteItemfromCartListener, int status) {
        this.mContext = mContext;
        this.alOrders = alOrders;
        this.deleteItemfromCartListener = deleteItemfromCartListener;
        this.status = status;
        deleteManager = new DeleteManager(mContext);
    }

    public class OrderListViewHolder extends RecyclerView.ViewHolder {
        ExoBoldTextView order_list_nombre;
        ExoBoldTextView order_list_nombre_descriptions, order_list_nombre_descriptions_2x;
        ExoBoldTextView order_list_price;
        ImageView order_list_deleteBtn;

        ExoBoldTextView order_list_nombre2x;
        ExoBoldTextView order_list_nombre_descriptions2x, order_list_nombre_descriptions22x;
        ExoBoldTextView order_list_price2x;
        ImageView order_list_deleteBtn2x;

        RelativeLayout OrderList_relativeLayout;

        public OrderListViewHolder(View view) {
            super(view);
            order_list_nombre = (ExoBoldTextView) view.findViewById(R.id.order_list_nombre);
            order_list_price = (ExoBoldTextView) view.findViewById(R.id.order_list_price);
            order_list_deleteBtn = (ImageView) view.findViewById(R.id.order_list_deleteBtn);
            order_list_nombre_descriptions = (ExoBoldTextView) view.findViewById(R.id.order_list_nombre_descriptions);
            order_list_nombre_descriptions_2x = (ExoBoldTextView) view.findViewById(R.id.order_list_nombre_descriptions_2x);

            order_list_nombre2x = (ExoBoldTextView) view.findViewById(R.id.order_list_nombre2x);
            order_list_price2x = (ExoBoldTextView) view.findViewById(R.id.order_list_price2x);
            order_list_deleteBtn2x = (ImageView) view.findViewById(R.id.order_list_deleteBtn2x);
            order_list_nombre_descriptions2x = (ExoBoldTextView) view.findViewById(R.id.order_list_nombre_descriptions2x);
            order_list_nombre_descriptions22x = (ExoBoldTextView) view.findViewById(R.id.order_list_nombre_descriptions22x);
            OrderList_relativeLayout = (RelativeLayout) view.findViewById(R.id.OrderList_relativeLayout);
        }
    }

    @Override
    public int getItemCount() {
        return alOrders.size();
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public OrderListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.order_list_item, parent, false);
        return new OrderListViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(OrderListViewHolder holder, final int position) {
        final AddToCartItem addToCartItem = alOrders.get(position);

        holder.order_list_nombre.setText(addToCartItem.getQty() + " X " + addToCartItem.getNombre());
        try {
            String s_p = String.valueOf(addToCartItem.getPrice());
            if (s_p.length() == 3) {
                s_p = s_p + "0";
            }
            holder.order_list_price.setText("$ " + new DecimalFormat("0.00").format(Float.parseFloat(s_p)));
        } catch (Exception e) {
            holder.order_list_price.setText("$ " + addToCartItem.getPrice());
            e.printStackTrace();
        }

        StringBuilder sb = new StringBuilder();
        ArrayList<ItemProductPrice> ipp = addToCartItem.getAlPriceo();
        if (ipp != null && ipp.size() > 0) {
            for (int j = 0; j < ipp.size(); j++) {
                sb.append(ipp.get(j).getGrupo() + " " + ipp.get(j).getMedida());
                sb.append(",");
            }
        }

        ArrayList<ItemAdditionalProduct> iap = addToCartItem.getAladiciona();
        if (iap != null && iap.size() > 0) {
            sb.append("ADICIONALES: ");
            for (int k = 0; k < iap.size(); k++) {
                sb.append("(" + iap.get(k).getAdiciona_qty() + ")");
                sb.append(iap.get(k).getNombre() + "; ");
            }
        }

        if (sb.toString().contains("BASE-") && !sb.toString().equals("")) {
            holder.order_list_nombre_descriptions.setText(addToCartItem.getDescripcion() + "\n" + sb.toString().replace("BASE-", ""));
        } else {
            holder.order_list_nombre_descriptions.setText(addToCartItem.getDescripcion() + "\n" + sb.toString());
        }

        //---------------2x1 functionality----------------------------//

        if (addToCartItem.getQtyE2x1() != null && !addToCartItem.getNombreE2x1().equals("")) {
            holder.order_list_nombre2x.setText(addToCartItem.getQtyE2x1() + " X " + addToCartItem.getNombreE2x1());

            try {
                String s_p = String.valueOf(addToCartItem.getPriceE2x1());
                if (s_p.length() == 3) {
                    s_p = s_p + "0";
                }
                holder.order_list_price2x.setText("$ " + new DecimalFormat("0.00").format(Float.parseFloat(s_p)));
            } catch (Exception e) {
                holder.order_list_price2x.setText("$ " + addToCartItem.getPrice());
                e.printStackTrace();
            }

            StringBuilder sbE2x1 = new StringBuilder();


            ArrayList<ItemProductPrice> ippE2x1 = addToCartItem.getAlPriceoE2x1();
            if (ippE2x1 != null && ippE2x1.size() > 0) {
                for (int j = 0; j < ippE2x1.size(); j++) {
                    sbE2x1.append(ippE2x1.get(j).getGrupo() + " " + ippE2x1.get(j).getMedida());
                    sbE2x1.append(",");
                }
            }

            final ArrayList<ItemAdditionalProduct> iapE2x1 = addToCartItem.getAladicionaE2x1();
            if (iapE2x1 != null && iapE2x1.size() > 0) {
                sbE2x1.append("ADICIONALES: ");
                for (int k = 0; k < iapE2x1.size(); k++) {
                    sbE2x1.append("(" + iapE2x1.get(k).getAdiciona_qty() + ")");
                    sbE2x1.append(iapE2x1.get(k).getNombre() + " ");
                }
            }

            String msg;
            if (addToCartItem.getPriceE2x1() == 0) {
                msg = "--Gratis 2x1-- ";
            } else {
                msg = "--Valor Adicionales-- ";
            }

            if (sbE2x1.toString().contains("BASE-") && !sbE2x1.toString().equals("")) {
                holder.order_list_nombre_descriptions22x.setText(msg + " " + addToCartItem.getDescripcionE2x1() + "\n" + sbE2x1.toString().replace("BASE-", ""));
               // holder.order_list_price.setTranslationX(130f);
            } else {
                holder.order_list_nombre_descriptions22x.setText(msg + " " + addToCartItem.getDescripcionE2x1() + "\n" + sbE2x1.toString());
            }

            holder.order_list_nombre2x.setVisibility(View.VISIBLE);
            holder.order_list_price2x.setVisibility(View.VISIBLE);

            holder.order_list_nombre_descriptions22x.setVisibility(View.VISIBLE);
            holder.OrderList_relativeLayout.setVisibility(View.VISIBLE);
        } else {
            holder.order_list_nombre2x.setVisibility(View.GONE);
            holder.order_list_price2x.setVisibility(View.GONE);
            holder.order_list_nombre_descriptions22x.setVisibility(View.GONE);
            holder.OrderList_relativeLayout.setVisibility(View.GONE);
        }

        if(status==2 ||status==4){
            holder.order_list_deleteBtn.setVisibility(View.INVISIBLE);
            //holder.order_list_price.setBackgroundColor(Color.parseColor("#000000"));
            holder.order_list_price.setTranslationX(130f);
            holder.order_list_price2x.setTranslationX(130f);

        }

        holder.order_list_deleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                delete_cart_dialog(addToCartItem, position);
            }
        });
    }

    public void delete_cart_dialog(final AddToCartItem addToCartItem, final int position) {
        final Dialog cityDialog = new Dialog(mContext);
        cityDialog.getWindow().setGravity(Gravity.CENTER);
        cityDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        cityDialog.setCanceledOnTouchOutside(false);
        cityDialog.setContentView(R.layout.delete_cart_popup);

        final TextView cityDialog_cancel = (TextView) cityDialog.findViewById(R.id.clear_cart_no);
        final TextView cityDialog_accept = (TextView) cityDialog.findViewById(R.id.clear_cart_yes);

        cityDialog_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (cityDialog != null) {
                    cityDialog.dismiss();
                }
            }
        });

        cityDialog_accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (cityDialog != null) {
                    cityDialog.dismiss();
                }
                //exportDB();
                AddToCartManager addToCartManager = new AddToCartManager(mContext);

                try {
                    if (addToCartItem.getEsDia2x1().equals("S")) {

                        if (addToCartItem.getTipoModelo2Por1().equals("1")
                                && addToCartItem.getTipoModelAdicional2Por1().equals("1")) {
                            TipoModel1_AdicionalModel1(addToCartManager, addToCartItem);
                        }

                        if (addToCartItem.getTipoModelo2Por1().equals("1")
                                && addToCartItem.getTipoModelAdicional2Por1().equals("2")) {
                            TipoModel1_AdicionalModel1(addToCartManager, addToCartItem);
                        }

                        if(addToCartItem.getTipoModelo2Por1().equals("2")
                                && addToCartItem.getTipoModelAdicional2Por1().equals("1")){
                            TipoModel2_AdicionalModel1(addToCartManager, addToCartItem);
                        }

                        if(addToCartItem.getTipoModelo2Por1().equals("2")
                                && addToCartItem.getTipoModelAdicional2Por1().equals("2")){
                            TipoModel2_AdicionalModel2(addToCartManager, addToCartItem);
                        }

                        if(addToCartItem.getTipoModelo2Por1().equals("3") &&
                                (addToCartItem.getTipoModelAdicional2Por1().equals("1") ||
                                addToCartItem.getTipoModelAdicional2Por1().equals("2"))){
                                TipoModel3_Adicional1_2(addToCartManager, addToCartItem);
                        }
                    }


                    if (addToCartItem.getEsDia2x1().equals("N")) {
                        if ((addToCartItem.getTipoModelo2Por1().equals("1") ||
                                addToCartItem.getTipoModelo2Por1().equals("2") ||
                                addToCartItem.getTipoModelo2Por1().equals("3"))
                                && (addToCartItem.getTipoModelAdicional2Por1().equals("1")
                                || addToCartItem.getTipoModelAdicional2Por1().equals("2"))) {
                            TipoModel3_Adicional1_2(addToCartManager, addToCartItem);
                        }
                    }
                } catch (Exception e) {
                    Log.e("eeeee", "" + e);
                }
            }
        });
        cityDialog.show();
    }

    private void TipoModel1_AdicionalModel1(AddToCartManager addToCartManager, AddToCartItem addToCartItem){
            if(addToCartItem.getIsIdStatus()==1){
                //Delete AdicionalModel and Precios from E2x1 table using E2x1 id
                deleteManager.delete_tbl_ProductoAdicionales_2x(addToCartItem.getIdE2x1());
                deleteManager.delete_tbl_ProductoPrecios_2x(addToCartItem.getIdE2x1());

                //Delete E2x1 product form product id
                deleteManager.delete_product_2x(addToCartItem.getId());

                //Delete Adicional and Precios from product table using product id
                deleteManager.delete_tbl_ProductoAdicionales(addToCartItem.getId());
                deleteManager.delete_tbl_ProductoPrecios(addToCartItem.getId());

                //Delete product form product id
                deleteManager.delete_product(addToCartItem.getId());

                deleteItemfromCartListener.delete();
            }

            if(addToCartItem.getIsIdStatus()==2){
                //Delete AdicionalModel and Precios from E2x1 table using E2x1 id
                deleteManager.delete_tbl_ProductoAdicionales_2x(addToCartItem.getId());
                deleteManager.delete_tbl_ProductoPrecios_2x(addToCartItem.getId());

                //Delete E2x1 product form product id
                deleteManager.delete_product_2x(addToCartItem.getIdE2x1());

                //Delete Adicional and Precios from product table using product id
                deleteManager.delete_tbl_ProductoAdicionales(addToCartItem.getIdE2x1());
                deleteManager.delete_tbl_ProductoPrecios(addToCartItem.getIdE2x1());

                //Delete product form product id
                deleteManager.delete_product(addToCartItem.getId());

                deleteItemfromCartListener.delete();
            }
    }

    private void TipoModel2_AdicionalModel1(AddToCartManager addToCartManager, AddToCartItem addToCartItem){
        if(addToCartItem.getIsIdStatus()==1){
            //If it pay price or free price status for table E2x1
            int product_E2xoffers_productId = deleteManager.getE2x1Product_E2xoffers_productId(addToCartItem.getId());
          //  if(product_E2xoffers_productId>0){

                /**
                 * Delete the ProductoPreciosE2x,tbl_ProductoAdicionalesE2x
                 */
                deleteManager.delete_tbl_ProductoPrecios_2x(product_E2xoffers_productId);
                deleteManager.delete_tbl_ProductoAdicionales_2x(product_E2xoffers_productId);

           // }
            /** Delete the prod_E2xoffers **/
            deleteManager.delete_product_2x(addToCartItem.getId());

            /** Delete the product and product Precios and Product Adicionales **/
            deleteManager.delete_product(addToCartItem.getId());
            deleteManager.delete_tbl_ProductoPrecios(addToCartItem.getId());
            deleteManager.delete_tbl_ProductoAdicionales(addToCartItem.getId());

            deleteItemfromCartListener.delete();
        }

        if(addToCartItem.getIsIdStatus()==2){
            //If it pay price or free price status for table E2x1

            int product_productId = deleteManager.getE2x1Product_product_productId(addToCartItem.getId());
           // if(product_productId>0){
                deleteManager.delete_tbl_ProductoPrecios(product_productId);
                deleteManager.delete_tbl_ProductoAdicionales(product_productId);
           // }
            deleteManager.delete_product(product_productId);


            /** Delete the prod_E2xoffers Adicional, Precios
             * from product_E2xoffers_productId**/
            deleteManager.delete_tbl_ProductoPrecios_2x(addToCartItem.getId());
            deleteManager.delete_tbl_ProductoAdicionales_2x(addToCartItem.getId());

            /** Delete the prod_E2xoffers  from product id**/
            deleteManager.delete_product_2x(product_productId);

            deleteItemfromCartListener.delete();
        }
    }

    private void TipoModel2_AdicionalModel2(AddToCartManager addToCartManager, AddToCartItem addToCartItem){
        if(addToCartItem.getIsIdStatus()==1){
            //If it pay price or free price status for table E2x1
            int product_E2xoffers_productId = deleteManager.getE2x1Product_E2xoffers_productId(addToCartItem.getId());
            deleteManager.delete_tbl_ProductoPrecios_2x(product_E2xoffers_productId);
            deleteManager.delete_tbl_ProductoAdicionales_2x(product_E2xoffers_productId);

            /** Delete the prod_E2xoffers **/
            deleteManager.delete_product_2x(addToCartItem.getId());

            /** Delete the product and product Precios and Product Adicionales **/
            deleteManager.delete_product(addToCartItem.getId());
            deleteManager.delete_tbl_ProductoPrecios(addToCartItem.getId());
            deleteManager.delete_tbl_ProductoAdicionales(addToCartItem.getId());

            deleteItemfromCartListener.delete();
        }
        if(addToCartItem.getIsIdStatus()==2){
            int product_productId = deleteManager.getE2x1Product_product_productId(addToCartItem.getId());

            deleteManager.delete_tbl_ProductoPrecios(product_productId);
            deleteManager.delete_tbl_ProductoAdicionales(product_productId);

            deleteManager.delete_product(product_productId);

            /** Delete the prod_E2xoffers Adicional, Precios
             * from product_E2xoffers_productId**/
            deleteManager.delete_tbl_ProductoPrecios_2x(addToCartItem.getId());
            deleteManager.delete_tbl_ProductoAdicionales_2x(addToCartItem.getId());

            /** Delete the prod_E2xoffers  from product id**/
            deleteManager.delete_product_2x(product_productId);

            deleteItemfromCartListener.delete();
        }
    }

    private void TipoModel3_Adicional1_2(AddToCartManager addToCartManager, AddToCartItem addToCartItem){

        deleteManager.delete_tbl_ProductoAdicionales(addToCartItem.getId());
        deleteManager.delete_tbl_ProductoPrecios(addToCartItem.getId());
        deleteManager.delete_product(addToCartItem.getId());

        deleteItemfromCartListener.delete();
    }
}
