package com.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.grupodeliveryec.deliveryEC.R;
import com.bean.PromocionesBean;
import com.github.siyamed.shapeimageview.RoundedImageView;
import com.squareup.picasso.Picasso;
import com.view.RegularTextView;

import java.util.Vector;

/**
 * Created by administrator on 15/4/17.
 */

public class PromocionesAdapter extends RecyclerView.Adapter<PromocionesAdapter.PromocionesViewHolder> {

    Context mContext;
    Vector<PromocionesBean> alPromociones;

    public PromocionesAdapter(Context mContext, Vector<PromocionesBean> alPromociones) {
        this.mContext = mContext;
        this.alPromociones = alPromociones;
    }

    @Override
    public int getItemCount() {
        return alPromociones.size();
    }

    @Override
    public PromocionesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.promocione_item_xml, parent, false);
        return new PromocionesViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(PromocionesViewHolder holder, int position) {
        PromocionesBean promocionesBean = alPromociones.get(position);

        //holder.promocines_NombreRestaurante.setText(promocionesBean.getNombreRestaurante());

        String imgUrl = "http://www.deliveryec.com/images/app/"+promocionesBean.getImagen();
        Picasso.with(mContext).load(imgUrl)
                .error(mContext.getResources().getDrawable(R.drawable.default_image))
                .placeholder(mContext.getResources().getDrawable(R.drawable.default_image))
                .noFade()
                .fit()
                .into(holder.promocines_restImageview);
    }

    public class PromocionesViewHolder extends RecyclerView.ViewHolder {

        RoundedImageView promocines_restImageview;
        RegularTextView promocines_NombreRestaurante;

        public PromocionesViewHolder(View view) {
            super(view);
            promocines_restImageview = (RoundedImageView)view.findViewById(R.id.promocines_restImageview);
            promocines_NombreRestaurante = (RegularTextView)view.findViewById(R.id.promocines_NombreRestaurante);
        }
    }
}
