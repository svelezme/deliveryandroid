package com.adapter;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.app.grupodeliveryec.deliveryEC.R;
import com.bean.MyOrderDetails;

import java.util.ArrayList;

/**
 * Created by administrator on 18/2/17.
 */

public class OrderItemAdapter2 extends BaseAdapter{

    Context mContext;
    ArrayList<MyOrderDetails> alOrderDetails;
    public OrderItemAdapter2(Context mContext, ArrayList<MyOrderDetails> alOrderDetails){
            this.mContext = mContext;
            this.alOrderDetails = alOrderDetails;
    }

    @Override
    public int getCount() {
        return alOrderDetails.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolderItem viewHolder;
        if(view==null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.order_item,viewGroup,false);

            viewHolder = new ViewHolderItem();
            viewHolder.NombreProducto = (TextView)view.findViewById(R.id.orderitem_NombreProducto);
            viewHolder.Observacion = (TextView)view.findViewById(R.id.orderitem_Observacion);
            viewHolder.Precio = (TextView)view.findViewById(R.id.orderitem_Precio);

            view.setTag(viewHolder);

        }else{
            viewHolder = (ViewHolderItem) view.getTag();
        }

        viewHolder.NombreProducto.setText(alOrderDetails.get(i).getCantidad()+" * "+
                                            alOrderDetails.get(i).getNombreProducto());

        viewHolder.Observacion.setText(Html.fromHtml(alOrderDetails.get(i).getObservacion()));

        viewHolder.Precio.setText(" $ "+alOrderDetails.get(i).getPrecio());

        return view;
    }

    static class ViewHolderItem {
        TextView NombreProducto;
        TextView Observacion;
        TextView Precio;
    }
}
