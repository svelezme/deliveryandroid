package com.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;

import com.app.grupodeliveryec.deliveryEC.R;
import com.bean.OpinionModel;

import com.utility.DateFormatters;
import com.view.GothamBoldTextView;
import com.view.GothamBookTextView;
import com.view.RegularTextView;
import com.view.SemiBoldItalicTextView;

import java.util.ArrayList;
import java.util.Vector;

/**
 * Created by administrator on 15/4/17.
 */


public class OpinionAdap extends RecyclerView.Adapter<OpinionAdap.MyViewHolder> {

    public ArrayList<OpinionModel> mList;
    Context mContext;

    public OpinionAdap(Context context, ArrayList mList) {
        this.mList = mList;
        mContext = context;
    }

    @Override
    public OpinionAdap.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = null;
        try {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.opinion_item, parent, false);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return new OpinionAdap.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(OpinionAdap.MyViewHolder holder, final int position) {
        try {
            OpinionModel restaurantListModel = mList.get(position);
            try {
                Double rate  = Double.parseDouble(restaurantListModel.getResta_average_rate());
                holder.rating.setRating(rate.floatValue());
            }catch (Exception e){
                Log.e("fsd" , ""+e);
                e.printStackTrace();
            }

            holder.name.setText("" + restaurantListModel.getTx_nombres());
            //String[] separated = restaurantListModel.getFecha_de_revision().split(" ");
            String date = restaurantListModel.getFecha_de_revision();
            if(date!=null) {
                holder.date.setText("" + DateFormatters.dateFormatters(restaurantListModel.getFecha_de_revision()));
            }

            try {
                holder.description.setText("" + restaurantListModel.getRevision());
            }catch (Exception e){
            }
        } catch (Exception e) {
            Log.e("eee", "" + e);
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public GothamBoldTextView name;
        public SemiBoldItalicTextView description;
        public GothamBoldTextView date;
        public RatingBar rating;

        public MyViewHolder(View view) {
            super(view);
            try {
                name = (GothamBoldTextView) view.findViewById(R.id.name);
                description = (SemiBoldItalicTextView) view.findViewById(R.id.description);
                date = (GothamBoldTextView) view.findViewById(R.id.date);
                rating = (RatingBar) view.findViewById(R.id.ratingBar1);
            } catch (Exception e) {
                Log.e("eee", "" + e);
            }
        }
    }


}







