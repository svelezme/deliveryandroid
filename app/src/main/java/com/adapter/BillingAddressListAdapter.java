package com.adapter;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.activity.DashBoardActivity;
import com.app.grupodeliveryec.deliveryEC.R;
import com.bean.BillingBean;
import com.com.rest.GlobalValues;
import com.com.rest.MethodListener;
import com.utility.Config;
import com.utility.FragmentIDs;
import com.utility.SessionManager;

import java.util.Vector;

/**
 * Created by administrator on 15/2/17.
 */

public class BillingAddressListAdapter extends RecyclerView.Adapter<BillingAddressListAdapter.MyViewHolder> {

    Context mContext;
    Vector<BillingBean> alBilling;
    public static int deletestatus = 0;

    public BillingAddressListAdapter(Context mContext, Vector<BillingBean> alBilling) {
        this.mContext = mContext;
        this.alBilling = alBilling;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        LinearLayout item;
        com.view.ExoSemiBoldTextView billing_list_Descripcion;
        com.view.ExoSemiBoldTextView billing_list_RazonSocial;
        com.view.ExoSemiBoldTextView billing_list_Identificacion;
        com.view.ExoSemiBoldTextView billing_list_Direccion;
        com.view.ExoSemiBoldTextView billing_list_Telefono;
        ImageView delet;

        public MyViewHolder(View view) {
            super(view);
            billing_list_Descripcion = (com.view.ExoSemiBoldTextView) view.findViewById(R.id.billing_list_Descripcion);
            billing_list_RazonSocial = (com.view.ExoSemiBoldTextView) view.findViewById(R.id.billing_list_RazonSocial);
            billing_list_Identificacion = (com.view.ExoSemiBoldTextView) view.findViewById(R.id.billing_list_Identificacion);
            billing_list_Direccion = (com.view.ExoSemiBoldTextView) view.findViewById(R.id.billing_list_Direccion);
            billing_list_Telefono = (com.view.ExoSemiBoldTextView) view.findViewById(R.id.billing_list_Telefono);
            item = (LinearLayout) view.findViewById(R.id.item);
            delet = (ImageView) view.findViewById(R.id.delted);
        }
    }

    @Override
    public int getItemCount() {
        return alBilling.size();
    }

    @Override
    public long getItemId(int position) {
        super.getItemId(position);
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        super.getItemViewType(position);
        return position;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.billing_item_xml, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        BillingBean billingBean = alBilling.get(position);

        holder.billing_list_Descripcion.setText(billingBean.getDescripcion());
        holder.billing_list_RazonSocial.setText(billingBean.getRazonSocial());
        holder.billing_list_Identificacion.setText(billingBean.getIdentificacion());
        holder.billing_list_Direccion.setText(billingBean.getDireccion());
        holder.billing_list_Telefono.setText(billingBean.getTelefono());
        holder.delet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                are_your_sure(position);
            }
        });

        holder.item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BillingBean billingBean = alBilling.get(position);
                Bundle b = new Bundle();
                b.putInt("billingType", Config.updateListBillingType);
                b.putSerializable("billingBean", billingBean);
                DashBoardActivity.displayFragment(FragmentIDs.BillingAddressFragment_Id, b);
            }
        });
    }


    public void are_your_sure(final int position) {
        final Dialog dialog = new Dialog(mContext);
        dialog.setCancelable(false);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_entrega_delete);

        dialog.show();
        TextView cancel_tv = (TextView) dialog.findViewById(R.id.cancletv);
        TextView question = (TextView) dialog.findViewById(R.id.question);
        TextView ok_tv = (TextView) dialog.findViewById(R.id.oktv);

        question.setText("¿Está seguro que desea eliminar estea dirección de facturación?");
        cancel_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });

        ok_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                delete_item(alBilling.get(position).getIdDireccionFactura(), dialog, position);
            }
        });
    }

    private void delete_item(int idDirecciones, final Dialog dialog, final int post) {

        String url = "https://deliveryec.azurewebsites.net/ServiciosWeb/WebRestauranteLogin.asmx/EliminaClienteDireccionFactura?pIdDirecciones=" + idDirecciones + "&pIdCliente=" + SessionManager.getUserId(mContext) + "&callback=";

        GlobalValues.getMethodManagerObj(mContext).makeStringReqWithoutCache(url, "delete_item", new MethodListener() {
            @Override
            public void onError() {
                //hideProgressbar();
                dialog.cancel();
                deletestatus = 0;
            }

            @Override
            public void onError(String response) {
                //hideProgressbar();
                dialog.cancel();
                deletestatus = 0;
            }

            @Override
            public void onSuccess(String response) {
                alBilling.remove(post);
                notifyDataSetChanged();
                dialog.cancel();
            }
        });
    }
}
