package com.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


import com.app.grupodeliveryec.deliveryEC.R;
import com.bean.MenuBean;
import com.utility.MenuList;

import java.util.ArrayList;

/**
 * Created by administrator on 12/9/16.
 */
public class MenuAdapter extends BaseAdapter
{

    Context activity ;
    ArrayList<MenuBean>dashBoardListItems;

    public  static  int selectedItemPos = 0;

    public MenuAdapter(Context activity)
    {
        this.activity = activity;
        MenuList dashBoardList = new MenuList();
        dashBoardListItems = dashBoardList.getDashBoardList(activity);
    }

    @Override
    public int getCount() {
        return dashBoardListItems.size();
    }

    @Override
    public Object getItem(int position) {
        return dashBoardListItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        ViewHolder  viewHolder ;
        if(convertView == null)
        {
            viewHolder = new ViewHolder();
            convertView = LayoutInflater.from(activity).inflate(R.layout.list_item_menu,null);
            viewHolder.tv_menu_name = (TextView) convertView.findViewById(R.id.tv_menu_name);
            convertView.setTag(viewHolder);
        }
        else
        {
           viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.tv_menu_name.setCompoundDrawablesWithIntrinsicBounds(dashBoardListItems.get(position).getResId(), 0, 0, 0);
        viewHolder.tv_menu_name.setText(dashBoardListItems.get(position).getMenu_name());

        return convertView;
    }
   public  class  ViewHolder
   {
       TextView tv_menu_name;
   }
}
