package com.manager;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.bean.AddToCartItem;
import com.database.DeliveryECDB;

import java.util.ArrayList;

/**
 * Created by administrator on 17/11/17.
 */

public class DeleteManager {

    SQLiteDatabase db;
    DeliveryECDB deliveryECDB;

    String TAG = getClass().getName();
    Context mcontext;

    public DeleteManager(Context mContext) {
        deliveryECDB = DeliveryECDB.getmInstance(mContext);
        mcontext = mContext;
    }

    public int getE2x1Product_E2xoffers_productId(int product_productId){
        int id = 0;
        db = deliveryECDB.getReadableDatabase();
        String sql = "SELECT DISTINCT(product_E2xoffers_productId) FROM prod_E2xoffers WHERE  product_productId='"+product_productId+"'";
        Cursor c = db.rawQuery(sql,null);
        while (c.moveToNext()){
            id = c.getInt(0);
        }
        return id;
    }

    public int getE2x1Product_product_productId(int product_productId){
        int id = 0;
        db = deliveryECDB.getReadableDatabase();
        String sql = "SELECT  DISTINCT(product_productId)  FROM prod_E2xoffers  where product_E2xoffers_productId = '"+product_productId+"'";
        db.rawQuery(sql,null);
        Cursor c = db.rawQuery(sql,null);
        while (c.moveToNext()){
            id = c.getInt(0);
        }
        return id;
    }

    public void delete_product_2x(int productid) {
        db = deliveryECDB.getWritableDatabase();
        db.delete(DeliveryECDB.prod_E2xoffers, "product_productId=?", new String[]{productid + ""});
        db.close();

        String sql = "select count(*) from prod_E2xoffers";
        db = deliveryECDB.getReadableDatabase();
        Cursor c = db.rawQuery(sql, null);
        int count = 0;
        while (c.moveToNext()) {
            count = c.getInt(0);
        }

        Log.i("count-product_2x", "" + count);
    }

    public void delete_tbl_ProductoPrecios_2x(int productid) {
        db = deliveryECDB.getWritableDatabase();
        db.delete(DeliveryECDB.tbl_ProductoPreciosE2x, "product_E2xoffers_productId=?", new String[]{productid + ""});
        db.close();

        String sql = "select count(*) from ProductoPreciosE2x";
        db = deliveryECDB.getReadableDatabase();
        Cursor c = db.rawQuery(sql, null);
        int count = 0;
        while (c.moveToNext()) {
            count = c.getInt(0);
        }

        Log.i("count-Precios_2x", "" + count);
    }

    public void delete_tbl_ProductoAdicionales_2x(int productid) {
        db = deliveryECDB.getWritableDatabase();
        db.delete(DeliveryECDB.tbl_ProductoAdicionalesE2x, "product_E2xoffers_productId=?", new String[]{productid + ""});
        db.close();

        String sql = "select count(*) from tbl_ProductoAdicionalesE2x";
        db = deliveryECDB.getReadableDatabase();
        Cursor c = db.rawQuery(sql, null);
        int count = 0;
        while (c.moveToNext()) {
            count = c.getInt(0);
        }

        Log.i("count-Adicionales_2x", "" + count);
    }


    public void delete_product(int productid) {
        db = deliveryECDB.getWritableDatabase();
        db.delete(DeliveryECDB.tbl_products, "product_productId=?", new String[]{productid + ""});
        db.close();
        AddToCartManager addToCartManager = new AddToCartManager(mcontext);
        ArrayList<AddToCartItem> sf = addToCartManager.getProduct();
        Log.e(" alOrders", "" + sf);
    }

    public void delete_tbl_ProductoPrecios(int productid) {
        db = deliveryECDB.getWritableDatabase();
        db.delete(DeliveryECDB.tbl_ProductoPrecios, "product_productId=?", new String[]{productid + ""});
        db.close();

        String sql = "select count(*) from ProductoPrecios";
        db = deliveryECDB.getReadableDatabase();
        Cursor c = db.rawQuery(sql, null);
        int count = 0;
        while (c.moveToNext()) {
            count = c.getInt(0);
        }

        Log.i("count-Precios", "" + count);
    }

    public void delete_tbl_ProductoAdicionales(int productid) {
        db = deliveryECDB.getWritableDatabase();
        db.delete(DeliveryECDB.tbl_ProductoAdicionales, "product_productId=?", new String[]{productid + ""});
        db.close();

        String sql = "select count(*) from ProductoAdicionales";
        db = deliveryECDB.getReadableDatabase();
        Cursor c = db.rawQuery(sql, null);
        int count = 0;
        while (c.moveToNext()) {
            count = c.getInt(0);
        }

        Log.i("count-Adicionales", "" + count);
    }






}
