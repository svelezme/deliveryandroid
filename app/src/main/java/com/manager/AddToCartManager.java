package com.manager;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.os.Environment;
import android.util.Log;
import com.bean.AddToCartItem;
import com.bean.OrderData;
import com.database.DeliveryECDB;
import com.listener.UpdateMessage;
import com.model.ItemAdditionalProduct;
import com.model.ItemProductPrice;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;

/**
 * Created by administrator on 15/3/17.
 */

public class AddToCartManager {

    SQLiteDatabase db;
    DeliveryECDB deliveryECDB;

    String TAG = getClass().getName();
    Context mcontext;
    UpdateMessage.UpdateMessageListener updateMessageListener;

    public AddToCartManager(Context mContext) {
        deliveryECDB = DeliveryECDB.getmInstance(mContext);
        mcontext = mContext;
    }

    public void setUpdateMessageListener(UpdateMessage.UpdateMessageListener updateMessageListener) {
        this.updateMessageListener = updateMessageListener;
    }

    public void dropCart() {
        try {
            db = deliveryECDB.getWritableDatabase();
            db.execSQL("DROP TABLE IF EXISTS " + DeliveryECDB.tbl_AddToCart);
            db.execSQL("DROP TABLE IF EXISTS " + DeliveryECDB.tbl_ProductoPrecios);
            db.execSQL("DROP TABLE IF EXISTS " + DeliveryECDB.tbl_ProductoAdicionales);
            db.execSQL("DROP TABLE IF EXISTS " + DeliveryECDB.tbl_products);

            db.execSQL("DROP TABLE IF EXISTS " + DeliveryECDB.tbl_ProductoPreciosE2x);
            db.execSQL("DROP TABLE IF EXISTS " + DeliveryECDB.tbl_ProductoAdicionalesE2x);
            db.execSQL("DROP TABLE IF EXISTS " + DeliveryECDB.prod_E2xoffers);

            deliveryECDB.onCreate(db);

        } catch (SQLiteException e) {
            Log.e(TAG, "" + e.toString());
        } catch (Exception e) {
            Log.e(TAG, "" + e.toString());
        }
    }

    public int updateProductE2x(String Nombre, String Descripcion,
                                int idRestaProducto,
                                int product_qty,
                                float totalPrice,
                                int insertId_ProductId,
                                String TipoModelo2Por1, String TipoModelAdicional2Por1) {

        int id = 0;
        int count = 0;
        float price = 0;
        String sql = "SELECT " + DeliveryECDB.product_qty + ", " + DeliveryECDB.product_price + " FROM " + DeliveryECDB.prod_E2xoffers +
                " WHERE " + DeliveryECDB.product_idRestaProducto + " = '" + idRestaProducto + "'";

        db = deliveryECDB.getReadableDatabase();
        Cursor c = db.rawQuery(sql, null);
        while (c.moveToNext()) {
            count = c.getInt(0);
            price = c.getFloat(1);
        }

        if (count == 0) {
            id = (int) insertProductE2XOffers(Nombre, Descripcion,
                    idRestaProducto, product_qty,
                    totalPrice, insertId_ProductId,
                    TipoModelo2Por1, TipoModelAdicional2Por1);
        } else {

            product_qty = count + product_qty;
            totalPrice = totalPrice + price;

            db = deliveryECDB.getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put(DeliveryECDB.product_qty, product_qty);
            cv.put(DeliveryECDB.product_price, totalPrice);
            id = db.update(DeliveryECDB.prod_E2xoffers,
                    cv,
                    DeliveryECDB.product_idRestaProducto + " = ?",
                    new String[]{String.valueOf(idRestaProducto)});

           // Log.v("update product", "" + id);
        }

        return id;
    }


    public void saveAdicionalesE2x(ItemAdditionalProduct iap, int insertId) {
        db = deliveryECDB.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(DeliveryECDB.adiciona_IdRelaRestaProduAdiciona, iap.getIdRelaRestaProduAdiciona());
        cv.put(DeliveryECDB.adiciona_IdRestaProducto, iap.getIdRestaProducto());
        cv.put(DeliveryECDB.adiciona_IdRestaExtradicional, iap.getIdRestaExtradicional());
        cv.put(DeliveryECDB.adiciona_Nombre, iap.getNombre());
        cv.put(DeliveryECDB.adiciona_Grupo, iap.getGrupo());
        cv.put(DeliveryECDB.adiciona_Medida, iap.getMedida());
        cv.put(DeliveryECDB.adiciona_Precio, iap.getPrecio());
        cv.put(DeliveryECDB.adiciona_Posicion, iap.getPosicion());
        cv.put(DeliveryECDB.adiciona_Estado, iap.getEstado());
        cv.put(DeliveryECDB.adiciona_qty, iap.getAdiciona_qty());
        cv.put(DeliveryECDB.product_E2xoffers_productId, insertId);

        long l = db.insert(DeliveryECDB.tbl_ProductoAdicionalesE2x, null, cv);
       // Log.v("insert adicionales", "" + l);
    }

    public void insertPreciosE2X(ItemProductPrice ipp, int qty, int insertId) {

        ContentValues cv = new ContentValues();
        cv.put(DeliveryECDB.precios_IdRestaProdPrecio, ipp.getIdRestaProdPrecio());
        cv.put(DeliveryECDB.precios_IdRestaProducto, ipp.getIdRestaProducto());
        cv.put(DeliveryECDB.precios_Grupo, ipp.getGrupo());
        cv.put(DeliveryECDB.precios_Medida, ipp.getMedida());
        cv.put(DeliveryECDB.precios_price, ipp.getPrecio());
        cv.put(DeliveryECDB.precios_Posicion, ipp.getPosicion());
        cv.put(DeliveryECDB.precios_Cantidad, ipp.getCantidad());
        cv.put(DeliveryECDB.precios_Estado, ipp.getEstado());
        cv.put(DeliveryECDB.precios_qty, qty);
        cv.put(DeliveryECDB.product_E2xoffers_productId, insertId);

        db = deliveryECDB.getWritableDatabase();
        long l = db.insert(DeliveryECDB.tbl_ProductoPreciosE2x, null, cv);
        //Log.v("insert Precios", "" + l);
    }


    public long insertProductE2XOffers(String Nombre, String Descripcion, int idRestaProducto,
                                       int product_qty,
                                       float totalPrice,
                                       int product_productId,
                                       String TipoModelo2Por1,
                                       String TipoModelAdicional2Por1) {

        ContentValues cv = new ContentValues();
        cv.put(DeliveryECDB.product_idRestaProducto, idRestaProducto);
        cv.put(DeliveryECDB.product_Nombre, Nombre);
        cv.put(DeliveryECDB.product_Descripcion, Descripcion);
        cv.put(DeliveryECDB.product_qty, product_qty);
        cv.put(DeliveryECDB.product_price, totalPrice);
        cv.put(DeliveryECDB.product_productId, product_productId);
        cv.put(DeliveryECDB.TipoModelo2Por1, TipoModelo2Por1);
        cv.put(DeliveryECDB.TipoModelAdicional2Por1, TipoModelAdicional2Por1);

        db = deliveryECDB.getWritableDatabase();
        long l = db.insert(DeliveryECDB.prod_E2xoffers, null, cv);

        //Log.v("E2X - product insert", "" + l);
        return l;
    }


    public long insertProduct(String Nombre, String Descripcion, int idRestaProducto,
                              int product_qty, float totalPrice,
                              String TipoModelo2Por1,
                              String TipoModelAdicional2Por1,String EsDia2x1) {

        ContentValues cv = new ContentValues();
        cv.put(DeliveryECDB.product_idRestaProducto, idRestaProducto);
        cv.put(DeliveryECDB.product_Nombre, Nombre);
        cv.put(DeliveryECDB.product_Descripcion, Descripcion);
        cv.put(DeliveryECDB.product_qty, product_qty);
        cv.put(DeliveryECDB.product_price, totalPrice);
        cv.put(DeliveryECDB.TipoModelo2Por1, TipoModelo2Por1);
        cv.put(DeliveryECDB.TipoModelAdicional2Por1, TipoModelAdicional2Por1);
        cv.put(DeliveryECDB.ESDia2x1,EsDia2x1);

        db = deliveryECDB.getWritableDatabase();
        long l = db.insert(DeliveryECDB.tbl_products, null, cv);

        //Log.v("product insert", "" + l);
        return l;
    }

    public int updateProduct(String Nombre, String Descripcion, int idRestaProducto,
                             int product_qty, float totalPrice, String TipoModelo2Por1,
                             String TipoModelAdicional2Por1,String EsDia2x1) {

        int id = 0;
        int count = 0;
        float price = 0;
        String sql = "SELECT " + DeliveryECDB.product_qty + ", " + DeliveryECDB.product_price + " FROM " + DeliveryECDB.tbl_products +
                " WHERE " + DeliveryECDB.product_idRestaProducto + " = '" + idRestaProducto + "'";

        db = deliveryECDB.getReadableDatabase();
        Cursor c = db.rawQuery(sql, null);
        while (c.moveToNext()) {
            count = c.getInt(0);
            price = c.getFloat(1);
        }

        if (count == 0) {
            id = (int) insertProduct(Nombre, Descripcion, idRestaProducto,
                    product_qty, totalPrice, TipoModelo2Por1, TipoModelAdicional2Por1,EsDia2x1);
        } else {

            product_qty = count + product_qty;
            totalPrice = totalPrice + price;

            db = deliveryECDB.getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put(DeliveryECDB.product_qty, product_qty);
            cv.put(DeliveryECDB.product_price, totalPrice);
            id = db.update(DeliveryECDB.tbl_products,
                    cv,
                    DeliveryECDB.product_idRestaProducto + " = ?",
                    new String[]{String.valueOf(idRestaProducto)});

            //Log.v("update product", "" + id);

            updateMessageListener.onUpdateMessage();
        }

        return id;
    }

    public void insertPrecios(ItemProductPrice ipp, int qty, int insertId) {

        ContentValues cv = new ContentValues();
        cv.put(DeliveryECDB.precios_IdRestaProdPrecio, ipp.getIdRestaProdPrecio());
        cv.put(DeliveryECDB.precios_IdRestaProducto, ipp.getIdRestaProducto());
        cv.put(DeliveryECDB.precios_Grupo, ipp.getGrupo());
        cv.put(DeliveryECDB.precios_Medida, ipp.getMedida());
        cv.put(DeliveryECDB.precios_price, ipp.getPrecio());
        cv.put(DeliveryECDB.precios_Posicion, ipp.getPosicion());
        cv.put(DeliveryECDB.precios_Cantidad, ipp.getCantidad());
        cv.put(DeliveryECDB.precios_Estado, ipp.getEstado());
        cv.put(DeliveryECDB.precios_qty, qty);
        cv.put(DeliveryECDB.product_productId, insertId);

        db = deliveryECDB.getWritableDatabase();
        long l = db.insert(DeliveryECDB.tbl_ProductoPrecios, null, cv);
        Log.v("insert Precios", "" + l);
    }


    public void saveAdicionales(ItemAdditionalProduct iap, int insertId) {
        db = deliveryECDB.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(DeliveryECDB.adiciona_IdRelaRestaProduAdiciona, iap.getIdRelaRestaProduAdiciona());
        cv.put(DeliveryECDB.adiciona_IdRestaProducto, iap.getIdRestaProducto());
        cv.put(DeliveryECDB.adiciona_IdRestaExtradicional, iap.getIdRestaExtradicional());
        cv.put(DeliveryECDB.adiciona_Nombre, iap.getNombre());
        cv.put(DeliveryECDB.adiciona_Grupo, iap.getGrupo());
        cv.put(DeliveryECDB.adiciona_Medida, iap.getMedida());
        cv.put(DeliveryECDB.adiciona_Precio, iap.getPrecio());
        cv.put(DeliveryECDB.adiciona_Posicion, iap.getPosicion());
        cv.put(DeliveryECDB.adiciona_Estado, iap.getEstado());
        cv.put(DeliveryECDB.adiciona_qty, iap.getAdiciona_qty());
        cv.put(DeliveryECDB.product_productId, insertId);

        long l = db.insert(DeliveryECDB.tbl_ProductoAdicionales, null, cv);
       // Log.v("insert adicionales", "" + l);
    }


    public AddToCartItem getOrderItemTipoModelo2_Modelo2(OrderData orderData) {

        AddToCartItem addToCartItem = new AddToCartItem();
        if (orderData.getIsIdStatus() == 1) {
           // addToCartItem = callProductTable(addToCartItem,orderData);

            String sql = "select * from products where product_productId = '" + orderData.getProduct_productId() + "'";
            Cursor c = db.rawQuery(sql, null);
            while (c.moveToNext()) {

                addToCartItem.setId(c.getInt(0));

                ArrayList<ItemProductPrice> alPriceo = getPreciosItem(c.getInt(0));
                ArrayList<ItemAdditionalProduct> aladiciona = getAdicionalesItem(c.getInt(0));

                if (alPriceo != null && alPriceo.size() > 0) {
                    addToCartItem.setAlPriceo(alPriceo);
                }

                if (aladiciona != null && aladiciona.size() > 0) {
                    addToCartItem.setAladiciona(aladiciona);
                }

                for(int i=0;i<alPriceo.size();i++){

                }


                addToCartItem.setIdRestaProducto(c.getString(1));
                addToCartItem.setNombre(c.getString(2));
                addToCartItem.setDescripcion(c.getString(3));
                addToCartItem.setQty(c.getString(4));
                addToCartItem.setPrice(c.getFloat(5));

                addToCartItem.setTipoModelAdicional2Por1(orderData.getTipoModelAdicional2Por1());
                addToCartItem.setTipoModelo2Por1(orderData.getTipoModelo2Por1_P());
                addToCartItem.setIsIdStatus(orderData.getIsIdStatus());
                addToCartItem.setEsDia2x1(orderData.getEsDia2x1());
            }


        }

        if (orderData.getIsIdStatus() == 2) {
            //Call the prod_E2xoffers Table
            String sql = "select * from prod_E2xoffers where product_E2xoffers_productId = '" + orderData.getProduct_productId() + "'";
            Cursor c = db.rawQuery(sql, null);
            while (c.moveToNext()) {
                addToCartItem.setId(c.getInt(0));

                ArrayList<ItemProductPrice> alPriceo = getPreciosItem_2x(c.getInt(0));
                ArrayList<ItemAdditionalProduct> aladiciona = getAdicionalesItem_2x(c.getInt(0));

                if (alPriceo != null && alPriceo.size() > 0) {
                    addToCartItem.setAlPriceo(alPriceo);
                }

                if (aladiciona != null && aladiciona.size() > 0) {
                    addToCartItem.setAladiciona(aladiciona);
                }
                addToCartItem.setIdRestaProducto(c.getString(1));
                addToCartItem.setNombre(c.getString(2));
                addToCartItem.setDescripcion(c.getString(3));
                addToCartItem.setQty(c.getString(4));
                addToCartItem.setPrice(c.getFloat(5));

                addToCartItem.setTipoModelAdicional2Por1(orderData.getTipoModelAdicional2Por1());
                addToCartItem.setTipoModelo2Por1(orderData.getTipoModelo2Por1_P());
                addToCartItem.setIsIdStatus(orderData.getIsIdStatus());
                addToCartItem.setEsDia2x1(orderData.getEsDia2x1());
            }
        }
        return addToCartItem;
    }


    public AddToCartItem getOrderItemTipoModelo2_Modelo2_ChargeOfAdicional(OrderData orderData) {

        AddToCartItem addToCartItem = new AddToCartItem();
        if (orderData.getIsIdStatus() == 1) {

            String sql = "select * from products where product_productId = '" + orderData.getProduct_productId() + "'";
            Cursor c = db.rawQuery(sql, null);
            while (c.moveToNext()) {

                addToCartItem.setId(c.getInt(0));

                ArrayList<ItemProductPrice> alPriceo = getPreciosItem(c.getInt(0));
                ArrayList<ItemAdditionalProduct> aladiciona = getAdicionalesItem(c.getInt(0));

                float preciosPrice = 0.0f;
                if (alPriceo != null && alPriceo.size() > 0) {
                    addToCartItem.setAlPriceo(alPriceo);

                    try{
                        for(int i=0;i<alPriceo.size();i++){
                            if(!alPriceo.get(i).getGrupo().startsWith("BASE-")){
                                preciosPrice+= alPriceo.get(i).getPrecio();
                            }
                        }
                    }catch (Exception e){
                        Log.e(TAG,""+e.toString());
                    }
                }

                float adicionalPrice = 0.0f;

                if (aladiciona != null && aladiciona.size() > 0) {
                    addToCartItem.setAladiciona(aladiciona);

                    try{
                        for(int i=0;i<aladiciona.size();i++){
                            adicionalPrice+=aladiciona.get(i).getPrice2x1();
                        }
                    }catch (Exception e){
                        Log.e(TAG,""+e.toString());
                    }
                }


                addToCartItem.setIdRestaProducto(c.getString(1));
                addToCartItem.setNombre(c.getString(2));
                addToCartItem.setDescripcion(c.getString(3));
                addToCartItem.setQty(c.getString(4));

                float totalPrice = preciosPrice+adicionalPrice;

                addToCartItem.setPrice(totalPrice);
                //addToCartItem.setPrice(c.getFloat(5));

                addToCartItem.setTipoModelAdicional2Por1(orderData.getTipoModelAdicional2Por1());
                addToCartItem.setTipoModelo2Por1(orderData.getTipoModelo2Por1_P());
                addToCartItem.setIsIdStatus(orderData.getIsIdStatus());
                addToCartItem.setEsDia2x1(orderData.getEsDia2x1());
            }


        }

        if (orderData.getIsIdStatus() == 2) {
            //Call the prod_E2xoffers Table
            String sql = "select * from prod_E2xoffers where product_E2xoffers_productId = '" + orderData.getProduct_productId() + "'";
            Cursor c = db.rawQuery(sql, null);
            while (c.moveToNext()) {
                addToCartItem.setId(c.getInt(0));

                ArrayList<ItemProductPrice> alPriceo = getPreciosItem_2x(c.getInt(0));
                ArrayList<ItemAdditionalProduct> aladiciona = getAdicionalesItem_2x(c.getInt(0));

                float preciosPrice = 0.0f;
                if (alPriceo != null && alPriceo.size() > 0) {
                    addToCartItem.setAlPriceo(alPriceo);

                    try{
                        for(int i=0;i<alPriceo.size();i++){
                            preciosPrice+=alPriceo.get(i).getPrecio();
                        }
                    }catch (Exception e){
                        Log.e(TAG,""+e.toString());
                    }
                }

                float adicionalPrice = 0.0f;
                if (aladiciona != null && aladiciona.size() > 0) {
                    addToCartItem.setAladiciona(aladiciona);

                    try{
                        for(int i=0;i<aladiciona.size();i++){
                            adicionalPrice+=aladiciona.get(i).getPrice2x1();
                        }
                    }catch (Exception e){
                        Log.e(TAG,""+e.toString());
                    }
                }


                addToCartItem.setIdRestaProducto(c.getString(1));
                addToCartItem.setNombre(c.getString(2));
                addToCartItem.setDescripcion(c.getString(3));
                addToCartItem.setQty(c.getString(4));

                float totalPrice = preciosPrice+adicionalPrice;

                /*addToCartItem.setPrice(c.getFloat(5));*/
                addToCartItem.setPrice(totalPrice);

                addToCartItem.setTipoModelAdicional2Por1(orderData.getTipoModelAdicional2Por1());
                addToCartItem.setTipoModelo2Por1(orderData.getTipoModelo2Por1_P());
                addToCartItem.setIsIdStatus(orderData.getIsIdStatus());
                addToCartItem.setEsDia2x1(orderData.getEsDia2x1());
            }
        }
        return addToCartItem;
    }


    public AddToCartItem callProductTable_modelo2x1_Adicionales_modelo1(AddToCartItem addToCartItem, OrderData orderData) {
        //Call the Product Table
        String sql = "select * from products where product_productId = '" + orderData.getProduct_productId() + "'";
        Cursor c = db.rawQuery(sql, null);
        while (c.moveToNext()) {

            addToCartItem.setId(c.getInt(0));

            ArrayList<ItemProductPrice> alPriceo = getPreciosItem(c.getInt(0));
            ArrayList<ItemAdditionalProduct> aladiciona = getAdicionalesItem(c.getInt(0));

            if (alPriceo != null && alPriceo.size() > 0) {
                addToCartItem.setAlPriceo(alPriceo);
            }

            if (aladiciona != null && aladiciona.size() > 0) {
                addToCartItem.setAladiciona(aladiciona);
            }


            addToCartItem.setIdRestaProducto(c.getString(1));
            addToCartItem.setNombre(c.getString(2));
            addToCartItem.setDescripcion(c.getString(3));
            addToCartItem.setQty(c.getString(4));
            addToCartItem.setPrice(0); //c.getFloat(5)

            addToCartItem.setTipoModelAdicional2Por1(orderData.getTipoModelAdicional2Por1());
            addToCartItem.setTipoModelo2Por1(orderData.getTipoModelo2Por1_P());
            addToCartItem.setIsIdStatus(orderData.getIsIdStatus());
            addToCartItem.setEsDia2x1(orderData.getEsDia2x1());
        }
        return addToCartItem;
    }


    public AddToCartItem callProductTable(AddToCartItem addToCartItem, OrderData orderData) {
        //Call the Product Table
        String sql = "select * from products where product_productId = '" + orderData.getProduct_productId() + "'";
        Cursor c = db.rawQuery(sql, null);
        while (c.moveToNext()) {

            addToCartItem.setId(c.getInt(0));

            ArrayList<ItemProductPrice> alPriceo = getPreciosItem(c.getInt(0));
            ArrayList<ItemAdditionalProduct> aladiciona = getAdicionalesItem(c.getInt(0));

            if (alPriceo != null && alPriceo.size() > 0) {
                addToCartItem.setAlPriceo(alPriceo);
            }

            if (aladiciona != null && aladiciona.size() > 0) {
                addToCartItem.setAladiciona(aladiciona);
            }


            addToCartItem.setIdRestaProducto(c.getString(1));
            addToCartItem.setNombre(c.getString(2));
            addToCartItem.setDescripcion(c.getString(3));
            addToCartItem.setQty(c.getString(4));
            addToCartItem.setPrice(c.getFloat(5));

            addToCartItem.setTipoModelAdicional2Por1(orderData.getTipoModelAdicional2Por1());
            addToCartItem.setTipoModelo2Por1(orderData.getTipoModelo2Por1_P());
            addToCartItem.setIsIdStatus(orderData.getIsIdStatus());
            addToCartItem.setEsDia2x1(orderData.getEsDia2x1());
        }
        return addToCartItem;
    }

    private AddToCartItem callProductTableE2x1(AddToCartItem addToCartItem, OrderData orderData) {

        //Call the prod_E2xoffers Table
        String sql = "select * from prod_E2xoffers where product_E2xoffers_productId = '" + orderData.getProduct_E2xoffers_productId() + "'";
        Cursor c = db.rawQuery(sql, null);
        while (c.moveToNext()) {
            addToCartItem.setId(c.getInt(0));

            ArrayList<ItemProductPrice> alPriceo = getPreciosItem_2x(c.getInt(0));
            ArrayList<ItemAdditionalProduct> aladiciona = getAdicionalesItem_2x(c.getInt(0));

            if (alPriceo != null && alPriceo.size() > 0) {
                addToCartItem.setAlPriceo(alPriceo);
            }

            if (aladiciona != null && aladiciona.size() > 0) {
                addToCartItem.setAladiciona(aladiciona);
            }


            addToCartItem.setIdRestaProducto(c.getString(1));
            addToCartItem.setNombre(c.getString(2));
            addToCartItem.setDescripcion(c.getString(3));
            addToCartItem.setQty(c.getString(4));
            addToCartItem.setPrice(c.getFloat(5));

            addToCartItem.setTipoModelAdicional2Por1(orderData.getTipoModelAdicional2Por1());
            addToCartItem.setTipoModelo2Por1(orderData.getTipoModelo2Por1_P());
            addToCartItem.setIsIdStatus(orderData.getIsIdStatus());
            addToCartItem.setEsDia2x1(orderData.getEsDia2x1());
        }
        return addToCartItem;
    }

    public AddToCartItem getProductItemModelAdicional2Por1_1(OrderData orderData) {
        AddToCartItem addToCartItem = new AddToCartItem();

        db = deliveryECDB.getReadableDatabase();
        if (orderData.getIsIdStatus() == 1) {
            addToCartItem = callProductTable(addToCartItem, orderData);


            String sql = "select * from prod_E2xoffers where product_E2xoffers_productId = '" + orderData.getProduct_E2xoffers_productId() + "'";
            Cursor c = db.rawQuery(sql, null);
            while (c.moveToNext()) {

                addToCartItem.setIdE2x1(c.getInt(0));
                addToCartItem.setIdRestaProductoE2x1(c.getString(1));
                addToCartItem.setNombreE2x1(c.getString(2));
                addToCartItem.setDescripcionE2x1(c.getString(3));
                addToCartItem.setQtyE2x1(c.getString(4));



                ArrayList<ItemProductPrice> alPriceo = getPreciosItem_2x(c.getInt(0));
                ArrayList<ItemAdditionalProduct> aladiciona = getAdicionalesItem_2x(c.getInt(0));

                if (alPriceo != null && alPriceo.size() > 0) {
                    addToCartItem.setAlPriceoE2x1(alPriceo);
                }

                if (aladiciona != null && aladiciona.size() > 0) {
                    addToCartItem.setAladicionaE2x1(aladiciona);
                }
                addToCartItem.setPriceE2x1(0.0f);//c.getFloat(5)
            }
        } else {

            addToCartItem = callProductTableE2x1(addToCartItem, orderData);

            String sql = "select * from products where product_productId = '" + orderData.getProduct_productId() + "'";
            Cursor c = db.rawQuery(sql, null);
            while (c.moveToNext()) {
                addToCartItem.setIdE2x1(c.getInt(0));
                addToCartItem.setIdRestaProductoE2x1(c.getString(1));
                addToCartItem.setNombreE2x1(c.getString(2));
                addToCartItem.setDescripcionE2x1(c.getString(3));
                addToCartItem.setQtyE2x1(c.getString(4));


                ArrayList<ItemProductPrice> alPriceo = getPreciosItem(c.getInt(0));
                ArrayList<ItemAdditionalProduct> aladiciona = getAdicionalesItem(c.getInt(0));

                if (alPriceo != null && alPriceo.size() > 0) {
                    addToCartItem.setAlPriceoE2x1(alPriceo);
                }

                if (aladiciona != null && aladiciona.size() > 0) {
                    addToCartItem.setAladicionaE2x1(aladiciona);
                }
                addToCartItem.setPriceE2x1(0.0f);//c.getFloat(5)
            }
        }
        return addToCartItem;
    }

    public AddToCartItem getProductItem(OrderData orderData) {

        AddToCartItem addToCartItem = new AddToCartItem();

        db = deliveryECDB.getReadableDatabase();
        if (orderData.getIsIdStatus() == 1) {
            addToCartItem = callProductTable(addToCartItem, orderData);


            String sql = "select * from prod_E2xoffers where product_E2xoffers_productId = '" + orderData.getProduct_E2xoffers_productId() + "'";
            Cursor c = db.rawQuery(sql, null);
            while (c.moveToNext()) {

                addToCartItem.setIdE2x1(c.getInt(0));
                addToCartItem.setIdRestaProductoE2x1(c.getString(1));
                addToCartItem.setNombreE2x1(c.getString(2));
                addToCartItem.setDescripcionE2x1(c.getString(3));
                addToCartItem.setQtyE2x1(c.getString(4));



                ArrayList<ItemProductPrice> alPriceo = getPreciosItem_2x(c.getInt(0));
                ArrayList<ItemAdditionalProduct> aladiciona = getAdicionalesItem_2x(c.getInt(0));

                if (alPriceo != null && alPriceo.size() > 0) {
                    addToCartItem.setAlPriceoE2x1(alPriceo);
                }

                if (aladiciona != null && aladiciona.size() > 0) {
                    addToCartItem.setAladicionaE2x1(aladiciona);
                }

                Float price2x1 = 0.0f;
                try{

                    for(int i=0;i<alPriceo.size();i++){
                        price2x1+= alPriceo.get(i).getPrecio();
                    }


                    for(int i=0;i<aladiciona.size();i++){
                        price2x1+= aladiciona.get(i).getPrice2x1();  // Float.valueOf(aladiciona.get(i).getPrecio())
                    }

                    price2x1 = price2x1*Float.valueOf(c.getString(4));

                }catch (Exception e){
                    Log.e(TAG,""+e.toString());
                }
                addToCartItem.setPriceE2x1(price2x1);//c.getFloat(5)
            }
        } else {
            addToCartItem = callProductTableE2x1(addToCartItem, orderData);

            String sql = "select * from products where product_productId = '" + orderData.getProduct_productId() + "'";
            Cursor c = db.rawQuery(sql, null);
            while (c.moveToNext()) {
                addToCartItem.setIdE2x1(c.getInt(0));
                addToCartItem.setIdRestaProductoE2x1(c.getString(1));
                addToCartItem.setNombreE2x1(c.getString(2));
                addToCartItem.setDescripcionE2x1(c.getString(3));
                addToCartItem.setQtyE2x1(c.getString(4));


                ArrayList<ItemProductPrice> alPriceo = getPreciosItem(c.getInt(0));
                ArrayList<ItemAdditionalProduct> aladiciona = getAdicionalesItem(c.getInt(0));

                if (alPriceo != null && alPriceo.size() > 0) {
                    addToCartItem.setAlPriceoE2x1(alPriceo);
                }

                if (aladiciona != null && aladiciona.size() > 0) {
                    addToCartItem.setAladicionaE2x1(aladiciona);
                }

                float price = 0.0f;

                for(int i=0;i<alPriceo.size();i++){
                    try {
                        boolean isBase = alPriceo.get(i).getGrupo().startsWith("BASE-");
                        if(isBase){
                            price+=0.0f;
                        }else{
                            price+=alPriceo.get(i).getPrecio();
                        }
                    }catch (Exception e){
                        price+=0.0f;
                        Log.e(TAG,""+e.toString());
                    }
                }
                for(int i=0;i<aladiciona.size();i++){
                    try {
                        price+= aladiciona.get(i).getPrice2x1(); //Float.valueOf(aladiciona.get(i).getPrecio());
                    }catch (Exception e){
                        price+= 0.0f;
                        Log.e(TAG,""+e.toString());
                    }
                }

                try{
                    price = (price*Float.valueOf(c.getString(4)));
                }catch (Exception e){
                    Log.e(TAG,""+e.toString());
                }
                addToCartItem.setPriceE2x1(price);//c.getFloat(5)
            }
        }
        return addToCartItem;
    }

    public AddToCartItem getOrderItemTipoModelo2_Modelo1(OrderData orderData,int freeStatus) {
        AddToCartItem addToCartItem = new AddToCartItem();

        if (orderData.getIsIdStatus() == 1) {
            if(freeStatus==1){
                addToCartItem = callProductTable(addToCartItem,orderData);
            }else{
                addToCartItem = callProductTable_modelo2x1_Adicionales_modelo1(addToCartItem,orderData);
            }
        }

        if (orderData.getIsIdStatus() == 2) {
            //Call the prod_E2xoffers Table
            String sql = "select * from prod_E2xoffers where product_E2xoffers_productId = '" + orderData.getProduct_productId() + "'";
            Cursor c = db.rawQuery(sql, null);
            while (c.moveToNext()) {
                addToCartItem.setId(c.getInt(0));

                ArrayList<ItemProductPrice> alPriceo = getPreciosItem_2x(c.getInt(0));
                ArrayList<ItemAdditionalProduct> aladiciona = getAdicionalesItem_2x(c.getInt(0));

                if (alPriceo != null && alPriceo.size() > 0) {
                    addToCartItem.setAlPriceo(alPriceo);
                }

                if (aladiciona != null && aladiciona.size() > 0) {
                    addToCartItem.setAladiciona(aladiciona);
                }
                addToCartItem.setIdRestaProducto(c.getString(1));
                addToCartItem.setNombre(c.getString(2));
                addToCartItem.setDescripcion(c.getString(3));
                addToCartItem.setQty(c.getString(4));

                if(freeStatus==1){
                    addToCartItem.setPrice(c.getFloat(5));
                }else{
                    addToCartItem.setPrice(0);
                }

                addToCartItem.setTipoModelAdicional2Por1(orderData.getTipoModelAdicional2Por1());
                addToCartItem.setTipoModelo2Por1(orderData.getTipoModelo2Por1_P());
                addToCartItem.setIsIdStatus(orderData.getIsIdStatus());
                addToCartItem.setEsDia2x1(orderData.getEsDia2x1());
            }
        }

        return addToCartItem;
    }

    public ArrayList<OrderData> getOrderData() {

        db = deliveryECDB.getReadableDatabase();
        ArrayList<OrderData> alOrderData = new ArrayList<>();

        String sql_TipoModelo2Por1 = "SELECT DISTINCT TipoModelo2Por1,TipoModelAdicional2Por1, ESDia2x1 FROM products";
        Cursor c_TipoModelo2Por1 = db.rawQuery(sql_TipoModelo2Por1, null);
        while (c_TipoModelo2Por1.moveToNext()) {

            String TipoModelo2Por1_P = c_TipoModelo2Por1.getString(0);
            String TipoModelAdicional2Por1 = c_TipoModelo2Por1.getString(1);
            String ESDia2x1 = c_TipoModelo2Por1.getString(2);

            /**
             * ESDia2x1 = S then calling the 2x1 Data
             */

            if (TipoModelo2Por1_P.equals("1") && ESDia2x1.equals("S")) {

                String sql = "SELECT * FROM products INNER JOIN prod_E2xoffers" +
                        " ON products.product_productId = prod_E2xoffers.product_productId";

                Cursor c = db.rawQuery(sql, null);
                while (c.moveToNext()) {
                    int product_productId = c.getInt(0);
                   // String idRestaProducto = c.getString(1);
                    float price = c.getFloat(5);
                    String TipoModelo2Por1 = c.getString(6);
                    int product_E2xoffers_productId = c.getInt(9);
                    float priceE2x = c.getFloat(14);

                    OrderData orderData = new OrderData();

                    if (TipoModelo2Por1.equals("1")) {
                        if (price > priceE2x) {
                            //get the products.product_productId from  product table
                            orderData.setIsIdStatus(1);
                        } else {
                            //get the prod_E2xoffers.product_productId from  prod_E2xoffers table
                            orderData.setIsIdStatus(2);
                        }

                        orderData.setProduct_productId(product_productId);
                        orderData.setProductPrice(price);
                        orderData.setProduct_E2xoffers_productId(product_E2xoffers_productId);
                        orderData.setProduct2x1Price(priceE2x);
                        orderData.setTipoModelo2Por1_P(TipoModelo2Por1_P);
                        orderData.setTipoModelAdicional2Por1(TipoModelAdicional2Por1);
                        orderData.setEsDia2x1(ESDia2x1);
                        alOrderData.add(orderData);
                    }
                }
            }

            if (TipoModelo2Por1_P.equals("2") && ESDia2x1.equals("S")) {
                String sql_product = "SELECT * FROM products Where ESDia2x1 = 'S'";
                String sql_E2x1 = "SELECT * FROM prod_E2xoffers";

                Cursor c_product = db.rawQuery(sql_product, null);
                Cursor c_productE2x1 = db.rawQuery(sql_E2x1, null);

                while (c_product.moveToNext()) {
                    int product_productId = c_product.getInt(0);
                    float price = c_product.getFloat(5);

                    OrderData orderData = new OrderData();
                    orderData.setProduct_productId(product_productId);
                    orderData.setProductPrice(price);
                    orderData.setTipoModelo2Por1_P(TipoModelo2Por1_P);
                    orderData.setTipoModelAdicional2Por1(TipoModelAdicional2Por1);
                    orderData.setEsDia2x1(ESDia2x1);
                    orderData.setIsIdStatus(1);
                    alOrderData.add(orderData);
                }

                while (c_productE2x1.moveToNext()) {
                    int product_productId = c_productE2x1.getInt(0);
                    float price = c_productE2x1.getFloat(5);

                    OrderData orderData = new OrderData();
                    orderData.setProduct_productId(product_productId);
                    orderData.setProductPrice(price);
                    orderData.setTipoModelo2Por1_P(TipoModelo2Por1_P);
                    orderData.setTipoModelAdicional2Por1(TipoModelAdicional2Por1);
                    orderData.setEsDia2x1(ESDia2x1);
                    orderData.setIsIdStatus(2);
                    alOrderData.add(orderData);
                }
            }

            if (TipoModelo2Por1_P.equals("3") && ESDia2x1.equals("S")) {
                String sql = "SELECT * FROM products WHERE ESDia2x1 = 'S'";
                Cursor c_product =db.rawQuery(sql,null);
                while (c_product.moveToNext()){
                    int product_productId = c_product.getInt(0);
                    float price = c_product.getFloat(5);

                    OrderData orderData = new OrderData();
                    orderData.setProduct_productId(product_productId);
                    orderData.setProductPrice(price);
                    orderData.setTipoModelo2Por1_P(TipoModelo2Por1_P);
                    orderData.setTipoModelAdicional2Por1(TipoModelAdicional2Por1);
                    orderData.setEsDia2x1(ESDia2x1);
                    orderData.setIsIdStatus(1);
                    alOrderData.add(orderData);
                }
            }


            /**
             *  Without 2x1 product
             */
            if((TipoModelo2Por1_P.equals("1") || TipoModelo2Por1_P.equals("2") || TipoModelo2Por1_P.equals("3")) && ESDia2x1.equals("N")){
                String sql = "SELECT * FROM products WHERE ESDia2x1 = 'N'";
                Cursor c_product =db.rawQuery(sql,null);
                while (c_product.moveToNext()){
                    int product_productId = c_product.getInt(0);
                    float price = c_product.getFloat(5);

                    OrderData orderData = new OrderData();
                    orderData.setProduct_productId(product_productId);
                    orderData.setProductPrice(price);
                    orderData.setTipoModelo2Por1_P(TipoModelo2Por1_P);
                    orderData.setTipoModelAdicional2Por1(TipoModelAdicional2Por1);
                    orderData.setEsDia2x1(ESDia2x1);
                    orderData.setIsIdStatus(1);
                    alOrderData.add(orderData);
                }
            }
        }
        return alOrderData;
    }

    public ArrayList<AddToCartItem> getProduct() {

        ArrayList<AddToCartItem> alItem = new ArrayList<>();

        String sql = "SELECT * FROM " + DeliveryECDB.tbl_products;

        db = deliveryECDB.getReadableDatabase();
        Cursor c = db.rawQuery(sql, null);
        while (c.moveToNext()) {

            AddToCartItem addToCartItem = new AddToCartItem();

            addToCartItem.setId(c.getInt(0));

            ArrayList<ItemProductPrice> alPriceo = getPreciosItem(c.getInt(0));
            ArrayList<ItemAdditionalProduct> aladiciona = getAdicionalesItem(c.getInt(0));

            if (alPriceo != null && alPriceo.size() > 0) {
                addToCartItem.setAlPriceo(alPriceo);
            }

            if (aladiciona != null && aladiciona.size() > 0) {
                addToCartItem.setAladiciona(aladiciona);
            }

            ArrayList<AddToCartItem> item_2x = getProduct_2x(c.getInt(0));

            addToCartItem.setItem_2x(item_2x);

            addToCartItem.setIdRestaProducto(c.getString(1));
            addToCartItem.setNombre(c.getString(2));
            addToCartItem.setDescripcion(c.getString(3));
            addToCartItem.setQty(c.getString(4));
            addToCartItem.setPrice(c.getFloat(5));

            alItem.add(addToCartItem);
        }
        return alItem;
    }

    public ArrayList<AddToCartItem> getProduct_2x(int p_id) {

        ArrayList<AddToCartItem> alItem = new ArrayList<>();

        String sql = "SELECT * FROM " + DeliveryECDB.prod_E2xoffers + " WHERE " + DeliveryECDB.product_productId + " = '" + p_id + "'";

        db = deliveryECDB.getReadableDatabase();
        Cursor c = db.rawQuery(sql, null);
        while (c.moveToNext()) {

            AddToCartItem addToCartItem = new AddToCartItem();

            addToCartItem.setId(c.getInt(0));

            ArrayList<ItemProductPrice> alPriceo = getPreciosItem_2x(c.getInt(0));
            ArrayList<ItemAdditionalProduct> aladiciona = getAdicionalesItem_2x(c.getInt(6));

            if (alPriceo != null && alPriceo.size() > 0) {
                addToCartItem.setAlPriceo(alPriceo);
            }

            if (aladiciona != null && aladiciona.size() > 0) {
                addToCartItem.setAladiciona(aladiciona);
            }

            addToCartItem.setIdRestaProducto(c.getString(1));
            addToCartItem.setNombre(c.getString(2));
            addToCartItem.setDescripcion(c.getString(3));
            addToCartItem.setQty(c.getString(4));
            addToCartItem.setPrice(c.getFloat(5));

            alItem.add(addToCartItem);
        }
        return alItem;
    }

    public Float getSumOfPrice() {
        float sumOfPrice = 0.0f;
        //SELECT  SUM(price) FROM products
        String sql = "SELECT SUM(" + DeliveryECDB.product_price + ") FROM " + DeliveryECDB.tbl_products;
        db = deliveryECDB.getReadableDatabase();
        Cursor c = db.rawQuery(sql, null);
        while (c.moveToNext()) {
            sumOfPrice = c.getFloat(0);
        }

        return sumOfPrice;
    }

    public int getTotalQuntity() {
        int sum = 0;
        String sql = "SELECT SUM(" + DeliveryECDB.product_qty + ") FROM " + DeliveryECDB.tbl_products;
        db = deliveryECDB.getReadableDatabase();
        Cursor c = db.rawQuery(sql, null);
        while (c.moveToNext()) {
            sum = c.getInt(0);
        }

        /**
         * product E*2X
         */

        int sum_1 = 0;
        String sql_1 = "SELECT SUM(" + DeliveryECDB.product_qty + ") FROM " + DeliveryECDB.prod_E2xoffers;
        Cursor cursor = db.rawQuery(sql_1, null);
        while (cursor.moveToNext()) {
            sum_1 = cursor.getInt(0);
        }

        sum = sum + sum_1;

        return sum;
    }

    public ArrayList<ItemProductPrice> getPreciosItem(int product_productId) {

        ArrayList<ItemProductPrice> alPrecios = new ArrayList<>();

        String sql = "SELECT * FROM " + DeliveryECDB.tbl_ProductoPrecios +
                " WHERE " + DeliveryECDB.product_productId + " = '" + product_productId + "'";

        db = deliveryECDB.getReadableDatabase();
        Cursor c = db.rawQuery(sql, null);
        while (c.moveToNext()) {
            ItemProductPrice ipp = new ItemProductPrice();

            String group = c.getString(3);

            ipp.setGrupo(group);
            ipp.setMedida(c.getString(4));
            ipp.setIdRestaProducto(c.getInt(10));
            float price = c.getFloat(5);
            ipp.setPrecio(price);
            alPrecios.add(ipp);
        }
        return alPrecios;
    }


    public ArrayList<ItemProductPrice> getPreciosItem_2x(int product_productId) {

        ArrayList<ItemProductPrice> alPrecios = new ArrayList<>();

        String sql = "SELECT * FROM " + DeliveryECDB.tbl_ProductoPreciosE2x +
                " WHERE " + DeliveryECDB.product_E2xoffers_productId + " = '" + product_productId + "'";

        db = deliveryECDB.getReadableDatabase();
        Cursor c = db.rawQuery(sql, null);
        while (c.moveToNext()) {
            ItemProductPrice ipp = new ItemProductPrice();
            ipp.setGrupo(c.getString(3));
            ipp.setMedida(c.getString(4));
            ipp.setPrecio(c.getFloat(5));
            /*ipp.setProduct_E2xoffers_productId(c.getInt(10));*/ //Comment by rahul
            alPrecios.add(ipp);
        }
        return alPrecios;
    }


    public ArrayList<ItemAdditionalProduct> getAdicionalesItem(int product_productId) {
        ArrayList<ItemAdditionalProduct> aladiciona = new ArrayList<>();
        String sql = "SELECT * FROM " + DeliveryECDB.tbl_ProductoAdicionales +
                " WHERE " + DeliveryECDB.product_productId + " = '" + product_productId + "'";
        db = deliveryECDB.getReadableDatabase();
        Cursor c = db.rawQuery(sql, null);
        while (c.moveToNext()) {
            ItemAdditionalProduct itm = new ItemAdditionalProduct();
            itm.setNombre(c.getString(4));
            itm.setAdiciona_qty(c.getInt(11));
            itm.setPrecio(c.getString(7));

            try{
                float price = (c.getInt(11)*Float.valueOf(c.getString(7)));
                itm.setPrice2x1(price);
            }catch (Exception e){
                Log.e(TAG,""+e.toString());
                itm.setPrice2x1(0);
            }
            aladiciona.add(itm);
        }
        return aladiciona;
    }

    public ArrayList<ItemAdditionalProduct> getAdicionalesItem_2x(int product_productId) {
        ArrayList<ItemAdditionalProduct> aladiciona = new ArrayList<>();
        String sql = "SELECT * FROM " + DeliveryECDB.tbl_ProductoAdicionalesE2x +
                " WHERE " + DeliveryECDB.product_E2xoffers_productId + " = '" + product_productId + "'";
        db = deliveryECDB.getReadableDatabase();
        Cursor c = db.rawQuery(sql, null);
        while (c.moveToNext()) {
            ItemAdditionalProduct itm = new ItemAdditionalProduct();
            itm.setNombre(c.getString(4));
            itm.setAdiciona_qty(c.getInt(11));
            itm.setPrecio(c.getString(7));
            try{
                float price = (c.getInt(11)*Float.valueOf(c.getString(7)));
                //Log.e("final Precio",""+price);
                itm.setPrice2x1(price);
            }catch (Exception e){
                Log.e(TAG,""+e.toString());
                itm.setPrice2x1(0);
            }
            aladiciona.add(itm);
        }
        return aladiciona;

    }

    public void deleteTheCartById(int insertId) {
        /**
         * Update the the database
         */
        db = deliveryECDB.getWritableDatabase();

        String sql1 = "DELETE FROM " + DeliveryECDB.tbl_ProductoPrecios +
                " WHERE " + DeliveryECDB.product_productId + " = '" + insertId + "'";

        db.execSQL(sql1);

        String sql2 = "DELETE FROM " + DeliveryECDB.tbl_ProductoAdicionales +
                " WHERE " + DeliveryECDB.product_productId + " = '" + insertId + "'";
        db.execSQL(sql2);

        String sql3 = "DELETE FROM " + DeliveryECDB.tbl_products +
                " WHERE " + DeliveryECDB.product_productId + " = '" + insertId + "'";

        db.execSQL(sql3);
    }


    public void backupDatabase() throws IOException {

        if (isSDCardWriteable()) {
            // Open your local db as the input stream
            String inFileName = "/data/data/com.app.grupodeliveryec.deliveryEC/databases/DeliveryECDB";
            File dbFile = new File(inFileName);
            FileInputStream fis = new FileInputStream(dbFile);

            String outFileName = Environment.getExternalStorageDirectory() + "/DeliveryECDB";
            // Open the empty db as the output stream
            OutputStream output = new FileOutputStream(outFileName);
            // transfer bytes from the inputfile to the outputfile
            byte[] buffer = new byte[1024];
            int length;
            while ((length = fis.read(buffer)) > 0) {
                output.write(buffer, 0, length);
            }
            // Close the streams
            output.flush();
            output.close();
            fis.close();
        }
    }

    private boolean isSDCardWriteable() {
        boolean rc = false;
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            rc = true;
        }
        return rc;
    }
}
