package com.listener;

import com.manager.AddToCartManager;

/**
 * Created by administrator on 20/7/17.
 */

public class UpdateMessage {

    UpdateMessageListener updateMessageListener;
    public UpdateMessage(UpdateMessageListener updateMessageListener){
        this.updateMessageListener = updateMessageListener;
    }

    public UpdateMessageListener getUpdateMessageListener() {
        return updateMessageListener;
    }

    public interface UpdateMessageListener{
        public void onUpdateMessage();
    }
}
