package com.com.rest;

import android.content.Context;

/**
 * Created by administrator on 25/1/17.
 */

public class GlobalValues {
    private static API api;

    public static API getMethodManagerObj(Context mContext) {
        if(api!=null)
            return api;
        else
            return new API(mContext);
    }
}
