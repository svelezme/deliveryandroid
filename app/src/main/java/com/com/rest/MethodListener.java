package com.com.rest;

/**
 * Created by administrator on 25/1/17.
 */

public interface MethodListener {
    public void onError();
    public void onError(String response);
    public void onSuccess(String response);
}
